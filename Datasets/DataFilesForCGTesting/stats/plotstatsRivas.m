clc
addpath('../../../matlab')
addpath('~/tmp/code/MD/matlab/thomas')
addpath('../../../ChuteRheologyFigures/')
%dbstop if error
if (false)
    %load stat files and sort
    d=dir('*.stat');
    l=cell(0);
    for i=1:length(d)
        %load everything except ...
        %if ~strcmp(d(i).name, '0.14865088937534.stat');%0.594603557501361.stat') ;
          l{end+1}=d(i).name;
        %end
    end
    data=loadstatistics(l);
    w=cellfun(@(d)d.w,data);
    [w,ix]=sort(w);
    data=data(ix);
    save('plotstats.mat','data','w')
else
  load('plotstats.mat')
end
close all

%correct w
w=cellfun(@(d)d.w/2,data);

% for i=1:length(data)
%     data{i}.MR=sqrt(sum(data{i}.MomentumEquationsRemainder.^2,2));
% end
c=jet(length(data));

%%
var='Density';
newFigure('CG2');
Y=[];
[~,pick ]=min(abs(w-0.05));
[~,pick2]=min(abs(w-1));
[~,zM]=min(abs(data{1}.z-27.5));
range=[22 30 36 188 zM]; 
c=lines(length(range));
%range=round(linspace(13,53+0*length(data{1}.z)-12,5));
for i=1:length(w)
%     plot(data{i}.z,data{i}.(var), ...
%         'Color',c(i,:),'DisplayName',['w=' num2str(w(i))])
    if (i==pick)
        h=plot(data{i}.z,data{i}.(var),'k','DisplayName',['$w=' num2str(w(i),1) '$']);
        plot(data{i}.z(range),data{i}.(var)(range),'ko');
    end    
    if (i==pick2)
        h2=plot(data{i}.z,data{i}.(var),'r','DisplayName',['$w=' num2str(w(i),3) '$'],'Linewidth',2);
        plot(data{i}.z(range),data{i}.(var)(range),'rx','Markersize',10);
    end    
    for j=1:length(range)
        Y(i,j)=data{i}.(var)(range(j));
        DispName{j}=['$z=' num2str(data{i}.z(range(j)),'%0.2f') '\,d$'];
    end
    hold on
end
xlabel('$z/d$')
ylabel('$\rho$','Rotation',0)
c=get(gca,'Children')
set(gca,'Children',c(end:-1:1))
legend([h h2],'Location','South')
set(legend,'Box','off')
axis tight
axis(axis+[-1 1 -1 1]*eps)
ylim([.4 .625]*6/pi)
set(gca,'YTick',[.4:.05:.6])
set(gca,'Layer','top')

%%
h=newFigure('CG3');
set(gca,'NextPlot','replacechildren','ColorOrder',lines(length(range)))
%Create rectangles
x = [0.0025 .1];
y = [.4 0.625]*6/pi;
fill(x([1 1 2 2]), y([1 2 2 1]),'y','FaceColor',[1 1 .7],'EdgeColor','none','DisplayName','sub-particle scale');
hold on
x = [0.6 1];
fill(x([1 1 2 2]), y([1 2 2 1]),'y','FaceColor',[1 .7 .7],'EdgeColor','none','DisplayName','particle scale');

p=get(gca,'ColorOrder')
set(gca,'ColorOrder',[p(1:end-1,:); 0 0 0])
plot(w,Y','.-','DisplayName',DispName)
set(gca,'Xscale','log')
xlabel('$w/d$')
ylabel('$\rho$','Rotation',0)
axis tight
ylim([.4 0.625]*6/pi)
xlim([1e-4 8])
hold on
plot(w(pick)*ones(size(range)),Y(pick,:),'ko','DisplayName',['$w=' num2str(w(pick),1) '\,d$'])
plot(w(pick2)*ones(size(range)),Y(pick2,:),'rx','DisplayName',['$w=' num2str(w(pick2),2) '\,d$'])
set(gca,'Layer','top')
separate_legend()
set(legend,'Box','off')
return
%%
%close all
var='KineticStressXX';
Y=[];
[~,pick ]=min(abs(w-0.05));
[~,pick2]=min(abs(w-1));
[~,zM]=min(abs(data{1}.z-29));
range=[36 43 52 188 zM]; 
for i=1:length(w)
    for j=1:length(range)
        Y(i,j)=data{i}.(var)(range(j));
        V=data{i}.KineticStressXX-data{i}.ShearRate.^2.*(w(i)/sqrt(3)).^2.*data{i}.Density;
        Z(i,j)=V(range(j));
    end
end

newFigure('CG4a');
set(gcf,'position',[1 1 .9 1].*get(gcf,'position'))
for i=1:length(w)
    if (i==pick2)
        h2=plot(data{i}.z,data{i}.(var),'r','DisplayName','$\sigma_{xx}^k|_{w=1}$');
        plot(data{i}.z(range),data{i}.(var)(range),'rx');
%  'Displayname',{'$\sigma_{xx}^k|_{w=0.05}$' '$\sigma_{xx}^k|_{w=1}$' '$\sigma_{xx}^k{}''|_{w=1}$' '$\sigma_{xx}^k{}^\star|_{w=1}$' });

    end    
    if (i==pick)
        h=plot(data{i}.z,data{i}.(var),'k','DisplayName','$\sigma_{xx}^k|_{w=0.05}$');
        plot(data{i}.z(range),data{i}.(var)(range),'ko');
    end    
    for j=1:length(range)
        Y(i,j)=data{i}.(var)(range(j));
        DispName{j}=['$z=' num2str(data{i}.z(range(j)),'%0.2f') '\,d$'];
    end
    hold on
end
xlabel('$z/d$')
%ylabel('$\nu$','Rotation',0)
axis tight
axis(axis+[-1 1 -1 1]*eps)

if (false) 
    global std stdw stdwFV
    stdwFV=loadstatistics('../../../newStats/FlucVelocity/H30A28L1M0.5B0.5.w2T500.stat',struct('timeaverage',true));
    stdwFV=addons(stdwFV,'stdwFV');
    stdw.KineticStressXXcorrection = stdw.ShearRate.^2.*(stdw.w/2/sqrt(3)).^2.*stdw.Density;
    h3=plot(std.z,[stdw.KineticStressXX-stdw.KineticStressXXcorrection stdwFV.KineticStressXX],'--',...
      'Displayname',{'$\sigma_{xx}^{k\prime}|_{w=1}$' '$\sigma_{xx}^{k\star}|_{w=1}$' });
    set(h3(1),'Color','b','LineStyle',':')
    xlabel('$z$')
    legend([h2 h h3'],'Location','South')
    set(legend,'Box','off','Interpreter','none')
    axis tight
    xlim([-.5 30.5])
    set(legend,'Position',get(legend,'Position').*[0 0 1 1]+[.65 .66 0 0])
end

%%
h=newFigure('CG4');
set(gca,'NextPlot','replacechildren','ColorOrder',lines(length(range)))
set(gcf,'position',[1 1 .7 1].*get(gcf,'position'))
%Create rectangles
x = [0.0025 .1];
y = [0.0668  10.0000];
fill(x([1 1 2 2]), y([1 2 2 1]),'y','FaceColor',[1 1 .7],'EdgeColor','none');
hold on

p=get(gca,'ColorOrder')
set(gca,'ColorOrder',[p(1:end-1,:); 0 0 0])
h=plot(w,Y','.-','DisplayName',DispName)
set(gca,'Xscale','log')
xlabel('$w/d$')
ylabel('$\sigma_{xx}^k$')
axis tight
ylim([min(ylim) 10])
xlim([0.0025 max(xlim)])
plot(w(pick)*ones(size(range)),Y(pick,:),'ko','DisplayName',['$w=' num2str(w(pick),3) '\,d$'])
plot(w(pick2)*ones(size(range)),Y(pick2,:),'rx','DisplayName',['$w=' num2str(w(pick2),2) '\,d$'])
%separate_legend()
set(gca,'YScale','log')
add_triangle_to_loglog2(2,.67,.5,true,.4)
%print_figures_ui()
v=axis
set(gca,'Layer','top')
set(gca,'XTick',[1e-2 1e-1 1])
set(gca,'XMinorTick','off')

%%
h=newFigure('CG4b');
set(gca,'NextPlot','replacechildren','ColorOrder',lines(length(range)))
set(gcf,'position',[1 1 .7 1].*get(gcf,'position'))
%Create rectangles
x = [0.0025 .1];
y = [0 1];
fill(x([1 1 2 2]), y([1 2 2 1]),'y','FaceColor',[1 1 .7],'EdgeColor','none','DisplayName','sub-particle scale');
hold on
x = [0.6 1];
fill(x([1 1 2 2]), y([1 2 2 1]),'y','FaceColor',[1 .7 .7],'EdgeColor','none','DisplayName','particle scale');

p=get(gca,'ColorOrder')
set(gca,'ColorOrder',[p(1:end-1,:); 0 0 0])
plot(w,Z','.-','DisplayName',DispName)
hold on
set(gca,'Xscale','log')
xlabel('$w/d$')
ylabel('$\sigma_{xx}^{k*}$')
axis(v)
ylim([0 .8])
xlim([0.0025 max(xlim)])

plot(w(pick)*ones(size(range)),Z(pick,:),'ko','DisplayName',['$w=' num2str(w(pick),3) '\,d$'])
plot(w(pick2)*ones(size(range)),Z(pick2,:),'rx','DisplayName',['$w=' num2str(w(pick2),2) '\,d$'])
set(gca,'Layer','top')
set(gca,'XTick',[1e-2 1e-1 1])
set(gca,'XMinorTick','off')

separate_legend()
set(legend,'Box','off')
%set(gca,'YScale','log')


%print_figures_ui()
