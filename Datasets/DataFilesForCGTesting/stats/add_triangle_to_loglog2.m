function add_triangle_to_loglog2(slope,lx,ly,reverse,scale)
% slope  denotes the slope that the triangle has to represent
% lx     denotes the rel. distance of the leftmost triangle point from the
%        left boundary, from 0 to 1 (default 0.1)
% ly     denotes the rel. distance of the bottommost triangle point from the
%        bottom boundary, from 0 to 1 (default 0.1)
if ~exist('lx','var'), lx = 0.1; end
if ~exist('ly','var'), ly = 0.1; end
if ~exist('scale','var'), scale = 1-2*lx; end 

hold on

X=log10(xlim);
Y=log10(ylim);

%size of triangle   
s=min(diff(X),diff(Y)/abs(slope))*scale;

if exist('reverse','var') && reverse==true;
  %  B---C
  %  |  /  
  %  A/
  A = [X(1)+lx*diff(X) Y(1)+ly*diff(Y)];
  B = A + [0 s*slope];
  C = B + [s 0];
  h=plot(10.^[A(1) B(1) C(1) A(1)],10.^[A(2) B(2) C(2) A(2)],'k-');
  text(10^((C(1)+B(1))/2), 10^((C(2)+B(2))/2+0.05*diff(Y)), num2str(1),'VerticalAlignment','top');
  text(10^((A(1)+B(1))/2-0.03*diff(X)), 10^((A(2)+B(2))/2), num2str(abs(slope)),'HorizontalAlignment','Right');
else
  %     /C
  %   /  |
  %  A---B
  A = [X(1)+lx*diff(X) Y(1)+ly*diff(Y)];
  B = A + [s 0];
  C = B + [0 s*slope];
  h=plot(10.^[A(1) B(1) C(1) A(1)],10.^[A(2) B(2) C(2) A(2)],'k-');
  text(10^((A(1)+B(1))/2), 10^((A(2)+B(2))/2-0.05*diff(Y)), num2str(1),'VerticalAlignment','top');
  text(10^((C(1)+B(1))/2+0.03*diff(X)), 10^((C(2)+B(2))/2), num2str(abs(slope)),'HorizontalAlignment','Right');
end
set(h,'HandleVisibility','off')
return