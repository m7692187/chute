%%
clc
addpath('../../../matlab')
%addpath('~/tmp/code/MD/matlab/thomas')
addpath('~/tmp_einder/code/MD/matlab/thomas')
addpath('../../../ChuteRheologyFigures/')
%dbstop if error
if (false)
    %load stat files and sort
    d=dir('*.stat');
    l=cell(0);
    for i=1:length(d)
        %load everything except ...
        %if ~strcmp(d(i).name, '0.14865088937534.stat');%0.594603557501361.stat') ;
          l{end+1}=d(i).name;
        %end
    end
    data=loadstatistics(l);
    w=cellfun(@(d)d.w,data);
    [w,ix]=sort(w);
    data=data(ix);
    save('plotstats.mat','data','w')
else
  load('plotstats.mat')
end
close all

%correct w
w=cellfun(@(d)d.w/2,data);

for i=1:length(data)
    data{i}.MR=sqrt(sum(data{i}.MomentumEquationsRemainder.^2,2));
end
c=jet(length(data));

%%
var='Density';
newFigure([var 'Movie']);
title('')
set(gcf,'Position',[0 0 500 300])
set(gcf,'Color','w')
xlabel('z/d')
ylabel('\rho','Interpreter','tex','Rotation',0)
range=[22 30 36 188 326]; 

h1=gca;
% h1=subplot(1,2,1);
hold(h1,'on')

% h2=subplot(1,2,2);
% hold(h2,'on')
% x = [0.0025 .1];
% y = [0 1.4];
% fill(x([1 1 2 2]), y([1 2 2 1]),'y','FaceColor',[1 1 .7],'EdgeColor','none','DisplayName','sub-particle scale');
% x = [0.6 1];
% fill(x([1 1 2 2]), y([1 2 2 1]),'y','FaceColor',[1 .7 .7],'EdgeColor','none','DisplayName','particle scale');
% legend(h2,'show')
% set(legend,'Location','South')
% set(h2,'XScale','log')
% set(h2,'XTick',[1e-4 1e-2 1e-0])

vidObj = avifile([var 'Movie']);
vidObj=set(vidObj,'fps',5);
%open(vidObj);
val=[]
for i=1:length(w)
    axes(h1);
    delete(get(h1,'Children'));
    plot(h1,data{i}.z,data{i}.(var))
%    scatter(h1,data{i}.z(range),data{i}.(var)(range),50,lines(length(range)),'filled')
    legend(h1,['w=' num2str(w(i),3) ],'Location','South')
    axis([-0.5 30.5 0 1.4])
    
%     axes(h2);
%     c=get(h2,'Children');
%     delete(c(1:end-2));
%     val(:,end+1)=data{i}.(var)(range);
%     semilogx(h2,w(1:i),val)
%     scatter(h2,w(i)*ones(size(range)),data{i}.(var)(range),50,lines(length(range)),'filled')
%     axis([1e-5 10 0 1.4])
    %writeVideo(vidObj,getframe);
    vidObj = addframe(vidObj,getframe(gcf));
end
vidObj=close(vidObj);
xlabel('$z/d$')
ylabel('$\rho$','Rotation',0)
print_figures(gcf,struct('pdf',true))
return

%%
var='StressZZ';
newFigure([var 'Movie']);
set(gcf,'Position',[0 0 500 300])

xlabel('$z/d$')
ylabel('$\rho$','Rotation',0)
range=[22 30 36 188 326]; 
h1=subplot(1,2,1);
hold(h1,'on')

h2=subplot(1,2,2);
hold(h2,'on')
x = [0.0025 .1];
y = [0 1.4];
fill(x([1 1 2 2]), y([1 2 2 1]),'y','FaceColor',[1 1 .7],'EdgeColor','none','DisplayName','sub-particle scale');
x = [0.6 1];
fill(x([1 1 2 2]), y([1 2 2 1]),'y','FaceColor',[1 .7 .7],'EdgeColor','none','DisplayName','particle scale');
legend(h2,'show')
set(legend,'Location','South')
set(h2,'XScale','log')
set(h2,'XTick',[1e-4 1e-2 1e-0])

%vidObj = VideoWriter([var 'Movie']);
vidObj = aviWriter([var 'Movie']);
set(vidObj,'FrameRate',10);
open(vidObj);
val=[]
for i=1:length(w)
    axes(h1);
    delete(get(h1,'Children'));
    plot(h1,data{i}.z,data{i}.(var))
    scatter(h1,data{i}.z(range),data{i}.(var)(range),50,lines(length(range)),'filled')
    legend(h1,['w=' num2str(w(i),3) ],'Location','South')
    axis([0 30 0 1.4])
    
    axes(h2);
    c=get(h2,'Children');
    delete(c(1:end-2));
    val(:,end+1)=data{i}.(var)(range);
    semilogx(h2,w(1:i),val)
    scatter(h2,w(i)*ones(size(range)),data{i}.(var)(range),50,lines(length(range)),'filled')
    axis([1e-5 10 0 1.4])
%     writeVideo(vidObj,getframe);
end
close(vidObj);
return
