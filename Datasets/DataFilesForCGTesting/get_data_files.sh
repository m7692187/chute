#!/bin/bash

#flowing case
if true; then
	mkdir -p ini
	f=H30A28L1M0.5B0.5
	cp ../../Flow/L1M0.5B0.5/$f.restart.0020 ini/$f.restart
	chmod -w ini/$f.restart

	#make -C ~/code/MD/DRIVERS/Chute restart
	#cp ~/code/MD/DRIVERS/Chute/restart.exe .

	~/bin/clusterscriptexecute ./restart.exe ini/$f -options_restart 1 -options_data 2 -options_fstat 2 -save_count 2500 -tmax 4000
fi

if false; then
	mkdir -p ini
	f=H10A20L1M0.5B0.5
	cp ../../Flow/L1M0.5B0.5/$f.restart.0020 ini/$f.restart
	chmod -w ini/$f.restart
	~/bin/clusterscriptexecute ./restart.exe ini/$f -options_restart 1 -options_data 2 -options_fstat 2 -save_count 2500 -tmax 2050
fi

