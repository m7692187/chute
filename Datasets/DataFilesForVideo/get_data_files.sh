#!/bin/bash

mkdir -p ini
f=H20A24L1M0.5B0.5
cp ../Flow/L1M0.5B0.5/$f.restart.0020 ini/$f.restart
chmod -w ini/$f.restart
rm -f H*

#make -C ~/code/MD/DRIVERS/Chute restart
#cp ~/code/MD/DRIVERS/Chute/restart.exe .

~/bin/clusterscriptexecute ./restart.exe ini/$f -options_restart 0 -options_data 1 -options_fstat 0 -save_count 125 -tmax 2200

