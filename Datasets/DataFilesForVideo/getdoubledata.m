%double length
in=fopen('H20A24L1M0.5B0.5.data');
out=fopen('H20A24L1M0.5B0.5.doubledata','w');
k=0; %timestep
while ~feof(in)
    tline = fgets(in);
    data = str2num(tline);
    if (length(data)~=14)
        i=0;
        k=k+1;
        disp(k)
        %if k>10; break; end
        data(1)=2*data(1);
        data(end-2)=40;
        fwrite(out,[num2str(data,'%g ') tline(end)]);
    else
        fwrite(out,tline);
        fwrite(out,[num2str(data(1)+20) tline(min(strfind(tline,' ')):end)]);
    end
end
fclose(in);
fclose(out);
disp('done')
