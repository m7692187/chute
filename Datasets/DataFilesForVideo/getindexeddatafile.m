img=imread('../../matlab/logo.png');
img=sum(img,3);
img=img(40:end-80,70:340);
image(img)
in=fopen('H20A24L1M0.5B0.5.data');
k=0; %timestep
kset=1000; %timestep where index is set
while ~feof(in)
    tline = fgets(in);
    data = str2num(tline);
    if (length(data)~=14)
        i=0;
        k=k+1;
        disp(k)
        if (k==kset) 
            N=data(1);
            index=false(1,N);
        elseif (k>kset)
            break;
        end
    else
        i=i+1;
        if (k==kset)
            x=ceil(data(1)/20*size(img,2));
            y=min(size(img,1),max(1,ceil(data(3)/24*1.5*size(img,1))));
            if (img(end+1-y,x)>0)
                index(i) = true;
            end
        end
    end
end
fclose(in);
disp('half done')



in=fopen('H20A24L1M0.5B0.5.data');
out=fopen('H20A24L1M0.5B0.5.newdata','w');
i=-1;
while ~feof(in)
    i=i+1;
    tline = fgets(in);
    if (mod(i,N+1)==0)
        disp(i/(N+1))
        %if i/(N+1)>10; break; end
    else
        if (index(mod(i,N+1))==true)
            tline(end-1)='1';
        end
    end
    fwrite(out,tline);
end
fclose(in);
fclose(out);
disp('done')

%./H20A24L1M0.5B0.5.disp -f H20A24L1M0.5B0.5.newdata -solidf -v0 -cmode 8 -cminset -1 -cmaxset 1 -step 10
