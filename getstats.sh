#this is the flow simulations
cd ~/DRIVERS/FlowRulePaper/runfinished7
root=$(pwd)

t=100
w=0.25
cd Flow 

for S in 0 1 2 3 5 16 18 19 4  #21 22 23 24 25 9 10 11 12 13 # 17 #in order of importance
do
	case $S in
  0) Case=L0M0.5B0.5 ;;
  1) Case=L0.5M0.5B0.5 ;;
  2) Case=L0.67M0.5B0.5 ;;
  3) Case=L0.83M0.5B0.5 ;;
  5) Case=L2M0.5B0.5 ;;
  16) Case=L0.17M0.5B0.5 ;;
  17) Case=L0.33M0.5B0.5 ;;
  18) Case=L1.5M0.5B0.5 ;;
  19) Case=L4M0.5B0.5 ;;
  4) Case=L1M0.5B0.5 ;;
  21) Case=L1M0.5B0.0625 ;;
  22) Case=L1M0.5B0.03125 ;;
  23) Case=L1M0.5B0.015625 ;;
  24) Case=L1M0.5B0.0078125 ;;
  25) Case=L1M0.5B0.000976562 ;;
  9) Case=L1M0.5B0 ;;
  10) Case=L1M0.5B1 ;;
  11) Case=L1M0.5B1e+20 ;;
  12) Case=L1M0.5B0.25 ;;
  13) Case=L1M0.5B0.125 ;;
  *) echo "Case does not exist"; exit 
	esac
	#echo $Case
	mkdir -p $Case
	cd $Case

	for H in 40 30 20 10 
	do
		if [ $H == 40 ]; then
			AList="20 22 24 26 28"
		else 
			AList="21 22 23 24 25 26 27 28 20 30"
		fi
	
		for A in $AList
		do
			#look for restart.0020
			
			#in home directory
			restartfile=H${H}A${A}${Case}.restart
			if [ -f $restartfile ] && [ ! $(stat -c%s "$restartfile") -lt 100000 ]; then
				statfile=H${H}A${A}${Case}.stat
				if [ -f $statfile ] && [ ! $(stat -c%s "$statfile") -lt 100000 ]; then
					continue
				fi
				statfile=H${H}A${A}${Case}.T5.stat
				if [ -f $statfile ] && [ ! $(stat -c%s "$statfile") -lt 100000 ]; then
					continue
				fi
				statfile=H${H}A${A}${Case}.T05.stat
				if [ -f $statfile ] && [ ! $(stat -c%s "$statfile") -lt 100000 ]; then
					continue
				fi
				echo H${H}A${A}${Case}.stat does not exist
				cp $restartfile ~/DRIVERS/FlowRulePaper/run6/Flow
				continue
			fi
			echo H${H}A${A}${Case} does not exist
		done
	done
	cd ..
done

t=2
w=0.25
cd $root/Hstop

for restartfile in $(ls L*M0.5B0.5/H*.restart)
do
	statfile=${restartfile/.restart/.stat}
	if [ -f $statfile ] && [ ! $(stat -c%s "$statfile") -lt 100000 ]; then
		continue
	fi
	echo $statfile does not exist
	cp $restartfile ~/DRIVERS/FlowRulePaper/run6/Hstop
done

