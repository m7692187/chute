close all
global std stdw
rho=std.Density;
nu=std.Nu;
T=std.Temperature;
p=std.Pressure;
pK=(std.KineticStressXX+std.KineticStressYY+std.KineticStressZZ)/3;
z=std.z;
%nu=0:.01:.7;
g=(1-nu/2)./((1-nu).^2);

figure()
plot(z,[rho.*T.*(1+2*nu.*g) p pK])
legend({'\rho T (1+2 nu g)' 'pressure' 'kinetic pressure'})
xlabel('z')
xlim([-1 31])
saveas(gcf,'ComparisonP.pdf')

figure()
y=[rho.*T.*(1+2*nu.*g) p pK];
plot(z,y./(ones(length(z),1)*max(y)))
legend({'\rho T (1+2 nu g)/max(..)' 'pressure/max(pressure)' 'kinetic p./max(kinetic p.)'})
xlabel('z')
xlim([-1 31])
saveas(gcf,'ComparisonPScaled.pdf')




