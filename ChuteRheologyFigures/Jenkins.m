clc; close all
%dbstop if error
load_ChuteRheology(false)
global std stdw muH muHw %#ok<NUSED>

figure(1);
set(gcf,'FileName','DiegoStandardCase')
set(gcf,'Position',[0 0 838 951])

for i=1:2
if i==1
    d=std;
else
    d=stdw;
    for j=1:8
        subplot(3,3,j);
        hold all
    end
end
ix=d.zhat<1&d.zhat>0;

subplot(3,3,1);
plot(d.z(ix),d.ShearRate(ix))
axis([0 30 0 1.6])
xlabel('$z$')
ylabel('$\partial_z u_x$')

subplot(3,3,2);
plot(d.z(ix),d.InertialNumber(ix))
axis([0 30 0 0.68])
xlabel('$z$')
ylabel('$I$')

subplot(3,3,3);
plot(d.z(ix),d.Nu(ix))
axis([0 30 0.47 0.6])
xlabel('$z$')
ylabel('$\nu$')

subplot(3,3,4);
plot(d.z(ix),d.VelocityX(ix))
axis([0 30 0 30])
xlabel('$z$')
ylabel('$u_x$')

subplot(3,3,5);
d.Temperature = ...
	(d.KineticStressXX - d.KineticStressXXcorrection + d.KineticStressYY + d.KineticStressZZ)./d.Density/3;
plot(d.z(ix),d.Temperature(ix))
axis([0 30 0 0.6])
xlabel('$z$')
ylabel('$T$')

subplot(3,3,6);
plot(d.Nu(ix),d.Pressure(ix)./d.Temperature(ix)/d.ParticleDensity)
axis tight
ylim([0 70]);
xlabel('$\nu$')
ylabel('$\frac{p}{\rho_p T}$')

subplot(3,3,7);
FabricTrace = d.FabricXX+d.FabricYY+d.FabricZZ;
plot(d.z(ix),FabricTrace(ix))
axis([0 30 0 0.5])
xlabel('$z$')
ylabel('$tr(F)$')

subplot(3,3,8);
for j=1:length(d.x(:))
	Fabric = [ ...
		d.FabricXX(j) d.FabricXY(j) d.FabricXZ(j); ...
		d.FabricXY(j) d.FabricYY(j) d.FabricYZ(j); ...
		d.FabricXZ(j) d.FabricYZ(j) d.FabricZZ(j) ];
	if (isequal(Fabric,zeros(3))|| sum(sum(isnan(Fabric))) )
		d.MacroFrictionCoefficient(j) = NaN;
		d.Sd(j) = nan;
		d.SdStar(j) = nan;
	else
		[v1,d1]=eig(Fabric);
		[v,ds]=dsort_ev(v1,d1);
    ds = diag(ds);
    p=sum(ds)/3;
		d.FabricSdStar(j) = sqrt(((ds(1)-ds(3))^2+(ds(2)-ds(3))^2+(ds(1)-ds(2))^2) / (6*p^2));
	end
end
plot(d.z(ix),d.FabricSdStar(ix))
axis([0 30 0.36 0.41])
xlabel('$z$')
ylabel('$s_D^\star(F)$')

end

load sol_e05
parameters=sol_e05.parameters;
solution=sol_e05.solution;

e  = parameters.e;                     % effective restitution coefficient
nus = parameters.nus;                   % concentration at shear rigidity
c = parameters.c;                       % parameter c in the correlation length
muw  = parameters.muw;                  % wall friction coefficient
ew = parameters.ew;                     % normal restitution coefficient with the walls
W   = parameters.W;                     % channel width in number of diameters
psi = parameters.psi;                   % bumpiness
thetag = parameters.thetag;             % bed slope parameter in [?]
holdup = parameters.holdup;             % mean concentration times flow depth

h = solution.parameters;
z = solution.x.*solution.parameters;
u = solution.y(3,:);
nu = solution.y(1,:);
T = solution.y(5,:).^2;
s = solution.y(2,:);
gammadot = solution.yp(3,:)./h;

sol = coeff_KT(e,nu,nus,c);
p = 4.*nu.*sol.G.*sol.F.*T;

I = gammadot./sqrt(p);
stress_ratio = s./p;

for j=1:6
    subplot(3,3,j);
    hold all
end

subplot(3,3,1);
plot(z,gammadot)
subplot(3,3,2);
plot(z,I)
subplot(3,3,3);
plot(z,nu)
subplot(3,3,4);
plot(z,u)
subplot(3,3,5);
plot(z,T)
subplot(3,3,6);
Nu=linspace(0.25,0.636,200);
g0CS=(2-Nu)./(2*(1-Nu).^3);
g0J=2./(0.636-Nu);
L=min(1,(0.636-Nu)./(0.636-0.49));
g0B=L.*g0CS+(1-L).*g0J;
e=0.6;
plot(Nu,Nu.*(1+2*(1+e)*Nu.*g0CS),'k','LineWidth',2)
plot(Nu,Nu.*(1+2*(1+e)*Nu.*g0J),'k','LineWidth',2)
plot(Nu,Nu.*(1+2*(1+e)*Nu.*g0B),'k','LineWidth',2)
