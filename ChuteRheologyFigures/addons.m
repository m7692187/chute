function data=addons(data,plotname,options)
if ~exist('plotname','var'), plotname=''; end
if ~exist('options','var'), options=struct([]); end
if ~iscell(data), data={data}; end
dataZlim=[min(cellfun(@(d)d.Base,data)) max(cellfun(@(d)d.Surface,data))];
dataColor=jet(length(data));
for i=1:length(data)
 data{i}=EigenvalueAnalysis2(data{i},options);
 if ~isfield(data{i},'title'), data{i}.title=data{i}.name; end
 data{i}.zhat=(data{i}.z-data{i}.Base)/data{i}.FlowHeight;
 data{i}.ShearRate = diff(data{i}.VelocityX)./diff(data{i}.z);
 data{i}.ShearRate(end+1) = data{i}.ShearRate(end);
 data{i}.InertialNumber=data{i}.ShearRate*data{i}.d./sqrt(data{i}.Pressure./data{i}.ParticleDensity(1));
 data{i}.BulkInertialNumber = median(data{i}.InertialNumber(data{i}.Density>0&data{i}.InertialNumber>0)); 
 data{i}.FlowInertialNumber = 5/2*max(data{i}.VelocityX(data{i}.zhat<=1))*data{i}.d/data{i}.FlowHeight/sqrt(-data{i}.Gravity(3)*data{i}.FlowHeight); 
 
 if (false)
   %the following line defines the bulk as all positions where the Inertial
   %number is colse to the Bulk inertial number
   %data{i}.Bulk = abs(data{i}.InertialNumber/data{i}.BulkInertialNumber-1)<0.3&abs(data{i}.zhat-0.5)<0.5;
 else
   %below, we smooth the inertial number
   ix=(data{i}.InertialNumber~=0);
   regularity=zeros(size(data{i}.z));
   regularity(ix)=smooth(abs(data{i}.InertialNumber(ix)/data{i}.BulkInertialNumber-1));
   data{i}.Bulk = abs(data{i}.zhat-0.5)<0.5;
   data{i}.Bulk(1:find(regularity(1:round(end/2))>0.1,1,'last'))=false;
   data{i}.Bulk(round(end/2)-1+find(regularity(round(end/2):end)>0.1,1,'first'):end)=false;
 end
 
 data{i}=load_FlowHeight(data{i});
 data{i}.ExtendedBulk = abs(data{i}.InertialNumber/data{i}.BulkInertialNumber-1)<0.9&abs(data{i}.zhat-0.5)<0.5;
 data{i}.Color = dataColor(i,:);
 data{i}.zlim = dataZlim;
 dzMomentumX = gradient(data{i}.MomentumX,diff(data{i}.z(1:2)));
 dzDensity = gradient(data{i}.Density,diff(data{i}.z(1:2)));
 data{i}.dzVelocityX = (dzMomentumX.*data{i}.Density - data{i}.MomentumX.*dzDensity)./(data{i}.Density).^2;

 %standard label
 data{i}.title= ['$tan^{-1}\muHw=' num2str(data{i}.ChuteAngle)  '^\circ$'];
 data{i}.axis = data{i}.BulkInertialNumber;
 data{i}.label = '$I$';
 data{i}.plotname = plotname;
 for XX={'XX';'XY';'XZ';'YX';'YY';'YZ';'ZX';'ZY';'ZZ';}'
  data{i}.(['ContactStress' XX{1}]) = data{i}.(['NormalStress' XX{1}]) + data{i}.(['TangentialStress' XX{1}]);
 end
  %  for XX={'YX';'ZX';'ZY'}'
  %   data{i}.(['Fabric' XX{1}]) = data{i}.(['Fabric' XX{1}(end:-1:1)]);
  %   data{i}.(['KineticStress' XX{1}]) = data{i}.(['KineticStress' XX{1}(end:-1:1)]);
  %  end
  data{i}.ExtendedStressZZ = data{i}.StressZZ...
    -diff(data{i}.z(1:2))*(sum(data{i}.TractionZ)-cumsum(data{i}.TractionZ)+data{i}.TractionZ/2);
  data{i}.ExtendedStressYZ = data{i}.StressYZ...
    -diff(data{i}.z(1:2))*(sum(data{i}.TractionY)-cumsum(data{i}.TractionY)+data{i}.TractionY/2);
  data{i}.ExtendedStressXZ = data{i}.StressXZ...
    -diff(data{i}.z(1:2))*(sum(data{i}.TractionX)-cumsum(data{i}.TractionX)+data{i}.TractionX/2);
end
%sort by axis
%[~,ix]=sort(cellfun(@(d)d.axis,data)); data = data(ix);
%remove cell structure for single case
if length(data)==1; data=data{1}; end
return

function data=load_FlowHeight(data)
StressZZ = (cumsum(data.Density)-sum(data.Density))*diff(data.z(1:2))*data.Gravity(3);

%find height
kappa=0.02;
data.Base=min(data.z(StressZZ<(1-kappa)*StressZZ(1)),[],1);
data.Surface=max(data.z.*(StressZZ>kappa*StressZZ(1)),[],1);
data.FlowHeight=data.Surface-data.Base;

%correct
data.FlowHeight=data.FlowHeight/(1-2*kappa);
data.Base=data.Base-kappa*data.FlowHeight;
data.Surface=data.Surface+kappa*data.FlowHeight;
return