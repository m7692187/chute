%comparison with Saeed
function plot_ChuteRheology_mod141217
clc; close all
dbstop if error %dbclear all
load_ChuteRheology(false)
global std stdw mu muw muH muHw %#ok<NUSED>
muFIwfit
%print_figures()
return

function paper()
paperstd()
%papermu()
%CG

%plots_FabricVsStress
%paper_additional()
%percentBulk
return

function paper_additional()
global std stdw muH muHw %#ok<NUSED>
%IP(muH{14})
%PrincipalLambdas(std,'KineticStress','k',struct('local',true));
% PrincipalLambdas(muH,'Stress','{}',struct('local',true,'K',true));
% addfittedK()
% PrincipalLambdas(muH,'Stress','{}',struct('local',true,'K2',true));
% addfittedK2()

muIRemco(muHw,struct('local',true));
set(gcf,'position',[1 1 1 2].*get(gcf,'position'))
colorbar off

return

PrincipalLambdas(muHw(cellfun(@(d)d.N>3000,muHw)),'Fabric','F',struct('local',true,'l12',true));
ylim([-.2 0])

PrincipalLambdas(muHw(cellfun(@(d)d.N>3000,muHw)),'Stress','\sigma',struct('local',true,'l12',true));
ylim([-.35 0])

PrincipalLambdas(muHw(cellfun(@(d)d.N>3000,muHw)),'Fabric','F',struct('local',true,'dphi',true));
ylim([-5 1])
xlim([0 .7])

PrincipalLambdas(muHw(cellfun(@(d)d.N>3000,muHw)),'Stress','\sigma',struct('local',true,'dphi',true));
ylim([-5 1])
xlim([0 .7])
return

function papermu()
global mu muw muH muHw std stdw %#ok<NUSED>

% [fitdphi] = PrincipalAngles(muH(cellfun(@(d)d.N>3000,muH)),'Stress','\sigma',struct('local',true));
% axis tight
% xlim([0 .7])
% ylim([-3.5 2])
% colorbar off
% set(legend,'Location','NorthEast')
% 
% [fitdphi] = PrincipalAngles(muHw(cellfun(@(d)d.N>3000,muH)),'Stress','\sigma',struct('local',true));
% axis tight
% xlim([0 .7])
% ylim([-3.5 2])
% colorbar off
% set(legend,'Location','NorthEast')

muIwfit
%plotStress
return


diffNuFit(muH(3:end),muHw(3:end));
ylim([0 3])
 return
 
 
PrincipalAngles(muH(cellfun(@(d)d.N>3000,muH)),'Fabric','F',struct('local',true));
PrincipalAngles(muH(cellfun(@(d)d.N>3000,muH)),'ContactStress','\sigma^c',struct('local',true));
PrincipalAngles(muH(cellfun(@(d)d.N>3000,muH)),'Stress','\sigma',struct('local',true));

return

diffNuFit(std,stdw)
ylim([-.2 .4])
%return

diffComponents(std,stdw,'ContactStress','\sigma^c')
set(legend,'Position',get(legend,'Position').*[0 1 1 1]+[.7 0 0 0])
ylim([-0.5,2])


diffComponents(muH(3:end),muHw(3:end),'ContactStress','\sigma^c')
ylim([0 3])

%muIfit()
return

%plotStress
%plotFabric
%cbar
%StressDecomposition(muH);
PrincipalLambdas(muH(cellfun(@(d)d.N>3000,muH)),'Stress','\sigma',struct('local',true,'l',true));
axis tight
xlim([0 .7])
ylim([-.6 .8])
set(legend,'Position',get(legend,'Position').*[0 0 1 1]+[.6 .5 0 0])
colorbar off

return

%set(gca,'XTick',[.05 .1 .5])
%set(gca,'XTickLabel',{'.05' '.1' '.5'})
ylim([0.001 0.1])
xlim([0.03 1])

% PrincipalLambdas(muH(cellfun(@(d)d.N>3000,muH)),'Stress','\sigma',struct('local',true,'lsd',true));
% axis tight
% xlim([0 .5])
% set(legend,'Position',get(legend,'Position').*[0 0 1 1]+[.5 .5 0 0])


%PrincipalLambdas(muHw(cellfun(@(d)d.N>3000,muH)),'Stress','\sigma',struct('local',true,'l2',true));
%PrincipalLambdas(muHw(cellfun(@(d)d.N>3000,muH)),'Stress','\sigma',struct('local',true,'l3',true));

Anisotropy(muH{22})
%PrincipalLambdas(muHw(cellfun(@(d)d.N>3000,muH)),'Stress','\sigma',struct('local',true,'sd',true));
return

function pBulk=percentBulk()
global mu muw muH muHw std stdw %#ok<NUSED>
ix=cellfun(@(d)d.N>3000&(d.ChuteAngle>20||d.N>7000)&d.ChuteAngle<30,muHw);
data=muH(ix);
nBulk=0;
nFlow=0;
for i=[1:length(data)]
   nBulk=nBulk+sum(data{i}.Bulk);
   nFlow=nFlow+sum(data{i}.zhat<1&data{i}.zhat>0);
end
pBulk=nBulk/nFlow;
return

function plots_FabricVsStress()
global mu muw muH muHw std stdw %#ok<NUSED>
ix=cellfun(@(d)d.N>3000&(d.ChuteAngle>20||d.N>7000),muHw);
ix2=cellfun(@(d)d.ChuteAngle==28&(d.ChuteAngle>20||d.N>7000)&d.ChuteAngle<30,muHw);
 FabricVsStress(muH(ix),struct('dphi',true))
 pos=get(gca,'Position');
 annotation('textarrow',pos(1)+pos(3)*[0.9 .5],pos(2)+pos(4)*[0.5 .2],'String','$I$')
% FabricVsStress(muHw(ix),struct('dphi',true))
% axis(v)
 FabricVsStress(muH(ix),struct('l12',true)); v=axis
 c=get(gca,'Children');
 delete(c(1))
 p=[1.6718 0.2409]
 h=plot(xlim,polyval(p,xlim),'--');
 legend(h,['$' num2str(p(1),'%.2f') '\Lambda_{12}' num2str(p(2),'%+.2f') '$'])
 set(legend,'Location','NorthWest','Box','off','Interpreter','none')
 set(gcf,'FileName',[get(gcf,'FileName') '_edited'])
 ylim([min(ylim) 0.5*max(ylim)])
 pos=get(gca,'Position');
 annotation('textarrow',pos(1)+pos(3)*[0.9 .5],pos(2)+pos(4)*[0.48 .18],'String','$I$')
% FabricVsStress(muHw(ix),struct('l12',true))
% axis(v)
 FabricVsStress(muH(ix),struct('sdstar',true))
 v=axis
 pos=get(gca,'Position');
 annotation('textarrow',pos(1)+pos(3)*[0.5 .9],pos(2)+pos(4)*[0.3 .6],'String','$I$')
% FabricVsStress(muHw(ix),struct('sdstar',true))
% axis(v)
%std.zhat=std.z/std.FlowHeight;
%stdw.zhat=stdw.z/stdw.FlowHeight;
%FabricVsStress({std stdw},struct('dphi',true))
return

function p=FabricVsStress(data,options)
name=fieldnames(options);
newFigure([data{1}.plotname 'FabricVsStress' name{1}],struct('size',.85));
for i=[1:length(data)]
    if isfield(options,'overI')
        z = data{i}.InertialNumber;
        zlabel('$I$');
    else
        z = data{i}.zhat';
        zlabel('$\dot\gamma$');
    end
    ix=data{i}.Bulk;
    %ix=data{i}.zhat<0.8&data{i}.zhat>0.2;
    if isfield(options,'dphi')
        dphiS=data{i}.(['PrincipalContactStressAngle'])(:,2)-45;
        dphiF=data{i}.(['PrincipalFabricAngle'])(:,2)-45;
        x=dphiS;
        y=dphiF;
        xlabel('$\Delta\phi$');
        ylabel('$\Delta\phi^F$');
    elseif isfield(options,'l12')
        L=data{i}.(['PrincipalContactStressLambda']);
        LF=data{i}.(['PrincipalFabricLambda']);
        P=sum(L,2)/3;
        PF=sum(LF,2)/3;
        D=L-P*[1 1 1];
        DF=LF-PF*[1 1 1];
        x=D(:,2)./D(:,1);
        y=DF(:,2)./DF(:,1);
        xlabel('$\Lambda_{12}$');
        ylabel('$\Lambda_{12}^F$');
    elseif isfield(options,'sdstar')
        L=data{i}.(['PrincipalContactStressLambda']);
        LF=data{i}.(['PrincipalFabricLambda']);
        P=sum(L,2)/3;
        PF=sum(LF,2)/3;
        sdstar=sqrt((L(:,1)-L(:,2)).^2+(L(:,2)-L(:,3)).^2+(L(:,3)-L(:,1)).^2)./(sqrt(6)*sum(L,2)/3);
        sdstarF=sqrt((LF(:,1)-LF(:,2)).^2+(LF(:,2)-LF(:,3)).^2+(LF(:,3)-LF(:,1)).^2)./(sqrt(6)*sum(LF,2)/3);
        x=sdstar;
        y=sdstarF;
        xlabel('$s_D^*$');
        ylabel('$s_D^{*F}$');
    elseif isfield(options,'sdremco')
        dphi=data{i}.(['PrincipalContactStressAngle'])(:,2)-45;
        dphiF=data{i}.(['PrincipalFabricAngle'])(:,2)-45;
        L=data{i}.(['PrincipalContactStressLambda']);
        LF=data{i}.(['PrincipalFabricLambda']);
        P=sum(L,2)/3;
        PF=sum(LF,2)/3;
        sdremco=(((L(:,1)-P)-(L(:,2)-P)/2).*cosd(2*dphi))./P;
        sdremcoF=(((LF(:,1)-PF)-(LF(:,2)-PF)/2).*cosd(2*dphiF))./PF;
        x=sdremco;
        y=sdremcoF;
        xlabel('$s_D^r$');
        ylabel('$s_D^{rF}$');
    elseif isfield(options,'sd12')
        dphi=data{i}.(['PrincipalContactStressAngle'])(:,2)-45;
        dphiF=data{i}.(['PrincipalFabricAngle'])(:,2)-45;
        L=data{i}.(['PrincipalContactStressLambda']);
        LF=data{i}.(['PrincipalFabricLambda']);
        P=sum(L,2)/3;
        PF=sum(LF,2)/3;
        sd12=(L(:,1)-L(:,2))./(2*P);
        sd12F=(LF(:,1)-LF(:,2))./(2*PF);
        %sd23=(L(:,2)-L(:,3))./(2*sum(L,2)/3);
        x=sd12;
        y=sd12F;
        xlabel('$s_D^{12}$');
        ylabel('$s_D^{12F}$');
    elseif isfield(options,'sd13')
        dphi=data{i}.(['PrincipalContactStressAngle'])(:,2)-45;
        dphiF=data{i}.(['PrincipalFabricAngle'])(:,2)-45;
        L=data{i}.(['PrincipalContactStressLambda']);
        LF=data{i}.(['PrincipalFabricLambda']);
        P=sum(L,2)/3;
        PF=sum(LF,2)/3;
        sd13=(L(:,1)-L(:,3))./(2*P);
        sd13F=(LF(:,1)-LF(:,3))./(2*PF);
        x=sd13;
        y=sd13F;
        xlabel('$s_D^{13}$');
        ylabel('$s_D^{13F}$');
    elseif isfield(options,'mu')
        mu=-data{i}.StressXZ./data{i}.StressZZ;
        muF=-data{i}.FabricXZ./data{i}.FabricZZ;
        x=mu;
        y=muF;
        xlabel('$\mu_{xyz}$');
        ylabel('$\mu_{xyz}^F$');
    end
    scatter3(x(ix),y(ix),-z(ix),10,z(ix), ...
      'filled','Marker','s', ...
      'Linewidth',1.5, ...
      'UserData',data{i});
    %scatter3(x,y,-z,1,z, ...
    %  'Marker','.');
end
caxis([0,round(max(caxis))])
axis tight

%fit
C=get(gca,'Children');
X=cell2mat(get(C','XData')');
Y=cell2mat(get(C','YData')');
ix=isfinite(X)&isfinite(Y);
X=X(ix);
Y=Y(ix);
p=polyfit(X,Y,1);
h=plot(xlim,polyval(p,xlim));
s=get(get(gca,'xlabel'),'String');
legend(h,['$' num2str(p(1),'%.2f') s(2:end-1) num2str(p(2),'%+.2f') '$'])
set(legend,'Location','NorthWest','Box','off','Interpreter','none')
return

function addfittedK()
xlim([0 1])
ylim([0.9 1.2])
I=0:.02:1;
L12=-0.2-0.1*I;
%
t1=tand(20.72);
t2=tand(41.48);
I0=0.568;
SD=t1+(t2-t1)*I./(I0+I);
%
delta=45+0.15-4.07*I;
c2=cosd(delta).^2;
s2=sind(delta).^2;
%
SDroot=SD./sqrt(1+L12+L12.^2);
K=(SDroot.*(c2+(-1-L12).*s2)+1) ./ (SDroot.*(s2+(-1-L12).*c2)+1);
plot(I,K)
%
% t1=tand(19.67);
% t2=tand(39.89);
% I0=0.617;
% mu=t1+(t2-t1)*I./(I0+I);
% theta=atand(mu);
% Kfit=1+(theta-21.3)/132;
% plot(I,[K; Kfit])
return

function addfittedK2()
xlim([0 1])
ylim([0.7 1])
I=0:.02:1;
L12=-0.2-0.1*I;
%
t1=tand(20.72);
t2=tand(41.48);
I0=0.568;
SD=t1+(t2-t1)*I./(I0+I);
%
delta=45+0.15-4.07*I;
c2=cosd(delta).^2;
s2=sind(delta).^2;
%
SDroot=SD./sqrt(1+L12+L12.^2);
K=(L12.*SDroot+1) ./ (SDroot.*(s2+(-1-L12).*c2)+1);
plot(I,K)
return


function paperstd()
global muH muHw std stdw %#ok<NUSED>

PrincipalAngles(std,'Stress','{}',struct('local',true));
ylim([-2 8])
hold on
plot(xlim,[0 0])
set(legend,'Location','North')
return


%IP(stdw)
%xlim([0 max(xlim)])

Shear(std,stdw)
xlim([0 max(xlim)])




Components(std,'Stress','\sigma')
set(legend,'Position',get(legend,'Position').*[0 1 1 1]+[.7 0 0 0])
text(0,-30*std.Gravity(3),{'$\frac{Nmg\cos\theta}{l_x l_y}\longrightarrow$'}, 'VerticalAlignment','middle', 'HorizontalAlignment','right')
xlim([0 std.FlowHeight]);


Anisotropy(std,struct('sdstar',true))
%set(legend,'Position',get(legend,'Position').*[0 0 1 1]+[.5 .29 0 0])
legend('show')
legend('hide')
ylim([0 .63]);
xlim([0 max(xlim)])

Anisotropy(std,struct('l12',true))
%set(legend,'Position',get(legend,'Position').*[0 0 1 1]+[.5 .7 0 0])
legend('show')
legend('hide')
ylim([-.5 0]);
xlim([0 max(xlim)])
set(gca,'Ytick',-0.5:.1:0)


Components(std,'ContactStress','\sigma^c')
set(legend,'Position',get(legend,'Position').*[0 1 1 1]+[.7 0 0 0])

diffComponents(std,stdw,'ContactStress','\sigma')
set(legend,'Position',get(legend,'Position').*[0 1 1 1]+[.7 0 0 0])
ylim([-0.11,.149])
xlim([0 10])

% diffNuFit(std,stdw);
% ylim([-.2 .2])
% xlim([0 10])

Components(std,'Stress','\sigma')
set(legend,'Position',get(legend,'Position').*[0 1 1 1]+[.7 0 0 0])

diffComponents(std,stdw,'Stress','\sigma')
set(legend,'Position',get(legend,'Position').*[0 1 1 1]+[.7 0 0 0])
ylim([-1.5,2])
 return


PrincipalAngles(std,'Stress','{}',struct('local',true));
ylim([-5 2])
set(legend,'Position',get(legend,'Position').*[0 0 1 1]+[.5 .25 0 0])

PrincipalLambdas(std,'Stress','{}',struct('local',true,'dev',true));
set(legend,'Position',get(legend,'Position').*[0 0 1 1]+[.7 .7 0 0])
return

Anisotropy(std,struct('sdstar',true))
%set(legend,'Position',get(legend,'Position').*[0 0 1 1]+[.5 .29 0 0])
legend('show')
legend('hide')
ylim([0 .63]);
return

Anisotropy(stdw,struct('sdstar',true))
%set(legend,'Position',get(legend,'Position').*[0 0 1 1]+[.5 .29 0 0])
legend('show')
legend('hide')
ylim([0 .63]);

PrincipalAngles(std,'Stress','{}',struct('local',true));
ylim([-2 -1])
set(legend,'Position',get(legend,'Position').*[0 0 1 1]+[.5 .25 0 0])

PrincipalAngles(stdw,'Stress','{}',struct('local',true));
ylim([-2 -1])
set(legend,'Position',get(legend,'Position').*[0 0 1 1]+[.5 .25 0 0])
return

Shear(std,stdw)
return




return

Anisotropy(std)
return

PrincipalAngles(std,'Stress','{}',struct('local',true));
ylim([-2 7])
set(legend,'Position',get(legend,'Position').*[0 0 1 1]+[.5 .7 0 0])

PrincipalAngles(std,'ContactStress','c',struct('local',true));
ylim([-2 7])
set(legend,'Position',get(legend,'Position').*[0 0 1 1]+[.5 .7 0 0])
PrincipalAngles(std,'KineticStress','k',struct('local',true));
ylim([-2 10])
set(legend,'Position',get(legend,'Position').*[0 0 1 1]+[.5 .7 0 0])
PrincipalAngles(std,'Fabric','F',struct('local',true));
ylim([-2 7])
set(legend,'Position',get(legend,'Position').*[0 0 1 1]+[.5 .7 0 0])


return

%NuFit(std)

%Anisotropy(std)

%Anisotropy2(std)


%IP(muH{14})

%ylim([0 .5])

% Viscosity(std)

Components(std,'KineticStress','\sigma^k')
set(legend,'Position',get(legend,'Position').*[0 1 1 1]+[.7 0 0 0])
%Components(stdw,'KineticStress','\sigma^k')

Components(std,'Fabric','F')
set(legend,'Position',get(legend,'Position').*[0 1 1 1]+[.7 0 0 0])
%diffComponents(std,stdw,'KineticStress','\sigma^k')


PrincipalLambdas(std,'Stress','{}',struct('local',true,'dev',true));
set(legend,'Position',get(legend,'Position').*[0 0 1 1]+[.7 .7 0 0])

PrincipalLambdas(std,'ContactStress','c',struct('local',true));
PrincipalLambdas(std,'KineticStress','k',struct('local',true));
PrincipalLambdas(std,'Fabric','F',struct('local',true));
%compare the three anisotropies
%[std.FlowPrincipalFabricLambda./mean(std.FlowPrincipalFabricLambda) std.FlowPrincipalContactStressLambda/mean(std.FlowPrincipalContactStressLambda) std.FlowPrincipalKineticStressLambda/mean(std.FlowPrincipalKineticStressLambda)]
return

function plotFabric()
global mu muw muH muHw %#ok<NUSED>

PrincipalAngles(muH(cellfun(@(d)d.N>3000,muH)),'Fabric','F',struct('local',true));
axis tight
xlim([0 .7])
ylim([-3.5 2])
colorbar off

PrincipalLambdas(muH(cellfun(@(d)d.N>3000,muH)),'Fabric','F',struct('local',true,'l',true));
axis tight
xlim([0 .7])
ylim([-.6 .8])
set(legend,'Position',get(legend,'Position').*[0 0 1 1]+[.6 .5 0 0])
colorbar off

PrincipalLambdas(muH(cellfun(@(d)d.N>3000,muH)),'Fabric','F',struct('local',true,'p',true));
set(gcf,'position',[1 1 1 .6].*get(gcf,'position'))
axis tight
xlim([0 .7])
ylim([.95 1.15])
set(legend,'Location','SouthWest')
colorbar off

PrincipalLambdas(muH(cellfun(@(d)d.N>3000,muH)),'Fabric','F',struct('local',true,'l12',true));
axis tight
xlim([0 .7])
ylim([-0.2 0])
set(legend,'Location','SouthWest')
colorbar off

PrincipalLambdas(muH(cellfun(@(d)d.N>3000,muH)),'Fabric','F',struct('local',true,'sd',true));
axis tight
xlim([0 .7])
ylim([0 .5])
set(legend,'Location','SouthWest')
colorbar off

PrincipalLambdas(muH(cellfun(@(d)d.N>3000,muH)),'Fabric','F',struct('local',true,'sd',true));
axis tight
xlim([1e-3 .7])
ylim([0 .5])
set(gca,'XScale','log')
set(legend,'Location','SouthWest')
colorbar off
set(gcf,'FileName',[get(gcf,'FileName') '_log']);



PrincipalAngles(muH(cellfun(@(d)d.N>3000,muH)),'Fabric','F',struct('local',true,'x','gamma'));
axis tight
xlim([0 2])
ylim([-3.5 2])
colorbar off

PrincipalLambdas(muH(cellfun(@(d)d.N>3000,muH)),'Fabric','F',struct('local',true,'l',true,'x','gamma'));
axis tight
xlim([0 2])
ylim([-.6 .8])
set(legend,'Position',get(legend,'Position').*[0 0 1 1]+[.6 .5 0 0])
colorbar off

PrincipalLambdas(muH(cellfun(@(d)d.N>3000,muH)),'Fabric','F',struct('local',true,'p',true,'x','gamma'));
set(gcf,'position',[1 1 1 .6].*get(gcf,'position'))
axis tight
xlim([0 2])
ylim([.95 1.15])
set(legend,'Location','SouthWest')
colorbar off

PrincipalLambdas(muH(cellfun(@(d)d.N>3000,muH)),'Fabric','F',struct('local',true,'l12',true,'x','gamma'));
axis tight
xlim([0 2])
ylim([-0.2 0])
set(legend,'Location','SouthWest')
colorbar off

PrincipalLambdas(muH(cellfun(@(d)d.N>3000,muH)),'Fabric','F',struct('local',true,'sd',true,'x','gamma'));
axis tight
xlim([0 2])
ylim([0 .5])
set(legend,'Location','SouthWest')
colorbar off

PrincipalLambdas(muH(cellfun(@(d)d.N>3000,muH)),'Fabric','F',struct('local',true,'sd',true,'x','gamma'));
axis tight
xlim([1e-3 2])
set(gca,'XScale','log')
ylim([0 .5])
set(legend,'Location','SouthWest')
colorbar off
set(gcf,'FileName',[get(gcf,'FileName') '_log']);
return

function plotStress()
global mu muw muH muHw %#ok<NUSED>

PrincipalAngles(muH(cellfun(@(d)d.N>3000,muH)),'Stress','\sigma',struct('local',true));
axis tight
xlim([0 .7])
ylim([-2 3.5])
colorbar off

PrincipalLambdas(muH(cellfun(@(d)d.N>3000,muH)),'Stress','\sigma',struct('local',true,'l12',true));
axis tight
xlim([0 .7])
ylim([-.3 -.15])
set(legend,'Location','NorthEast')
colorbar off

PrincipalLambdas(muH(cellfun(@(d)d.N>3000,muH)),'Stress','\sigma',struct('local',true,'p',true));
set(gcf,'position',[1 1 1 .6].*get(gcf,'position'))
axis tight
xlim([0 .7])
ylim([.95 1.15])
set(legend,'Location','SouthWest')
colorbar off

muI(muH,struct('local',true));
ylim([.35 .65])
colorbar off

muI(muH,struct('local',true));
ylim([.35 .65])
colorbar off
xlim([1e-3 2])
set(gca,'XScale','log')
set(gcf,'FileName',[get(gcf,'FileName') '_log']);

PrincipalLambdas(muH(cellfun(@(d)d.N>3000,muH)),'Stress','\sigma',struct('local',true,'l',true));
axis tight
xlim([0 .7])
ylim([-.6 .8])
set(legend,'Position',get(legend,'Position').*[0 0 1 1]+[.6 .5 0 0])
colorbar off
return

function muIfit()
global mu muw muH muHw std stdw %#ok<NUSED>

[fitp] = PrincipalLambdas(muH(cellfun(@(d)d.N>3000,muH)),'Stress','\sigma',struct('local',true,'p',true));
%set(gcf,'position',[1 1 1 .6].*get(gcf,'position'))
axis tight
xlim([0 .7])
ylim([.95 1.15])
set(legend,'Location','SouthWest')
colorbar off

[fitsdstar, fitmu, fitsd13, fitsd23] = muIstar(muH,struct('local',true));
ylim([0 .65])
set(gcf,'position',[1 1 1 2].*get(gcf,'position'))
colorbar off

[fitdphi] = PrincipalAngles(muH(cellfun(@(d)d.N>3000,muH)),'Stress','\sigma',struct('local',true));
axis tight
xlim([0 .7])
ylim([-3.5 2])
colorbar off
set(legend,'Location','NorthEast')

[fitL12] = PrincipalLambdas(muH(cellfun(@(d)d.N>3000,muH)),'Stress','\sigma',struct('local',true,'l12',true));
axis tight
xlim([0 .7])
ylim([-.3 -.15])
set(legend,'Location','NorthEast')
colorbar off

%%
In=0:.01:.7;
mu1=feval(fitmu,In);
L12=feval(fitL12,In);
phi=(feval(fitdphi,In)+45);
c=cosd(phi);
s=sind(phi);

sd=feval(fitsdstar,In);
M=sd./sqrt(L12.^2+L12+1);
l1=M;
l2=M.*L12;
l3=M.*(-1-L12);
Sxz=(l3-l1).*c.*s;
Szz=1+l3.*c.^2+l1.*s.^2;
mu2=-Sxz./Szz;

sd13=feval(fitsd13,In);
sd23=feval(fitsd23,In);
l1b=2/3*(2*sd13-sd23);
l2b=2/3*(2*sd23-sd13);
l3b=2/3*(-sd13-sd23);
Sxz=(l3b-l1b).*c.*s;
Szz=1+l3b.*c.^2+l1b.*s.^2;
mu3=-Sxz./Szz;

data=muH(cellfun(@(d)d.N>3000,muH));

newFigure([data{1}.plotname 'lambdaIfit'],struct('size',1.3));
plot(In,[l1 l2 l3 l1b l2b l3b])
hold on
I=cell(1,length(data));
for i=1:length(data)
  ix=data{i}.Bulk;
  jx=abs(data{i}.zhat-0.5)<0.5 & (~ix);
  I{i}=data{i}.InertialNumber;
  P=data{i}.zhat;
  scatter3(I{i}(ix),data{i}.PrincipalStressLambda(ix,1)./data{i}.Pressure(ix)-1,-P(ix),10,P(ix),'filled','Marker','s','Displayname','$\mu$ data');
  scatter3(I{i}(ix),data{i}.PrincipalStressLambda(ix,2)./data{i}.Pressure(ix)-1,-P(ix),10,P(ix),'filled','Marker','s','Displayname','$\mu$ data');
  scatter3(I{i}(ix),data{i}.PrincipalStressLambda(ix,3)./data{i}.Pressure(ix)-1,-P(ix),10,P(ix),'filled','Marker','s','Displayname','$\mu$ data');
  scatter3(I{i}(jx),data{i}.PrincipalStressLambda(jx,1)./data{i}.Pressure(jx)-1,-P(jx),1 ,P(jx),'Marker','.','Displayname','$\mu$ data');
  scatter3(I{i}(jx),data{i}.PrincipalStressLambda(jx,2)./data{i}.Pressure(jx)-1,-P(jx),1 ,P(jx),'Marker','.','Displayname','$\mu$ data');
  scatter3(I{i}(jx),data{i}.PrincipalStressLambda(jx,3)./data{i}.Pressure(jx)-1,-P(jx),1 ,P(jx),'Marker','.','Displayname','$\mu$ data');
end
axis tight
xlabel('$I$')
ylabel('$\lambda_i/p$')
xlim([0,.7])
ylim([-.6 .8])
colorbar off

newFigure([data{1}.plotname 'muIfit'],struct('size',1.3));
plot(In,[mu1 mu2 mu3],'DisplayName',{'mu','sdstar','sd12',})
legend('show')
hold on
I=cell(1,length(data));
for i=1:length(data)
  ix=data{i}.Bulk;
  %jx=abs(data{i}.zhat-0.5)<0.5;
  I{i}=data{i}.InertialNumber(ix);
  mu{i}=-data{i}.ExtendedStressXZ(ix)./data{i}.ExtendedStressZZ(ix);
  P=data{i}.zhat(ix);
  k=scatter3(I{i},mu{i},-P,10,P,'Marker','x','Displayname','$\mu$ data');
end
axis tight
xlim([0,.7])
ylim([.3 .7])
colorbar off
%%

muI(muH,struct('local',true,'sdstar',true));
ylim([.33 .65])
colorbar off

muI(muH,struct('local',true));
ylim([.33 .65])
plot(In,[mu2],'b','DisplayName','fit to $\mu$ using Eq. \ref{eq:mustar}')
%set(gcf,'position',[1 1 1 2].*get(gcf,'position'))
c=get(gca,'Children');
legend(c(1:2))
set(legend,'Location','NorthWest','Box','off')
colorbar off

return

function muFIwfit()
global muH muHw std stdw %#ok<NUSED>
data=muHw(cellfun(@(d)d.N>3000,muHw)&cellfun(@(d)d.ChuteAngle<30,muHw));
data=[data(1:5) data(7:end)]
for i=1:length(data)
   %below, we smooth the inertial number
   ix=(data{i}.InertialNumber~=0);
   regularity=zeros(size(data{i}.z));
   regularity(ix)=smooth(abs(data{i}.InertialNumber(ix)/data{i}.BulkInertialNumber-1));
   data{i}.Bulk = abs(data{i}.zhat-0.5)<0.5;
   data{i}.Bulk(1:find(regularity(1:round(end/2))>0.1,1,'last'))=false;
   data{i}.Bulk(round(end/2)-1+find(regularity(round(end/2):end)>0.05,1,'first'):end)=false;
end

[fitdphi] = PrincipalAngles(data,'Fabric','F',struct('local',true));
axis tight
xlim([0 .4])
ylim([-2.5 1])
colorbar off
set(legend,'Location','NorthWest')

[fitL12] = PrincipalLambdas(data,'Fabric','F',struct('local',true,'l12',true));
axis tight
xlim([0 .4])
ylim([-0.16 -0.1])
set(legend,'Location','NorthWest')
colorbar off

muI(data,struct('local',true,'Fsdstar',true));
xlim([0 .4])
ylim([0.2 .42])
colorbar off
return

function muIwfit()
global muH muHw std stdw %#ok<NUSED>
data=muHw(cellfun(@(d)d.N>3000,muHw)&cellfun(@(d)d.ChuteAngle<30,muHw));
data=[data(1:5) data(7:end)]
for i=1:length(data)
   %below, we smooth the inertial number
   ix=(data{i}.InertialNumber~=0);
   regularity=zeros(size(data{i}.z));
   regularity(ix)=smooth(abs(data{i}.InertialNumber(ix)/data{i}.BulkInertialNumber-1));
   data{i}.Bulk = abs(data{i}.zhat-0.5)<0.5;
   data{i}.Bulk(1:find(regularity(1:round(end/2))>0.1,1,'last'))=false;
   data{i}.Bulk(round(end/2)-1+find(regularity(round(end/2):end)>0.05,1,'first'):end)=false;
end


[fitdphi] = PrincipalAngles(data,'Stress','\sigma',struct('local',true));
axis tight
xlim([0 .4])
ylim([-1 2.5])
colorbar off
set(legend,'Location','NorthWest')

[fitL12] = PrincipalLambdas(data,'Stress','\sigma',struct('local',true,'l12',true));
axis tight
xlim([0 .4])
ylim([0.15 0.25])
set(legend,'Location','NorthWest')
colorbar off

muI(data,struct('local',true,'sdstar',true));
xlim([0 .4])
ylim([0.3 .6])
colorbar off
return

function teststd()
global std stdw muH muHw
%cellfun(@(d)d.ChuteAngle,muH)
%cellfun(@(d)round((d.N-289)/200),muH)
i=18%17
IP(muHw{i})
% PrincipalAngles(muHw{i},'ContactStress','c',struct('local',true));
% ylim([-7 2])
PrincipalAngles(muHw{i},'Fabric','F',struct('local',true));
ylim([-7 2])
% PrincipalLambdas(muHw{i},'ContactStress','c',struct('local',true));
% PrincipalLambdas(muHw{i},'Fabric','F',struct('local',true));
% PrincipalLambdas(muHw{i},'KineticStress','k',struct('local',true));
return

function StressDecomposition(data)
newFigure([data{1}.plotname 'StressDecomposition']);
c=lines(3);

%tensor={'ContactStress';'KineticStress';'Stress'};
for i=1:length(data)
%   S(i)=sum(data{i}.FlowPrincipalStressLambda)/3;
%   K(i)=sum(data{i}.FlowPrincipalKineticStressLambda)/3;
%   C(i)=sum(data{i}.FlowPrincipalContactStressLambda)/3;
%   I(i)=data{i}.FlowInertialNumber;
  ix=data{i}.Bulk;
  scatter(data{i}.InertialNumber(ix),sum(data{i}.PrincipalKineticStressLambda(ix,:),2)./sum(data{i}.PrincipalContactStressLambda(ix,:),2),10,data{i}.zhat(ix), ...
        'filled','Marker','s', ...
        'Linewidth',1.5);
end
axis tight; a=axis;
for i=1:length(data)
    scatter(data{i}.InertialNumber,...
      sum(data{i}.PrincipalKineticStressLambda,2)./sum(data{i}.PrincipalContactStressLambda,2),...
      .2,data{i}.zhat,'.');
end
axis(a);
% plot(I,K./S,'x');
xlabel('$I$'); 
ylabel('$\sigma_{\alpha\alpha}^k/\sigma_{\alpha\alpha}$','Rotation',90);
caxis([0,round(max(caxis))])
c=colorbar
xlabel(c,'$\hat{z}$')
set(legend,'Location','South','Box','off','Interpreter','none')
xlim([1e-2 2e-0])
ylim([.5e-3 .5e-0])
add_triangle_to_loglog2(2,.5,.55,true,.4)
set(gca,'XScale','log')
set(gca,'YScale','log')
return

function CG()
global std stdw stdwFV
newFigure(['stdCG']);
stdw.KineticStressXXcorrection = stdw.ShearRate.^2.*(stdw.w/2/sqrt(3)).^2.*stdw.Density;
plot(std.z,[std.KineticStressXX stdw.KineticStressXX stdw.KineticStressXX-stdw.KineticStressXXcorrection stdwFV.KineticStressXX],...
  'Displayname',{'$\sigma_{xx}^k|_{w=0.05}$' '$\sigma_{xx}^k|_{w=1}$' '$\sigma_{xx}^k{}''|_{w=1}$' '$\sigma_{xx}^k{}^\star|_{w=1}$' });
legend('show')
xlabel('$z$')
set(legend,'Box','off','Interpreter','none')
axis tight
xlim([-.5 30.5])
set(legend,'Position',get(legend,'Position').*[0 0 1 1]+[.6 .62 0 0])
return

function Anisotropy(data,options)
newFigure([data.plotname 'Anisotropy']);
if ~exist('options','var')
    options=struct('sdstar',true);
end

tensor={'Stress';'ContactStress';'KineticStress';'Fabric'};
tensorname={'{}';'c';'F';'k'};
tensorname={'\sigma';'\sigma^c';'\sigma^k';'F'};
c=lines(length(tensorname));
for i=1:length(tensorname)
  D=data.(['Principal' tensor{i} 'Lambda']);
  L=D-sum(D,2)/3*[1 1 1];
  %sd=abs(L(:,1)-L(:,3))./(2*sum(L,2)/3);

  %sd2=abs(L(:,1)-L(:,2))./(2*sum(L,2)/3);
  
  ix=data.z>data.Base&data.z<data.Surface;
  if isfield(options,'sdstar')
      set(gcf,'FileName',[data.plotname 'sdstar'])
      sdstar=sqrt((L(:,1)-L(:,2)).^2+(L(:,2)-L(:,3)).^2+(L(:,3)-L(:,1)).^2)./(sqrt(6)*sum(D,2)/3);
      plot(data.z(ix),sdstar(ix),...
         'Color',c(i,:),'DisplayName',{['$' tensorname{i} '$']});
      ylabel('$\sd$')
  end
  if isfield(options,'phi')
      set(gcf,'FileName',[data.plotname 'phi'])
      phi=data.(['Principal' tensor{i} 'Angle'])(:,2)-45;
      plot(data.z(ix),phi(ix),...
         'Color',c(i,:),'DisplayName',{['$' tensorname{i} '$']});
      ylabel('$\dphi$')
  end
  if isfield(options,'l12')
      set(gcf,'FileName',[data.plotname 'l12'])
      l21=L(:,2)./L(:,1);
      plot(data.z(ix),l21(ix),...
         'Color',c(i,:),'DisplayName',{['$' tensorname{i} '$']});
      ylabel('$\Lambda_{12}$')
  end
end
legend('show')
xlabel('$z$'); 
axis tight
xlim([data.Base data.Surface]);
ylim([0 max(ylim)]);
set(legend,'Location','North','Box','off','Interpreter','none')
return

function Anisotropy2(data)
newFigure([data.plotname 'Anisotropy2']);
c=lines(3);

tensor={'ContactStress';'Fabric';'KineticStress'};
tensorname={'c';'F';'k'};
for i=1:3
  L=data.(['Principal' tensor{i} 'Lambda']);
  %sd=abs(L(:,1)-L(:,3))./(2*sum(L,2)/3);
  l21=L(:,2)./L(:,1);
  %sd2=abs(L(:,1)-L(:,2))./(2*sum(L,2)/3);
  
  ix=data.z>data.Base&data.z<data.Surface;
  plot(data.z(ix),l21(ix),...
     'Color',c(i,:),'DisplayName',{['${S_D^*}^' tensorname{i} '$']});
end
legend('show')
xlabel('$z/d$'); 
axis tight
xlim([data.Base data.Surface]);
ylim([0 max(ylim)]);
set(legend,'Location','South','Box','off','Interpreter','none')
return

function [c] = PrincipalLambdas(data,tensor,tensorname,options)
if isempty(tensor), tensor='Stress'; end

if iscell(data) && exist('options','var') && isfield(options,'local')
  %plots local PSA of a data array
  newFigure([data{1}.plotname 'LocalPrincipal' tensor 'Lambdas'],struct('size',1.3));
  fitx=[];
  fity=[];
  for i=1:length(data)
    if isfield(options,'x') & strcmp(options.x,'gamma')
      x = data{i}.ShearRate; 
    else
      x = data{i}.InertialNumber;  
    end
    z = data{i}.zhat';
    ix=data{i}.Bulk;
    if isfield(options,'sd')
      set(gcf,'FileName',[data{1}.plotname 'LocalPrincipal' tensor 'Lambdas_sd'])
      L=data{i}.(['Principal' tensor 'Lambda']);
      %sd=abs(L(:,1)-L(:,3))./(2*sum(L,2)/3);
      y=sqrt((L(:,1)-L(:,2)).^2+(L(:,2)-L(:,3)).^2+(L(:,3)-L(:,1)).^2)./(sqrt(6)*sum(L,2)/3);
      label='$sd$';
    elseif isfield(options,'phiInt')
        set(gcf,'FileName',[data{1}.plotname 'LocalPrincipal' tensor 'Lambdas_phiint'])
        XX=data{i}.([tensor 'XX']);
        ZZ=data{i}.([tensor 'ZZ']);
        phiInt=asind(sqrt(mean( XX-ZZ )./mean( XX+ZZ )));
        plot(data{i}.BulkInertialNumber,phiInt,'x',...
            'DisplayName',{['$\phi_{int}^' tensorname '$']});
        x=0; y=0; z=0; ix=1; label='s'
    elseif isfield(options,'l') || isfield(options,'lsd')
      if (isfield(options,'lsd')), set(gcf,'FileName',[data{1}.plotname 'LocalPrincipal' tensor 'Lambdas_lsd']); end
      L=data{i}.(['Principal' tensor 'Lambda']);
      %L=[data{i}.([tensor 'XX']) data{i}.([tensor 'YY']) data{i}.([tensor 'ZZ']) ];
      P=sum(L,2)/3;
      D=L-P*[1 1 1];
      label='$\lambda_i/p$';
      if isfield(options,'lsd')
          sdstar=sqrt((L(:,1)-L(:,2)).^2+(L(:,2)-L(:,3)).^2+(L(:,3)-L(:,1)).^2)./(sqrt(6)*sum(L,2)/3);
          P=P.*sdstar;
          label='$\lambda_i/p/s_D^\star$';
          set(gcf,'FileName',[get(gcf,'FileName') 'lsd'])
      end
      %P=data{i}.([tensor 'ZZ']);
      %D=L;
      y=D(:,1)./P;
      ixf=data{i}.z>data{i}.Base&data{i}.z<data{i}.Surface;
%       plot(x(ixf),y(ixf),'.','Color',[1 1 1]*.8,'MarkerSize',1);
%       plot(x(ixf),y(ixf),'.','Color',[1 1 1]*.8,'MarkerSize',1);
      scatter3(x(ixf),y(ixf),-z(ixf),1,z(ixf), ...
        'Marker','.', ...
        'Linewidth',1.5);
      scatter3(x(ix),y(ix),-z(ix),10,z(ix), ...
        'filled','Marker','s', ...
        'Linewidth',1.5);
      y=D(:,2)./P;
      %plot(x(ixf),y(ixf),'.','Color',[1 1 1]*.8,'MarkerSize',1);
      scatter3(x(ixf),y(ixf),-z(ixf),1,z(ixf), ...
        'Marker','.', ...
        'Linewidth',1.5);
      scatter3(x(ix),y(ix),-z(ix),10,z(ix), ...
        'filled','Marker','s', ...
        'Linewidth',1.5);
      y=D(:,3)./P;
      %plot(x(ixf),y(ixf),'.','Color',[1 1 1]*.8,'MarkerSize',1);
      scatter3(x(ixf),y(ixf),-z(ixf),1,z(ixf), ...
        'Marker','.');
      fitx=[fitx; x(ix)];  %#ok<AGROW>
      fity=[fity; D(ix,:)./(P(ix)*[1 1 1])]; %#ok<AGROW>
    elseif isfield(options,'l12')
      set(gcf,'FileName',[data{1}.plotname 'LocalPrincipal' tensor 'Lambdas_l12'])
      L=data{i}.(['Principal' tensor 'Lambda']);
      P=sum(L,2)/3;
      D=L-P*[1 1 1];
      label='$\Lambda_{12}$';
      y=D(:,2)./D(:,1);
      fitx=[fitx; x(ix)]; %#ok<AGROW>
      fity=[fity; y(ix)]; %#ok<AGROW>
    elseif isfield(options,'eta')
      set(gcf,'FileName',[data{1}.plotname 'LocalPrincipal' tensor 'eta'])
      L=data{i}.(['Principal' tensor 'Lambda']);
      dphi=data{i}.(['Principal' tensor 'Angle'])(:,2)-45;
      P=sum(L,2)/3;
      D=L-P*[1 1 1];
      label='$\mu*=(\lambda_1-\lambda_2/2)\cos(2\Delta\phi)/p$';
      y=((D(:,1)-D(:,2)/2).*cosd(2*dphi))./P;
      fitx=[fitx; x(ix)]; %#ok<AGROW>
      fity=[fity; y(ix)]; %#ok<AGROW>
    elseif isfield(options,'dphi')
      set(gcf,'FileName',[data{1}.plotname 'LocalPrincipal' tensor 'dphi'])
      y=data{i}.(['Principal' tensor 'Angle'])(:,2)-45;
      label='$\Delta\phi$';
      fitx=[fitx; x(ix)]; %#ok<AGROW>
      fity=[fity; y(ix)]; %#ok<AGROW>
    elseif isfield(options,'K')
      set(gcf,'FileName',[data{1}.plotname 'K'])
      y=data{i}.([tensor 'XX'])./data{i}.([tensor 'ZZ']);
      %label='$K$';
      label='$\sigma_{xx}/\sigma_{zz}$';
    elseif isfield(options,'K2')
      set(gcf,'FileName',[data{1}.plotname 'K2'])
      y=data{i}.([tensor 'YY'])./data{i}.([tensor 'ZZ']);
      label='$\sigma_{yy}/\sigma_{zz}$';
     elseif isfield(options,'p')
      set(gcf,'FileName',[data{1}.plotname 'LocalPrincipal' tensor 'Lambdas_p'])
      L=data{i}.([tensor 'ZZ']);
      P=(data{i}.([tensor 'XX'])+data{i}.([tensor 'YY'])+data{i}.([tensor 'ZZ']))/3;
      y=L./P;
      label=['$' tensorname '_{zz}/p$'];
      fitx=[fitx; x(ix)]; %#ok<AGROW>
      fity=[fity; y(ix)]; %#ok<AGROW>
    else
      y = data{i}.(['Principal' tensor 'Lambda'])(:,2)';
      label='$\lambda_2$';
    end
    %data{i}.Height=data{i}.Surface-data{i}.Base;
    %ix=data{i}.z>data{i}.Base+3+data{i}.Height*0.3&data{i}.z<data{i}.Surface-3-data{i}.Height*0.3;
    scatter3(x(ix),y(ix),-z(ix),10,z(ix), ...
      'filled','Marker','s', ...
      'Linewidth',1.5, ...
      'UserData',data{i});
%     scatter3(x,y,-z,1,z, ...
%       'Marker','.');
  end
  try
    color='rbg';
    for i=1:size(fity,2)
      if isfield(options,'p')
          s = fitoptions('Method','NonlinearLeastSquares',...
          'Lower',[-Inf],...
          'Upper',[Inf],...
          'Startpoint',[0]);
          f = fittype('b+0*I','options',s,'independent','I',...
             'coefficients',{'b'});
          ix=isfinite(fity(:,i));
          c = fit(fitx(ix),fity(ix,i),f);
          xlim([0 .7])
          h=plot(c);
          val=coeffvalues(c);
          set(h,'Color',color(i),'DisplayName',['$' num2str(val(1),3) '$'])
      else
          s = fitoptions('Method','NonlinearLeastSquares',...
          'Lower',[-Inf -Inf],...
          'Upper',[Inf Inf],...
          'Startpoint',[0 0]);
          f = fittype('a*I+b','options',s,'independent','I',...
             'coefficients',{'a','b'});
          ix=isfinite(fity(:,i));
          c = fit(fitx(ix),fity(ix,i),f);
          xlim([0 .7])
          h=plot(c);
          val=coeffvalues(c);
          set(h,'Color',color(i),'DisplayName',['$' num2str(val(1),2) '\,I' num2str(val(2),'%+.2f') '$'])
      end
    end
  catch exception
      warning(exception.identifier,exception.message);
  end
  if isfield(options,'x') & strcmp(options.x,'gamma')
      xlabel('$\dot\gamma$');
      set(gcf,'FileName',[get(gcf,'FileName') '_gamma']);
  else
      xlabel('$I$');
  end
  ylabel(label)
  zlabel('$\hat{z}$')
  caxis([0,round(max(caxis))])
  colorbar
  xlabel(colorbar,'$\hat{z}$')
  %legend('show')
  set(legend,'Location','Best','Box','off','Interpreter','none')
elseif iscell(data)
  %plot global PSA of a data array
  newFigure([data{1}.plotname 'Principal' tensor 'Lambdas']);
  x = cellfun(@(d)d.axis,data)'; xlabel(data{1}.label); 
  y = cellfun(@(d)d.FlowInertialNumber,data)'; ylabel('$\bar{I}$','Interpreter','none'); 
  %y = cellfun(@(d)d.FixedParticleRadius*2,data)'; ylabel('$\lambda$'); 
  %FlowLambdaxy = cellfun(@(d)d.(['FlowPrincipal' tensor 'Lambda'])(1),data);
  FlowLambda1 = cellfun(@(d)d.(['FlowPrincipal' tensor 'Lambda'])(1),data);
  %FlowLambdayz = cellfun(@(d)d.(['FlowPrincipal' tensor 'Lambda'])(3),data);
  %   plot3(x,y,[FlowLambdaxy;FlowLambdaxz-45;FlowLambdayz], ...
  %     'x','DisplayName',{'$\bar\lambda_{yz}$','$\bar\lambda_{xz}-\phi_\eps$','$\bar\lambda_{xy}$'},...
  %     'UserData',data);
  plot3(x,y,FlowLambda1, ...
    'x','DisplayName','$\bar\lambda_1$',...
    'UserData',data);
  %legend('show')
  %set(legend,'Location','Best','Box','off')
  zlabel('$\bar\lambda_{xz}$')
  view(0,0)
  %view(90,0)
else
  %plots local PSA of a single data set
  if isfield(options,'sd')
    newFigure([data.plotname 'Principal' tensor 'Anisotropy']);
    L=data.(['Principal' tensor 'Lambda']);
    sd=abs(L(:,1)-L(:,3))./(2*sum(L,2)/3);
    sdstar=sqrt((L(:,1)-L(:,2)).^2+(L(:,2)-L(:,3)).^2+(L(:,3)-L(:,1)).^2)./(sqrt(6)*sum(L,2)/3);
    %sd2=abs(L(:,1)-L(:,2))./(2*sum(L,2)/3);
    ix=data.z>data.Base&data.z<data.Surface;
    plot(data.z(ix),[sd(ix) sdstar(ix)],...
       'DisplayName',{['$S_D^' tensorname '$'],['${S_D^*}^' tensorname '$']});
  else
    newFigure([data.plotname 'Principal' tensor 'Lambdas']);
    y=[data.(['Principal' tensor 'Lambda'])(:,1)'; data.(['Principal' tensor 'Lambda'])(:,2)'; data.(['Principal' tensor 'Lambda'])(:,3)']
    if isfield(options,'dev')
       y=y-[1;1;1]*sum(y,1)/3;
    end
    plot(data.z,y,...
       'DisplayName',{['$\lambda_1^' tensorname '$'],['$\lambda_2^' tensorname '$'],['$\lambda_3^' tensorname '$']});
  end
  legend('show')
  xlabel('$z/d$'); 
  axis tight
  xlim([data.Base data.Surface]);
  set(legend,'Location','NorthEast','Box','off','Interpreter','none')
end
dcm_obj = datacursormode(gcf);
set(dcm_obj,'UpdateFcn',@DataTip);
axis([-1 1 -1 1].*max([-1 1 -1 1].*axis,0)); %make sure the origin is included
return

function [fitdphi]=PrincipalAngles(data,tensor,tensorname,options)
if isempty(tensor), tensor='Stress'; end

if iscell(data) & exist('options','var') & isfield(options,'local')
  %plots local PSA of a data array
  xmax=0;
  ymax=0;
  ymin=0;
  newFigure([data{1}.plotname 'LocalPrincipal' tensor 'Angles'],struct('size',1.3));
  for i=1:length(data)
    if isfield(options,'x') & strcmp(options.x,'gamma')
      x = data{i}.ShearRate(:); xlabel('$\bar{I}$','Interpreter','none'); 
    else
      x = data{i}.InertialNumber(:); xlabel('$\bar{I}$','Interpreter','none');    
    end
    y = -45+data{i}.(['Principal' tensor 'Angle'])(:,2)';
    %y= (data{i}.PrincipalFabricAngle(:,2)'-45)./(data{i}.PrincipalContactStressAngle(:,2)'-45);
    z = data{i}.zhat(:)';
    data{i}.Height=data{i}.Surface-data{i}.Base;
    ix=data{i}.z>data{i}.Base&data{i}.z<data{i}.Surface;
%     scatter3(x(ix),y(ix),-z(ix),1,z(ix), ...
%       'Marker','.', ...
%       'UserData',data{i});
    ix=data{i}.Bulk;
    %ix = data{i}.zhat>0.3 & data{i}.zhat<0.8;
    %ix=data{i}.z>data{i}.Base+5&data{i}.z<data{i}.Surface-5;
    if any(ix)
      scatter3(x(ix),y(ix),-z(ix),10,z(ix), ...
        'filled','Marker','s', ...
        'UserData',data{i});
      xmax=max(xmax,max(x(ix)));
      ymax=max(ymax,max(y(ix)));
      ymin=min(ymin,min(y(ix)));
    end
  end
  axis([0 xmax ymin ymax]);
  
  %plots FlowPrincipalAngles
  if false
    x = cellfun(@(d)d.axis,data)'; xlabel(data{1}.label); 
    y = cellfun(@(d)d.BulkInertialNumber,data)'; ylabel('$\bar{I}$','Interpreter','none'); 
    FlowAnglexz = cellfun(@(d)d.(['FlowPrincipal' tensor 'Angle'])(2),data)';
    plot(y,FlowAnglexz-45, ...
      'x','DisplayName','$\dphi [{}^\circ]$',...
      'UserData',data);
    v=[1 1];
    s = fitoptions('Method','NonlinearLeastSquares',...
    'Lower',-Inf*v,...
    'Upper',Inf*v,...
    'Startpoint',0*v);
    f = fittype('a*I+b','options',s,'independent','I',...
       'coefficients',{'a','b'});
    c = fit(y,FlowAnglexz-45,f);
    fitdphi=c;
    disp(c)
    xlim([0 .7])
    h=plot(c);
    set(h,'Color','k')
    val=coeffvalues(c);
    set(h,'DisplayName',['$' num2str(val(1),3) '\,I' num2str(val(2),'%+3f') '$'])
  end
  
  %creates fit
  I=[]; A=[];
  for i=1:length(data)
    %ix=data{i}.z>data{i}.Base&data{i}.z<data{i}.Surface;
    ix=data{i}.Bulk;
    I = [I; data{i}.InertialNumber(ix)];
    A = [A; -45+data{i}.(['Principal' tensor 'Angle'])(ix,2)];
  end
  v=[1 1];
  s = fitoptions('Method','NonlinearLeastSquares',...
  'Lower',-Inf*v,...
  'Upper',Inf*v,...
  'Startpoint',0*v);
  f = fittype('a*I+b','options',s,'independent','I',...
     'coefficients',{'a','b'});
  c = fit(I(isfinite(A)),A(isfinite(A)),f);
  fitdphi=c;
  disp(c)
  h=plot(c);
  val=coeffvalues(c);
  set(h,'DisplayName',['$' num2str(val(1),3) '\,I' num2str(val(2),'%+.3f') '$'])
  if isfield(options,'x') & strcmp(options.x,'gamma')
      xlabel('$\dot\gamma$');
      set(gcf,'FileName',[get(gcf,'FileName') '_gamma']);
  else
      xlabel('$I$');
  end
  ylabel(['$\Delta\phi_' tensorname '\,[{}^\circ]$'])
  zlabel('$\hat{z}$','Interpreter','none')
  %caxis([0,max(caxis)])
  caxis([0,1])
  c=colorbar
  xlabel(c,'$\hat{z}$','Interpreter','none')
  %legend('show')
  set(legend,'Location','SouthEast','Box','off','Interpreter','none')
elseif iscell(data)
  %plot global PSA of a data array
  newFigure([data{1}.plotname 'Principal' tensor 'Angles']);
  x = cellfun(@(d)d.axis,data)'; xlabel(data{1}.label); 
  y = cellfun(@(d)d.BulkInertialNumber,data)'; ylabel('$\bar{I}$','Interpreter','none'); 
  %y = cellfun(@(d)d.FixedParticleRadius*2,data)'; ylabel('$\lambda$'); 
  %FlowAnglexy = cellfun(@(d)d.(['FlowPrincipal' tensor 'Angle'])(1),data);
  FlowAnglexz = cellfun(@(d)d.(['FlowPrincipal' tensor 'Angle'])(2),data);
  %FlowAngleyz = cellfun(@(d)d.(['FlowPrincipal' tensor 'Angle'])(3),data);
  %   plot3(x,y,[FlowAnglexy;FlowAnglexz-45;FlowAngleyz], ...
  %     'x','DisplayName',{'$\bar\alpha_{yz}$','$\bar\alpha_{xz}-\phi_\eps$','$\bar\alpha_{xy}$'},...
  %     'UserData',data);
  plot3(x,y,FlowAnglexz-45, ...
    'x','DisplayName','$\dphi\,[{}^\circ]$',...
    'UserData',data);
  %legend('show')
  %set(legend,'Location','Best','Box','off')
  zlabel('$\dphi\,[{}^\circ]$')
  ylabel(['$[{}^\circ]$'])
  view(0,0)
  view(90,0)
else
  %plots local PSA of a single data set
  newFigure([data.plotname 'Principal' tensor 'Angles']);
  plot(data.z,45-data.(['Principal' tensor 'Angle'])(:,2)','b',...
      'DisplayName','$\phi_\eps-\phi_\sigma\,[{}^\circ]$')
%   plot(data.z,[data.(['Principal' tensor 'Angle'])(:,1)'; data.(['Principal' tensor 'Angle'])(:,2)'-45; data.(['Principal' tensor 'Angle'])(:,3)'],...
%      'DisplayName',{['$\alpha_{yz}^' tensorname '~[{}^\circ]$'],['$\alpha_{xz}^' tensorname '-\phi_\eps\,[{}^\circ]$'],['$\alpha_{xy}^' tensorname '~[{}^\circ]$']})
%   plot(data.z,(data.PrincipalFabricAngle(:,2)'-45)./(data.PrincipalContactStressAngle(:,2)'-45),...
%      'DisplayName',{['$ratio \alpha$']})
  legend('show')
%   plot([data.Base data.Surface],(data.(['FlowPrincipal' tensor 'Angle'])'-[0 45 0]')*[1 1],...
%      'HandleVisibility','off');
  xlabel('$z/d$'); 
  %ylabel(['$[{}^\circ]$'])
  xlim([data.Base data.Surface]);
  set(legend,'Location','SouthEast','Box','off','Interpreter','none')
end
dcm_obj = datacursormode(gcf);
set(dcm_obj,'UpdateFcn',@DataTip);
axis([-1 1 -1 1].*max([-1 1 -1 1].*axis,0)); %make sure the origin is included
return

function PrincipalAngles2(data,dataw,options)
if iscell(data) && exist('options','var') && isfield(options,'local')
  %plots local PSA of a data array
  newFigure([data{1}.plotname 'LocalPrincipal' tensor 'Angles']);
  for i=1:length(data)
    x = data{i}.InertialNumber; xlabel('$\bar{I}$','Interpreter','none'); 
    FlowAnglexy = cellfun(@(d)d.(['FlowPrincipal' tensor 'Angle'])(1),data);
    FlowAnglexz = cellfun(@(d)d.(['FlowPrincipal' tensor 'Angle'])(2),data);
    FlowAngleyz = cellfun(@(d)d.(['FlowPrincipal' tensor 'Angle'])(3),data);
    y=data{i}.(['Principal' tensor 'Angle'])(:,2)'-45;
    z=data{i}.StressZZ';
    %ix=logical(ones(size(data{i}.x)));
    ix=data{i}.ExtendedBulk;
    %h=plot3(x(ix),y(ix),z(ix)-eps,'-','Color',.9*[1 1 1]);
    k=scatter3(x(ix),y(ix),z(ix),3,z(ix), ...
      'DisplayName',{'$\bar\alpha_{yz}$','$\bar\alpha_{xz}$','$\bar\alpha_{xy}$'},...
      'Marker','o', ...
      'Linewidth',1.5, ...
      'UserData',data{i});
  end
  xlabel('$I$')
  ylabel('$\dphi~[{}^\circ]$')
  zlabel('$P$')
  caxis([0,max(caxis)])
  c=colorbar
  xlabel(c,'$P$')
  %legend('show')
  set(legend,'Location','Best','Box','off')
elseif iscell(data)
  %plot global PSA of a data array
  newFigure([data{1}.plotname 'Principal' tensor 'Angles']);
  x = cellfun(@(d)d.axis,data)'; xlabel(data{1}.label); 
  y = cellfun(@(d)d.FlowInertialNumber,data)'; ylabel('$\bar{I}$','Interpreter','none'); 
  %y = cellfun(@(d)d.FixedParticleRadius*2,data)'; ylabel('$\lambda$'); 
  FlowAnglexy = cellfun(@(d)d.(['FlowPrincipal' tensor 'Angle'])(1),data);
  FlowAnglexz = cellfun(@(d)d.(['FlowPrincipal' tensor 'Angle'])(2),data);
  FlowAngleyz = cellfun(@(d)d.(['FlowPrincipal' tensor 'Angle'])(3),data);
  plot3(x,y,[FlowAnglexy;FlowAnglexz-45;FlowAngleyz], ...
    'x','DisplayName',{'$\bar\alpha_{yz}$','$\bar\alpha_{xz}$','$\bar\alpha_{xy}$'},...
    'UserData',data);
  ylabel(['$[{}^\circ]$'])
  zlabel('')
  %legend('show')
  set(legend,'Location','Best','Box','off')
  view(0,0)
  %view(90,0)
else
  %plots local PSA of a single data set
  newFigure([data.plotname 'Principal' tensor 'Angles']);
  plot(data.z,[data.(['Principal' tensor 'Angle'])(:,1)'; data.(['Principal' tensor 'Angle'])(:,2)'-45; data.(['Principal' tensor 'Angle'])(:,3)']-[dataw.(['Principal' tensor 'Angle'])(:,1)'; dataw.(['Principal' tensor 'Angle'])(:,2)'-45; dataw.(['Principal' tensor 'Angle'])(:,3)'],...
     'DisplayName',{'$\alpha_{yz}$','$\alpha_{xz}-\phi_\eps$','$\alpha_{xy}$'})
  plot([data.Base data.Surface],0*[1 1],...
     'HandleVisibility','off');
  xlabel('$z/d$');
    ylabel(['$[{}^\circ]$'])
  xlim([data.Base data.Surface]);
  legend('show')
  set(legend,'Location','South')
end
dcm_obj = datacursormode(gcf);
set(dcm_obj,'UpdateFcn',@DataTip);
return

function RegimeAll
global regime regimeTitle
colors=lines(length(regime));

for field={'Nu','VelocityX','StressZZ','StressXX','Pressure',{'PrincipalContactStressAngle',2}}
  if (iscell(field{1})) 
    num=field{1}{2}; 
    field{1}=field{1}{1};
  else
    num=1;
  end
  newFigure(['Regime' field{1}]);
  for i=1:length(regime)
    plot(regime{i}.zhat,regime{i}.(field{1})(:,num), ...
      '-','DisplayName',regimeTitle{i},'Color',colors(i,:))
  end
  xlabel('$z$'); ylabel(field{1}); legend('show'); set(legend,'Location','NorthEast','Interpreter','none','Box','off');  xlim([0 1])
end

  newFigure(['RegimeMomentumRate']);
  for i=4
    plot(regime{i}.zhat,regime{i}.MomentumRate.Z, ...
      '-','DisplayName',regimeTitle{i},'Color',colors(i,:))
  end
  xlabel('$z$'); ylabel(field{1}); legend('show'); set(legend,'Location','NorthEast','Interpreter','none','Box','off');  xlim([0 1])

return

function NuFit(data)
newFigure([data.plotname 'NuFit']);
try
    s = fitoptions('Method','NonlinearLeastSquares',...
    'Lower',[0 0    -Inf 0   0],...
    'Upper',[1 2*pi  Inf Inf Inf],...
    'Startpoint',[.5 0 0 1 1]);
    f = fittype('nu0+alpha*cos(2*pi*(x-xw)/L)*exp(-(x-xw)/x0)','options',s,'independent','x',...
        'coefficients',{'nu0','xw','alpha','x0','L'});
    ix=data.z>data.Base+1&data.z<data.Surface-1;
    c = fit(data.z(ix),data.Nu(ix),f);
    save('fitNu.mat','c');
    plot(c,data.z(ix),data.Nu(ix))
    %CoeffHstop = coeffvalues(c)';
  %plot(std.z,NuFit, ...
  %    'r-','DisplayName',regimeTitle{i})
  xlabel('$z$','Interpreter','none'); 
  ylabel('$\nu','Interpreter','none'); 
  legend('show'); 
  set(legend,'Location','Best','Interpreter','none'); 
  axis tight
  plot(data.z(ix),data.Nu(ix), 'k-','DisplayName',data.title)
  %xlim([0 1])
  disp('fitted Nu')
catch exception
    warning(exception.identifier,exception.message);
end
return

function diffNuFit(data,dataw)
if iscell(data)
    newFigure([data{1}.plotname 'diffNuFit']);
    for i=1:length(data)
        ix=data{i}.z>data{i}.Base+1&data{i}.z<data{i}.Base+10;
        Nu=(data{i}.Nu-dataw{i}.Nu)./dataw{i}.Nu;
        [c,d]=fit_oscillations(data{i}.z(ix),Nu(ix));
        xw(i)=d(1);
        alpha(i)=d(2);
        x0(i)=d(3);
        L(i)=d(4);
        I(i)=data{i}.BulkInertialNumber;
        R(i)=median(data{i}.Density);
    end
    hold off
    plot(I,[x0/median(x0(I>.2)); alpha/median(alpha(I>.2))],'x','DisplayName',{'x0''' 'alpha'''})
    %plot(I,[x0/median(x0(I>.2)); xw/median(xw(I>.2)); L/median(L(I>.2)); alpha/median(alpha(I>.2))],'x','DisplayName',{'x0''' 'xw''' 'L''' 'alpha'''})
    legend('show')
    set(legend,'Location','North')
    xlabel('$I$','Interpreter','none'); 
    axis tight
    xlim([0 .7])
    %ylim([0 2])
    %set(gca,'YScale','log')
else
    newFigure([data.plotname 'diffNuFit']);
    ix=data.z>data.Base+1&data.z<data.Base+10;
    disp('fit Nu')
    Nu=(data.Nu-dataw.Nu)./dataw.Nu;
    c=fit_oscillations(data.z(ix),Nu(ix))
    save('fitNu.mat','c');
    plot(c,data.z,Nu)
    text(c.xw,feval(c,c.xw)*1.15,{'$\!\!\downarrow z_w$'}, 'VerticalAlignment','bottom', 'HorizontalAlignment','left')
    %CoeffHstop = coeffvalues(c)';
    xlabel('$z$','Interpreter','none'); 
    ylabel('$\tilde\nu$','Interpreter','none'); 
    legend('hide');
    axis tight
    xlim([0 10])
    ylim(max(abs(ylim))*[-1 1])
end
return

function Shear(data,dataw)
colors=lines(length(2));
newFigure([data.plotname 'Shear']);
ix=data.z>data.Base+0&data.z<data.Surface-0;
%plot(data.z(ix),data.VelocityX_dz(ix),'.',Displayname','data')
plot(data.z(ix),dataw.ShearRate(ix),'.','Displayname','data')

% add Bagnold
hold on
%plot(data.z(ix),sqrt(data.Surface-data.z(ix))*mean(data.VelocityX_dz(ix))/mean(sqrt(data.Surface-data.z(ix))),'k--','Displayname','Bagnold profile')
p=mean(dataw.Density(dataw.Bulk))*(-dataw.Gravity(3))*(dataw.Surface-dataw.z(ix));
I=dataw.BulkInertialNumber;
plot(dataw.z(ix),sqrt(p/data.ParticleDensity)*I*sqrt(1.05),'r--','Displayname','Isotropic profile (38)')
plot(dataw.z(ix),sqrt(p/data.ParticleDensity)*I,'b-','Displayname','Anisotropic profile (39)')
p=dataw.Pressure(ix);
%I=dataw.InertialNumber(ix);
%plot(dataw.z(ix),sqrt(p/dataw.ParticleDensity).*I,'r','Displayname','Bagnold profile')

global mu2 I0 beta slip
%maximum mu at I\to\infty
mu2=0.62;
%when the particle spends 50% of the time on rearrangements
I0=0.6;
%spatial extent of stress fluctuations (the bigger teh quicker it decays)
beta=3.3;
slip=0;
h=data.FlowHeight
[z,shearrate] = getShearrate(h,data.ChuteAngle);
v=cumsum(shearrate)*diff(z(1:2));
%plot(z,shearrate*mean(data.VelocityX_dz(ix))/mean(shearrate),'Displayname','fit by Pouliquen');


xlabel('$z$','Interpreter','none'); 
ylabel('$\partial_z u_x$','Interpreter','none'); 
legend('show');
set(legend,'Location','South','Interpreter','none','Box','off')
axis tight
ylim([0 max(ylim)])
return

function muI(data,options)
colors=lines(length(data));
newFigure([data{1}.plotname 'muI'],struct('size',1.3));

if exist('options','var') && isfield(options,'local')
    hold on
    I=cell(1,length(data));
    mu=cell(1,length(data));
    k=cell(1,length(data));
    mustar=cell(1,length(data));
    for i=1:length(data)
      ix=data{i}.Bulk;
      jx=abs(data{i}.zhat-0.5)<0.5;
      I{i}=data{i}.InertialNumber(ix);
      P=data{i}.zhat(ix);
      if isfield(options,'sdstar')
          set(gcf,'FileName',[data{1}.plotname 'sdstar']);
          L=data{i}.(['PrincipalStressLambda']);
          sdstar=sqrt((L(:,1)-L(:,2)).^2+(L(:,2)-L(:,3)).^2+(L(:,3)-L(:,1)).^2)./(sqrt(6)*sum(L,2)/3);
          %sdstar=(L(:,1)-L(:,3))./(2*sum(L,2)/3);
          mustar{i}=sdstar(ix);
          scatter3(I{i},mustar{i},-P,10,P,'filled','Marker','s','Displayname','$s_D^\star$ data');
          %scatter3(data{i}.InertialNumber(jx),sdstar(jx),-data{i}.zhat(jx),1,data{i}.zhat(jx),'Marker','.','Displayname','$s_D^\star$ data');
      elseif isfield(options,'Fsdstar')
          set(gcf,'FileName',[data{1}.plotname 'sdstar']);
          L=data{i}.(['PrincipalFabricLambda']);
          sdstar=sqrt((L(:,1)-L(:,2)).^2+(L(:,2)-L(:,3)).^2+(L(:,3)-L(:,1)).^2)./(sqrt(6)*sum(L,2)/3);
          %sdstar=(L(:,1)-L(:,3))./(2*sum(L,2)/3);
          mustar{i}=sdstar(ix);
          scatter3(I{i},mustar{i},-P,10,P,'filled','Marker','s','Displayname','$s_D^\star$ data');
          %scatter3(data{i}.InertialNumber(jx),sdstar(jx),-data{i}.zhat(jx),1,data{i}.zhat(jx),'Marker','.','Displayname','$s_D^\star$ data');
      else
          mu{i}=-data{i}.ExtendedStressXZ(ix)./data{i}.ExtendedStressZZ(ix);     
          scatter3(I{i},mu{i},-P,10,P,'Marker','x','Displayname','$\mu$ data');
          scatter3(data{i}.InertialNumber(jx),-data{i}.ExtendedStressXZ(jx)./data{i}.ExtendedStressZZ(jx),-data{i}.zhat(jx),1,data{i}.zhat(jx),'Marker','.','Displayname','$s_D^\star$ data');
      end
    end
    axis tight
    xlim([0,.7])
    ylim([.3 .7])
    v=[1 1 1];
    s = fitoptions('Method','NonlinearLeastSquares',...
    'Lower',-Inf*v,...
    'Upper',Inf*v,...
...%    'Startpoint',0*v);
    'Startpoint',[.3 .7 .4]);
    f = fittype('t1+(t2-t1)/(bAinv/I+1)','options',s,'independent','I',...
     'coefficients',{'t1','t2','bAinv'});
    x=cell2mat(I');
    if isfield(options,'sdstar')||isfield(options,'Fsdstar')
        y=cell2mat(mustar');    
    else
        y=cell2mat(mu');
    end
    ix=isfinite(x)&isfinite(y);
    c = fit(x(ix),y(ix),f);
    disp(c)
    h=plot(c,'r');%,'predobj')
    set(h(1),'DisplayName','$\mu$, using Eq. \ref{eq:mufit}')  
    %X=get(h(1),'XData')
    %Y=get(h(1),'YData')
    %set(h(1),'XData',X(1:10:end),'YData',Y(1:10:end))  
    val=coeffvalues(c);
    val=[atand(val(1:2)) val(3)];
    con=confint(c);
    con=[atand(con(:,1:2)) con(:,3)];
    %confidence interval vs. standard deviation: http://www.physics.csbsju.edu/stats/descriptive2.html
    sigma=diff(con)/2/1.96;
    disp(['$\theta_1=' num2str(val(1)) '\pm' num2str(sigma(1),1) ...
      '^\circ$, $\theta_2='   num2str(val(2)) '\pm' num2str(sigma(2),2) ...
      '^\circ$, $I_0=' num2str(val(3)) '\pm' num2str(sigma(3),1) '$'])

    xlabel('$I$','Interpreter','none'); 
    ylabel('','Interpreter','none');  
    colorbar
    xlabel(colorbar,'$\hat{z}$','Interpreter','none')
    if ~isfield(options,'sdstar')
        ylabel('$\mu$')
    else
        ylabel('$\sd$')
    end
    zlabel('$\hat{z}$','Interpreter','none'); 
    legend('hide')
    %set(legend,'Location','NorthWest','Box','off','Interpreter','none')

else
    for i=1:length(data)
      ix=data{i}.Bulk;
      I=data{i}.InertialNumber(ix);
      mu=-data{i}.ExtendedStressXZ(ix)./data{i}.ExtendedStressZZ(ix);
      P=data{i}.ExtendedStressZZ(ix);
      k=scatter3(I,mu,P,3,P, ...
        'Marker','o','MarkerEdgeColor',colors(i,:),'Linewidth',1.5);
      plot(data{i}.BulkInertialNumber,tand(data{i}.ChuteAngle),'o')
    end
    xlabel('$I$','Interpreter','none'); 
    ylabel('$\mu$','Interpreter','none'); 
    axis tight
      caxis([0,1])
      c=colorbar
      xlabel(c,'$\hat{z}$','Interpreter','none')

    I=linspace(min(xlim),max(xlim),20);
    %fit from non-I flow rule
    % t1=tand(17.561); t21=tand(32.257)-t1; bA=2.5*0.191/3.685; 
    % mu=t1+t21./(bA./I+1);
    % plot(I,mu)
    %fit from I flow rule
    t1=tand(17.6); t21=tand(33.3)-t1; bA=1/7.2; 
    Mu=t1+t21./(bA./I+1);
    plot(I,Mu)
end
return

function [fitsdstar, fitmu, fitsd13, fitsd23, fitsdremco] = muIstar(data,options)
colors=lines(length(data));
newFigure([data{1}.plotname 's123'],struct('size',1.3));

if exist('options','var') && isfield(options,'local')
    hold on
    I=cell(1,length(data));
    mu=cell(1,length(data));
    k=cell(1,length(data));
    mustar=cell(1,length(data));
    mu13=cell(1,length(data));
    mu23=cell(1,length(data));
    for i=1:length(data)
      ix=data{i}.Bulk;
      jx=abs(data{i}.zhat-0.5)<0.5;
      I{i}=data{i}.InertialNumber(ix);
      mu{i}=-data{i}.ExtendedStressXZ(ix)./data{i}.ExtendedStressZZ(ix);
      P=data{i}.zhat(ix);
      %k=scatter3(I{i},mu{i},-P,10,P,'Marker','x','Displayname','$\mu$ data');
      
      L=data{i}.(['PrincipalStressLambda']);
      sdstar=sqrt((L(:,1)-L(:,2)).^2+(L(:,2)-L(:,3)).^2+(L(:,3)-L(:,1)).^2)./(sqrt(6)*sum(L,2)/3);
      sd13=(L(:,1)-L(:,3))./(2*sum(L,2)/3);
      sd23=(L(:,2)-L(:,3))./(2*sum(L,2)/3);
      mustar{i}=sdstar(ix);
      mu13{i}=sd13(ix);
      mu23{i}=sd23(ix);
      dphi=data{i}.(['PrincipalStressAngle'])(:,2)-45;
      p=sum(L,2)/3;
      sdremco=(((L(:,1)-p)-(L(:,2)-p)/2).*cosd(2*dphi))./p;
      muremco{i}=sdremco(ix);

      
      %scatter3(I{i},mustar{i},-P,10,P,'filled','Marker','s','Displayname','$s_D^\star$ data');
      %
      scatter3(I{i},mu13{i},-P,10,P,'filled','Marker','s','Displayname','$s_D^{13}$ data');
      scatter3(I{i},mu23{i},-P,10,P,'filled','Marker','s','Displayname','$s_D^{23}$ data');
      scatter3(data{i}.InertialNumber(jx),sd13(jx),-data{i}.zhat(jx),1,data{i}.zhat(jx),'Marker','.','Displayname','$s_D^{13}$ data');
      scatter3(data{i}.InertialNumber(jx),sd23(jx),-data{i}.zhat(jx),1,data{i}.zhat(jx),'Marker','.','Displayname','$s_D^{23}$ data');
      %scatter3(data{i}.InertialNumber(jx),sdremco(jx),-data{i}.zhat(jx),1,data{i}.zhat(jx),'Marker','.','Displayname','$\mu_{Remco}=(\lambda_1-\lambda_2/2)\cos(2\Delta\phi)/p$ data');

    end
    axis tight
    xlim([0,.7])
    ylim([.3 .7])
    v=[1 1 1];
    s = fitoptions('Method','NonlinearLeastSquares',...
    'Lower',-Inf*v,...
    'Upper',Inf*v,...
...%    'Startpoint',0*v);
    'Startpoint',[.3 .7 .4]);
    f = fittype('t1+(t2-t1)/(bAinv/I+1)','options',s,'independent','I',...
     'coefficients',{'t1','t2','bAinv'});
    x=cell2mat(I');
    y=cell2mat(mu');
    ix=isfinite(x)&isfinite(y);
    c = fit(x(ix),y(ix),f);
    fitmu = c;
    disp(c)
    %h=plot(c,'b');%,'predobj')
    %set(h(1),'DisplayName','fit to $\mu=-\sigma_{xz}''/\sigma_{zz}''$')  
    val=coeffvalues(c);
    val=[atand(val(1:2)) val(3)];
    con=confint(c);
    con=[atand(con(:,1:2)) con(:,3)];
    %confidence interval vs. standard deviation: http://www.physics.csbsju.edu/stats/descriptive2.html
    sigma=diff(con)/2/1.96;
    disp(['\mu : $\theta_1=' num2str(val(1)) '\pm' num2str(sigma(1),1) ...
      '^\circ$, $\theta_2='   num2str(val(2)) '\pm' num2str(sigma(2),2) ...
      '^\circ$, $I_0=' num2str(val(3)) '\pm' num2str(sigma(3),1) '$'])
    
    
    y=cell2mat(mustar');
    ix=isfinite(x)&isfinite(y);
    c2 = fit(x(ix),y(ix),f);
    fitsdstar = c2;
    y=cell2mat(mu13');
    c3 = fit(x(ix),y(ix),f);
    fitsd13 = c3;
    y=cell2mat(mu23');
    c4 = fit(x(ix),y(ix),f);
    fitsd23 = c4;
    y=cell2mat(muremco');
    c5 = fit(x(ix),y(ix),f);
    fitsdremco = c5;
    
    disp(c2)
    h=plot(c3,'r');%,'predobj')
    set(h(1),'DisplayName','fit to $s_D^{13}$')%|\V\sigma_D|/p  
    h=plot(c4,'k');%,'predobj')
    set(h(1),'DisplayName','fit to $s_D^{23}$')%|\V\sigma_D|/p  
    %h=plot(c5,'k');%,'predobj')
    %set(h(1),'DisplayName','fit to $\mu_{Remco}$')%|\V\sigma_D|/p  
    val=coeffvalues(c2);
    val=[atand(val(1:2)) val(3)];
    con=confint(c2);
    con=[atand(con(:,1:2)) con(:,3)];
    %confidence interval vs. standard deviation: http://www.physics.csbsju.edu/stats/descriptive2.html
    sigma=diff(con)/2/1.96;
    disp(['s_D*: $\theta_1=' num2str(val(1)) '\pm' num2str(sigma(1),1) ...
      '^\circ$, $\theta_2='   num2str(val(2)) '\pm' num2str(sigma(2),2) ...
      '^\circ$, $I_0=' num2str(val(3)) '\pm' num2str(sigma(3),1) '$'])
    
    xlabel('$I$','Interpreter','none'); 
    ylabel('','Interpreter','none');  
    colorbar
    xlabel(colorbar,'$\hat{z}$','Interpreter','none')
    zlabel('$\hat{z}$','Interpreter','none'); 
    set(legend,'Location','NorthWest','Box','off','Interpreter','none')
    
    if false
        %fit from I flow rule
        mu=cellfun(@(d)tand(d.ChuteAngle),data)
        I=cellfun(@(d)median(d.InertialNumber(isfinite(d.InertialNumber))),data);
        plot(I,mu,'o')
        I=linspace(min(xlim),max(xlim),20);
        t1=tand(17.6); t21=tand(33.3)-t1; bA=1/7.2; 
        Mu=t1+t21./(bA./I+1);
        plot(I,Mu)
    end

else
    for i=1:length(data)
      ix=data{i}.Bulk;
      I=data{i}.InertialNumber(ix);
      mu=-data{i}.ExtendedStressXZ(ix)./data{i}.ExtendedStressZZ(ix);
      P=data{i}.ExtendedStressZZ(ix);
      k=scatter3(I,mu,P,3,P, ...
        'Marker','o','MarkerEdgeColor',colors(i,:),'Linewidth',1.5);
      plot(data{i}.BulkInertialNumber,tand(data{i}.ChuteAngle),'o')
    end
    xlabel('$I$','Interpreter','none'); 
    ylabel('$\mu$','Interpreter','none'); 
    axis tight
      caxis([0,1])
      c=colorbar
      xlabel(c,'$\hat{z}$','Interpreter','none')

    I=linspace(min(xlim),max(xlim),20);
    %fit from non-I flow rule
    % t1=tand(17.561); t21=tand(32.257)-t1; bA=2.5*0.191/3.685; 
    % mu=t1+t21./(bA./I+1);
    % plot(I,mu)
    %fit from I flow rule
    t1=tand(17.6); t21=tand(33.3)-t1; bA=1/7.2; 
    Mu=t1+t21./(bA./I+1);
    plot(I,Mu)
end
return

function [fitsdstar, fitmu, fitsd13, fitsd23, fitsdremco] = muIRemco(data,options)
colors=lines(length(data));
newFigure([data{1}.plotname 'remco'],struct('size',1));

hold on
I=cell(1,length(data));
mu=cell(1,length(data));
k=cell(1,length(data));
muremco=cell(1,length(data));
aremco=cell(1,length(data));
for i=1:length(data)
  ix=data{i}.Bulk;
  jx=abs(data{i}.zhat-0.5)<0.5;
  I{i}=data{i}.InertialNumber(ix);
  L=data{i}.PrincipalStressLambda;
  P=sum(L,2)/3;
  D=L-P*[1 1 1];
  dphi=data{i}.PrincipalContactStressAngle(:,2)-45;
  sdremco=(D(:,1)+D(:,2)/2).*cosd(2*dphi)./P;
  aremco_=D(:,2)./P/2;
  muremco{i}=sdremco(ix);
  aremco{i}=aremco_(ix);

  scatter3(data{i}.InertialNumber(ix),sdremco(ix),-data{i}.zhat(ix),10,data{i}.zhat(ix),'filled','Marker','s','LineWidth',1.5,'Displayname','$\mu_{Remco}=(\lambda_1-\lambda_2/2)\cos(2\Delta\phi)/p$ data');
  scatter3(data{i}.InertialNumber(ix),aremco_(ix),-data{i}.zhat(ix),10,data{i}.zhat(ix),'filled','Marker','s','LineWidth',1.5,'Displayname','$a_{Remco}/p=\lambda_2/p$ data');
  scatter3(data{i}.InertialNumber(jx),sdremco(jx),-data{i}.zhat(jx),1,data{i}.zhat(jx),'Marker','.','Displayname','$\mu_{Remco}=(\lambda_1-\lambda_2/2)\cos(2\Delta\phi)/p$ data');
  scatter3(data{i}.InertialNumber(jx),aremco_(jx),-data{i}.zhat(jx),1,data{i}.zhat(jx),'Marker','.','Displayname','$a_{Remco}/p=\lambda_2/p$ data');
end
axis tight
xlim([0,.7])
v=[1 1 1];
s = fitoptions('Method','NonlinearLeastSquares',...
'Lower',-Inf*v,...
'Upper',Inf*v,...
...%    'Startpoint',0*v);
'Startpoint',[.3 .7 .4]);
f = fittype('t1+(t2-t1)/(bAinv/I+1)','options',s,'independent','I',...
 'coefficients',{'t1','t2','bAinv'});

x=cell2mat(I');
y=cell2mat(muremco');
ix=isfinite(x)&isfinite(y);
c5 = fit(x(ix),y(ix),f);
fitsdremco = c5;
y=cell2mat(aremco');
c6 = fit(x(ix),y(ix),f);
fitsdremco = c6;

disp(c5)
disp(c6)
h=plot(c5,'r');%,'predobj')
set(h(1),'DisplayName','fit to $\mu_{remco}=(\lambda_1+\lambda_2/2)\cos(2\Delta\phi)/p$')%|\V\sigma_D|/p  
h=plot(c6,'k');%,'predobj')
set(h(1),'DisplayName','fit to $a_{remco}/p=(\lambda_2/2)/p$')%|\V\sigma_D|/p  

val=coeffvalues(c5);
val=[atand(val(1:2)) val(3)];
con=confint(c5);
con=[atand(con(:,1:2)) con(:,3)];
%confidence interval vs. standard deviation: http://www.physics.csbsju.edu/stats/descriptive2.html
sigma=diff(con)/2/1.96;
disp(['\mu_{remco}: $\theta_1=' num2str(val(1)) '\pm' num2str(sigma(1),1) ...
  '^\circ$, $\theta_2='   num2str(val(2)) '\pm' num2str(sigma(2),2) ...
  '^\circ$, $I_0=' num2str(val(3)) '\pm' num2str(sigma(3),1) '$'])

val=coeffvalues(c6);
val=[atand(val(1:2)) val(3)];
con=confint(c6);
con=[atand(con(:,1:2)) con(:,3)];
%confidence interval vs. standard deviation: http://www.physics.csbsju.edu/stats/descriptive2.html
sigma=diff(con)/2/1.96;
disp(['a_{remco}/p: $\theta_1=' num2str(val(1)) '\pm' num2str(sigma(1),1) ...
  '^\circ$, $\theta_2='   num2str(val(2)) '\pm' num2str(sigma(2),2) ...
  '^\circ$, $I_0=' num2str(val(3)) '\pm' num2str(sigma(3),1) '$'])

xlabel('$I$','Interpreter','none'); 
ylabel('','Interpreter','none');  
colorbar
xlabel(colorbar,'$\hat{z}$','Interpreter','none')
zlabel('$\hat{z}$','Interpreter','none'); 
set(legend,'Location','West','Box','off','Interpreter','none')
    
return

function IP(data)
if isstruct(data)
  newFigure([data.plotname 'IP']);
  jx=data.Bulk;
  I=data.InertialNumber(jx);
  P=data.ExtendedStressZZ(jx);
  z=data.z(jx);
  k=plot(z,I,'.','Displayname','bulk data');
  ix=data.z>data.Base+0&data.z<data.Surface-0;
  I=data.InertialNumber(ix&~jx);
  P=data.ExtendedStressZZ(ix&~jx);
  z=data.z(ix&~jx);
  k=plot(z,I,'.','Color',.5*[1 1 1],'Displayname','non-bulk data');
  
  %I2=data.InertialNumber(ix).*sqrt(data.StressZZ(ix)./data.Pressure(ix))./sqrt(data.Density(ix)./data.ParticleDensity);
  %k=plot(data.zhat(ix),I2,'x','Color',.8*[0 1 1],'Displayname','I with P data');

  xlabel('$z$','Interpreter','none'); 
  ylabel('$I$','Interpreter','none'); 
  axis tight
  ylim([0 quantile(I,0.98)])
  
  plot(z,data.BulkInertialNumber*ones(size(P)),'--','Displayname','constant fit');
  
  global mu2 I0 beta slip
  %maximum mu at I\to\infty
  mu2=0.62;
  %when the particle spends 50% of the time on rearrangements
  I0=0.6;
  %spatial extent of stress fluctuations (the bigger teh quicker it decays)
  beta=3.3;
  slip=0;
  h=data.FlowHeight
  [z,shearrate] = getShearrate(h,data.ChuteAngle);
  FlowDensity = sum(data.Density.^2,1)./sum(data.Density,1);
  P = -FlowDensity * data.Gravity(3) * (max(z)-z+data.d);
  In=abs(shearrate)*data.d./sqrt(P);
  %plot(z/max(z),In*data.BulkInertialNumber/mean(In),'Displayname','fit by Pouliquen');
  legend('show')
  set(legend,'Location','South','Interpreter','none','Box','off');  

else
  newFigure([data{1}.plotname 'IP']);
  N=max(cellfun(@(d)d.N,data));
  H=round(N/2000);
  colors=lines(H);
  for i=1:length(data)
    if median(data{i}.InertialNumber)<0.01; 
      continue; 
    end
    ix=data{i}.z>data{i}.Base+0&data{i}.z<data{i}.Surface-0;
    I=data{i}.InertialNumber(ix)/data{i}.BulkInertialNumber;
    mu=-data{i}.ExtendedStressXZ(ix)./data{i}.ExtendedStressZZ(ix);
    P=data{i}.ExtendedStressZZ(ix);
    c=colors(round(data{i}.N/2000),:);
    k=scatter(P,I,3, ...
      'Marker','o','MarkerEdgeColor',c,'Linewidth',1.5);
  end
  xlabel('$P$','Interpreter','none'); 
  ylabel('$I/\bar{I}$','Interpreter','none'); 
  axis tight
  ylim([0 2])
end
return

function Chialvo(data)
newFigure([data{1}.plotname 'Chialvo']);
%colormap(hsv)
k=2e5;
d=1;
for i= 1:length(data)
  ix=data{i}.Bulk;
  h=scatter(...
    data{i}.ShearRate(ix)*d/sqrt(k/data{i}.ParticleDensity(1)/d),...
    -data{i}.StressXZ(ix)*d/k,...
    10,data{i}.Nu(ix),'.')
  hold on
end
set(gca,'Xscale','log')
set(gca,'Yscale','log')
xlabel('$\dot\gamma d/\sqrt{k/\rho_pd}$')
ylabel('$pd/k$')
xlim([1e-4 1e-2])
%ylim([1e-5 2e-4])
c=colorbar
xlabel(c,'$\nu$','Interpreter','tex')

newFigure([data{1}.plotname 'Chialvo2']);
%colormap(hsv)
for i= 1:length(data)
  ix=data{i}.Bulk;
  scatter(...
    data{i}.ShearRate(ix)./abs(data{i}.Nu(ix)-0.587).^(4/3)*d/sqrt(k/data{i}.ParticleDensity(1)/d),...
    -data{i}.StressXZ(ix)./abs(data{i}.Nu(ix)-0.587).^(2/3)*d/k,...
    10,data{i}.Nu(ix),'.')
  hold on
end
xlabel('$\dot\gamma* d/\sqrt{k/\rho_pd}$')
ylabel('$p*d/k$')
set(gca,'Xscale','log')
set(gca,'Yscale','log')
xlim([2e-2 3])
%ylim([3e-5 5e-3])
return

function flowrule(data)
newFigure([data{1}.plotname 'flowrule']);
t1=tand(17.561); t2=tand(32.257); b=0.191; g=-0.045; A=3.685; 
for i=1:length(data)
  F=data{i}.Froude;
  h=data{i}.Height;
  t=tand(data{i}.ChuteAngle);
  hs=A*(t2-t)/(t-t1)
  plot(h,F,'x')
end
hhs=linspace(min(xlim),max(xlim),20)
plot(hhs,b*hhs+g,'-')
xlabel('h','Interpreter','none'); 
ylabel('F','Interpreter','none'); 
axis tight

newFigure(['height']);
t1=tand(17.561); t2=tand(32.257); A=3.685; %b=0.191; g=-0.045; 
a=nan(1,length(data)); hs=nan(1,length(data));
for i=1:length(data)
  %F=data{i}.Froude;
  h=data{i}.Height;
  a(i)=data{i}.ChuteAngle;
  hs(i)=A*(t2-tand(a(i)))/(tand(a(i))-t1);
  plot(a(i),h,'x')
end
plot(a,hs,'.-')
xlabel('H/Hs','Interpreter','none'); 
ylabel('F','Interpreter','none'); 
axis tight
return

function devStress(data,dataw)
newFigure([data.plotname 'Shear']);
ix=data.z>data.Base+0&data.z<data.Surface-0;
plot(data.z(ix),data.VelocityX_dz(ix))
plot(data.z(ix),dataw.VelocityX_dz(ix),'r')
ylabel('$\partial_z u_x$','Interpreter','none'); 
ylabel('$shear$','Interpreter','none'); 
legend('show'); 
set(legend,'Location','NorthEast','Interpreter','none','Box','off');  
axis tight
return

function Components(data,tensor,tensorlabel)
if isempty(tensor), tensor='Stress'; tensorlabel='\sigma'; end
newFigure([data.plotname tensor]);
ix=data.z>data.Base+0&data.z<data.Surface-0;
if isfield(data,[tensor 'ZX'])
  h=plot(data.z(ix),[data.([tensor 'XX'])(ix) data.([tensor 'ZZ'])(ix) data.([tensor 'YY'])(ix) -data.([tensor 'XZ'])(ix) -data.([tensor 'ZX'])(ix)],...
    'Displayname',{['$' tensorlabel '_{xx}$'],['$' tensorlabel '_{zz}$'],['$' tensorlabel '_{yy}$'],['$-' tensorlabel '_{xz}$'],['$-' tensorlabel '_{zx}$']})
  set(h(2),'LineStyle','--')
  set(h(4),'LineStyle','--')
else
  h=plot(data.z(ix),[data.([tensor 'XX'])(ix) data.([tensor 'ZZ'])(ix) data.([tensor 'YY'])(ix) -data.([tensor 'XZ'])(ix)],...
    'Displayname',{['$' tensorlabel '_{xx}$'],['$' tensorlabel '_{zz}$'],['$' tensorlabel '_{yy}$'],['$-' tensorlabel '_{xz}$']})
end
xlabel('$z$','Interpreter','none'); 
ylabel(['$' tensorlabel '_{\alpha\beta}$'],'Interpreter','none'); 
legend('show'); 
set(legend,'Location','NorthEast','Interpreter','none','Box','off');  
axis tight
axis([-1 1 -1 1].*max([-1 1 -1 1].*axis,0)); %make sure the origin is included
return

function diffComponents(data,dataw,tensor,tensorlabel)
if isempty(tensor), tensor='Stress'; tensorlabel='\sigma'; end

if iscell(data)
    newFigure([data{1}.plotname 'diff' tensor]);
    for i=1:length(data)
        ix=data{i}.z>data{i}.Base+1&data{i}.z<data{i}.Base+10;
        Comp=(data{i}.([tensor 'XX'])-dataw{i}.([tensor 'XX']))./dataw{i}.([tensor 'XX']);
        [c,d]=fit_oscillations(data{i}.z(ix),Comp(ix));
        xw(i)=d(1);
        alpha(i)=d(2);
        x0(i)=d(3);
        L(i)=d(4);
        I(i)=data{i}.BulkInertialNumber;
        R(i)=median(data{i}.Density);
    end
    hold off
    plot(I,[x0/median(x0(I>.2)); xw/median(xw(I>.2)); L/median(L(I>.2)); alpha/median(alpha(I>.2))],'x','DisplayName',{'x0''' 'xw''' 'L''' 'alpha'''})
    text(c.xw,feval(c,c.xw)*1.15,{'$\!\!\downarrow z_w$'}, 'VerticalAlignment','bottom', 'HorizontalAlignment','left')
    legend('show')
    set(legend,'Location','North')
    xlabel('$I$','Interpreter','none'); 
    axis tight
    xlim([0 .7])
    %ylim([0 2])
    %set(gca,'YScale','log')
else
    newFigure([data.plotname 'diff' tensor]);
    ix=data.z>data.Base+0&data.z<data.Surface-0;
    if isfield(data,[tensor 'ZX'])
      S=[data.([tensor 'XX'])(ix) data.([tensor 'ZZ'])(ix) data.([tensor 'YY'])(ix) -data.([tensor 'XZ'])(ix) -data.([tensor 'ZX'])(ix)];
      Sw=[dataw.([tensor 'XX'])(ix) dataw.([tensor 'ZZ'])(ix) dataw.([tensor 'YY'])(ix) -dataw.([tensor 'XZ'])(ix) -dataw.([tensor 'ZX'])(ix)];
      h=plot(data.z(ix),(S-Sw)./Sw,...
        'Displayname',{['$\tilde' tensorlabel '_{xx}$'],['$\tilde' tensorlabel '_{zz}$'],['$\tilde' tensorlabel '_{yy}$'],['$-\tilde' tensorlabel '_{xz}$'],['$-\tilde' tensorlabel '_{zx}$']})
      set(h(2),'LineStyle','--')
      set(h(4),'LineStyle','--')

    else
      plot(data.z(ix),[data.([tensor 'XX'])(ix) data.([tensor 'YY'])(ix) data.([tensor 'ZZ'])(ix) -data.([tensor 'XZ'])(ix)]...
        -[dataw.([tensor 'XX'])(ix) dataw.([tensor 'YY'])(ix) dataw.([tensor 'ZZ'])(ix) -dataw.([tensor 'XZ'])(ix)],...
        'Displayname',{['$\tilde' tensorlabel '_{xx}$'],['$\tilde' tensorlabel '_{yy}$'],['$\tilde' tensorlabel '_{zz}$'],['$-\tilde' tensorlabel '_{xz}$']})
    end

    %fit component XX
    disp(['fit ' tensor 'XX'])
    ix=data.z>data.Base+1&data.z<data.Base+10;
    Comp=(data.([tensor 'XX'])-dataw.([tensor 'XX']))./dataw.([tensor 'XX']);
    c=fit_oscillations(data.z(ix),Comp(ix));
    text(c.xw,feval(c,c.xw)*1.15,{'$\!\!\downarrow z_w$'}, 'VerticalAlignment','bottom', 'HorizontalAlignment','left')
    xlabel('$z$','Interpreter','none'); 
    ylabel(['$\tilde' tensorlabel '_{\alpha\beta}$'],'Interpreter','none'); 
    legend('show'); 
    set(legend,'Location','NorthEast','Interpreter','none','Box','off');  
    axis tight
end
return

function Bagnold(data)
newFigure(['Bagnold']);
for i=1:length(data)
  if data{i}.ChuteAngle~=24; continue; end
  dz=diff(data{i}.z(1:2));
  z=sqrt(sum(data{i}.MomentumFluxXX*dz)/sum(data{i}.Density*dz));
  plot(data{i}.Surface-data{i}.Base,z,'x');  
end
axis tight
disp(['Slope: ' num2str(diff(log(ylim))./diff(log(xlim)))])
set(gca,'XScale','log')
set(gca,'YScale','log')
xlabel('$h$','Interpreter','none'); 
ylabel('$<v^2>^{1/2}$','Interpreter','none'); 
return

function cbar() %#ok<*DEFNU>
newFigure('colorbar');
c=colorbar('Location','West')
caxis([0,round(max(caxis))])
set(gca,'Visible','off')
xlabel(c,'$\hat{z}$')
return

function [c,d]=fit_oscillations(z,fun)
try
    s = fitoptions('Method','NonlinearLeastSquares',...
    'Lower',[1    0   0   0],...
    'Upper',[2 Inf Inf Inf],...
    'Startpoint',[1.5 1 1 1]);
    %alpha amplitude
    %xw phase
    %L period
    %x0/log(2) distance at which the signal decayed by half
    f = fittype('alpha*cos(2*pi*(x-xw)/L)*exp(-(x-xw)/x0)','options',s,'independent','x',...
        'coefficients',{'xw','alpha','x0','L'});
    c = fit(z(isfinite(fun)),fun(isfinite(fun)),f);
catch exception
    warning(exception.identifier,exception.message);
end
d = coeffvalues(c);
p = 3; %precision
disp(['period $L=' num2str(d(4),p) '$, $1/e$-decay distance $x_0=' num2str(d(3),p) '$, first peak $x_w=' num2str(d(1),p) '$ and amplitude at first peak $\alpha=' num2str(d(2),p) '$.'])
return