%reproduces the figures in Pouliquens paper
function NonLocalStress()
clc
close all
global mu2 I0 beta
mu2=0.62;
I0=0.26;
beta=3.3;

reproduceFigures()

%% reproduce figure 7
%plot these h's
figure(2); clf
hl=linspace(20,20,1);
for hi=1:length(hl); h=hl(hi);
  disp(h);
  [z,shearrate] = getShearrate(h,18.7);
  v=cumsum(shearrate)*diff(z(1:2));
  %plot(z/h,v/v(end),'Color',hi/length(hl)*[0 0 1]);
  plot(z/h,shearrate/shearrate(end),'Color',hi/length(hl)*[0 0 1]);
  hold on
end
xlabel('z/h'); ylabel('u/u(h)'); title('figure 7b for theta=19 in PouliquenForterre2009')
xlabel('z/h'); ylabel('d_zu/d_zu(h)'); title('figure 7b for theta=19 in PouliquenForterre2009')



% figure(7); clf
% 
% thetal=22:4:28;
% for thetai=1:length(thetal); theta=thetal(thetai);
%   hl=linspace(3,25,2);
%   for hi=1:length(hl); h=hl(hi);
%     [z,shearrate] = getShearrate(h,theta);
%     v=cumsum(shearrate)*diff(z(1:2));
%     I=mean(v)./sqrt(cosd(theta)*h)
%     ul(hi)=mean(v)./sqrt(cosd(theta));
%     Fl(hi)=mean(v)./sqrt(cosd(theta)*h);
%   end
%   figure(3);
%   plot(hl,ul,'x-','Color',thetai/length(thetal)*[0 0 1]);
%   xlabel('h'); ylabel('u/\sqrt(gd)','Interpreter','none'); title('figure 6c in PouliquenForterre2009')
%   ylim([0 5])
%   hold on
%   figure(4);
%   plot(hl./hstop(thetastop==theta),Fl,'x-','Color',thetai/length(thetal)*[0 0 1]);
%   xlabel('h/h_{stop}'); ylabel('F=u/\sqrt(gh)','Interpreter','none'); title('figure 6d in PouliquenForterre2009')
%   ylim([0 5])
%   hold on
% end
% 

return

function reproduceFigures()
% this reproduces the plots for granular chute flows based on
% Pouliquen-Forterre's non-local rheology
global mu2 I0 beta

%% reproduce figure 4
figure(1); clf
mu=0:.001:1;
I=I0*(-1 +exp(-(mu2-mu))*sqrt(pi)./sqrt(beta*(mu2-mu)) ...
         -exp(-(mu2+mu))*sqrt(pi)./sqrt(beta*(mu2+mu)) );
plot(I,mu);
xlim([0 1]);
xlabel('I'); ylabel('\tau/P'); title('figure 4 in PouliquenForterre2009')

%% reproduce figure 7
%plot these h's
figure(2); clf
hl=linspace(4,38,5);
for hi=1:length(hl); h=hl(hi);
  [z,shearrate] = getShearrate(h,24);
  v=cumsum(shearrate)*diff(z(1:2));
  plot(v/v(end),z/h,'Color',hi/length(hl)*[0 0 1]);
  hold on
end
ylabel('z/h'); xlabel('u/u(h)'); title('figure 7b in PouliquenForterre2009')

%% reproduce figure 5; get hstop values
figure(7); clf
global thetastop hstop
if isempty(thetastop)
  %thetastop=18.65+12*2.^(-10:0);
  %thetastop=19:28;
  thetastop=22:28;
  hstop=zeros(size(thetastop));
  for thetai=1:length(thetastop); theta=thetastop(thetai);
    hmin=0; hmax=15;
    %get initial hmax
    while hmax<200
      [z,shearrate] = getShearrate(hmax,theta);
      Velocity=cumsum(shearrate)*diff(z(1:2));
      Froude=mean(Velocity)/sqrt(cosd(theta));
      if Froude==0;
        hmin=hmax;
        hmax=hmax*2;
        disp(['setting hmax' num2str(hmax)]);
      else
        break;
      end
    end
    %get hstop using bisection algorithm
    while (hmax/hmin>1.1)
      h=(hmax+hmin)/2;
      [z,shearrate] = getShearrate(h,theta);
      Velocity=cumsum(shearrate)*diff(z(1:2));
      Froude=mean(Velocity)/sqrt(cosd(theta));
      if Froude==0;
        hmin=h;
      else
        hmax=h;
      end
    end
    disp(['h_stop(' num2str(theta) ')=' num2str(h) ])
    hstop(thetai)=(hmax+hmin)/2;
  end
end
plot(thetastop,hstop,'x-');
xlabel('\theta'); ylabel('h'); title('figure 5 in PouliquenForterre2009')
ylim([0 max(ylim)]);
axis tight
hold on


%% reproduce figure 6
figure(3); clf
figure(4); clf
thetal=thetastop;
for thetai=1:length(thetal); theta=thetal(thetai);
  hl=linspace(3,25,12);
  %hl=hstop(theta==thetastop)*[0.8:.4:3];
  ul=zeros(size(hl));
  Fl=zeros(size(hl));
  for hi=1:length(hl); h=hl(hi);
    [z,shearrate] = getShearrate(h,theta);
    v=cumsum(shearrate)*diff(z(1:2));
    ul(hi)=mean(v)./sqrt(cosd(theta));
    Fl(hi)=mean(v)./sqrt(cosd(theta)*h);
  end
  figure(3);
  plot(hl,ul,'x-','Color',thetai/length(thetal)*[0 0 1]);
  xlabel('h'); ylabel('u/\sqrt(gd)','Interpreter','none'); title('figure 6c in PouliquenForterre2009')
  ylim([0 5])
  hold on
  figure(4);
  plot(hl./hstop(thetastop==theta),Fl,'x-','Color',thetai/length(thetal)*[0 0 1]);
  xlabel('h/h_{stop}'); ylabel('F=u/\sqrt(gh)','Interpreter','none'); title('figure 6d in PouliquenForterre2009')
  ylim([0 5])
  hold on
end


order_figures()
return