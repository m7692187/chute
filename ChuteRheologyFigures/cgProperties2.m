% function cgProperties
%evaluates the properties (std dev, volume) of certain cg functions
clc
syms r w real
GaussianCG = exp(-r^2/2)/sqrt(2*pi)^3;
LucyCG = (((-3*(r)+8)*(r)-6)*(r)^2+1)/((16*pi)/105);

dx=.05;
[X,Y,Z]=meshgrid(-4:dx:4);
R=sqrt(X.^2+Y.^2+Z.^2);
G=subs(GaussianCG,r,R);
sum(G(:)*dx^3)

[X,Y,Z]=meshgrid(-1:dx:1);
R=sqrt(X.^2+Y.^2+Z.^2);
L=subs(LucyCG,r,R);
L(R>1)=0;
sum(L(:)*dx^3)


