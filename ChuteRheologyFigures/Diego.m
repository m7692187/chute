load_ChuteRheology
%close all
global muH muHw  lambda

%%
figure(2); clf
set(gcf,'FileName','DiegoHeight')
set(gcf,'Position',[0 0 838 951])
hold all

iList=find(cellfun(@(d)d.ChuteAngle,muH)==28);
title('$\theta=28^\circ, w/d=0.1$')
for i=iList
    d=muH{i};
    ix=d.z<d.FlowHeight&d.z>0.5;
    d.KineticStressXXcorrection = d.ShearRate.^2.*(d.w/2/sqrt(3)).^2.*d.Density;
    d.Temperature = (d.KineticStressXX-d.KineticStressXXcorrection+d.KineticStressYY+d.KineticStressZZ)/3./d.Density;
    plot(d.Nu(ix),d.Pressure(ix)./d.Temperature(ix)/d.ParticleDensity,...
        '.:','Displayname',['$h=' num2str(d.FlowHeight,2) '$'])
end
% for i=iList
%     d=muHw{i};
%     ix=d.z<d.FlowHeight&d.z>0.5;
%     d.KineticStressXXcorrection = d.ShearRate.^2.*(d.w/2/sqrt(3)).^2.*d.Density;
%     d.Temperature = (d.KineticStressXX-d.KineticStressXXcorrection+d.KineticStressYY+d.KineticStressZZ)/3./d.Density;
%     plot(d.Nu(ix),d.Pressure(ix)./d.Temperature(ix)/d.ParticleDensity,...
%         '.:','Displayname',['$h=' num2str(d.FlowHeight,2) '$'])
% end
% 
axis tight
set(gca,'YScale','log')
xlabel('$\nu$')
ylabel('$p/(\rho_p T)$')
legend('show')
set(legend,'Location','NorthWest')

nuc=0.587;
Nu=linspace(0,1,500);
g0CS=(2-Nu)./(2*(1-Nu).^3);
g0J=2./(nuc-Nu);
L=min(1,(nuc-Nu)./(nuc-0.49));
g0B=L.*g0CS+(1-L).*g0J;
e=0.7;
plot(Nu,Nu.*(1+2*(1+e)*Nu.*g0CS),'k','LineWidth',1)
plot(Nu,Nu.*(1+2*(1+e)*Nu.*g0J),'k','LineWidth',1)
plot(Nu,Nu.*(1+2*(1+e)*Nu.*g0B),'k','LineWidth',2)

%%
figure(3); clf
set(gcf,'FileName','DiegoAngle')
set(gcf,'Position',[0 0 838 951])
hold all

iList=find(cellfun(@(d)abs(d.N-6289)<1000 && d.FlowVelocityX>0.1,muHw));
title('$h\approx30, w/d=0.1$')
for i=iList([1 2 4 6 8 9 ])
    d=muH{i};
    ix=d.z<d.FlowHeight&d.z>0.5;
    d.KineticStressXXcorrection = d.ShearRate.^2.*(d.w/2/sqrt(3)).^2.*d.Density;
    d.Temperature = (d.KineticStressXX-d.KineticStressXXcorrection+d.KineticStressYY+d.KineticStressZZ)/3./d.Density;
    plot(d.Nu(ix),d.Pressure(ix)./d.Temperature(ix)/d.ParticleDensity,...
        '.:','Displayname',['$\theta=' num2str(d.ChuteAngle) ', I=' num2str(d.FlowInertialNumber,2) '$'])
end

axis tight
set(gca,'YScale','log')
%ylim([0.1 1000]);
%xlim([0 0.636]);
xlabel('$\nu$')
ylabel('$p/(\rho_p T)$')
legend('show')
set(legend,'Location','NorthWest')
plot(Nu,Nu.*(1+2*(1+e)*Nu.*g0B),'k','LineWidth',2)

%%
% figure(4); clf
% set(gcf,'Position',[0 0 838 951])
% set(gcf,'FileName','Smoothness')
% 
% i=length(muH)
% 
% 
% d=muHw{i};
% ix=d.z<d.FlowHeight&d.z>0;
% d.KineticStressXXcorrection = d.ShearRate.^2.*(d.w/2/sqrt(3)).^2.*d.Density;
% d.Temperature = (d.KineticStressXX-d.KineticStressXXcorrection+d.KineticStressYY+d.KineticStressZZ)/3./d.Density;
% %plot(d.Nu,[d.Temperature d.KineticStressXX./d.Density (d.KineticStressXX-d.KineticStressXXcorrection)./d.Density])
% %plot(d.z(ix),d.ShearRate(ix).^2./d.Temperature(ix))
% plot(d.z,d.Nu)
% 
% hold on
% d=muH{i};
% d.KineticStressXXcorrection = d.ShearRate.^2.*(d.w/2/sqrt(3)).^2.*d.Density;
% d.Temperature = (d.KineticStressXX-d.KineticStressXXcorrection+d.KineticStressYY+d.KineticStressZZ)/3./d.Density;
% %plot(d.Nu,[d.Temperature d.KineticStressXX./d.Density (d.KineticStressXX-d.KineticStressXXcorrection)./d.Density],'--')
% %plot(d.z(ix),d.ShearRate(ix).^2./d.Temperature(ix))
% plot(d.z,d.Nu,'--')
% 
% legend({'$T^{corr}, w/d=1$' '$\sigma_{xx}/\rho, w/d=1$' '$\sigma_{xx}^{corr}/\rho, w/d=1$' '$T^{corr}, w/d=0.1$' '$\sigma_{xx}/\rho, w/d=0.1$' '$\sigma_{xx}^{corr}/\rho, w/d=0.1$'})
% set(legend,'Box','off','FontSize',12)
% 
% %xlim([0 d.FlowHeight])
% %ylim([0 max(d.Temperature)*1.2])
% 
%%
figure(5); clf
lambda=loadstatistics('../Flow/L0M0.5B0.5/H*A20L*.stat');
set(gcf,'Position',[0 0 838 951])
set(gcf,'FileName','DiegoSmoothness')
hold all

title('$h\approx30, \theta=28^\circ, w/d=1/4$')
for i=[1:length(lambda)]
    d=lambda{i};
    ix=d.z<d.FlowHeight&d.z>0.5;
    d.KineticStressXXcorrection = d.ShearRate.^2.*(d.w/2/sqrt(3)).^2.*d.Density;
    d.Temperature = (d.KineticStressXX-d.KineticStressXXcorrection+d.KineticStressYY+d.KineticStressZZ)/3./d.Density;
    L=d.name(max(strfind(d.name,'L'))+1:max(strfind(d.name,'M'))-1);
    plot(d.Nu(ix),d.Pressure(ix)./d.Temperature(ix)/d.ParticleDensity,...
        '.:','Displayname',['$\lambda=' L '$'])
end

axis tight
set(gca,'YScale','log')
%ylim([0.1 1000]);
%xlim([0 0.636]);
xlabel('$\nu$')
ylabel('$p/(\rho_p T)$')
legend('show')
set(legend,'Location','NorthWest')
plot(Nu,Nu.*(1+2*(1+e)*Nu.*g0B),'k','LineWidth',2)
