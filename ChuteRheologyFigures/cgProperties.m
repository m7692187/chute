function cgProperties
%evaluates the properties (std dev, volume) of certain cg functions
clc

syms r w real
GaussianCG = exp(-r^2/2/w^2)/sqrt(2*pi*w^2)^3;
LucyCG = (((-3*(r/w)+8)*(r/w)-6)*(r/w)^2+1)/((16*pi*w^3)/105);
%GaussianCG = exp(-r^2/2/w^2)/sqrt(2*pi*w^2);
%LucyCG = (((-3*(r/w)+8)*(r/w)-6)*(r/w)^2+1)/((4*w)/5);

disp('Gaussian')
volume=cgVolume(GaussianCG)
width=cgWidth(GaussianCG)

disp('Lucy')
volume=cgVolume(LucyCG,'cutoff')
width=cgWidth(LucyCG,'cutoff')%1/sqrt(12)

return

function v=cgVolume(cg,opt)
%evaluates the volume of a cg function
syms r w real
if exist('opt','var'), l=w; else l=inf; end
%vs=int(cg*2,r,0,l);
vs=int(cg*4*pi*r^2,r,0,l);
v=eval(sym(subs(vs,w,rand)));
return

function v=cgWidth(cg,opt)
%evaluates the standard deviation of a cg function
syms r w real
if exist('opt','var'), l=w; else l=inf; end
%vs=sqrt(int(r^2*cg*2,r,0,l));
vs=sqrt(int(r^2*cg*4*pi*r^2/3,r,0,l));
v=subs(vs,w,1);
v=eval(sym(v));
return