function [z,shearrate,N] = getShearrate(h,theta)
err=1;
%at least 3 points per diameter
z=linspace(0,h,max(200,3*h));
shearrate=ones(size(z));
N=0;
while err>1e-4
  [shearrate, err] = getShearrateIter(z,shearrate,theta);
  N=N+1;
  if (N>1000)
    disp('evaluation aborted'); return
  end
end
return

function [g,err] = getShearrateIter(z,shearrate,theta)
global mu2 I0 beta slip

d=1;
gravity = 1;
rhop = 6/pi;
%rho = 0.6 * rhop;
rho= 0.9917; %value for the standard case
P = rho * gravity * cosd(theta) * (max(z)+d-z);
tau = tand(theta) * P;

% %add the next line to create slip
if ~isempty(slip)&&slip~=0
  shearrate(1)=slip*mean(shearrate)*max(z)/diff(z(1:2));
end
% %add to create longer force chains
%beta=3.3-1.9*shearrate./(.26+shearrate);
g=zeros(size(z));
for i=1:length(z)
  g(i)=sum( ...
    shearrate./(1+shearrate(i)*d/I0/sqrt(P(i)/rhop)) .*( ...
    exp(-(mu2*P(i)-tau(i))*(1+beta.*abs(z(i)-z).^2/d^2)./P) - ...
    exp(-(mu2*P(i)+tau(i))*(1+beta.*abs(z(i)-z).^2/d^2)./P) ...
    ) )*diff(z(1:2))/d;
end
%   g(1)=g(1)+sum( ...
%     slip*mean(shearrate)*max(z)/diff(z(1:2))./(1+shearrate(i)*d/I0/sqrt(P(i)/rhop)) .*( ...
%     exp(-(mu2*P(i)-tau(i))*(1+beta.*(z(i)-z).^2/d^2)./P) - ...
%     exp(-(mu2*P(i)+tau(i))*(1+beta.*(z(i)-z).^2/d^2)./P) ...
%     ) )*diff(z(1:2))/d;

shear=abs(sum(g)*diff(z(1:2)))+eps;
if abs(shear)<1e-3; 
  g=zeros(size(g)); 
  disp(['shear set to zero for theta ' num2str(theta) ', h ' num2str(max(z)) ])
  err=0;
else
  err=norm(g(2:end)-shearrate(2:end))/shear;
end
%disp([err shear])
return
