% the first equation, dV/dz=I/d sqrt(p/rho_p), uses the assumption of
a constant inertial number
% the second equation, dp/dz=rho g_z , uses the assumption of a
constant density and p=sigma_zz
% the boundary conditions are no slip (v(0)=0) and no athmospheric
pressure (p(1)=0)
% all constants (h,rho_p,I,rho,g_z,d) are set to 1
sol = dsolve('Dv = sqrt(p)', 'Dp = -1', 'v(0)=0', 'p(1)=0')
z=[0:.01:1];
plot(z,subs(sol.v,z))
xlabel('z')
ylabel('v(z)')
title('Bagnold profile')