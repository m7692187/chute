function solution = coeff_KT(e,nu,nus,c)

g0 = ((2-0.49)/2/(1-0.49)^3)*(nus-0.49)./(nus-nu); % radial distribution function Torquato
Dg0 =  ((2-0.49)/2/(1-0.49)^3)*(nus-0.49)./(nus-nu).^2;

G = nu.*g0;

%--------------------------------------------------------------------------
% Non-dimensional auxiliary coefficients from the kinetic theory of Garzo and Dufty
%--------------------------------------------------------------------------

pstar = (1+2*(1+e)*nu.*g0);
cstar = 32*(1-e)*(1-2*e^2)*(81-17*e+30*e^2*(1-e))^(-1);
% cstar=0;

chi0star = 5/12*g0*(1-e^2)*(1+3/32*cstar);
nuetastar = g0*(1-1/4*(1-e)^2)*(1-1/64*cstar);
gammastar = 128/5/pi*nu.^2.*g0*(1+e)*(1-1/32*cstar);
etakstar = (nuetastar-1/2*chi0star).^(-1).*(1-6/15*(1+e)*(1-3*e)*nu.*g0);
etastar = etakstar.*(1+12/15*nu.*g0*(1+e))+3/5*gammastar;

nukappastar = 1/3*(1+e)*g0*(1+33/16*(1-e)+(19-3*e)/1024*cstar);
kappakappastar = 2/3* (nukappastar-2*chi0star).^(-1).* ( 1+1/2*(1+pstar)*cstar +3/5*nu.*g0*(1+e)^2*(2*e-1+(1/2*(1+e)-5/3/(1+e))*cstar));
kappastar = kappakappastar.*(1+6/5*nu.*g0*(1+e))+(16/5)^2/pi*nu.^2.*g0*(1+e)*(1+7/32*cstar);

% Dn1 = der(ln g0)  derivative of the logarithm of the radial distribution function
Dn1 = 1./g0.*Dg0;
% Dn2 = der(ln p*)  derivative of the logarithm of pstar
Dn2 = 1./pstar*2*(1+e).*(g0+nu.*Dg0);

mukappastar = 2*(2*nukappastar-3*chi0star).^(-1).*((1+nu.*Dn1).*chi0star.*kappakappastar+pstar/3.*(1+nu.*Dn2)*cstar-12/15*nu.*g0.*(1+1/2*nu.*Dn1)*(1+e)*(e*(1-e)+1/4*(4/3+e*(1-e))*cstar));
mustar = mukappastar.*(1+6/5*nu.*g0*(1+e));

chistar = chi0star;
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------

solution.E = 25*pi/192*etastar./(4*nu.*G);
solution.F = (1+e)/2+1./(4*G);
solution.G = G;
solution.M = kappastar*25*pi/8/64./nu./G;
solution.N = mustar;
solution.H = solution.F.*G+nu/2*(1+e).*(g0+nu.*Dg0); % H = der(nu*F*G)
solution.cstar = cstar;

solution.Lstar = 1/2*c*G.^(1/3);
% solution.Lstar =  (2*(1-e)/15*(g0-g0f)+1)^(3/2)*(15*(1-e^2)/2/solution.E)^(-1/2) ;
% solution.Lstar =  (1-wei)*(2*(1-e)/15*(g0-g0f)+1)^(3/2)*(15*(1-e^2)/2/solution.E)^(-1/2) + (wei)*(1/2*c*G^(1/3));

end