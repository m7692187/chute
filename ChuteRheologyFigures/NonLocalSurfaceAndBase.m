function NonLocalSurfaceAndBase()
clc
close all
global mu2 I0 beta
global thetastop hstop

tl=thetastop;
for ti=1:length(tl); theta=tl(ti);
  c=ti/length(tl)*[1 0 1];
  hl=hstop(theta==thetastop)*[0:.02:2];
  for hi=1:length(hl); h=hl(hi);
      z=linspace(0,h,max(200,3*h));
      %[z,shearrate] = getShearrate(h,theta);

      rhop = 6/pi;
      d=1;
      gravity = 1;
      rho = 0.6 * rhop;
      P = rho * gravity * cosd(theta) * (h+d-z);
      tau = tand(theta) * P;

      i=length(z);
      shearrateS = sum( ...
      1 .*( ...
      exp(-(mu2*P(i)-tau(i))*(1+beta.*(z(i)-z).^2/d^2)./P) - ...
      exp(-(mu2*P(i)+tau(i))*(1+beta.*(z(i)-z).^2/d^2)./P) ...
      ) )*diff(z(1:2))/d;

      i=1;
      shearrateB = sum( ...
      1 .*( ...
      exp(-(mu2*P(i)-tau(i))*(1+beta.*(z(i)-z).^2/d^2)./P) - ...
      exp(-(mu2*P(i)+tau(i))*(1+beta.*(z(i)-z).^2/d^2)./P) ...
      ) )*diff(z(1:2))/d;

      plot(h/hstop(theta==thetastop),shearrateS-1,'x','Color',c); 
      %plot(h/hstop(theta==thetastop),shearrateB,'d','Color',c);

      %plot((h)/(hstop(theta==thetastop)),shearrateB*(d/I0/sqrt(P(1)/rhop)),'x','Color',c);
      %plot(h/hstop(theta==thetastop),shearrateS*(d/I0/sqrt(P(end)/rhop)),'d','Color',c);
      
      hold on
      %disp([h shearrateB*(d/I0/sqrt(P(1)/rhop)) shearrateS*(d/I0/sqrt(P(end)/rhop))])
  end
end
plot([1 1],ylim,'k-'); 
order_figures();
return

