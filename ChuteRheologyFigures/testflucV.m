function testflucV

N=2000;
xp = rand(N,1);
vp = (xp-0.5)+0.1*(rand(N,1)-0.5);

x=0:.001:1;
w=.005;
G=@(r,rp) exp(-(r-rp).^2/(2*w)^2)/sqrt(2*pi)/w;

J=0;
m=0;
for i=1:N
    J = J + vp(i) * G(x,xp(i));
    m = m + G(x,xp(i));
end
V=J./m;

for i=1:N
    [~,ix]=min(abs(xp(i)-x));
    Vfluc{i} = vp(i)-V;
    Vfluc2{i} = vp(i)-(xp(i)-0.5);
end

A=0;
B=0;
for i=1:N
    A = A + Vfluc{i}.*Vfluc{i}.*G(x,xp(i));
    B = B + Vfluc2{i}.*Vfluc2{i}.*G(x,xp(i));
end
plot(x,[V;Vfluc{1}])
disp(Vfluc2{1})
disp(xp(1))
disp(vp(1))

plot(x,[A;B])
legend({'G';'S'})
return