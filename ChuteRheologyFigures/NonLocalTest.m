function NonLocalTest()
clc
close all
plot_nonlocal()
plot_real()


return

function plot_real()
global muH muHw mu muw
v=axis()
hold on
for i=1:length(muw)
  data=mu{i};
  plot(data.zhat,smooth(data.VelocityX_dz,20)*2.7,'.','Color',i/length(muw)*[1 0 1])
end
axis([0 1 0 8])
% global muH muHw
% v=axis()
% hold on
% for i=1:length(muHw)
%   data=muH{i};
%   if data.ChuteAngle~=26; continue; end
%   plot(data.zhat,smooth(data.VelocityX_dz,20)*3,'.','Color',i/length(muHw)*[1 0 1])
% end
% axis([0 1 0 3])

return

function plot_nonlocal()
global mu2 I0 beta slip
%maximum mu at I\to\infty
mu2=0.62;
%when the particle spends 50% of the time on rearrangements
I0=0.6;
%spatial extent of stress fluctuations (the bigger teh quicker it decays)
beta=3.3;
slip=0;

%for beta>4.15 mu2=0.62, H=30, theta=20, flow ceases
%beta=0 is a bagnold profile, beta=4.15 is convex
%for beta=3.3, mu2>=0.66, H=30, theta=20, flow ceases
%I0 changes max(shearrate), but theshape shearrate/max(eps,shearrate(end))
%is constant
newfigure('nonlocal',struct('size',800)); clf

al = [18.8:.2:18.9 19:2:30];
%al=26;
for ai=1:length(al); a=al(ai);
hl=[30];
%hl=10:10:40;
for hi=1:length(hl); h=hl(hi);
  disp(['h=' num2str(h)]);
  [z,shearrate] = getShearrate(h,a);
  v=cumsum(shearrate)*diff(z(1:2));
  plot(z/h,shearrate,'Color',(ai+hi-1)/(length(al)+length(hl)-1)*[1 1 0]);
  max(shearrate)
  hold on
end
end
xlabel('z/h'); ylabel('d_zu/d_zu(h)'); 

return
% load_ChuteRheology
% global std mu muSmooth muSmooth lambda mub regime muH
% hold on
% for i=find(cellfun(@(d)d.FlowHeight<15&&d.FixedParticleRadius==0.5,muH))
%   data=muH{i};
%   %axis tight
%   dzu=[0; diff(data.VelocityX(data.zhat<1))];
%   plot(data.zhat(data.zhat<1), dzu/max(dzu),'r')
% end

newfigure('nonlocal',struct('size',800)); clf
for a = 19:2:31
for h = 10:10:40;
  disp(['h=' num2str(h) ', a=' num2str(h)]);
  [z,shearrate] = getShearrate(h,a);
  v=cumsum(shearrate)*diff(z(1:2));
  alpha=sum(v.^2)./(sum(v)^2*diff(z(1:2))/max(z));
  plot(a,alpha,'x');
  hold on
end
end
xlabel('z/h'); ylabel('d_zu/d_zu(h)'); 



return