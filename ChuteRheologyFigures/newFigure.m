function h=newFigure(name,opt)
if ~exist('opt','var')
  opt.size=1;
end
%opens new figure and sets defaults
h=sfigure(); clf; hold on
set(h,'defaulttextinterpreter','none')
ColorOrder=get(gca,'ColorOrder'); ColorOrder(1,3)=0;
title(name)
set(gca,'ColorOrder',ColorOrder);
set(gcf,'Position',opt.size*455*[floor((h-1)/3) mod(h-1,3) 1 0.75])
if exist('name','var'), set(gcf,'FileName',name); end
return
