function load_ChuteRheology(fullReload)
addpath('~/Mercury/Trunk/Matlab/thomas/')

% loads data sets

% determines if you load from stats files instead of the saved mat file
if ~exist('fullReload','var'), fullReload = false; end

load_from_mat(fullReload);

if (false) %do addons
  global std stdw stdwFV %#ok<TLEV>
  std =addons(std,'std');
  stdw=addons(stdw,'stdw',struct('kinfix',true));
  stdwFV =addons(stdwFV,'stdwFV');
end
if (false) %do addons
  global muH muHw %#ok<TLEV>
  muH = addons(muH,'muH');
  muHw = addons(muHw,'muHw');
  for i=1:length(muHw)
   ix=(muHw{i}.InertialNumber~=0);
   regularity=zeros(size(muHw{i}.z));
   regularity(ix)=smooth(abs(muHw{i}.InertialNumber(ix)/muHw{i}.BulkInertialNumber-1));
   muHw{i}.Bulk = abs(muHw{i}.zhat-0.5)<0.5;
   muHw{i}.Bulk(1:find(regularity(1:round(end/2))>0.1,1,'last'))=false;
   muHw{i}.Bulk(round(end/2)-1+find(regularity(round(end/2):end)>0.1,1,'first'):end)=false;
   muH{i}.Bulk=muHw{i}.Bulk;
  end
end


return

function load_from_mat(fullReload)
%loads from mat file, if full reload is not required, data is not already
%loaded, and mat file exists
global std
loadLambdaData

if ~fullReload && isempty(std) && exist('load_ChuteRheology.mat','file')
    %load quickly from mat file
    load('load_ChuteRheology.mat')
    disp('load load_ChuteRheology.mat')
else
    if fullReload; 
        clear global std mu muFlat muSmooth lambda mub regime muH stdw muw muFlatw muSmoothw lambdaw mubw regimew muHw
    end
    loadStandardData
    loadMuHData
    if (false)
      loadMuData
      loadMuSmoothData
      loadMuFlatData
      loadMubData
      loadRegimeData
      loadLambdaData
    end
    if fullReload; 
      saveStandardData
    end
end
return

function saveStandardData
disp('saving load_ChuteRheology.mat')
global std mu muFlat muSmooth lambda mub regime muH stdw muw muFlatw muSmoothw lambdaw mubw regimew muHw %#ok<NUSED>
save('load_ChuteRheology.mat','std','mu','muFlat','muSmooth','lambda','mub','regime','muH','stdw','muw','muFlatw','muSmoothw','lambdaw','mubw','regimew','muHw');
return

function loadStandardData
%loads the standard case
global std stdw stdwFV
if ~isempty(std), disp('std already loaded'); return; else disp('loading std'); end
std =loadstatistics('../newStats/stats/H30A28L1M0.5B0.5.w0.1.stat',struct('timeaverage',true));
%std =loadstatistics('../newStats/stats/H30A28L1M0.5B0.5.longtime.stat',struct('timeaverage',true));
stdw=loadstatistics('../newStats/stats/H30A28L1M0.5B0.5.w2.stat',struct('timeaverage',true));
stdwFV=loadstatistics('../newStats/FlucVelocity/H30A28L1M0.5B0.5.w2T500.stat',struct('timeaverage',true));
std =addons(std,'std');
stdw=addons(stdw,'stdw',struct('kinfix',true));
stdwFV=addons(stdwFV,'stdwFV');
return

function loadMuHData
global muH muHw
if ~isempty(muH), disp('muH already loaded'); return; else disp('loading muH'); end
muH=loadstatistics('../newStats/statsLucy/H*A*L1M0.5B0.5.w0.1.stat',struct('timeaverage',true,'removeFirstSteps',1));
muHw=loadstatistics('../newStats/statsLucy/H*A*L1M0.5B0.5.w2.stat',struct('timeaverage',true,'removeFirstSteps',1));
muH = addons(muH,'muH');
muHw = addons(muHw,'muHw');
for i=1:length(muHw)
   ix=(muHw{i}.InertialNumber~=0);
   regularity=zeros(size(muHw{i}.z));
   regularity(ix)=smooth(abs(muHw{i}.InertialNumber(ix)/muHw{i}.BulkInertialNumber-1));
   muHw{i}.Bulk = abs(muHw{i}.zhat-0.5)<0.5;
   muHw{i}.Bulk(1:find(regularity(1:round(end/2))>0.1,1,'last'))=false;
   muHw{i}.Bulk(round(end/2)-1+find(regularity(round(end/2):end)>0.1,1,'first'):end)=false;
   muH{i}.Bulk=muHw{i}.Bulk;

%    ix=(muH{i}.InertialNumber~=0);
%    regularity=zeros(size(muH{i}.z));
%    regularity(ix)=abs(smooth(muH{i}.InertialNumber(ix),10)/muH{i}.BulkInertialNumber-1);
%    muH{i}.Bulk = abs(muH{i}.zhat-0.5)<0.5;
%    muH{i}.Bulk(1:find(regularity(1:round(end/2))>0.1,1,'last'))=false;
%    muH{i}.Bulk(round(end/2)-1+find(regularity(round(end/2):end)>0.1,1,'first'):end)=false;
end
return

function loadMuData
global mu muw
if ~isempty(mu), disp('mu already loaded'); return; else disp('loading mu'); end
mu=loadstatistics('../newStats/stats*/H30*L1M0.5B0.5.w0.1.stat',struct('timeaverage',true));
muw=loadstatistics('../newStats/stats*/H30*L1M0.5B0.5.w2.stat',struct('timeaverage',true));
mu=mu(2:end);
muw=muw(2:end);
mu = addons(mu,'mu');
muw = addons(muw,'muw');
return

function loadMuSmoothData
global muSmooth muSmoothw
if ~isempty(muSmooth), disp('muSmooth already loaded'); return; else disp('loading muSmooth'); end
muSmooth=loadstatistics('../newStats/stats/H30*L0.5M0.5B0.5.w0.1.stat',struct('timeaverage',true));
muSmoothw=loadstatistics('../newStats/stats/H30*L0.5M0.5B0.5.w2.stat',struct('timeaverage',true));
muSmooth = addons(muSmooth,'muSmooth');
muSmoothw = addons(muSmoothw,'muSmoothw');
return

function loadMuFlatData
global muFlat muFlatw
if ~isempty(muFlat), disp('muFlat already loaded'); return; else disp('loading muFlat'); end
muFlat=loadstatistics('../newStats/stats/H30*L0.5M0.5B0.5.w0.1.stat',struct('timeaverage',true));
muFlatw=loadstatistics('../newStats/stats/H30*L0.5M0.5B0.5.w2.stat',struct('timeaverage',true));
muFlat = addons(muFlat,'muFlat');
muFlatw = addons(muFlatw,'muFlatw');
return

function loadMubData
global mub
if ~isempty(mub), disp('mub already loaded'); return; else disp('loading mub'); end
%load files
mub=loadstatistics('../Flow/L1M0.5B*/H30A26L*.stat');
%add extras
mub = addons(mub);
for i=1:length(mub)
  %0=2^-5, infty=2^15
  mub{i}.axis = max(-15,min(5,log2(mub{i}.Mu(end))));
  mub{i}.title= ['$\log_2\mu_b=' num2str(log2(mub{i}.Mu(end)))];
end
%sort mub
[~,ix]=sort(cellfun(@(d)d.axis,mub)); mub = mub(ix);
%add group properties to first element
mub{1}.label = '$\log_2\mu_b$';
mub{1}.plotname = 'Mub';
return

function loadLambdaData
global lambda
if ~isempty(lambda), disp('lambda already loaded'); return; else disp('loading lambda'); end
%load files
lambda=loadstatistics('../Flow/L*M0.5B0.5/H30A28L*.stat');
%add extras
lambda = addons(lambda);
for i=1:length(lambda)
  lambda{i}.title= ['$\lambda=' num2str(lambda{i}.FixedParticleRadius*2)  '$'];
  lambda{i}.axis = lambda{i}.FixedParticleRadius*2;
  %add group properties to first element
  lambda{i}.label = '$\lambda$';
  lambda{i}.plotname = 'Lambda';
end
%sort lambda
[~,ix]=sort(cellfun(@(d)d.axis,lambda)); lambda = lambda(ix);
return

function loadRegimeData
global regime
if ~isempty(regime), disp('regime already loaded'); return; else disp('loading regime'); end
%load files
regime=loadstatistics({ ...
  '../Flow/L1M0.5B0.5/H30A26L1M0.5B0.5.stat'...
  '../Flow/L0.5M0.5B0.5/H30A22L0.5M0.5B0.5.stat'...
  '../Flow/L0M0.5B0.5/H30A24L0M0.5B0.5.stat'...
  '../Flow/L1M0.5B0.5/H20A20L1M0.5B0.5.stat'...
  });
%add extras
regime = addons(regime);
regimeTitle={'steady','layered','decellerating','static'};
for i=1:length(regime)
  regime{i}.title= regimeTitle{i};
  regime{i}.axis = i;
end
%sort regime
[~,ix]=sort(cellfun(@(d)d.axis,regime)); regime = regime(ix);
%add group properties to first element
regime{1}.label = 'Regime';
regime{1}.plotname = 'Regime';
return


%todo: find out why some data points in stress are missing