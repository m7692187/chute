close all
%clear all
if ~exist('d','var')
    addpath('~/Mercury/Trunk/Matlab/thomas/')
    d=loadstatistics('H*.T50W1.stat')
    addpath('~/Documents/Work/ChuteRheology/ChuteRheologyFigures/')
    do=loadstatistics('~/Documents/Work/ChuteRheology/Flow/L1M0.5B0.5/H30*.stat')
    d3=loadstatistics('*T500.stat',struct('timeaverage',true))
end
li=0.2; %inertial number within 40% of median
for i=1:length(d)
    d{i}=EigenvalueAnalysis2(d{i})
    d{i}.zhat=(d{i}.z-d{i}.Base)/d{i}.FlowHeight;
    d{i}.ShearRate = diff(d{i}.VelocityX)./diff(d{i}.z);
    d{i}.ShearRate(end+1) = d{i}.ShearRate(end);
    d{i}.InertialNumber=d{i}.ShearRate*d{i}.d./sqrt(d{i}.Pressure./d{i}.ParticleDensity(1));
    d{i}.BulkInertialNumber = median(d{i}.InertialNumber(d{i}.Density>0&d{i}.InertialNumber>0));
    d{i}.FlowInertialNumber = 5/2*max(d{i}.VelocityX(d{i}.zhat<=1))*d{i}.d/d{i}.FlowHeight/sqrt(-d{i}.Gravity(3)*d{i}.FlowHeight);
    %below, we smooth the inertial number
    ix=(d{i}.InertialNumber~=0);
    regularity=zeros(size(d{i}.z));
    regularity(ix)=smooth(abs(d{i}.InertialNumber(ix)/d{i}.BulkInertialNumber-1));
    d{i}.Bulk = abs(d{i}.zhat-0.5)<0.3;
    d{i}.Bulk(1:find(regularity(1:round(end/2))>li,1,'last'))=false;
    d{i}.Bulk(round(end/2)-1+find(regularity(round(end/2):end)>li,1,'first'):end)=false;
    ev=d{i}.PrincipalContactStressLambda;
    dev=diff(ev(:,[1 2 3 1]),1,2);
    d{i}.SdStarContact = sqrt(sum(dev.^2,2)/6)./mean(ev,2);
    evF=d{i}.PrincipalFabricLambda;
    devF=diff(evF(:,[1 2 3 1]),1,2);
    d{i}.SdStarFabric = sqrt(sum(devF.^2,2)/6)./mean(evF,2);
end
for i=1:length(do)
    do{i}=EigenvalueAnalysis2(do{i})
    do{i}.zhat=(do{i}.z-do{i}.Base)/do{i}.FlowHeight;
    do{i}.ShearRate = diff(do{i}.VelocityX)./diff(do{i}.z);
    do{i}.ShearRate(end+1) = do{i}.ShearRate(end);
    do{i}.InertialNumber=do{i}.ShearRate*do{i}.d./sqrt(do{i}.Pressure./do{i}.ParticleDensity(1));
    do{i}.BulkInertialNumber = median(do{i}.InertialNumber(do{i}.Density>0&do{i}.InertialNumber>0));
    do{i}.FlowInertialNumber = 5/2*max(do{i}.VelocityX(do{i}.zhat<=1))*do{i}.d/do{i}.FlowHeight/sqrt(-do{i}.Gravity(3)*do{i}.FlowHeight);
    %below, we smooth the inertial number
    ix=(do{i}.InertialNumber~=0);
    regularity=zeros(size(do{i}.z));
    regularity(ix)=smooth(abs(do{i}.InertialNumber(ix)/do{i}.BulkInertialNumber-1));
    do{i}.Bulk = abs(do{i}.zhat-0.5)<0.3;
    do{i}.Bulk(1:find(regularity(1:round(end/2))>li,1,'last'))=false;
    do{i}.Bulk(round(end/2)-1+find(regularity(round(end/2):end)>li,1,'first'):end)=false;
    ev=do{i}.PrincipalContactStressLambda;
    dev=diff(ev(:,[1 2 3 1]),1,2);
    do{i}.SdStarContact = sqrt(sum(dev.^2,2)/6)./mean(ev,2);
    evF=do{i}.PrincipalFabricLambda;
    devF=diff(evF(:,[1 2 3 1]),1,2);
    do{i}.SdStarFabric = sqrt(sum(devF.^2,2)/6)./mean(evF,2);
end
for i=1:length(d3)
    d3{i}.d=1;
    d3{i}=EigenvalueAnalysis2(d3{i})
    d3{i}.zhat=(d3{i}.z-d3{i}.Base)/d3{i}.FlowHeight;
    d3{i}.ShearRate = diff(d3{i}.VelocityX)./diff(d3{i}.z);
    d3{i}.ShearRate(end+1) = d3{i}.ShearRate(end);
    d3{i}.InertialNumber=d3{i}.ShearRate*d3{i}.d./sqrt(d3{i}.Pressure./d3{i}.ParticleDensity(1));
    d3{i}.BulkInertialNumber = median(d3{i}.InertialNumber(d3{i}.Density>0&d3{i}.InertialNumber>0));
    d3{i}.FlowInertialNumber = 5/2*max(d3{i}.VelocityX(d3{i}.zhat<=1))*d3{i}.d/d3{i}.FlowHeight/sqrt(-d3{i}.Gravity(3)*d3{i}.FlowHeight);
    %below, we smooth the inertial number
    ix=(d3{i}.InertialNumber~=0);
    regularity=zeros(size(d3{i}.z));
    regularity(ix)=smooth(abs(d3{i}.InertialNumber(ix)/d3{i}.BulkInertialNumber-1));
    d3{i}.Bulk = abs(d3{i}.zhat-0.5)<0.3;
    d3{i}.Bulk(1:find(regularity(1:round(end/2))>li,1,'last'))=false;
    d3{i}.Bulk(round(end/2)-1+find(regularity(round(end/2):end)>li,1,'first'):end)=false;
    ev=d3{i}.PrincipalContactStressLambda;
    dev=diff(ev(:,[1 2 3 1]),1,2);
    d3{i}.SdStarContact = sqrt(sum(dev.^2,2)/6)./mean(ev,2);
    evF=d3{i}.PrincipalFabricLambda;
    devF=diff(evF(:,[1 2 3 1]),1,2);
    d3{i}.SdStarFabric = sqrt(sum(devF.^2,2)/6)./mean(evF,2);
end
w=cellfun(@(d)d.w,d)
t=cellfun(@(d)d.time(1),d)
a=cellfun(@(d)d.ChuteAngle,d)
h=cellfun(@(d)d.FlowHeight,d)
c='rrggbb';

%% plot sd
figure(1)
% for i=1:length(d)
%     scatter(d{i}.InertialNumber(d{i}.Bulk),d{i}.SdStarContact(d{i}.Bulk),20,'filled')
%     hold all
%     scatter(d{i}.InertialNumber(~d{i}.Bulk),d{i}.SdStarContact(~d{i}.Bulk),1)
% end
for i=1:length(do)
    scatter(do{i}.InertialNumber(do{i}.Bulk),do{i}.SdStarContact(do{i}.Bulk),20,'x','k')
    hold all
end
for i=1:length(d3)
    scatter(d3{i}.InertialNumber(d3{i}.Bulk),d3{i}.SdStarContact(d3{i}.Bulk),20,'MarkerEdgeColor',c(i))
    %hold all
end
axis([0 .4 .35 0.6])
xlabel('$I$')
ylabel('$s_D^\star$')
hold off

%% plot lambda12
figure(2)
% for i=1:length(d)
%     L1=d{i}.PrincipalContactStressLambda(:,1)-mean(d{i}.PrincipalContactStressLambda,2);
%     L2=d{i}.PrincipalContactStressLambda(:,2)-mean(d{i}.PrincipalContactStressLambda,2);
%     d{i}.L12=L2./L1;
%     scatter(d{i}.InertialNumber(d{i}.Bulk),d{i}.L12(d{i}.Bulk),20,'filled')
%     hold all
%     scatter(d{i}.InertialNumber(~d{i}.Bulk),d{i}.L12(~d{i}.Bulk),1)
%     hold all
% end
for i=1:length(do)
    L1=do{i}.PrincipalContactStressLambda(:,1)-do{i}.Pressure;
    L2=do{i}.PrincipalContactStressLambda(:,2)-do{i}.Pressure;
    do{i}.L12=L2./L1;
    scatter(do{i}.InertialNumber(do{i}.Bulk),do{i}.L12(do{i}.Bulk),20,'x','k')
    hold all
end
for i=1:length(d3)
    L1=d3{i}.PrincipalContactStressLambda(:,1)-d3{i}.Pressure;
    L2=d3{i}.PrincipalContactStressLambda(:,2)-d3{i}.Pressure;
    d3{i}.L12=L2./L1;
    scatter(d3{i}.InertialNumber(d3{i}.Bulk),d3{i}.L12(d3{i}.Bulk),20,'MarkerEdgeColor',c(i))
    %scatter(d2{i}.InertialNumber(~d2{i}.Bulk),d2{i}.L12(~d2{i}.Bulk),1)
end
axis([0 .4 -.3 0])
xlabel('$I$')
ylabel('$\Lambda_{12}$')
hold off

%% plot dphi
figure(3)
% for i=1:length(d)
%     %y = 45-data{i}.(['Principal' tensor 'Angle'])(:,2)';
%     d{i}.dPhi=d{i}.PrincipalContactStressAngle(:,2)-45;
%     scatter(d{i}.InertialNumber(d{i}.Bulk),d{i}.dPhi(d{i}.Bulk),20,'filled')
%     hold all
%     scatter(d{i}.InertialNumber(~d{i}.Bulk),d{i}.dPhi(~d{i}.Bulk),1)
%     hold all
% end
for i=1:length(do)
    do{i}.dPhi=do{i}.PrincipalContactStressAngle(:,2)-45;
    scatter(do{i}.InertialNumber(do{i}.Bulk),do{i}.dPhi(do{i}.Bulk),10,'x','k')
    hold all
end
for i=1:length(d3)
    d3{i}.dPhi=d3{i}.PrincipalContactStressAngle(:,2)-45;
    scatter(d3{i}.InertialNumber(d3{i}.Bulk),d3{i}.dPhi(d3{i}.Bulk),10,'MarkerEdgeColor',c(i))
end
axis([0 .4 -2 2])
xlabel('$I$')
ylabel('$\Delta\phi$')
hold off
title('black/red mu=0.5 r=0.88, green mu=0.3 r=0.1, blue mu=0.3 r=0.88')

%% plot sdF
figure(4)
for i=1:length(do)
    scatter(do{i}.InertialNumber(do{i}.Bulk),do{i}.SdStarFabric(do{i}.Bulk),20,'x','k')
    hold all
end
for i=1:length(d3)
    scatter(d3{i}.InertialNumber(d3{i}.Bulk),d3{i}.SdStarFabric(d3{i}.Bulk),20,'MarkerEdgeColor',c(i))
    %hold all
end
axis([0 .4 .2 0.41])
xlabel('$I$')
ylabel('$s_D^{\star,F}$')
hold off

%% plot lambda12F
figure(5)
for i=1:length(do)
    L1=do{i}.PrincipalFabricLambda(:,1)-mean(do{i}.PrincipalFabricLambda,2);
    L2=do{i}.PrincipalFabricLambda(:,2)-mean(do{i}.PrincipalFabricLambda,2);
    do{i}.L12=L2./L1;
    scatter(do{i}.InertialNumber(do{i}.Bulk),do{i}.L12(do{i}.Bulk),20,'x','k')
    hold all
end
for i=1:length(d3)
    L1=d3{i}.PrincipalFabricLambda(:,1)-mean(d3{i}.PrincipalFabricLambda,2);
    L2=d3{i}.PrincipalFabricLambda(:,2)-mean(d3{i}.PrincipalFabricLambda,2);
    d3{i}.L12=L2./L1;
    scatter(d3{i}.InertialNumber(d3{i}.Bulk),d3{i}.L12(d3{i}.Bulk),20,'MarkerEdgeColor',c(i))
    %scatter(d2{i}.InertialNumber(~d2{i}.Bulk),d2{i}.L12(~d2{i}.Bulk),1)
end
axis([0 .4 -.2 0])
xlabel('$I$')
ylabel('$\Lambda_{12}^F$')
hold off

%% plot dphi
figure(6)
for i=1:length(do)
    do{i}.dPhi=do{i}.PrincipalFabricAngle(:,2)-45;
    scatter(do{i}.InertialNumber(do{i}.Bulk),do{i}.dPhi(do{i}.Bulk),10,'x','k')
    hold all
end
for i=1:length(d3)
    d3{i}.dPhi=d3{i}.PrincipalFabricAngle(:,2)-45;
    scatter(d3{i}.InertialNumber(d3{i}.Bulk),d3{i}.dPhi(d3{i}.Bulk),10,'MarkerEdgeColor',c(i))
end
axis([0 .4 -4 4])
xlabel('$I$')
ylabel('$\Delta\phi^F$')
hold off
title('black/red mu=0.5 r=0.88, green mu=0.3 r=0.1, blue mu=0.3 r=0.88')

%%
for i=1:3
    figure(i)
    set(gca,'XScale','log')
end
print_figures()