close all
if ~exist('d','var')
    addpath('~/Mercury/Trunk/Matlab/thomas/')
    d=loadstatistics('H*.T50W1.stat')
    addpath('~/Documents/Work/ChuteRheology/ChuteRheologyFigures/')
    d2=loadstatistics('~/Documents/Work/ChuteRheology/Flow/L1M0.5B0.5/H30*.stat')
end
    li=0.2; %inertial number within 40% of median
    for i=1:length(d)
        d{i}=EigenvalueAnalysis2(d{i})
        d{i}.zhat=(d{i}.z-d{i}.Base)/d{i}.FlowHeight;
        d{i}.ShearRate = diff(d{i}.VelocityX)./diff(d{i}.z);
        d{i}.ShearRate(end+1) = d{i}.ShearRate(end);
        d{i}.InertialNumber=d{i}.ShearRate*d{i}.d./sqrt(d{i}.Pressure./d{i}.ParticleDensity(1));
        d{i}.BulkInertialNumber = median(d{i}.InertialNumber(d{i}.Density>0&d{i}.InertialNumber>0));
        d{i}.FlowInertialNumber = 5/2*max(d{i}.VelocityX(d{i}.zhat<=1))*d{i}.d/d{i}.FlowHeight/sqrt(-d{i}.Gravity(3)*d{i}.FlowHeight);
        %below, we smooth the inertial number
        ix=(d{i}.InertialNumber~=0);
        regularity=zeros(size(d{i}.z));
        regularity(ix)=smooth(abs(d{i}.InertialNumber(ix)/d{i}.BulkInertialNumber-1));
        d{i}.Bulk = abs(d{i}.zhat-0.5)<0.5;
        d{i}.Bulk(1:find(regularity(1:round(end/2))>li,1,'last'))=false;
        d{i}.Bulk(round(end/2)-1+find(regularity(round(end/2):end)>li,1,'first'):end)=false;
        ev=d{i}.PrincipalContactStressLambda;
        dev=diff(ev(:,[1 2 3 1]),1,2);
        d{i}.SdStarContact = sqrt(sum(dev.^2,2)/6)./mean(ev,2);
    end
    for i=1:length(d2)
        d2{i}=EigenvalueAnalysis2(d2{i})
        d2{i}.zhat=(d2{i}.z-d2{i}.Base)/d2{i}.FlowHeight;
        d2{i}.ShearRate = diff(d2{i}.VelocityX)./diff(d2{i}.z);
        d2{i}.ShearRate(end+1) = d2{i}.ShearRate(end);
        d2{i}.InertialNumber=d2{i}.ShearRate*d2{i}.d./sqrt(d2{i}.Pressure./d2{i}.ParticleDensity(1));
        d2{i}.BulkInertialNumber = median(d2{i}.InertialNumber(d2{i}.Density>0&d2{i}.InertialNumber>0));
        d2{i}.FlowInertialNumber = 5/2*max(d2{i}.VelocityX(d2{i}.zhat<=1))*d2{i}.d/d2{i}.FlowHeight/sqrt(-d2{i}.Gravity(3)*d2{i}.FlowHeight);
        %below, we smooth the inertial number
        ix=(d2{i}.InertialNumber~=0);
        regularity=zeros(size(d2{i}.z));
        regularity(ix)=smooth(abs(d2{i}.InertialNumber(ix)/d2{i}.BulkInertialNumber-1));
        d2{i}.Bulk = abs(d2{i}.zhat-0.5)<0.5;
        d2{i}.Bulk(1:find(regularity(1:round(end/2))>li,1,'last'))=false;
        d2{i}.Bulk(round(end/2)-1+find(regularity(round(end/2):end)>li,1,'first'):end)=false;		
        ev=d2{i}.PrincipalContactStressLambda;
        dev=diff(ev(:,[1 2 3 1]),1,2);
        d2{i}.SdStarContact = sqrt(sum(dev.^2,2)/6)./mean(ev,2);
    end

w=cellfun(@(d)d.w,d)
t=cellfun(@(d)d.time(1),d)
a=cellfun(@(d)d.ChuteAngle,d)
h=cellfun(@(d)d.FlowHeight,d)

%% plot sd
figure(1)
for i=1:length(d)
    scatter(d{i}.InertialNumber(d{i}.Bulk),d{i}.SdStarContact(d{i}.Bulk),20,'filled')
    hold all
    scatter(d{i}.InertialNumber(~d{i}.Bulk),d{i}.SdStarContact(~d{i}.Bulk),1)
end
for i=1:length(d2)
    scatter(d2{i}.InertialNumber(d2{i}.Bulk),d2{i}.SdStarContact(d2{i}.Bulk),20,'x','k')
    %scatter(d2{i}.InertialNumber(~d2{i}.Bulk),d2{i}.SdStar(~d2{i}.Bulk),1)
end
axis([0 .5 .15 0.6])
xlabel('$I$')
ylabel('$s_D^\star$')
hold off

%% plot lambda12
figure(2)
for i=1:length(d)
    L1=d{i}.PrincipalContactStressLambda(:,1)-mean(d{i}.PrincipalContactStressLambda,2);
    L2=d{i}.PrincipalContactStressLambda(:,2)-mean(d{i}.PrincipalContactStressLambda,2);
    d{i}.L12=L2./L1;
    scatter(d{i}.InertialNumber(d{i}.Bulk),d{i}.L12(d{i}.Bulk),20,'filled')
    hold all
    scatter(d{i}.InertialNumber(~d{i}.Bulk),d{i}.L12(~d{i}.Bulk),1)
    hold all
end
for i=1:length(d2)
    L1=d2{i}.PrincipalContactStressLambda(:,1)-d2{i}.Pressure;
    L2=d2{i}.PrincipalContactStressLambda(:,2)-d2{i}.Pressure;
    d2{i}.L12=L2./L1;
    scatter(d2{i}.InertialNumber(d2{i}.Bulk),d2{i}.L12(d2{i}.Bulk),20,'x','k')
    %scatter(d2{i}.InertialNumber(~d2{i}.Bulk),d2{i}.L12(~d2{i}.Bulk),1)
end
axis([0 .5 -.4 0])
xlabel('$I$')
ylabel('$\Lambda_{12}$')
hold off

%% plot dphi
figure(3)
for i=1:length(d)
    %y = 45-data{i}.(['Principal' tensor 'Angle'])(:,2)';
    d{i}.dPhi=d{i}.PrincipalContactStressAngle(:,2)-45;
    scatter(d{i}.InertialNumber(d{i}.Bulk),d{i}.dPhi(d{i}.Bulk),20,'filled')
    hold all
    scatter(d{i}.InertialNumber(~d{i}.Bulk),d{i}.dPhi(~d{i}.Bulk),1)
    hold all
end
for i=1:length(d2)
    d2{i}.dPhi=d2{i}.PrincipalContactStressAngle(:,2)-45;
    scatter(d2{i}.InertialNumber(d2{i}.Bulk),d2{i}.dPhi(d2{i}.Bulk),10,'x','k')
end
axis([0 .5 -4 4])
xlabel('$I$')
ylabel('$\Delta\phi$')
hold off

%%
%print_figures()