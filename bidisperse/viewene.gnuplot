plot \
	'../H20A24P1D2.6N0.5.ene' u 1:8 w l, \
	'../H20A24P1D1.8N0.5.ene' u 1:8 w l, \
	'../H20A24P1D1.4N0.5.ene' u 1:8 w l, \
	'../H20A24P1D1.2N0.5.ene' u 1:8 w l, \
	'../H20A24P1D1.1N0.5.ene' u 1:8 w l

pause -1

plot \
	'../H20A24P2.6D1N0.5.ene' u 1:8 w l, \
	'../H20A24P1.8D1N0.5.ene' u 1:8 w l, \
	'../H20A24P1.4D1N0.5.ene' u 1:8 w l, \
	'../H20A24P1.2D1N0.5.ene' u 1:8 w l, \
	'../H20A24P1.1D1N0.5.ene' u 1:8 w l


