#include "SilbertPeriodic.h"
using namespace std;

class FlowRule : public SilbertPeriodic {
public:

	//save only every 100th restart data in a new file
	void save_restart_data() {
		static int counter=0;
		if (!(counter%100)) {
			//save_restart_data_counter--;
			MD::save_restart_data();
		}
		counter++;
	}

	void setname() {
		stringstream name;
		name << "H" << get_InflowHeight() 
		<< "A" << get_ChuteAngleDegrees() 
		<< "P" << get_Polydispersity()
		<< "D" << get_Densityvariation()
		<< "N" << NumberFraction;
		set_name(name.str().c_str());
		set_data_filename();
	}
	
	void run(int argc, char *argv[])
	{
		//Set up a parameter study
		set_savecount(1e4);
		set_tmax(2000);
		set_options_restart(2);
		set_options_data(0);
		set_options_fstat(0);
		set_options_ene(1);

		//1 is the base species
		NumberFraction = 0.5;
		createBaseSpecies();
		set_Polydispersity(1);
		set_Densityvariation(1);
		
		readArguments(argc, argv);
		setname();
	
		//Save the info to disk
		save_info_to_disk();

		//Check if the run has been done before
		if (FileExists(data_filename.str())) {
			//If it has move on to the next run immedently
			cout << "Run " << get_name() << " has already been done " << endl;
		} else {
			//launch this code
			solve();
		}
	}
	
	///Sets variable values for particles that are created at the inflow
	void create_inflow_particle()
	{
		//fraction of (large) species 1
		//double NumberFraction = 1.-1./(1.+VolumeFraction/(1.-VolumeFraction)*cubic(get_Polydispersity()));
		
		P0.indSpecies = random(0,1)<NumberFraction;
		P0.Radius = P0.indSpecies ? get_MaxInflowParticleRadius() : get_MinInflowParticleRadius();
		P0.compute_particle_mass(Species);

		P0.Position.X = random(get_xmin()+2.0*P0.Radius,get_xmax());
		P0.Position.Y = random(get_ymin()+2.0*P0.Radius,get_ymax());
		P0.Position.Z = random(get_zmin()+2.0*P0.Radius,get_InflowHeight());
		P0.Velocity = Vec3D(0.0,0.0,0.0);
	}
	
	//changes radius of (large) species 0
	void set_Polydispersity(double Polydispersity) {
		if (Polydispersity>=1.) {
			//double NumberFraction = 1.-1./(1.+VolumeFraction/(1.-VolumeFraction)*cubic(Polydispersity));
			set_MinInflowParticleRadius(get_FixedParticleRadius()*pow((NumberFraction+cubic(Polydispersity)*(1-NumberFraction)),-1./3.));
			set_MaxInflowParticleRadius(get_MinInflowParticleRadius()*Polydispersity);
			cout 
			<< "r0=" << get_MinInflowParticleRadius()
			<< "r1=" << get_MaxInflowParticleRadius()
			<< "Numberfraction=" << NumberFraction
			<< endl;
	} else {
			cerr<<"Error: polydispersity " << Polydispersity << " needs to be >=1"<<endl; exit(1);
		}
	}

	//changes density of species 0
	void set_Densityvariation(double Densityvariation) {
		if (Densityvariation>0) set_rho(get_rho(1)*Densityvariation,0);
		else {cerr<<"Error: densityvariation needs to be positive"<<endl; exit(-1);}
		cout 
		<< "rho0=" << get_rho(0)
		<< "rho1=" << get_rho(1)
		<< endl;
	}

	double get_Densityvariation(){return get_rho(0)/get_rho(1);}
	
	double get_Polydispersity(){return get_MaxInflowParticleRadius()/get_MinInflowParticleRadius();}

private:

	int readNextArgument(unsigned int& i, unsigned int argc, char *argv[]) {
		if (!strcmp(argv[i],"-polydispersity")) {
			set_Polydispersity(atof(argv[i+1]));
		} else if (!strcmp(argv[i],"-densityvariation")) {
			set_Densityvariation(atof(argv[i+1]));
		} else return SilbertPeriodic::readNextArgument(i, argc, argv); //if argv[i] is not found, check the commands in HGRID_3D
		return true; //returns true if argv[i] is found
	}
		
	double NumberFraction;

};

int main(int argc, char *argv[])
{
	FlowRule problem;
	problem.run(argc, argv);
}
