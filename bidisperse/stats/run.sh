#!/bin/bash
set -x

# create executable
make -C ../../ statistics_while_running &&
cp ../../statistics_while_running.exe . &&
#make -C ../../../Simple_MD centerofmass &&
#cp ../../../Simple_MD/centerofmass.exe . &&

#create restart files in ini
mkdir -p ini
for file in $(ls ../*.restart.0000)
do
	name=${file/.0000/}
	lastfile=`ls $name.* |tail -n1`
	cp $lastfile ${name/../ini}
done

# set parameters
t=5
w=0.5

# evaluate statistics for all restart files in ini/
for file in $(ls ini/*.restart)
do
	name=${file/.restart/};
	~/clusterscriptexecute ./statistics_while_running.exe $name $t \
	-w $w -options_data 1
	~/clusterscriptexecute ./statistics_while_running.exe $name $t \
	-w $w -options_data 1 -indSpecies 0 -o ${name/ini/.}.Species0.stat
	~/clusterscriptexecute ./statistics_while_running.exe $name $t \
	-w $w -options_data 1 -indSpecies 1 -o ${name/ini/.}.Species1.stat
	#./centerofmass.exe ${file/.restart/}
done
