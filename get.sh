#this is the flow simulations
cd ~/DRIVERS/FlowRulePaper/runfinished7
root=$(pwd)

if true; then
t=100
w=0.25
cd Flow 
for S in 0 1 2 3 5 18 19 4 21 22 23 24 25 9 10 11 12 13 16 17 #in order of importance
do
	case $S in
  0) Case=L0M0.5B0.5 ;;
  1) Case=L0.5M0.5B0.5 ;;
  2) Case=L0.67M0.5B0.5 ;;
  3) Case=L0.83M0.5B0.5 ;;
  5) Case=L2M0.5B0.5 ;;
  16) Case=L0.17M0.5B0.5 ;;
  17) Case=L0.33M0.5B0.5 ;;
  18) Case=L1.5M0.5B0.5 ;;
  19) Case=L4M0.5B0.5 ;;
  4) Case=L1M0.5B0.5 ;;
  21) Case=L1M0.5B0.0625 ;;
  22) Case=L1M0.5B0.03125 ;;
  23) Case=L1M0.5B0.015625 ;;
  24) Case=L1M0.5B0.0078125 ;;
  25) Case=L1M0.5B0.000976562 ;;
  9) Case=L1M0.5B0 ;;
  10) Case=L1M0.5B1 ;;
  11) Case=L1M0.5B1e+20 ;;
  12) Case=L1M0.5B0.25 ;;
  13) Case=L1M0.5B0.125 ;;
  *) echo "Case does not exist"; exit 
	esac
	#echo $Case
	mkdir -p $Case
	cd $Case

	for H in 40 30 20 10 
	do
		if [ $H == 40 ]; then
			AList="20 22 24 26 28"
		else 
			AList="21 22 23 24 25 26 27 28 20 30" #30"
		fi
	
		for A in $AList
		do
			#look for restart.0020
			
			#in home directory
			testfile=H${H}A${A}${Case}.restart.0020
			if [ -f $testfile ] && [ ! $(stat -c%s "$testfile") -lt 100000 ]; then
				#if stat file exists
				statfile=${testfile/'.restart.0020'/'.stat'}				
				if [ -f $statfile ]; then
					#remove extra stat files
					if [ $(stat -c%s "$statfile") -lt 100000 ]; then
						rm -f ${testfile/'.restart.0020'/}.stat
					else
						rm -f ${testfile/'.restart.0020'/}.*.stat
					fi
				else
					if [ -f ../../../runfinished6/Flow/$Case/$statfile ] && [ ! $(stat -c%s "../../../runfinished6/Flow/$Case/$statfile") -lt 100000 ]; then
						cp ../../../runfinished6/Flow/$Case/$statfile .
						echo H${H}A${A}${Case}.stat copied from runfinished6
					else
						#if statfile does not yet exist, look in run6
						statfilerun=~/DRIVERS/FlowRulePaper/run6/Flow/$statfile				
						if [ -f $statfilerun ] && [ ! $(stat -c%s "$statfilerun") -lt 100000 ]; then
							cp $statfilerun .
						else
							if [ -f $statfilerun ]; then
								echo H${H}A${A}${Case}.stat 'in process'
							else
								echo H${H}A${A}${Case}.stat missing
								if [ ! -f ${statfilerun/'.stat'/'.restart'} ]; then
									cp $testfile ${statfilerun/'.stat'/'.restart'}
									echo H${H}A${A}${Case}.restart copied
								fi
							fi
							statfile=${testfile/'.restart.0020'/'.T5.stat'}				
							if [ -f $statfile ] && [ ! $(stat -c%s "$statfile") -lt 100000 ]; then
								rm -f ${testfile/'.restart.0020'/}.T05.stat
							fi
						fi
					fi
				fi
				#add restart file if not existent
				if [ ! -f ${testfile/'.restart.0020'/'.restart'} ]; then
					cp $testfile ${testfile/'.restart.0020'/'.restart'}
				fi
				#echo H${H}A${A}${Case} already exists
				continue
			fi

			#in runfinished4
			testfile=~/DRIVERS/FlowRulePaper/runfinished4/Flow/$Case/H${H}A${A}${Case}.restart.0020
			if [ -f $testfile ] && [ ! $(stat -c%s "$testfile") -lt 100000 ]; then
				cp ${testfile/'.restart.0020'/}.* .
				echo H${H}A${A}${Case} copied from runfinished4
				continue
			fi
			
			#in runfinished3
			testfile=~/DRIVERS/FlowRulePaper/runfinished4/Flow/$Case/H${H}A${A}${Case}.restart.0020
			if [ -f $testfile ] && [ ! $(stat -c%s "$testfile") -lt 100000 ]; then
				cp ${testfile/'.restart.0020'/}.* .
				echo H${H}A${A}${Case} copied from runfinished3
				continue
			fi
			
			#in run5
			testfile=~/DRIVERS/FlowRulePaper/run5/$Case/H${H}A${A}${Case}.restart.0020
			if [ -f $testfile ] && [ ! $(stat -c%s "$testfile") -lt 100000 ]; then
				cp ${testfile/'.restart.0020'/}.* .
				echo H${H}A${A}${Case} copied from run5				
				continue
			else
				testfile2=${testfile/'.0020'/'.0000'}
				if [ -f $testfile2 ] && [ ! $(stat -c%s "$testfile2") -lt 1000 ]; then
					echo H${H}A${A}${Case} in progress $(ls ${testfile/'.0020'/}.*|wc -l)/21
					continue
				fi
			fi

			echo H${H}A${A}${Case} does not exist

		done
	done
	cd ..
done
fi

#get remaining Hstop (L4 L1 L0 still missing)
#cd ~/DRIVERS/FlowRulePaper/runfinished7/Hstop/L0.33M0.5B0.5
#cp  ../../../run3/*/H[5,6]?A*L0.33M0.5B0.5.* .
#cd ~/DRIVERS/FlowRulePaper/runfinished7/Hstop/L0.83M0.5B0.5
#cp ../../../run3/hstopS3/H[4,5,6]?A*L0.83M0.5B0.5.* ..
#cd ~/DRIVERS/FlowRulePaper/runfinished7/Hstop/L1.5M0.5B0.5
#cp ../../../run3/hstopS18/H[4,5,6]?A*L1.5M0.5B0.5.* .
#cd ~/DRIVERS/FlowRulePaper/runfinished7/Hstop/L2M0.5B0.5
#cp ../../../run3/hstopS5/H[4,5,6]?A*L2M0.5B0.5.* .
#cd ~/DRIVERS/FlowRulePaper/runfinished7/Hstop/L4M0.5B0.5
#cp ../../../run3/hstopS19/H[3,4,5,6]?A*L4M0.5B0.5.* .
#cd ~/DRIVERS/FlowRulePaper/runfinished7/Hstop/L0M0.5B0.5
#cp  ../../../run3/*New/H[5,6]?A*L0M0.5B0.5.* .
#cd ~/DRIVERS/FlowRulePaper/runfinished7/Hstop/L1M0.5B0.5
#cp  ../../../run3/*New/H[5,6]?A*L1M0.5B0.5.* .
#cd ~/DRIVERS/FlowRulePaper/runfinished7/Hstop/L4M0.5B0.5
#cp  ../../../run3/*New/H[5,6]?A*L4M0.5B0.5.* .

cd ~/DRIVERS/FlowRulePaper/runfinished7/Hstop
for dir in $(ls *B0.5/ -d)
do
	cd $dir
	for restartfile in $(ls *.restart)
	do
		statfile=${restartfile/'.restart'/'.stat'}
		if [ ! -f $statfile ] || [ $(stat -c%s "$statfile") -lt 80000 ]; then
			statfile2=../../../run6/Hstop/$statfile
			if [ -f $statfile2 ] && [ ! $(stat -c%s "$statfile2") -lt 80000 ]; then
				cp $statfile2 .
				echo $statfile2 copied
			else
				statfilerun=~/DRIVERS/FlowRulePaper/run6/Hstop/$statfile				
				if [ -f $statfilerun ]; then
					echo $statfile 'in process'
				else
					echo $statfile missing
					cp $restartfile ../../../run6/Hstop/
				fi
			fi
		fi
	done
	cd ..
done

#clean
#set -x
#cd $root
#mkdir -p obsolete
#mv Hstop/L*M0.5B0.5/H30A24* obsolete
#mv Hstop/L*M0.5B0.5/H46A20* obsolete
#mv Hstop/L0.3*M0.5B0.5/H46A18.5* obsolete
#mv Hstop/L0.[5,6]*M0.5B0.5/H46A19* obsolete
#mv Hstop/L1M0.5B0.5/H4A23* obsolete


