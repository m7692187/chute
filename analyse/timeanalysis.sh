#!/bin/bash
#cp ../../statistics_while_running.exe .

#check if name is given as argument
if [ "$1" == "" ]; then
	echo please enter name
	exit
fi

#store name
name=$1

#locate files
files=$(ls ../Flow/*/$name.restart.*)

#check if files exist
if [ "$files" == "" ]; then
	echo file not found
	exit
fi

set +x

mkdir $name
cp $files $name
cd $name
for file in $(ls $name.restart.*); do
	#dir=$(dirname $file)
	#base=${file/$dir/}
	mv $file ${file/'.restart'/}.restart
done

if true; then
echo statting flow cases
t=2
w=0.25
for file in $(ls $name.*.restart)
do
		base=${file/'.restart'/}
	../statistics_while_running.exe $base 1e-10 -w 0.25 -n 1 -o out -options_data 1
	statfile=${file/'.restart'/'.stat'}
	if false && [ ! -f $statfile ]; then
		echo 'cd '$(pwd)'
		../statistics_while_running.exe '$base' '$t' -w '$w' -o '$statfile'
		' >exe.$base
		chmod +x exe.$base
		~/clusterscriptexecute ./exe.$base
	fi
done	
cd ..
fi
