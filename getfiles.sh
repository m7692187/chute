#!/bin/bash
dir=$(pwd)
mkdir -p Flow
cd ../runfinished4
for dir in $(ls Flow/L1M0.5*/ -d); do
	mkdir -p ../runfinished6/$dir
done

for rfile in $(ls Flow/L1M0.5*/H*.restart.0019)
do
	cp $rfile ../runfinished6/${rfile/'.restart.0019'/'.restart'}
	cp ${rfile/'.restart.0019'/'.restart.0020'} ../runfinished6/${rfile/'.restart.0019'/'.restart'}
	cp ${rfile/'.restart.0019'/'.ene'} ../runfinished6/${rfile/'.restart.0019'/'.ene'}
	
	sfile=${rfile/'.restart.0019'/'.stat'}
	if [ -f $sfile ] && [ ! $(stat -c%s "$sfile") -lt 100000 ]; then
		#echo $sfile found
		cp $sfile ../runfinished6/$sfile					
		continue
	else
		sfile=../runfinished3/${rfile/'.restart.0019'/'.stat'}
		if [ -f $sfile ] && [ ! $(stat -c%s "$sfile") -lt 100000 ]; then
				cp $sfile ../runfinished6/${rfile/'.restart.0019'/'.stat'}		
				#echo $sfile found
				continue
		else
			sfile=${rfile/'.restart.0019'/'.T5.stat'}
			if [ -f $sfile ] && [ ! $(stat -c%s "$sfile") -lt 100000 ]; then
				cp $sfile ../runfinished6/$sfile
				continue
			else
				sfile=../runfinished3/${rfile/'.restart.0019'/'.T5.stat'}
				if [ -f $sfile ] && [ ! $(stat -c%s "$sfile") -lt 100000 ]; then
					cp $sfile ../runfinished6/${rfile/'.restart.0019'/'.T5.stat'}		
					continue
				else
					sfile=${rfile/'.restart.0019'/'.T05.stat'}
					if [ -f $sfile ] && [ ! $(stat -c%s "$sfile") -lt 100000 ]; then
						cp $sfile ../runfinished6/$sfile
						continue
					fi
				fi
			fi
		fi
	fi
	echo !!!! $sfile not found
	ls -full -h ../runfinished[3,4]/${rfile/'.restart.0019'/'.*stat'}
done

#now use the corrections done in runfinished5
cd ../runfinished5; for file in $(ls Flow/*/*.T5.stat); do if [ -f ../runfinished6/$file ]; then cp $file ../runfinished6/$file; fi; done; cd ../runfinished6
