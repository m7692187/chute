#make restart out of restart.0020
for f in $(ls ini/*.restart.0020); do 
	if [ ! -f ${f/'.0020'/} ]; then 
		cp $f ${f/'.0020'/}; 
	fi; 
done

#create link to sc-directory
ln -s ../../run/sc sc

#get restart executable and xballs
#make -C ../../../Chute/ restart 
#cp ../../../Chute/restart.exe .
#cp ../../run/xballs .

#filetype
ft=eps

#
if true; then
for f in $(ls ini/*.restart); do
	base=${f/'ini/'/}
	base=${base/'.restart'/}
	mkdir -p $base
	cd $base
	g=$base; g=${g/'.'/'_'}; g=${g/'.'/'_'}; g=${g/'.'/'_'}; g=${g/'.'/'_'}
	#../restart.exe ../${f/'.restart'/} -tmax 1999.00001 -options_data 1 -options_restart 1; 
	 ./$base.disp -of $g.$ft -die -noborder 4 -v0 -solidf -rottheta .445 -o 200 -h 800 -rgbb 60 -rgbg 60 -rgbr 60 -rgbs 70
	cd ..
done
base=H20A24L0M0.5B0.5
cd  $base
g=$base; g=${g/'.'/'_'}; g=${g/'.'/'_'}; g=${g/'.'/'_'}; g=${g/'.'/'_'}
./$base.disp -of $g.$ft -die -noborder 4 -v0 -solidf -rottheta .445 -o 200 -h 800 -rgbb 60 -rgbg 60 -rgbr 60 -rgbs 70
cd ..
fi

#scp */000001* $CTW:~/svnrepo/docs/ChuteRheology/figures
