clear all
close all
clc
addpath('../matlab')
addpath('~/code/MD/matlab')
addpath('~/code/MD/matlab/thomas')

%% Step 1
% We create a simple steady state by creating a variable x whose 
% accelleration is given by a = k-x, where k is a random variable in the
% range [-.5 .5].
% We solve the system with a time step delta using Euler-Cromer, i.e. the
% random accelleration is applied for time delta, then changed.
% We run the system for N time steps, and repeat the process D times.

%set parameters: time stepping interval delta, number of time steps N,
%number of repetitions D, smoothing length sl (determines correlation)
delta=.02;
N=5e6; 
D=20;
sl=50*delta;

%checks
if (N*D>3e8)
  D=floor(3e8/N);
  warning(['memory overflow expected, decreasing D to' num2str(D)])
end
if (sl<delta)
  delta=sl;
  warning(['smoothing length is less than time step, decreased delta to' num2str(delta)])
end

%create random variable k
k1=rand(D,N)-0.5;
for i=1:D, k(i,:)=smooth(k1(i,:),round(sl/delta)); end

%integrate \ddot{x}=k-x with small damping using Euler-Cromer
x=zeros(D,N);
v=zeros(D,N);
a=zeros(D,N);
x(1)=0;
for i=2:N
  a(:,i)=k(:,i-1)-x(:,i-1);
  v(:,i)=0.999*(v(:,i-1)+delta*a(:,i));
  x(:,i)=x(:,i-1)+delta*v(:,i);
end

%% plot position, velocity and acceleration
newFigure('stochasticODE');
%figure(1); clf
ix=(N-499:N);
plot([x(1,ix);v(1,ix);a(1,ix);k(1,ix)]')
legend({'x';'v';'a';'k';})

%% Step 2
% We calculate the mean rate of change of x (i.e. v) by averaging the
% values of v over time for a given time step dt and a total number of N
% averaging timesteps, mean(v)=sum(v(1:dt:dt*N)) for each case 1:D. 
% Then we take the L2 norm of all D cases, mn=norm(mean(v)).
% This is done in a cumulative way here, i.e. we use cumsum to get these
% values for all N.
% Then we plot this mean as a function of N and for different dt.

newFigure('stochasticODE2');
%figure(2); clf
dtlist=2.^(0:14);
c=lines(length(dtlist));
for i=1:length(dtlist); dt=dtlist(i);
  mn=sum(cumsum(v(:,N/10:dt:end),2).^2,1);
  mn=sqrt(mn)./(1:length(mn));
  loglog(delta*dt*(1:length(mn)),mn,'.','Color',c(i,:));
  hold on
end
set(gca,'XScale','log','YScale','log')
axis tight
xlim([min(xlim)*50 max(xlim)])
ylimits=max(ylim)*[1 10^-diff(log10(xlim))];
plot(xlim,ylimits,'k')
ylimits=max(ylim)*[1 10^(-diff(log10(xlim))/2)];
plot(xlim,ylimits,'k--')
xlabel('$T=N*dt$')%xlabel('T=delta*N*dt')
ylabel('$\partial_t\rho$')
legend(num2str(log2(dtlist(1:7))','$dt=2^{%d}$'),'Location','SouthWest','Orientation','vertical')
set(legend,'Box','off')
title(['smoothing length sl=' num2str(floor(sl/delta)) ' delta'])
%saveas(gcf,'StochasticSteadyState.jpeg')

%% Step 3
% Next, we compare to a random function
newFigure('random');
%newFigure(3); clf
dtlist=2.^(1:14);
c=lines(length(dtlist));
for i=1:length(dtlist); dt=dtlist(i);
  mn=sum(cumsum(k1(:,N/10:dt:end),2).^2,1);
  mn=sqrt(mn)./(1:length(mn));
  loglog((1:length(mn)),mn,'.','Color',c(i,:));
  hold on
end
set(gca,'XScale','log','YScale','log')
axis tight
%xlim([min(xlim)*50 max(xlim)])
ylimits=max(ylim)*[1 10^-diff(log10(xlim))];
plot(xlim,ylimits,'k')
ylimits=max(ylim)*[1 10^(-diff(log10(xlim))/2)];
plot(xlim,ylimits,'k--')
xlabel('$N$')
ylabel('$\partial_t\rho$')
legend(num2str(log2(dtlist(1:7))','$dt=2^{%d}$'),'Location','SouthWest','Orientation','vertical')
set(legend,'Box','off')
saveas(gcf,'StochasticSteadyStateForRandom.jpeg')

%% Step 4
% Next, we plot the autocorrelation function
newFigure('autocorrelation');
set(gcf,'Position',455*[0 0 1 1.5])

%figure(4)
subplot(3,1,1)
autocorr(x(1,:),500)
title('')
ylabel('x')
xlabel('timesteps')
subplot(3,1,2)
autocorr(k1(1,:))
title('')
ylabel('random')
xlabel('timesteps')
subplot(3,1,3)
autocorr(k(1,:),60)
title('')
ylabel('random smooth')
xlabel('timesteps')

%% Step 5
% Next, we plot the fourier function
newFigure('fourier');

%figure(5); clf
Fs = 1/delta;                    % Sampling frequency
T = 1/Fs;                     % Sample time
L = N*delta;                     % Length of signal

% Plot single-sided amplitude spectrum.
y=k1(1,:);
NFFT = 2^nextpow2(L); % Next power of 2 from length of y
Y = fft(y,NFFT)/L;
f = Fs/2*linspace(0,1,NFFT/2+1);
semilogy(f,2*abs(Y(1:NFFT/2+1)),'g') 
hold on

y=k(1,:);
NFFT = 2^nextpow2(L); % Next power of 2 from length of y
Y = fft(y,NFFT)/L;
f = Fs/2*linspace(0,1,NFFT/2+1);
semilogy(f,2*abs(Y(1:NFFT/2+1)),'r') 

y=x(1,:);
NFFT = 2^nextpow2(L); % Next power of 2 from length of y
Y = fft(y,NFFT)/L;
f = Fs/2*linspace(0,1,NFFT/2+1);
semilogy(f,2*abs(Y(1:NFFT/2+1)),'k') 

legend({'random';'random smooth';'x'})
set(legend,'Location','SouthWest');
title('Single-Sided Amplitude Spectrum of y(t)')
xlabel('Frequency (Hz)')
ylabel('$|Y(f)|$')
axis tight
xlim([1e-3 1]*max(xlim))
set(gca,'XScale','log','YScale','log')

%% Print
print_figures_ui()
