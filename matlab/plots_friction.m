% create all plots needed for the flow rule paper
function plots_friction()

close all
clc;
loadData_friction();
plot_flow_rule_Mu_fit_oneplot(1:6);

%paper()
return


function paper() 
close all
MuB=[1:11];

%plot_hstop_diagram(MuB,'errorbarsonly');
%plot_hstop_diagram(MuB([1 4 5 7 9 11]),'errorbarsonly');
%plot_hstop_diagram(MuB(1:11),'errorbarsonly');
for i=[1 9]
  %plot_flow_rule_Hstop_fit(i);
  %plot_simple_hstop_diagram(i);
  %axis(axis+1e-10*[-1 1 -1 1])
end
%plot_hstop_diagram([1,2]);
%axis(axis+1e-10*[-1 1 -1 1])
%plot_beta_mu(MuB);
%plot_A_theta1_theta2(MuB);
%plot_flow_rule_Hstop_fit([1:11]);
plot_silbert_comparison();
%depthprofile_VelocityX()

% 
%plot_flow_rule_Hstop_fit_color_oneplot(1:2:11); %table
% plot_flow_rule_Hstop_fit_color_oneplot(MuB(1:2)); %table
%plot_flow_rule_Hstop_fit(MuB(1));
%plot_flow_rule_Hstop_fit(MuB(2));
% global Data Stop
% j=Stop{MuB(9)}.H==min(Stop{MuB(9)}.H);
% figure;
% d=Data{MuB(9)}.hstop{j};
% plot(d.z,d.StressZZ);
% hold on
% plot([1 1]*d.Base,ylim);
% plot([1 1]*d.Surface,ylim);
% hold off

%plot_hstop_diagram(MuB); %table
%codestatus_flow(11:21)
%plot_flow_rule_Hstop_fit([11 13 21]); %table
%plot_hstop_diagram([11 13]); %table
%plot_flow_rule_Hstop_fit_color_oneplot([11 12:3:21]); %table
% plot_hstop_diagram(11:13); %table
%figure_profiles(MuB([1:2:end-1 end])); %last figure
%figure_flowrule_rough();
%  for i=MuB
%  plot_hstop_diagram(i,'crude');
%  end
% for i=1:8
%   plot_hstop_diagram(i,'crude')
% end
%plot_hstop_diagram(16,'crude')
%plot_hstop_diagram(17,'crude')
% plot_hstop_diagram(16),%,'crude');
% plot_hstop_diagram(17),%,'crude');
% plot_hstop_diagram(Mu),%,'crude');
% set(legend,'Location','NorthWest');
%plot_hstop_diagram(MuB,'crude');
%plot_hstop_diagram(MuB(2),'errorbars');
% plot_hstop_diagram(MuB(3),'errorbars');
% plot_hstop_diagram(MuB(4),'errorbars');
%ylim([0,max(ylim)])
% plot_hstop_diagram(MuB(3));
% plot_hstop_diagram(MuB(4));
%set(legend,'Location','North');

%plot_flow_rule_Hstop_fit_color_oneplot(Mu);
% set(legend,'Location','NorthWest');
%set(legend,'Location','NorthWest');
%H=[30];A=[12 24];
%plot_depthprofile(MuB,H,A,[],'VelocityX')
%plot_depthprofile(Mu(1:end),H,A,[],'StrainProfile')
%plot_depthprofile(MuB,H,A,[],'StrainProfile')

%print_figures();
order_figures(3);
return

function depthprofile_VelocityX
global Title
CaseList=[1 4 5 7 9 11];
H=[30];A=[12 24];
%plot_depthprofile(MuB([1:end]),H,A,[],'VelocityX')
plot_depthprofile(CaseList,H,A,[],'VelocityX')
set(gcf,'Position',[0 680 600 .8*420])
set(gcf,'FileName',['depthprofile_VelocityX' num2str(CaseList,'_%d')])
ylabel('$u/\sqrt{gd}$')
legend(Title{CaseList})
set(legend,'Box','off','Location','SouthEast')
return

function plot_beta_mu(CaseList)
global Data Flow Title
sfigure(); clf; hold on
set(gcf,'Position',[0 680 600 1*420])
set(gcf,'FileName',['beta_mu' num2str(CaseList,'_%d')])

mu=min(2,cellfun(@(d) unique(d.B),Flow(CaseList)));
beta=cellfun(@(d) d.betaPO(2),Flow(CaseList));
gamma=cellfun(@(d) -d.betaPO(1),Flow(CaseList));
betamin=cellfun(@(d)  d.confPO(2,1),Flow(CaseList));
betamax=cellfun(@(d)  d.confPO(2,2),Flow(CaseList));
gammamin=cellfun(@(d) -d.confPO(1,2),Flow(CaseList));
gammamax=cellfun(@(d) -d.confPO(1,1),Flow(CaseList));
%var=cellfun(@(d) d.varPO,Flow(CaseList));
disp(['beta: ' num2str(beta)])
disp(['gamma: ' num2str(gamma)])
hold on
% plot(1:length(mu),betamin,':','DisplayName',{'$\beta_{min}$'},'LineWidth',0.5);
% plot(1:length(mu),betamax,'-.','DisplayName',{'$\beta_{max}$'},'LineWidth',0.5);
% plot(1:length(mu),gammamin,':','Color','g','DisplayName',{'$\gamma_{min}$'},'LineWidth',0.5);
% plot(1:length(mu),gammamax,'-.','DisplayName',{'$\gamma_{max}$'},'LineWidth',0.5);
errorbar(1:length(mu),beta,beta-betamin,betamax-beta,'Color',.8*[1 1 1 ],'DisplayName','$\beta$','LineWidth',0.5)
errorbar(1:length(mu),gamma,gamma-gammamin,gammamax-gamma,'Color',.8*[1 1 1 ],'DisplayName','$\gamma$','LineWidth',0.5)
plot(1:length(mu),[beta; gamma],'x-','DisplayName',{'$\beta$';'$\gamma$'},'LineWidth',0.5);

% plot(1:length(mu),[ gammamin],':','DisplayName',{'$\beta$';'$\gamma$'},'LineWidth',0.5);
% plot(1:length(mu),[ gammamax],'.-','DisplayName',{'$\beta$';'$\gamma$'},'LineWidth',0.5);
c=get(gca,'Children');
set(c(1),'Marker','x'); 
set(c(2),'Marker','o'); 
set(gca,'XTick',1:length(mu))
set(gca,'XTickLabel',{'0';'$2^{-10}$';'$2^{-7}$';'$2^{-6}$';'$2^{-5}$';'$2^{-4}$';'$1/8$';'$1/4$';'$1/2$';'1';'$\infty$'}) 
xlabel('$\mub$')
legend(c(1:2));
set(legend,'Location','South','Box','off','Orientation','horizontal');
ylim([min([beta gamma 0]),max([beta gamma 0])]);
axis tight

return

function plot_A_theta1_theta2(CaseList)
global Data Stop Title CoeffHstop VarHstop

sfigure(); clf; hold on
set(gcf,'Position',[0 680 600 1*420])
set(gcf,'FileName',['A_theta1_theta2' num2str(CaseList,'_%d')])

mu=max(2^-11,min(2,cellfun(@(d) unique(d.B),Stop(CaseList))));
A=cellfun(@(d) d(3),CoeffHstop(CaseList));

global Flow
beta=cellfun(@(d) d.betaPO(2),Flow(CaseList));
disp(['beta/A' num2str(beta./A)])

t1=cellfun(@(d) atand(d(1)),CoeffHstop(CaseList));
t2=cellfun(@(d) atand(d(2)),CoeffHstop(CaseList));
Avar=cellfun(@(d) d(3),VarHstop(CaseList));
t1var=cellfun(@(d) atand(d(1)),VarHstop(CaseList));
t2var=cellfun(@(d) atand(d(2)),VarHstop(CaseList));
disp(['A: ' num2str(A)])
disp(['delta1: ' num2str(t1)])
disp(['delta2: ' num2str(t2)])
errorbar(1:length(mu),A,Avar,'Color',0.6*[1 1 1])
errorbar(1:length(mu),t1,t1-atand(tand(t1)-tand(t1var)),atand(tand(t1)+tand(t1var))-t1,'Color',0.6*[1 1 1])
errorbar(1:length(mu),t2,t2-atand(tand(t2)-tand(t2var)),atand(tand(t2)+tand(t2var))-t2,'Color',0.6*[1 1 1])
plot(1:length(mu),[A; t1; t2;],'x-','DisplayName',{'$A$';'$\delta_1$';'$\delta_2$';},'LineWidth',0.5);
c=get(gca,'Children');
set(c(1),'Marker','x'); 
set(c(2),'Marker','o'); 
set(c(3),'Marker','d'); 
xlabel('$\mub$')
legend(c(1:3));
set(legend,'Location','North','Box','off','Orientation','horizontal');
ylim([min([t2 t1 A 0]),max([t2 t1 A 40])]);
set(gca,'XTick',mu)
set(gca,'XTick',1:length(mu))
set(gca,'XTickLabel',{'0';'$2^{-10}$';'$2^{-7}$';'$2^{-6}$';'$2^{-5}$';'$2^{-4}$';'$1/8$';'$1/4$';'$1/2$';'1';'$\infty$'}) 
xlim([1-eps,length(mu)+eps])

return

function order_figures(nx,ny,figlist)
if ~exist('nx','var'), nx=3; end
if ~exist('ny','var'), ny=2; end
if ~exist('figlist','var'), figlist=get(0,'Children'); end

Init=[0 0];%[1289 -293];
Corner=Init;
count=0;
for i=figlist(end:-1:1)'
  Axes=get(i,'CurrentAxes');
  %title(Axes(1),[get(i,'FileName')],'Interpreter','none')
  P=get(i,'Position');
  set(i,'Position',[Corner, P(3:4)]);
  count=count+1;
  if mod(count,nx)
    Corner(1)=Corner(1)+P(3); %move right
  else
    Corner(1)=Init(1);
    Corner(2)=Corner(2)+P(4)+100; %move up
  end
end
return

function figure_profiles(CaseList)
%new figure
sfigure(); clf; hold on
set(gcf,'Position',[0 680 560 .7*420])
set(gcf,'FileName',['VelocityX' num2str(CaseList,'_%d')])
colors=mylines(max(CaseList));

hold on;
plot_depthprofile2(CaseList,30,24,'VelocityX')
set(legend,'Location','SouthEast','FontSize',8)

sfigure(); clf; hold on
set(gcf,'Position',[0 680 560 .7*420])
set(gcf,'FileName',['Stress' num2str(CaseList,'_%d')])
colors=mylines(max(CaseList));

hold on;
%set(legend,'Location','FontSize',8)
plot_depthprofile2(CaseList(2),30,24,'Stress')
set(legend,'Location','NorthEast')

return

function figure_profiles2(CaseList)
%new figure
sfigure(); clf; hold on
set(gcf,'Position',[0 680 2*560 .6*420]/0.7)
set(gcf,'FileName',['profiles' num2str(CaseList,'_%d')])
colors=mylines(max(CaseList));

subplot(1,3,1); hold on;
plot_depthprofile2(CaseList,30,24,'Density')
set(legend,'Location','SouthWest')
subplot(1,3,2); hold on;
set(legend,'Location','FontSize',8)
plot_depthprofile2(CaseList,30,24,'VelocityX')
subplot(1,3,3); hold on;
set(legend,'Location','FontSize',8)
plot_depthprofile2(CaseList(2),30,24,'Stress')
set(legend,'Location','NorthEast')

return


function plot_depthprofile2(CaseList,HList,AList,Var)
global Data Flow Title
%set(gcf,'Position',[0  680 560 420])
%set(gcf,'FileName',['depthprofile_Nu' num2str(Case,'_%d') '_H' num2str(H,'_%d')])

for Case=CaseList
if isempty(HList), HList = unique(Flow{Case}.H)'; end
if isempty(AList), AList = unique(Flow{Case}.A)'; end
for H=HList
for A=AList
k=find(Flow{Case}.A==A&Flow{Case}.H==H);
if ~isempty(k)
  c=get(gca,'Children');
  colors=mylines(length(c)+1);
  color=colors(end,:);
  if strcmp(Var,'VelocityX')
    h=plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.VelocityX);
    xlabel('$(z-b)/h$','Interpreter','none')
    ylabel('$v_x$','Interpreter','none')
    axis tight; v=axis; axis([0 1 v(3:4)]);
  elseif strcmp(Var,'Alpha')
    data=Data{Case}.flow{k};
    meanVelocityX=  mean(data.VelocityX(data.z>data.Base&data.z<data.Surface));
    meanVelocityX2= mean(data.VelocityX(data.z>data.Base&data.z<data.Surface).^2);
    Alpha=meanVelocityX2./(meanVelocityX.^2);
    h=plot(Flow{Case}.A(k),Alpha,'o');
    xlabel('$(z-b)/h$','Interpreter','none')
    ylabel('$v_x$','Interpreter','none')
    axis tight; %v=axis; axis([0 1 v(3:4)]);
  elseif strcmp(Var,'Momentum')
    h=plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.MomentumZ,Data{Case}.flow{k}.VelocityX./meanVelocityX);
    xlabel('$(z-b)/h$','Interpreter','none')
    ylabel('$v_x$','Interpreter','none')
    axis tight; v=axis; axis([0 1 v(3:4)]);
  elseif strcmp(Var,'VelocityProfile')
    meanVelocityX = mean(Data{Case}.flow{k}.VelocityX(Data{Case}.flow{k}.z>0&Data{Case}.flow{k}.z<Data{Case}.flow{k}.FlowHeight));
    h=plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.VelocityX./meanVelocityX);
    xlabel('$(z-b)/h$','Interpreter','none')
    ylabel('$v_x$','Interpreter','none')
    axis tight; v=axis; axis([0 1 v(3:4)]);
  elseif strcmp(Var,'StrainProfile')
    meanVelocityX = max(Data{Case}.flow{k}.VelocityX(Data{Case}.flow{k}.z>0&Data{Case}.flow{k}.z<Data{Case}.flow{k}.FlowHeight));
    h=plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,...
       deriv(Data{Case}.flow{k}.VelocityX./meanVelocityX,Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight));
%      1-Data{Case}.flow{k}.StressXZ./min(Data{Case}.flow{k}.StressXZ));
    xlabel('$1-\hat\sigma_{xz}$','Interpreter','none')
    ylabel('$\hat{\partial_zv_x}$','Interpreter','none')
    axis tight; v=axis; axis([0 1 0 2.5]);
  elseif strcmp(Var,'logVelocityProfile')
    meanVelocityX = mean(Data{Case}.flow{k}.VelocityX(Data{Case}.flow{k}.z>0&Data{Case}.flow{k}.z<Data{Case}.flow{k}.FlowHeight));
    h=plot(1-Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,max(Data{Case}.flow{k}.VelocityX./meanVelocityX)-Data{Case}.flow{k}.VelocityX./meanVelocityX);
    xlabel('$1-(z-b)/h$','Interpreter','none')
    ylabel('$|\vec{u}|$','Interpreter','none')
    axis tight; v=axis; axis([0 1 v(3:4)]);
  elseif strcmp(Var,'StressZZ')
    h=plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.StressZZ,...
      '-','Color',color);
    xlabel('$(z-b)/h$','Interpreter','none')
    ylabel('$\sigma_{zz}$','Interpreter','none')
    axis tight; v=axis; axis([-0.1 1 v(3:4)]);
  elseif strcmp(Var,'Stress')
    h=plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,...
      [Data{Case}.flow{k}.StressZZ -Data{Case}.flow{k}.StressXZ Data{Case}.flow{k}.StressXX Data{Case}.flow{k}.StressYY ...
      ],'-','DisplayName',{'$\sigma_{zz}$','$-\sigma_{xz}$','$\sigma_{xx}$','$\sigma_{yy}$','$-\int_z^\infty \rho g_z dz$','$\int_z^\infty \rho g_x dz$'});
      %[Data{Case}.flow{k}.StressXX Data{Case}.flow{k}.StressYY Data{Case}.flow{k}.StressZZ -Data{Case}.flow{k}.StressXZ ...
      %...%-diff(Data{Case}.flow{k}.z(1:2))*(sum(Data{Case}.flow{k}.Density)-cumsum(Data{Case}.flow{k}.Density))*Data{Case}.flow{k}.Gravity(3) ...
      %...%diff(Data{Case}.flow{k}.z(1:2))*(sum(Data{Case}.flow{k}.Density)-cumsum(Data{Case}.flow{k}.Density))*Data{Case}.flow{k}.Gravity(1)...
      %],'-','DisplayName',{'$\sigma_{xx}$','$\sigma_{yy}$','$\sigma_{zz}$','$-\sigma_{xz}$','$-\int_z^\infty \rho g_z dz$','$\int_z^\infty \rho g_x dz$'});
    set(h(3),'LineStyle','--')
    set(h(4),'LineStyle','-.')
    xlabel('$(z-b)/h$','Interpreter','none')
    ylabel('$\sigma_{zz}$','Interpreter','none')
    axis tight; v=axis; axis([-0.0 1 v(3:4)]);
  elseif strcmp(Var,'Ene')
    MeanEneEla=mean(Data{Case}.flow{k}.EneShort.Ela(2:end-1));
    %MeanEneEla=1;%mean(Data{Case}.flow{k}.EneShort.Kin(end-1));
    %h=plot(Data{Case}.flow{k}.EneShort.Time(2:end-1),Data{Case}.flow{k}.EneShort.Kin(2:end-1)./MeanEneEla);
    h=plot(Data{Case}.flow{k}.Ene.Time(2:end-1),Data{Case}.flow{k}.Ene.Kin(2:end-1)./MeanEneEla);
    %axis tight; v=axis; axis([v(1:3) min(v(4),2*v(3))]);
    xlabel('$t$','Interpreter','none')
    ylabel('$E_{kin}/\langle E_{ela}\rangle$','Interpreter','none')
    axis tight;
  else
    h=plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.Nu);
    xlabel('$(z-b)/h$','Interpreter','none')
    ylabel('$\rho/\rho_p$','Interpreter','none')
    axis tight; v=axis; axis([0 1 0.5 v(4)]);
  end
  if ~strcmp(Var,'Stress')
    set(h,'Color',color,...
      'DisplayName',[Title{Case}(1:end-1) ...%' , \theta=' num2str(Flow{Case}.A(k)) '^\circ'
      ...%', H=' num2str(Flow{Case}.H(k),'%.0f') 
      '$']);
  end
else
  disp(['A= ' num2str(A) ',H= ' num2str(H,1) ' not found']); 
end
end
end
end

if strcmp(Var,'VelocityProfile')
  z=0:.05:1;
  u=(5/3)*(1-(1-z).^1.5);
  plot(z,u,'--k','Linewidth',2,'DisplayName','Bagnold');
% elseif strcmp(Var,'VelocityX')
%   z=0:.05:1;
%   u=.93*max(ylim)*(1-(1-z).^1.5);
%   plot(z,u,'--k','Linewidth',2,'DisplayName','Bagnold');
elseif strcmp(Var,'StrainProfile')
  z=0:.001:1;
  u=(1-(1-z).^1.5);
  plot(z,deriv(u,z),'--k','Linewidth',2,'DisplayName','Bagnold');
elseif strcmp(Var,'logVelocityProfile')
  z=0:.05:1;
  u=(5/3)*z.^1.5+(rand(size(z))-.7)*.5;
    plot(z,max(u)-u,'-g','Linewidth',1,'DisplayName','Bagnold');

  z=0:.05:1;
  u=(5/3)*z.^1.5;
  plot(z,u,'--k','Linewidth',2,'DisplayName','Bagnold');
  set(gca,'YScale','log')
  set(gca,'XScale','log')
  v=axis()
  xlim([1e-2 v(2)])
  ylim([1e-2 v(4)])
end
C=get(gca,'Children');
legend(C(end:-1:1),'Location','Best')

set(legend,'Box','off')
return


function figure_enekin() 
plot_arresting_H20();
% plot_arresting(5,20);
for child=get(gca,'Children')'
  Angle=str2double(get(child,'Tag'));
  if Angle>29.5
    set(child,'LineStyle','--');
  elseif Angle<21
    set(child,'LineWidth',2);
  end
end
xlim([0,1600])
% plot_accelerating(5,20);
return

function figure_flowrule_rough() 
%plot_ha_bottom_friction(5);
for Case=5
plot_flow_rule_Hstop_fit(Case,'jenkins');
plot_flow_rule_Hstop_fit(Case);
end
return

function figure_flowrule() 
%plot_hstop_diagram(4);
plot_hstop_diagram(2:6,'hstoponly');
%plot_hstop_diagram(2);
plot_flow_rule_Hstop_fit(1);
plot_flow_rule_Hstop_fit(2);
plot_flow_rule_Hstop_fit_color_oneplot(1:6);
% %  plot_ha_alpha(3);
% plot_hstop_diagram(2)
return

function figure_depthprofiles() 
plot_depthprofile_VelocityX_H(5,30);
ylim([0,2.2])
plot_depthprofile_VelocityX_HA(1:6,30,24);
%plot_depthprofile(2,30,[21 22 23 24 25 26 27 28 ],[],'VelocityX')
%plot_depthprofile(5,30,[21 22 23 24 25 26 27 28 ],[],'VelocityX')
%plot_depthprofile_HA(1:6,30,24);
%plot_depthprofile_StressZZ_HA(1:6,30,24);
return

function figure_hstop() 
plot_silbert_comparison();

% plot_depthprofile(2,20,24,[],'Ene');
% %title('$H=20,\theta=24^\circ$')
% set(gca,'FontSize',20)
% set(get(gca,'XLabel'),'FontSize',20)
% set(get(gca,'YLabel'),'FontSize',20)
% set(get(gca,'Title'),'FontSize',20)
% legend('off')
% %legend({'H=10','H=20','H=30'},'FontSize',10)
% xlim([0,2000])
% set(gcf,'Position',[0  680 560 420])
% set(gcf,'FileName','oscillating_ene')
% 
% plot_depthprofile(2,20,22,[],'Nu');
% %title('$H=20,\theta=22^\circ$')
% set(gca,'FontSize',20)
% set(get(gca,'XLabel'),'FontSize',20)
% set(get(gca,'YLabel'),'FontSize',20)
% set(get(gca,'Title'),'FontSize',20)
% legend('off')
% xlim([-.1 1.1])
% set(gcf,'Position',[0  680 560 420])
% set(gcf,'FileName','layered_nu')


plot_hstop_all(2);
c=get(gca,'Children');
delete(c(2:4));

v=axis();
plot([24.5 24.5 25.5 25.5],[v(4) 15 15 v(3)],'-.','Color',[1 1 1]*0.7)
plot([29 29],[v(4) v(3)],'--','Color',[1 1 1]*0.7)
text(24.5,v(4)*.95,['$\delta_3^{1/2}\rightarrow$'],'HorizontalAlignment','right')
text(29  ,v(4)*.95,['$\delta_{acc}^{1/2}\rightarrow$'],'HorizontalAlignment','right')

return

function figure_mu() 
plot_depthprofile_mu(5,20);
plot_depthprofile_I(5,20);
plot_depthprofile_mu(5,10);
plot_depthprofile_I(5,10);
plot_ha_bottom_friction(5);
plot_flow_rule_Mu_fit([2 5 6]);
plot_flow_rule_Mu_fit_oneplot(1:6);
return

function figure_shape_alpha() 
%C=2; H=40; A=20:30; plot_depthprofile(C,H,A,4,'VelocityX');
%C=5; H=20; A=20:30; plot_depthprofile(C,H,A,5,'VelocityX');
%plot_depthprofile_VelocityX_H(5,30);
%plot_depthprofile_VelocityX_H(5,10,'linear');
plot_ha_alpha(1);
plot_ha_alpha(2);
plot_ha_alpha(3);
plot_ha_alpha_List(1:6);
%set(legend,'Position',get(legend,'Position')+[.05 .1 0 0])
%table_alpha();

%plot_depthprofile_VelocityX_H(3,30);
%plot_depthprofile_VelocityX_H(3,10,'linear');
%plot_ha_alpha(3);
return

function figure_shape_K() 
%plot_depthprofile_NormalStresses(5,10,30);
plot_depthprofile_NormalStresses(5,30,30);
%plot_ha_K([5 2]);
plot_ha_K(1);
plot_ha_K(5);
%plot_ha_K_hstop(5);
%plot_ha_K_List(1:6);
%plot_depthprofile_NormalStresses(6,30,24);
%plot_depthprofile_NormalStresses(2,20,23);
%plot_depthprofile_NormalStresses(2,20,21);
%plot_ha_K(2);
% plot_depthprofile_NormalStressesWhalf(5,20);
return

function figure_shape_density() 
% plot_depthprofile_H(5,30);
% plot_depthprofile_H(1,30);
% %plot_ha_density(5);
% plot_ha_density(5,'centre');
% plot_ha_density(6,'centre');
% plot_ha_density(1,'centre');
plot_ha_density_fit(2:6);
%ylim([0.47,0.62])
%plot_depthprofile_H(2,30);
%plot_ha_density2(2:5);
%plot_ha_density_centre(2:5);
%plot_ha_density_centre(5);
%plot_ha_density2(2);
%plot_ha_density_centre(2);
% plot_depthprofile_NormalStressesWhalf(5,20);
return


function figure_oscillating() 
plot_depthprofile_H(2,30);

data=loadstatistics({'../stat_flowrule_T600short/H30A26L0.5M0.5B0.5.stat','../stat_flowrule/H30A26L0.5M0.5B0.5.stat'});
sfigure(); clf; hold on
set(gcf,'Position',[0 680 560 420])
set(gcf,'FileName','oscillating')

plot(data{1}.z,[data{1}.Nu data{2}.Nu])

legend({'t=600','t=2000'},'Location','NorthEast')
xlabel('$(z-b)/h$','Interpreter','none')
ylabel('$\rho/\rho_p$','Interpreter','none')
axis tight; v=axis; axis([0 v(2:4)]);

codestatus_oscillating(1:6);
return

function table_fit()
disp('Fitting for $\bar\rho(\theta,h)$')
disp('$$\begin{array}{|r|r|r|r|r|r|r|r|r|r|r|r|}\hline')
disp('\lambda & 1 & (h-40) & (\theta-30) & (h-40)^2 & (\theta-30)^2 & (h-40)(\theta-30) & \text{var}\\\hline')
table_density_fit(1:6);
disp('\hline\end{array}$$')

disp('Fitting for $\alpha(\theta,h)$')
disp('$$\begin{array}{|r|r|r|r|r|r|r|r|r|r|r|r|}\hline')
disp('\lambda & 1 & (h-40) & (\theta-30) & (h-40)^2 & (\theta-30)^2 & (h-40)(\theta-30) & \text{var}\\\hline')
table_alpha_fit(1:6);
disp('\hline\end{array}$$')

disp('Fitting for $K(\theta,h)$')
disp('$$\begin{array}{|r|r|r|r|r|r|r|r|r|r|r|r|}\hline')
disp('\lambda & 1 & (h-40) & (\theta-30) & (h-40)^2 & (\theta-30)^2 & (h-40)(\theta-30) & \text{var}\\\hline')
table_K_fit(1:6);
disp('\hline\end{array}$$')
return

function table_K_fit(CaseList)
global Data Flow Title
sfigure(); hold on
colors='kbrg';
%disp('K')
for i=1:length(CaseList); Case=CaseList(i);
  ind=Flow{Case}.Flowing&~Flow{Case}.Oscillating&~Flow{Case}.Accelerating;
%   ind=Flow{Case}.Flowing&Flow{Case}.Oscillating&~Flow{Case}.FullyOscillating&~Flow{Case}.Accelerating;
  Heights=unique(Flow{Case}.H(ind));
  Angles=unique(Flow{Case}.A(ind));
  FlowStressXX=cellfun(@(data) mean(data.StressXX(data.z>data.Base+2&data.z<data.Surface)), Data{Case}.flow);
  FlowStressZZ=cellfun(@(data) mean(data.StressZZ(data.z>data.Base+2&data.z<data.Surface)), Data{Case}.flow);
  K=FlowStressXX./FlowStressZZ;
  x1=Flow{Case}.Height(ind)-40;
  x2=Flow{Case}.A(ind)-28;
  y=K(ind);
%  X = [ones(size(x1))  x1  x2  x2.^2  x1.^2  x1.*x2  x1.^3  x2.^3  x1.^2.*x2  x2.^2.*x1];
%   X = [ones(size(x1))  x1  x2  x2.^2  x1.^2 x1.*x2];
  X = [ones(size(x1))  x1  x2];
  a = X\y;
  Y = X*a;
  MaxErr = max(abs(Y - y));
  StdErr = stderr(Y-y); 
  disp([Title{Case}(10:end-1) ' & ' num2str(a','%.6f & ') num2str(StdErr,'%.3f\\\\')])
  for j=1:length(Heights)
    indH=(Heights(j)==Flow{Case}.H)&(ind);
    plot(Flow{Case}.A(indH),Y(Heights(j)==Flow{Case}.H(ind)),'-','Color',colors(j))
    plot(Flow{Case}.A(indH),K(indH),'x','Color',colors(j))
  end
end
return

function table_alpha_fit(CaseList)
global Data Flow Title
sfigure(); hold on
colors='kbrg';
%disp('A')
for i=1:length(CaseList); Case=CaseList(i);
  ind=Flow{Case}.Flowing&~Flow{Case}.Oscillating&~Flow{Case}.Accelerating;
  % ind=~Flow{Case}.Accelerating;
  Heights=unique(Flow{Case}.H(ind));
  Angles=unique(Flow{Case}.A(ind));
% %   meanVelocityX=  cellfun(@(data) sum(data.VelocityX.*data.Nu)/sum(data.Nu), Data{Case}.flow);
%   meanVelocityX2= cellfun(@(data) sum(data.VelocityX.^2.*data.Nu)/sum(data.Nu), Data{Case}.flow);
  meanVelocityX=  cellfun(@(data) mean(data.VelocityX(data.z>data.Base&data.z<data.Surface)), Data{Case}.flow);
  meanVelocityX2= cellfun(@(data) mean(data.VelocityX(data.z>data.Base&data.z<data.Surface).^2), Data{Case}.flow);
  Alpha=meanVelocityX2./(meanVelocityX.^2);
  x1=Flow{Case}.Height(ind)-40;
  x2=Flow{Case}.A(ind)-28;
  y=Alpha(ind);
%  X = [ones(size(x1))  x1  x2  x2.^2  x1.^2  x1.*x2  x1.^3  x2.^3  x1.^2.*x2  x2.^2.*x1  x1.^4  x2.^4  x1.^3.*x2  x2.^3.*x1  x2.^2.*x1.^2];
%  X = [ones(size(x1))  x1  x2  x2.^2  x1.^2  x1.*x2  x1.^3  x2.^3  x1.^2.*x2  x2.^2.*x1];
  X = [ones(size(x1))  x1  x2  x2.^2  x1.^2 x1.*x2];
  a = X\y;
  Y = X*a;
  MaxErr = max(abs(Y - y));
  StdErr = stderr(Y-y); 
  disp([Title{Case}(10:end-1) ' & ' num2str(a','%.6f & ') num2str(StdErr,'%.3f\\\\')])
  for j=1:length(Heights)
    indH=(Heights(j)==Flow{Case}.H)&(ind);
    plot(Flow{Case}.A(indH),Y(Heights(j)==Flow{Case}.H(ind)),'-','Color',colors(j))
    plot(Flow{Case}.A(indH),Alpha(indH),'x','Color',colors(j))
  end
end
return

function table_density_fit(CaseList)
global Data Flow Title
sfigure(); hold on
colors='kbrg';
%disp('\rho')
for i=1:length(CaseList); Case=CaseList(i);
  ind=Flow{Case}.Flowing&~Flow{Case}.Oscillating&~Flow{Case}.Accelerating;
  % ind=~Flow{Case}.Accelerating;
  Heights=unique(Flow{Case}.H(ind));
  Angles=unique(Flow{Case}.A(ind));
  Density=cellfun(@(data) sum(data.Nu)*diff(data.z(1:2))/data.FlowHeight, Data{Case}.flow);
%    Density=cellfun(@(data) mean(data.Nu(data.z>data.Base&data.z<data.FlowHeight)), Data{Case}.flow);
  x1=Flow{Case}.Height(ind)-40;
  x2=Flow{Case}.A(ind)-28;
  y=Density(ind);
  %X = [ones(size(x1))  x1  x2  x2.^2  x1.^2  x1.*x2  x1.^3  x2.^3  x1.^2.*x2  x2.^2.*x1];
  X = [ones(size(x1))  x1  x2  x2.^2  x1.^2 x1.*x2];
  a = X\y;
  Y = X*a;
  MaxErr = max(abs(Y - y));
  StdErr = stderr(Y-y); 
  disp([Title{Case}(10:end-1) ' & ' num2str(a','%.6f & ') num2str(StdErr,'%.3f\\\\')])
  for j=1:length(Heights)
    indH=(Heights(j)==Flow{Case}.H)&(ind);
    plot(Flow{Case}.A(indH),Y(Heights(j)==Flow{Case}.H(ind)),'-','Color',colors(j))
    plot(Flow{Case}.A(indH),Density(indH),'x','Color',colors(j))
  end
end
return

function table_fit2()
Case=2:6;

table_density_fit2(Case);

% table_alpha_fit2(Case);
% 
% table_K_fit2(2:6);
% table_Kyy_fit2(Case);

%print_figures(1000);
return

function table_K_fit2(CaseList)
global Data Flow Title CoeffHstop
f=sfigure(); hold on
set(gcf,'Position',[0 0 4*560 1.2*420])
set(gcf,'FileName',['table_K_fit2' num2str(CaseList,'_%d')])
colors='kbrg';
markers='oxd+';
%disp('K')
disp('Fitting for $K(\theta,h)=c_0+c_1(\theta-24^\circ)/1^\circ$')
disp('$$\begin{array}{|r|r|r|r|r|r|r|r|r|r|r|r|}\hline')
disp('c_0 & c_1 & \text{var}\\\hline')

H=[];
A=[];
L=[];
K=[];
for i=1:length(CaseList); Case=CaseList(i);
  ind=Flow{Case}.Flowing&~Flow{Case}.Oscillating&~Flow{Case}.Accelerating;
  H = [H; Flow{Case}.Height(ind)];
  A = [A; Flow{Case}.A(ind)];
  L = [L; Flow{Case}.L(ind)];
  FlowStressXX=cellfun(@(data) mean(data.StressXX(data.z>data.Base+2&data.z<data.Surface)), Data{Case}.flow(ind));
  FlowStressZZ=cellfun(@(data) mean(data.StressZZ(data.z>data.Base+2&data.z<data.Surface)), Data{Case}.flow(ind));
  K=[K; FlowStressXX./FlowStressZZ];
end
%fitting
y = K;
X = [ones(size(H)) A-24];
a = X\y;
Y = X*a;
MaxErr = max(abs(Y - y));
StdErr = stderr(Y-y);%sqrt(mean((Y-y).^2));

plot(A,Y,'-k')
plot(A,K,'x')

disp([num2str(a','%.6f & ') num2str(StdErr,'%.3f\\\\')])
disp('\hline\end{array}$$')

return

function table_Kyy_fit2(CaseList)
global Data Flow Title CoeffHstop
f=sfigure(); hold on
set(gcf,'Position',[0 0 4*560 1.2*420])
set(gcf,'FileName',['table_K_fit2' num2str(CaseList,'_%d')])
colors='kbrg';
markers='oxd+';
%disp('K')
disp('Fitting for $K_{yy}(\theta,h)=c_0+c_1(\theta-24^\circ)/1^\circ$')
disp('$$\begin{array}{|r|r|r|r|r|r|r|r|r|r|r|r|}\hline')
disp('c_0 & c_1 & \text{var}\\\hline')

H=[];
A=[];
L=[];
K=[];
for i=1:length(CaseList); Case=CaseList(i);
  ind=Flow{Case}.Flowing&~Flow{Case}.Oscillating&~Flow{Case}.Accelerating;
  H = [H; Flow{Case}.Height(ind)];
  A = [A; Flow{Case}.A(ind)];
  L = [L; Flow{Case}.L(ind)];
  FlowStressYY=cellfun(@(data) mean(data.StressYY(data.z>data.Base+2&data.z<data.Surface)), Data{Case}.flow(ind));
  FlowStressZZ=cellfun(@(data) mean(data.StressZZ(data.z>data.Base+2&data.z<data.Surface)), Data{Case}.flow(ind));
  K=[K; FlowStressYY./FlowStressZZ];
end
%fitting
y = K;
X = [ones(size(H)) A-24];
a = X\y;
Y = X*a;
MaxErr = max(abs(Y - y));
StdErr = stderr(Y-y);%sqrt(mean((Y-y).^2));

plot(A,Y,'-k')
plot(A,K,'x')

disp([num2str(a','%.6f & ') num2str(StdErr,'%.3f\\\\')])
disp('\hline\end{array}$$')

return

function table_alpha_fit2(CaseList)
global Data Flow Title CoeffHstop

markers='oxd+';
colors='kbrg';
%disp('A')
disp('Fitting for $\alpha(\theta,h)=c_0+c_1\exp(-c_2(\theta-\theta_s(h)/1^\circ)/h$')
disp('$$\begin{array}{|r|r|r|r|r|r|r|r|r|r|r|r|}\hline')
disp('\lambda & c_0 & c_1 & c_2 & \text{var}\\\hline')
for i=1:length(CaseList); Case=CaseList(i);
  sfigure(); hold on
  set(gcf,'Position',[0  340 560 420])
  set(gcf,'FileName',['table_alpha_fit2' num2str(Case,'%d')])
  ind=Flow{Case}.Flowing&~Flow{Case}.Oscillating&~Flow{Case}.Accelerating&Flow{Case}.Angle>20;
  % ind=~Flow{Case}.Accelerating;
  Heights=unique(Flow{Case}.H(ind));
  Angles=unique(Flow{Case}.A(ind));
% %   meanVelocityX=  cellfun(@(data) sum(data.VelocityX.*data.Nu)/sum(data.Nu), Data{Case}.flow);
%   meanVelocityX2= cellfun(@(data) sum(data.VelocityX.^2.*data.Nu)/sum(data.Nu), Data{Case}.flow);
  meanVelocityX=  cellfun(@(data) mean(data.VelocityX(data.z>data.Base&data.z<data.Surface)), Data{Case}.flow);
  meanVelocityX2= cellfun(@(data) mean(data.VelocityX(data.z>data.Base&data.z<data.Surface).^2), Data{Case}.flow);
  Alpha=meanVelocityX2./(meanVelocityX.^2);
  if (true)
    x1=Flow{Case}.Height(ind);
    x2=Flow{Case}.A(ind)-atand(tanthetastop(CoeffHstop{Case}, Flow{Case}.Height(ind)));
    x3=x2.*x1;
    y=Alpha(ind);
  %  X = [ones(size(x1))  x1  x2  x2.^2  x1.^2  x1.*x2  x1.^3  x2.^3  x1.^2.*x2  x2.^2.*x1  x1.^4  x2.^4  x1.^3.*x2  x2.^3.*x1  x2.^2.*x1.^2];
  %  X = [ones(size(x1))  x1  x2  x2.^2  x1.^2  x1.*x2  x1.^3  x2.^3
  %  x1.^2.*x2  x2.^2.*x1];
    if Case<3
      fitting = @(a) a(1)*ones(size(x2));
    else
      fitting = @(a) a(1)+a(2)*exp(-a(3)*x2)./x1;
    end
    error = @(a) sum((fitting(a)-y).^2);
    options=optimset('MaxFunEvals',1000);
    [a,f] = fminsearch(@(a) error(a),[1 1 0]',options); 
    Y=fitting(a);
  else
    x1=Flow{Case}.Height(ind)-40;
    %x1=Flow{Case}.Height(ind)./hstop(CoeffHstop{5},Flow{Case}.A(ind)); 
    x2=Flow{Case}.A(ind)-28;
    y=Alpha(ind);
  %  X = [ones(size(x1))  x1  x2  x2.^2  x1.^2  x1.*x2  x1.^3  x2.^3  x1.^2.*x2  x2.^2.*x1  x1.^4  x2.^4  x1.^3.*x2  x2.^3.*x1  x2.^2.*x1.^2];
  %  X = [ones(size(x1))  x1  x2  x2.^2  x1.^2  x1.*x2  x1.^3  x2.^3  x1.^2.*x2  x2.^2.*x1];
    X = [ones(size(x1))  x1  x2  x2.^2];
    a = X\y;
    Y = X*a;
  end
  MaxErr = max(abs(Y - y));
  StdErr = stderr(Y-y); 
  if Case<3
    disp([Title{Case}(10:end-1) ' & ' num2str(a(1),'%.6f & & & ') num2str(StdErr,'%.3f\\\\')])
  else
    disp([Title{Case}(10:end-1) ' & ' num2str(a','%.6f & ') num2str(StdErr,'%.3f\\\\')])
  end
  for j=1:length(Heights)
    indH=(Heights(j)==Flow{Case}.H)&(ind);
     plot(Flow{Case}.A(indH)-atand(tanthetastop(CoeffHstop{Case}, Heights(j))),...
       Y(Heights(j)==Flow{Case}.H(ind)),'-','Color',colors(j))
     plot(Flow{Case}.A(indH)-atand(tanthetastop(CoeffHstop{Case}, Heights(j))),...
       Alpha(indH),markers(j),'Color',colors(j))

    %plot((Flow{Case}.A(indH)-atand(tanthetastop(CoeffHstop{Case}, Heights(j))))*Heights(j),Alpha(indH),'x-','Color',colors(j))
%    plot(Flow{Case}.A(indH)-atand(tanthetastop(CoeffHstop{Case}, Heights(j))),Alpha(indH),'x-','Color',colors(j))
  end
  legend('show')
  title(Title{Case});
  xlabel('$\theta-\theta_s(h)$')
  ylabel('$\alpha$')
  %set(gca,'YScale','log')
  %ylim([0 20])

  if (Case==5),
    sfigure(1000);
    set(gcf,'Position',[2000 340 560*2 420*.67])
    set(gcf,'FileName',['table_fittingfull' num2str(Case,'%d')])
    subplot(1,3,2); cla; hold on
    for j=length(Heights):-1:1
      indH=(Heights(j)==Flow{Case}.H)&(ind);
      plot(Flow{Case}.A(indH)-0*atand(tanthetastop(CoeffHstop{Case}, Heights(j))),...
        Alpha(indH),...
        markers(j),'Color',colors(j),'MarkerSize',3,...
        'DisplayName',['$H=' num2str(Heights(j)) '$']);
    end
    %legend('show')
    %set(legend,'Box','off')
    for j=1:length(Heights)
      indH=(Heights(j)==Flow{Case}.H)&(ind);
      plot(Flow{Case}.A(indH)-0*atand(tanthetastop(CoeffHstop{Case}, Heights(j))),...
         Y(Heights(j)==Flow{Case}.H(ind)),'-','Color',colors(j))
    end
%     xlabel('$\theta-\theta_s(h)$')
    xlabel('$\theta$')
    ylabel('$\alpha$')
    axis tight
  end
end
disp('\hline\end{array}$$')
return

function table_density_fit2(CaseList)

global Data Flow Title CoeffHstop
f=sfigure(); hold on
set(gcf,'Position',[0 0 4*560 1.2*420])
set(gcf,'FileName',['table_K_fit2' num2str(CaseList,'_%d')])
colors='kbrg';
markers='oxd+';
%disp('K')
disp('Fitting for $K(\theta,h)=c_0+c_1(\theta-24^\circ)/1^\circ$')
disp('$$\begin{array}{|r|r|r|r|r|r|r|r|r|r|r|r|}\hline')
disp('c_0 & c_1 & \text{var}\\\hline')

H=[];
A=[];
L=[];
Density=[];
for i=1:length(CaseList); Case=CaseList(i);
  ind=Flow{Case}.Flowing&~Flow{Case}.Oscillating&~Flow{Case}.Accelerating;
  H = [H; Flow{Case}.Height(ind)];
  A = [A; Flow{Case}.A(ind)];
  L = [L; Flow{Case}.L(ind)];
  Density=[Density; cellfun(@(data) sum(data.Nu)*diff(data.z(1:2))/data.FlowHeight, Data{Case}.flow(ind))];
end
%fitting
y=Density;
fitting = @(a) a(1)-a(2)*exp(a(3)*(A-24))./H.^a(4);
error = @(a) sum((fitting(a)-y).^2);
options=optimset('MaxFunEvals',10000,'MaxIter',10000);
[a,f] = fminsearch(@(a) error(a),[0.6 0.03 0.2 0.2]',options); 
Y=fitting(a);
MaxErr = max(abs(Y - y));
StdErr = stderr(Y-y);%sqrt(mean((Y-y).^2));

plot(A,Y,'-k')
plot(A,Density,'x')

disp([num2str(a','%.6f & ') num2str(StdErr,'%.3f\\\\')])
disp('\hline\end{array}$$')

return

function table_fit3()
Case=1:6;

% table_density_fit3(5);

% table_alpha_fit3(Case);

% table_K_fit3();

%print_figures(1000);
return

function table_K_fit3(CaseList,opt)
global Data Flow Title CoeffHstop
if ~exist('CaseList','var')
  CaseList=2:6;
end
f=sfigure(); hold on
set(gcf,'Position',[0 0 560 420])
set(gcf,'FileName',['table_K_fit2' num2str(CaseList,'_%d')])
colors='kbrg';
markers='oxd+';
%disp('K')
disp('Fitting for $K(\theta,h)=c_0+c_1(\theta-24^\circ)/1^\circ$')
disp('$$\begin{array}{|r|r|r|r|r|r|r|r|r|r|r|r|}\hline')
disp('\lambda & c_0 & c_1 & \text{var}\\\hline')

x=[];
y=[];
H=[];
%  for Case=2:6
for Case=CaseList
  ind=Flow{Case}.Flowing&~Flow{Case}.Oscillating&~Flow{Case}.Accelerating;
  if exist('opt','var')&strcmp(opt,'layered')
    ind=Flow{Case}.Flowing&Flow{Case}.Oscillating&~Flow{Case}.FullyOscillating&~Flow{Case}.Accelerating;
  end
  x=[x; Flow{Case}.A(ind)];
  H=[H; Flow{Case}.H(ind)];
  FlowStressXX=cellfun(@(data) mean(data.StressXX(data.z>data.Base+2&data.z<data.Surface)), Data{Case}.flow(ind));
  FlowStressZZ=cellfun(@(data) mean(data.StressZZ(data.z>data.Base+2&data.z<data.Surface)), Data{Case}.flow(ind));
  K=FlowStressXX./FlowStressZZ;
  y=[y; K];
end
X = [ones(size(x)) x];
a = X\y;
Y = X*a;
%print x:1=a1+a2x -> x=(1-a1)/a2
a(1)=(1-a(1))/a(2);
StdErr = stderr(Y-y); 
disp([Title{Case}(10:end-1) ' & ' num2str(a','%.6f & ') num2str(StdErr,'%.3f\\\\')])

ind=H>0;
[xs,ix]=sort(x);
plot(xs,Y(ix),'k-');
plot(x(ind),y(ind),'kx');
xlabel('$\theta$')
ylabel('$K$')
% for i=1:length(CaseList); Case=CaseList(i);
%   for j=1:length(Heights)
%     indH=(Heights(j)==Flow{Case}.H)&(ind);
%      plot(Flow{Case}.A(indH),...
%        Y(Heights(j)==Flow{Case}.H(ind)),'-','Color',colors(j))
%      plot(Flow{Case}.A(indH),...
%        K(indH),'x:','Color',colors(j))
%   end
%   title(Title{Case});
% end

disp('\hline\end{array}$$')
return

function table_alpha_fit3(CaseList,opt)
global Data Flow Title CoeffHstop
f=sfigure(); hold on
set(gcf,'Position',[0 0 4*560 1.2*420])
set(gcf,'FileName',['table_K_fit2' num2str(CaseList,'_%d')])

markers='oxd+';
colors='kbrg';
%disp('A')
disp('Fitting for $\alpha(\theta,h)=c_0+c_1\exp(-c_2(\theta-\theta_s(h)/1^\circ)/h$')
disp('$$\begin{array}{|r|r|r|r|r|r|r|r|r|r|r|r|}\hline')
disp('\lambda & c_0 & c_1 & c_2 & \text{var}\\\hline')
for i=1:length(CaseList); Case=CaseList(i);
  sfigure(f); subplot(1,length(CaseList),i); hold on
  ind=Flow{Case}.Flowing&~Flow{Case}.Oscillating&~Flow{Case}.Accelerating&Flow{Case}.Angle>20;
  % ind=~Flow{Case}.Accelerating;
  Heights=unique(Flow{Case}.H(ind));
  Angles=unique(Flow{Case}.A(ind));
% %   meanVelocityX=  cellfun(@(data) sum(data.VelocityX.*data.Nu)/sum(data.Nu), Data{Case}.flow);
%   meanVelocityX2= cellfun(@(data) sum(data.VelocityX.^2.*data.Nu)/sum(data.Nu), Data{Case}.flow);
  meanVelocityX=  cellfun(@(data) mean(data.VelocityX(data.z>data.Base&data.z<data.Surface)), Data{Case}.flow);
  meanVelocityX2= cellfun(@(data) mean(data.VelocityX(data.z>data.Base&data.z<data.Surface).^2), Data{Case}.flow);
  Alpha=meanVelocityX2./(meanVelocityX.^2);
  if (true)
    x1=Flow{Case}.Height(ind);
    x2=Flow{Case}.A(ind)-atand(tanthetastop(CoeffHstop{Case}, Flow{Case}.Height(ind)));
    x3=x2.*x1;
    y=Alpha(ind);
  %  X = [ones(size(x1))  x1  x2  x2.^2  x1.^2  x1.*x2  x1.^3  x2.^3  x1.^2.*x2  x2.^2.*x1  x1.^4  x2.^4  x1.^3.*x2  x2.^3.*x1  x2.^2.*x1.^2];
  %  X = [ones(size(x1))  x1  x2  x2.^2  x1.^2  x1.*x2  x1.^3  x2.^3
  %  x1.^2.*x2  x2.^2.*x1];
    if Case<3
      fitting = @(a) a(1)*ones(size(x2));
    else
      fitting = @(a) a(1)+a(2)*exp(-a(3)*x2)./x1;
    end
    error = @(a) sum((fitting(a)-y).^2);
    options=optimset('MaxFunEvals',1000);
    a = fminsearch(@(a) error(a),[1 1 0]',options); 
    Y=fitting(a);
  else
    x1=Flow{Case}.Height(ind)-40;
    %x1=Flow{Case}.Height(ind)./hstop(CoeffHstop{5},Flow{Case}.A(ind)); 
    x2=Flow{Case}.A(ind)-28;
    y=Alpha(ind);
  %  X = [ones(size(x1))  x1  x2  x2.^2  x1.^2  x1.*x2  x1.^3  x2.^3  x1.^2.*x2  x2.^2.*x1  x1.^4  x2.^4  x1.^3.*x2  x2.^3.*x1  x2.^2.*x1.^2];
  %  X = [ones(size(x1))  x1  x2  x2.^2  x1.^2  x1.*x2  x1.^3  x2.^3  x1.^2.*x2  x2.^2.*x1];
    X = [ones(size(x1))  x1  x2  x2.^2];
    a = X\y;
    Y = X*a;
  end
  MaxErr = max(abs(Y - y));
  StdErr = stderr(Y-y); 
  if Case<3
    disp([Title{Case}(10:end-1) ' & ' num2str(a(1),'%.6f & & & ') num2str(StdErr,'%.3f\\\\')])
  else
    disp([Title{Case}(10:end-1) ' & ' num2str(a','%.6f & ') num2str(StdErr,'%.3f\\\\')])
  end
  for j=1:length(Heights)
    indH=(Heights(j)==Flow{Case}.H)&(ind);
     plot(Flow{Case}.A(indH)-atand(tanthetastop(CoeffHstop{Case}, Heights(j))),...
       Y(Heights(j)==Flow{Case}.H(ind)),'-','Color',colors(j))
     plot(Flow{Case}.A(indH)-atand(tanthetastop(CoeffHstop{Case}, Heights(j))),...
       Alpha(indH),markers(j),'Color',colors(j))

    %plot((Flow{Case}.A(indH)-atand(tanthetastop(CoeffHstop{Case}, Heights(j))))*Heights(j),Alpha(indH),'x-','Color',colors(j))
%    plot(Flow{Case}.A(indH)-atand(tanthetastop(CoeffHstop{Case}, Heights(j))),Alpha(indH),'x-','Color',colors(j))
  end
  %legend('show')
  title(Title{Case});
  xlabel('$\theta-\theta_s(h)$')
  ylabel('$\alpha$')
  %set(gca,'YScale','log')
  %ylim([0 20])

  if (Case==5),
    sfigure(1000);
    set(gcf,'Position',[2000 340 560*2 420*.67])
    set(gcf,'FileName',['table_fittingfull' num2str(Case,'%d')])
    subplot(1,3,2); cla; hold on
    for j=length(Heights):-1:1
      indH=(Heights(j)==Flow{Case}.H)&(ind);
      plot(Flow{Case}.A(indH)-0*atand(tanthetastop(CoeffHstop{Case}, Heights(j))),...
        Alpha(indH),...
        markers(j),'Color',colors(j),'MarkerSize',3,...
        'DisplayName',['$H=' num2str(Heights(j)) '$']);
    end
    %legend('show')
    %set(legend,'Box','off')
    for j=1:length(Heights)
      indH=(Heights(j)==Flow{Case}.H)&(ind);
      plot(Flow{Case}.A(indH)-0*atand(tanthetastop(CoeffHstop{Case}, Heights(j))),...
         Y(Heights(j)==Flow{Case}.H(ind)),'-','Color',colors(j))
    end
%     xlabel('$\theta-\theta_s(h)$')
    xlabel('$\theta$')
    ylabel('$\alpha$')
    axis tight
  end
end
disp('\hline\end{array}$$')
C=get(f,'Children');
YLimF=cell2mat(get(C,'YLim'));
set(C,'YLim',[min(YLimF(:,1)) max(YLimF(:,2))]);
% set(C,'YLim',[.96  1.08]);
% set(C,'XLim',[20  28]);
return

function table_density_fit3(CaseList)
global Data Flow Title CoeffHstop
g=sfigure(); hold on
set(gcf,'Position',[0 0 560 420])
set(gcf,'FileName',['rho_fit' num2str(CaseList,'_%d')])

f=sfigure(); hold on
set(gcf,'Position',[0 0 4*560 1.2*420])
set(gcf,'FileName',['table_K_fit2' num2str(CaseList,'_%d')])

colors='kbrg';
markers='oxd+*<';
%disp('\rho')
disp('Fitting for $\bar\rho(\theta,h)=c_0-c_1\exp(c_2(\theta-24^\circ)/1^\circ)/h^{c_3}$')
disp('$$\begin{array}{|r|r|r|r|r|r|r|r|r|r|r|r|}\hline')
disp('\lambda & 1 & c_0 & c_1 & c_2 & c_3 & \text{var}\\\hline')

x=[];
y=[];
z=[];
for Case=1:6
  ind=Flow{Case}.Flowing&~Flow{Case}.Oscillating&~Flow{Case}.Accelerating&Flow{Case}.Angle>20;
  b=2;
  if (Case<4) 
    ind=ind&Flow{Case}.H>10;
    b=9; 
  end
  CentralDensity=cellfun(@(data) mean(data.Nu(data.z>data.Base+b&data.z<data.Surface-4)), Data{Case}.flow);
  Density=cellfun(@(data) sum(data.Nu)*diff(data.z(1:2))/data.FlowHeight, Data{Case}.flow);
  x=[x; Flow{Case}.A(ind)];
  y=[y; CentralDensity(ind)];
  z=[z; Density(ind)];
end
fittingCD = @(a) a(1)-exp(a(2)*(x-a(3)));
error = @(a) sum((fittingCD(a)-y).^2);
options=optimset('MaxFunEvals',100000,'MaxIter',100000);
a = fminsearch(@(a) error(a),[.6 0.26 40]',options); 
Y=fittingCD(a);
StdErr = stderr(Y - y); 
StdErrZ = stderr(Y - z); 
disp(['- & ' num2str(a','%.6f & ') num2str(StdErr,'%.3f & ') num2str(StdErrZ,'%.3f\\\\')])

for i=1:length(CaseList); Case=CaseList(i);
  sfigure(f); subplot(1,length(CaseList),i); hold on

  ind=Flow{Case}.Flowing&~Flow{Case}.Oscillating&~Flow{Case}.Accelerating&Flow{Case}.Angle>20;
  % ind=~Flow{Case}.Accelerating;
  Heights=unique(Flow{Case}.H(ind));
  Angles=unique(Flow{Case}.A(ind));
   Density=cellfun(@(data) sum(data.Nu)*diff(data.z(1:2))/data.FlowHeight, Data{Case}.flow);
%   Density=cellfun(@(data) mean(data.Nu(data.z>data.Base&data.z<data.Surface)), Data{Case}.flow);
  b=2;
%   if (Case<4) 
%     ind=ind&Flow{Case}.H>10;
%     b=9; 
%   end
  CentralDensity=cellfun(@(data) mean(data.Nu(data.z>data.Base+b&data.z<data.Surface-4)), Data{Case}.flow);

  x=Flow{Case}.A(ind);
  fittingCD = @(a) a(1)-exp(a(2)*(x-a(3)));
  CD=fittingCD(a);

%   %fit deviation from central density
%   x1=Flow{Case}.Height(ind);
%   thetas=atand(tanthetastop(CoeffHstop{Case}, Flow{Case}.Height(ind)));
%   x2=(Flow{Case}.A(ind)-thetas)./(30-thetas); 
%   y=Density(ind);
%   fitting = @(b) CD.*(1-(b(1)+b(2).*x2+b(3).*x2.^2)./x1);
%   error = @(b) sum((fitting(b)-y).^2);
%   options=optimset('MaxFunEvals',10000,'MaxIter',10000);
%   b = fminsearch(@(b) error(b),[.05 .05 .05]',options); 
%   D=fitting(b);
%   CentralDensityFit=y./(1-(b(1)+b(2).*x2+b(3).*x2.^2)./x1);
% 
  D=CD;
  CentralDensityFit=Density(ind);
  
  MaxErr = max(abs(D - Density(ind)));
  StdErr = stderr(D - Density(ind)); 
%   StdErr = sqrt(var(CD - CentralDensityFit)); 
%   disp([Title{Case}(10:end-1) ' & ' num2str(b','%.6f & ') num2str(StdErr,'%.3f\\\\')])
  disp([Title{Case}(10:end-1) ' & ' num2str(StdErr,'%.3f\\\\')])

  [x,ix]=sort(Flow{Case}.A(ind));
  plot(x,CD(ix),'k-');
  
  for j=1:length(Heights)
    indH=(Heights(j)==Flow{Case}.H)&(ind);
    h=Flow{Case}.Height(indH);
    plot(Flow{Case}.A(indH),CentralDensityFit(Heights(j)==Flow{Case}.H(ind)),['' markers(j)],'Color',colors(j));
    % plot(x,CentralDensity(indH),['-' markers(j)],'Color',colors(j));
%     plot(x,CD(Heights(j)==Flow{Case}.H(ind)),'-','Color',colors(j));
%     plot(x,Density(indH),['' markers(j)],'Color',colors(j));
%     plot(x,D(Heights(j)==Flow{Case}.H(ind)),'-','Color',colors(j));
  end
  
  sfigure(g);
  plot(Flow{Case}.A(ind),CentralDensity(ind),'k*','DisplayName',['$\bar\rho_c/\rho_p$']);
  plot(x,CD(ix),'k-','DisplayName',['$\rho_c^{fit}/\rho_p$']);
  for j=length(Heights):-1:1
    indH=(Heights(j)==Flow{Case}.H)&(ind);
    h=Flow{Case}.Height(indH);
    plot(Flow{Case}.A(indH),CentralDensityFit(Heights(j)==Flow{Case}.H(ind)),...
      ['' markers(j)],'MarkerSize',4,'Color',colors(j),'DisplayName',['$H=' num2str(Heights(j)) '$']);
  end
  xlabel('$\theta$')
%   ylabel('$\bar\rho h/(h-(c_0+c_1\theta))$')
  ylabel('$\bar\rho/\rho_p$')
  axis tight
  legend('show')
  set(legend,'Location','SouthWest','Interpreter','none','Box','off')
end
disp('\hline\end{array}$$')

if length(CaseList)>1
  sfigure(f)
  C=get(f,'Children');
  YLimF=cell2mat(get(C,'YLim'));
  set(C,'YLim',[min(YLimF(:,1)) max(YLimF(:,2))]);
  XLimF=cell2mat(get(C,'XLim'));
  set(C,'XLim',[min(XLimF(:,1)) max(XLimF(:,2))]);
end
return

function table_flowrule(CaseList,jenkinscorrection)  
%plot_hstop_diagram(2:6,'hstoponly');
%plot_hstop_diagram(1);
%plot_hstop_diagram(2);
%load data
global Data Title CoeffHstop CoeffHstop_exp Flow

display('\lambda & \delta_1 & \delta_2 & A \\\hline\hline')
for k=1:length(CaseList); Case=CaseList(k);
  disp([...
    num2str(Title{Case}(10:end-1),'%.3f') ' & '...
    num2str(atand(CoeffHstop{k}(1:2)),'%.3f & ') ' '...
    num2str(CoeffHstop{k}(3),'%.3f') '\\\hline'])
end

display('\lambda & \beta & \gamma & var & \delta_{min} & \delta_{max} & Range\\\hline\hline')
for k=1:length(CaseList); Case=CaseList(k);
  if (Case<=2)
    disp([...
      num2str(Title{Case}(10:end-1),'%.3f') ' & '...
      num2str(atand(CoeffHstop{k}(1:2)),'%.3f & ') ...
      num2str(CoeffHstop{k}(3),'%.3f') ' & ' ...
      num2str(Flow{Case}.betaPOlayered(2),'%.3f') ' & ' ...
      num2str(-Flow{Case}.betaPOlayered(1),'%.3f') ' & '...
      num2str(Flow{Case}.varPOlayered,'%.3f') ' & ' ...
      '\\'])
  end
  disp([...
    num2str(Title{Case}(10:end-1),'%.3f') ' & '...
    num2str(atand(CoeffHstop{k}(1:2)),'%.3f & ') ...
    num2str(CoeffHstop{k}(3),'%.3f') ' & ' ...
    num2str(Flow{Case}.betaPO(2),'%.3f') ' & ' ...
    num2str(-Flow{Case}.betaPO(1),'%.3f') ' & '...
    num2str(Flow{Case}.varPO,'%.3f') ' & ' ...
    '\\\hline'])
end

return

function table_alpha()  
%load data
global Data Title CoeffHstop CoeffHstop_exp Flow
CaseList=1:6

display('\lambda & \beta & \gamma & \delta_1 & \delta_2 & A \\\hline\hline')
for k=1:length(CaseList); Case=CaseList(k);
  disp([...
    num2str(Title{Case}(10:end-1),'%.3f') ' & '...
    num2str(Flow{Case}.betaPO(2),'%.3f') ' & ' num2str(-Flow{Case}.betaPO(1),'%.3f') ' & '...
    num2str(atand(CoeffHstop{k}(1:2)),'%.3f & ') ' '...
    num2str(CoeffHstop{k}(3),'%.3f') '\\\hline'])
end
return


function codestatus_oscillating(CaseList)
%load data
global Data Title Flow

%new figure
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['codestatus_flow' num2str(CaseList,'_%d')])

%create subplots
if ~exist('CaseList','var'), CaseList=1:length(Data); end
ny=ceil(sqrt(length(CaseList)));
nx=ceil(length(CaseList)/ny);

for k=1:length(CaseList); Case=CaseList(k);
  subplot(nx,ny,k)
  H= cellfun(@(data)str2double(data.name(strfind(data.name,'H')+1:strfind(data.name,'A')-1)),Data{Case}.flow);
  A= cellfun(@(data)str2double(data.name(strfind(data.name,'A')+1:strfind(data.name,'L')-1)),Data{Case}.flow);
  V= cellfun(@(data)data.FlowVelocityX,Data{Case}.flow);
  T= cellfun(@(data)data.time(2),Data{Case}.flow);

  scatter(A,H,50,Flow{k}.Oscillating,'filled')
  caxis([0 .2])
  axis ([20,60,10,40.1])
  title(Title{Case})

  %check FlowVelocity
  disp(Title{Case})
  for Angle= unique(A)'
    if ~all(diff(V(A==Angle))>-0.001)
      disp(['Angle:' num2str(Angle) ', height' mat2str(H(A==Angle)) ', vel:' mat2str(V(A==Angle),2) ', time:' mat2str(T(A==Angle),2)]);
    end
  end
  for Height= unique(H)'
    if ~all(diff(V(H==Height))>-0.001)
      disp(['Height:' num2str(Height) ', angle' mat2str(A(H==Height)) ', vel:' mat2str(V(H==Height),2) ', time:' mat2str(T(H==Height),2)]);
    end
  end

end

return

function codestatus_flow(CaseList)
%load data
global Data Title Flow

%new figure
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['codestatus_flow' num2str(CaseList,'_%d')])

%create subplots
if ~exist('CaseList','var'), CaseList=1:length(Data); end
ny=ceil(sqrt(length(CaseList)));
nx=ceil(length(CaseList)/ny);

for k=1:length(CaseList); Case=CaseList(k);
  subplot(nx,ny,k)
  H= cellfun(@(data)str2double(data.name(strfind(data.name,'H')+1:strfind(data.name,'A')-1)),Data{Case}.flow);
  A= cellfun(@(data)str2double(data.name(strfind(data.name,'A')+1:strfind(data.name,'L')-1)),Data{Case}.flow);
  V= cellfun(@(data)data.FlowVelocityX,Data{Case}.flow);
  T= cellfun(@(data)data.time(2),Data{Case}.flow);

  scatter(A,H,50,Flow{Case}.Accelerating,'filled')
  caxis([0 .2])
  axis ([20,30.1,10,40.1])
  title(Title{Case})

  %check FlowVelocity
  disp(Title{Case})
  for Angle= unique(A)'
    if ~all(diff(V(A==Angle))>-0.001)
      disp(['Angle:' num2str(Angle) ', height' mat2str(H(A==Angle)) ', vel:' mat2str(V(A==Angle),2) ', time:' mat2str(T(A==Angle),2)]);
    end
  end
  for Height= unique(H)'
    if ~all(diff(V(H==Height))>-0.001)
      disp(['Height:' num2str(Height) ', angle' mat2str(A(H==Height)) ', vel:' mat2str(V(H==Height),2) ', time:' mat2str(T(H==Height),2)]);
    end
  end

end

return

function codestatus_hstop(CaseList)
%load data
global Data Title

%create figure
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['codestatus_hstop' num2str(CaseList,'_%d')])

%create subplots
if ~exist('CaseList','var'), CaseList=1:length(Data); end
ny=ceil(sqrt(length(CaseList)));
nx=ceil(length(CaseList)/ny);

for k=1:length(CaseList); Case=CaseList(k);
  subplot(nx,ny,k)
  H= cellfun(@(data)str2double(data.name(strfind(data.name,'H')+1:strfind(data.name,'A')-1)),Data{Case}.hstop);
  A= cellfun(@(data)str2double(data.name(strfind(data.name,'A')+1:strfind(data.name,'L')-1)),Data{Case}.hstop);
  
 	static=cellfun(@(data)data.time(end)<499,Data{Case}.hstop);
  plot(A(static),H(static),'ro','MarkerFaceColor','red'); hold on
  plot(A(~static),H(~static),'bo','MarkerFaceColor','blue')
  title(Title{Case})
end
return

function status_ArrestedFlowingAccelerating(CaseList)
%load data
global Data Title Flow

%new figure
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['status_ArrestedFlowingAccelerating' num2str(CaseList,'_%d')])

%create subplots
if ~exist('CaseList','var'), CaseList=1:length(Data); end
ny=ceil(sqrt(length(CaseList)));
nx=ceil(length(CaseList)/ny);

for k=1:length(CaseList); Case=CaseList(k);
  subplot(nx,ny,k)
  H= cellfun(@(data)str2double(data.name(strfind(data.name,'H')+1:strfind(data.name,'A')-1)),Data{Case}.flow);
  A= cellfun(@(data)str2double(data.name(strfind(data.name,'A')+1:strfind(data.name,'L')-1)),Data{Case}.flow);
  V= cellfun(@(data)data.FlowVelocityX,Data{Case}.flow);
  T= cellfun(@(data)data.time(2),Data{Case}.flow);

%   Oscillating = cellfun(@(data)mean(abs(diff(data.Nu(data.z>0.3*data.FlowHeight&data.z>0.9*data.FlowHeight)))),Data{Case}.flow);
  Time= cellfun(@(data)diff(data.EneShort.Time([1,end]))>40,Data{Case}.flow);
  ind=(~Flow{Case}.Oscillating)&Time&Flow{Case}.Flowing;
  scatter(A(ind),H(ind),50,Flow{Case}.Accelerating(ind),'filled')
%   caxis([0 2])
  axis ([20,60,10,40.1])
  title(Title{Case})

  %check FlowVelocity
  disp(Title{Case})
  for Angle= unique(A)'
    if ~all(diff(V(A==Angle))>-0.001)
      disp(['Angle:' num2str(Angle) ', height' mat2str(H(A==Angle)) ', vel:' mat2str(V(A==Angle),2) ', time:' mat2str(T(A==Angle),2)]);
    end
  end
  for Height= unique(H)'
    if ~all(diff(V(H==Height))>-0.001)
      disp(['Height:' num2str(Height) ', angle' mat2str(A(H==Height)) ', vel:' mat2str(V(H==Height),2) ', time:' mat2str(T(H==Height),2)]);
    end
  end

end

return

% plot in Angle-Height parameter space with color indicating arresting or
% flowing
function plot_hstop_diagram(CaseList,opt) 
%load data
global Data Astop Hstop Astop_exp Title Stop Flow CoeffHstop

%new figure
figure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['hstop_diagram' num2str(CaseList,'_%d')])
colors=mylines(max(CaseList));
if ~exist('CaseList','var'), CaseList=1:length(Data); end

%create subplots
if ~exist('opt','var')|~strcmp(opt,'errorbarsonly')
  for k=1:length(CaseList); Case=CaseList(k);
    plot(Stop{Case}.Angle(Stop{Case}.static),Stop{Case}.Height(Stop{Case}.static),...
      'o','Color',colors(k,:),'MarkerFaceColor',colors(k,:),'MarkerSize',4); 
    hold on
    plot(Stop{Case}.Angle(~Stop{Case}.static),Stop{Case}.Height(~Stop{Case}.static),...
      'o','Color',colors(k,:),'MarkerSize',4)
  end
  axis tight; v=axis;
end
for k=1:length(CaseList); Case=CaseList(k);
  if exist('opt','var')&strcmp(opt,'crude')
    %plot linear interpolation
    H=min(60,[Stop{Case}.leftRange(:,1); mean(Stop{Case}.downRange(1:end,2:3),2)]);
    A=[mean(Stop{Case}.leftRange(:,2:3),2); Stop{Case}.downRange(1:end,1)];
    [H,ix]=sort(H);
    A=A(ix);
    plot(A,H,'Color',colors(k,:),'LineWidth',1,'DisplayName',Title{Case});%,linetype(1:length(CaseList),k));
  else
    %plot Pouliquen fit
    plot(Astop{Case},Hstop{Case},'Color',colors(k,:),'LineWidth',1,'DisplayName',Title{Case});%,linetype(1:length(CaseList),k));
    disp([ 'Case: ' num2str(Case) ', \delta_i: ' num2str(atand(CoeffHstop{Case}(1:2)))  ', A: ' num2str(CoeffHstop{Case}(3)) ])
  end
end
xlabel('$\theta$')
ylabel('$h/d$')
C=get(gca,'Children');
legend(C(length(CaseList):-1:1))
axis tight
ylim([0 53])
set(legend,'Box','off')

if exist('opt','var')&strcmp(opt,'errorbars'),  
  for k=1:length(CaseList); Case=CaseList(k);
    %for each flowing case, look left and down for next static case
    downRange=Stop{Case}.downRange(2:end,:);
    leftRange=Stop{Case}.leftRange;
    errorbar(downRange(:,1),(downRange(:,3)+downRange(:,2))/2,(downRange(:,3)-downRange(:,2))/2,'LineStyle','none','Color',colors(k,:))
    h=herrorbar((leftRange(:,3)+leftRange(:,2))/2,leftRange(:,1),(leftRange(:,3)-leftRange(:,2))/2);
    set(h(2),'LineStyle','none')
    set(h(1),'Color',colors(k,:))
  end
  axis tight; v=axis;
else
  if length(CaseList)<3
    for k=1:length(CaseList); Case=CaseList(k);
      %for each flowing case, look left and down for next static case
      downRange=Stop{Case}.downRange(2:end,:);
      leftRange=Stop{Case}.leftRange;
      %plot(downRange(:,1),(downRange(:,3)+downRange(:,2))/2,'x','Color',colors(k,:))
      %plot((leftRange(:,3)+leftRange(:,2))/2,leftRange(:,1),'x','Color',colors(k,:));
      errorbar(downRange(:,1),(downRange(:,3)+downRange(:,2))/2,(downRange(:,3)-downRange(:,2))/2,...
        'LineStyle','none','Color',colors(k,:))
      h=herrorbar((leftRange(:,3)+leftRange(:,2))/2,leftRange(:,1),(leftRange(:,3)-leftRange(:,2))/2);
      set(h(2),'LineStyle','none')
      set(h,'Color',colors(k,:))
    end
    axis tight; v=axis;
  else
%     for k=1:length(CaseList); Case=CaseList(k);
%       %for each flowing case, look left and down for next static case
%       downRange=Stop{Case}.downRange;
%       downRange(1,3)=downRange(1,2);
%       leftRange=Stop{Case}.leftRange;
% %       plot(downRange(:,1),(downRange(:,3)+downRange(:,2))/2,'x','Color',colors(k,:))
% %       plot((leftRange(:,3)+leftRange(:,2))/2,leftRange(:,1),'x','Color',colors(k,:));
% 
%       [x,ix]=sort([downRange(:,1);(leftRange(:,3)+leftRange(:,2))/2]);
%       y=[(downRange(:,3)+downRange(:,2))/2;leftRange(:,1)];
%       plot(x,y(ix),['' markers(Case)],'Linewidth',1,'Color',colors(Case,:),'MarkerSize',4);
% 
%       %errorbar(downRange(:,1),(downRange(:,3)+downRange(:,2))/2,(downRange(:,3)-downRange(:,2))/2,'LineStyle','none','Color',colors(k,:))
%       %h=herrorbar((leftRange(:,3)+leftRange(:,2))/2,leftRange(:,1),(leftRange(:,3)-leftRange(:,2))/2);
%       %set(h(2),'LineStyle','none')
%       %set(h(1),'Color',colors(k,:))
%     end
  end
end

% for k=1:length(CaseList); Case=CaseList(k);
%     a1=max(Flow{Case}.Angle(Flow{Case}.Steady));
%     a2=min(Flow{Case}.Angle(Flow{Case}.Accelerating));
%     if ~isempty(a1)&~isempty(a2)
%       plot([1 1]*(a1+a2)/2+0.01*k,ylim,'-.','DisplayName','$h_{acc}$','Color',colors(k,:));
%       y=(1-0.1*k)*max(ylim);
%       x=(a1+a2)/2;
%       if (Case==15)
%           text((a1+a2)/2,y,['$\leftarrow ' Title{Case}(2:end-1) '$'],'HorizontalAlignment','left','FontSize',8)
%       else
%           text((a1+a2)/2,y,['$' Title{Case}(2:end-1) '\rightarrow$'],'HorizontalAlignment','right','FontSize',8)
%       end
%     end
%     %a=min(Astop{Case}(Hstop{Case}<y))
%     %text(a,y,['$\longleftarrow ' Title{Case}(2:end-1) '$'],'HorizontalAlignment','left')
% end
% axis tight
% legend('off')

ylim([0 max(ylim)])
return

function plot_simple_hstop_diagram(CaseList,opt) 
%load data
global Data Astop Hstop Astop_exp Title Stop Flow CoeffHstop
M=5;

%new figure
if (CaseList(1)==9); 
  open('hstopOldMu0.5.fig'); hold on; 
elseif (CaseList(1)==1); 
  open('hstopOldMu0.fig'); hold on; 
else
  sfigure(); clf; hold on
end
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['hstop_diagram' num2str(CaseList,'_%d')])
if ~exist('CaseList','var'), CaseList=1:length(Data); end

%create subplots
if ~exist('opt','var')|~strcmp(opt,'errorbarsonly')
  for k=1:length(CaseList); Case=CaseList(k);
    plot(Stop{Case}.Angle(Stop{Case}.static),Stop{Case}.Height(Stop{Case}.static),...
      'or','MarkerFaceColor','r','MarkerSize',M); 
    hold on
    plot(Stop{Case}.Angle(~Stop{Case}.static),Stop{Case}.Height(~Stop{Case}.static),...
      'or','MarkerSize',M)
  end
  axis tight; v=axis;
end
for k=1:length(CaseList); Case=CaseList(k);
  %plot Pouliquen fit
  plot(Astop{Case},Hstop{Case},'Color','r','LineWidth',1,'DisplayName',Title{Case});%,linetype(1:length(CaseList),k));
  disp([ 'Case: ' num2str(Case) ', \delta_i: ' num2str(atand(CoeffHstop{Case}(1:2)))  ', A: ' num2str(CoeffHstop{Case}(3)) ])
end

for k=1:length(CaseList); Case=CaseList(k);
  Height=cellfun(@(data)data.FlowHeight,Data{Case}.flow);
  Angle=cellfun(@(data)data.ChuteAngle,Data{Case}.flow);
  for H=unique(Flow{Case}.H)'
    ind = Flow{Case}.H==H & Flow{Case}.Flowing;
    plot(Angle(ind),Height(ind),'or','MarkerSize',M);
  end
end

xlabel('$\theta$')
ylabel('$h/d$')
C=get(gca,'Children');
legend(C([3,9]),[Title{CaseList} ' new'],[Title{CaseList} ' old'])
axis tight
set(legend,'Box','off')
ylim([0 55])
xlim([18 29])
return

% plot in Angle-Height parameter space with color indicating arresting or
% flowing; data is grouped by the three case studies
function plot_hstop_all(CaseList) 
%load data
global Data Astop Hstop Astop_exp Title Stop Flow

%new figure
figure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['hstop_diagram' num2str(CaseList,'_%d')])
colors=lines(length(CaseList));
if length(CaseList)==1
  colors=[0 0 0];
end

%create subplots
if ~exist('CaseList','var'), CaseList=1:length(Data); end

if ~exist('hstoponly','var')
  for k=1:length(CaseList); Case=CaseList(k);
    Height=cellfun(@(data)data.FlowHeight,Data{Case}.hstop);
    Angle=cellfun(@(data)data.ChuteAngle,Data{Case}.hstop);

    %  	static=cellfun(@(data)data.Ene.Kin(end-1)/data.Ene.Ela(end-1)<1e-5,Data{Case}.hstop);
    static=Stop{Case}.static;
    ix=static&(Stop{Case}.H>=4);
    plot(Angle(ix),Height(ix),'o','Color',colors(k,:),'MarkerFaceColor',colors(k,:),...
      'DisplayName','arrested','MarkerSize',4); 
    ix=static&(Stop{Case}.H<4);
    plot(Angle(ix),Height(ix),'s','Color',colors(k,:),'MarkerFaceColor',colors(k,:),...
      'DisplayName','arrested','MarkerSize',4); 
    hold on
    if (Case==2)
      plot(Angle(~static),Height(~static),'x','Color',colors(k,:),'MarkerSize',4)
    else
      ix=~static&(Stop{Case}.H>=4);
      plot(Angle(ix),Height(ix),'o','Color',colors(k,:),'MarkerSize',4)
      ix=~static&(Stop{Case}.H<4);
      plot(Angle(ix),Height(ix),'s','Color',colors(k,:),'MarkerSize',4)
    end
  end
  axis tight; v=axis;
end

Height=cellfun(@(data)data.FlowHeight,Data{Case}.flow);
Angle=cellfun(@(data)data.ChuteAngle,Data{Case}.flow);

% plot(Angle,Height,'k.','MarkerFaceColor','g'); 
plot(Angle(Flow{Case}.Accelerating&Flow{Case}.A<31&Flow{Case}.A>29),Height(Flow{Case}.Accelerating&Flow{Case}.A<31&Flow{Case}.A>29),'k*','MarkerFaceColor','g',...
  'DisplayName','accelerating','MarkerSize',4); 
plot(Angle(Flow{Case}.Steady&~Flow{Case}.Oscillating),Height(Flow{Case}.Steady&~Flow{Case}.Oscillating),'ko',...
  'DisplayName','steady','MarkerSize',4); 
plot(Angle(Flow{Case}.Oscillating&~Flow{Case}.FullyOscillating),Height(Flow{Case}.Oscillating&~Flow{Case}.FullyOscillating),'kx',...
  'DisplayName','layered','MarkerSize',4); 
plot(Angle(Flow{Case}.FullyOscillating),Height(Flow{Case}.FullyOscillating),'kd',...
  'DisplayName','oscillating','MarkerSize',4); 
%plot(Angle(static),Height(static),'ko','MarkerFaceColor','k')
for k=1:length(CaseList); Case=CaseList(k);
  plot(Astop{Case},Hstop{Case},'Color',colors(k,:),...
      'DisplayName','$h_{stop}(\theta)$'); 
  if false
    plot(Astop_exp{Case},Hstop{Case},':','Color',colors(k,:),...
      'DisplayName','$h_{stop}`(\theta)$'); 
  end
end


xlabel('$\theta$')
ylabel('$h/d$')
axis tight; v=axis; xlim([18 30]); ylim([0 v(4)])
set(legend,'Box','off')

if (false)%length(CaseList)==1
  for k=1:length(CaseList); Case=CaseList(k);
    %for each flowing case, look left and down for next static case
    downRange=Stop{Case}.downRange(2:end,:);
    leftRange=Stop{Case}.leftRange;
    %plot(downRange(:,1),(downRange(:,3)+downRange(:,2))/2,'x','Color',colors(k,:))
    %plot((leftRange(:,3)+leftRange(:,2))/2,leftRange(:,1),'x','Color',colors(k,:));
    errorbar(downRange(:,1),(downRange(:,3)+downRange(:,2))/2,(downRange(:,3)-downRange(:,2))/2,'LineStyle','none','Color',[.8 .8 .8])
    h=herrorbar((leftRange(:,3)+leftRange(:,2))/2,leftRange(:,1),(leftRange(:,3)-leftRange(:,2))/2);
    set(h(2),'LineStyle','none')
    set(h(1),'Color',[.8 .8 .8])
  end
  axis tight; v=axis;
end


return

function figure_shape_Strain();
%load data
global Data Astop Hstop Astop_exp Title Stop Flow
CaseList=[5 3 1];
colors=lines(max(CaseList));

%new figure
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['strain' num2str(CaseList,'_%d')])

linetype={'-','-','-',':','-',':',};
linewidth=[1 1 1 1 1 1];
for Case=CaseList
  k=find(Flow{Case}.A==24&Flow{Case}.H==30);
  if (Case==1) k=find(Flow{Case}.A==24&Flow{Case}.H==30); end
  meanVelocityX = mean(Data{Case}.flow{k}.VelocityX(Data{Case}.flow{k}.z>Data{Case}.flow{k}.Base&Data{Case}.flow{k}.z<Data{Case}.flow{k}.Surface));
  strain=smooth(deriv(Data{Case}.flow{k}.VelocityX./meanVelocityX,Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight),1);
  meanStrain(Case) = mean(strain(Data{Case}.flow{k}.z>Data{Case}.flow{k}.Base&Data{Case}.flow{k}.z<Data{Case}.flow{k}.Surface));
%   strain=strain./meanStrain(Case); meanStrain(Case)=1;
  h=plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,...
    strain,linetype{Case},...
    'Color',colors(Case,:),...
    'LineWidth',linewidth(Case),...
    'DisplayName',Title{Case});
  xlabel('$(z-b)/h$','Interpreter','none')
  ylabel('$h/\bar{u}\partial_zu$','Interpreter','none')
  axis tight; v=axis; axis([0 1 0 3.5]);

end

Case=5;
z=0:.001:1;
h=Data{Case}.flow{k}.FlowHeight;
b=2.5/h;
s=(h-5)/h;
du=5/2*(1-z).^0.5;
du(z>s)=5/2*(1-s).^0.5;
du(z<b)=5/2*(1-b).^0.5*(1-2/3*(b-z(z<b))/b);
du=du/mean(du)*meanStrain(Case);
plot(z,du,'-k','Linewidth',2,'Color',colors(Case,:),'DisplayName',['fit ' Title{Case}]);
Case=1;
du=1./(2*z);
du(z<0.5/h)=h;
u=cumsum(du)*diff(z(1:2));
du=du/mean(du)*meanStrain(Case);
plot(z,du,'-k','Linewidth',2,'Color',colors(Case,:),'DisplayName',['fit ' Title{Case}]);
Case=3;
du=1.3-z;
du=du/mean(du)*meanStrain(Case);
plot(z,du,'-k','Linewidth',2,'Color',colors(Case,:),'DisplayName',['fit ' Title{Case}]);

legend('show')
set(legend,'Box','off')
C=get(gca,'Children')
set(gca,'Children',C(end:-1:1))
return

function plot_silbert_comparison() 
plot_hstop_all(9);
set(gcf,'FileName','silbert_comparison')
hstop = [5 10 20 30 40];
astop = [24 22 20.5 19.5 19];
hold on; 
% plot(astop,hstop,'rx',...
%   'DisplayName','Silbert data');
 
global Astop Hstop
y=ylim;
h=0.95*y(2);
a=min(Astop{9}(Hstop{9}<h))
text(a,h,['$\leftarrow h_{stop}$'],'HorizontalAlignment','left')

plot([29 29],[1.8,max(ylim)],'k:','DisplayName','$\theta_{acc}$');
text(29,h,['$\theta_{acc}\rightarrow$'],'HorizontalAlignment','right')

h=1:.1:50;
t1=tand(19.55);
t2=tand(27.75);
l=8.5;
a=atand(t1+(t2-t1)*exp(-h/l));
plot(a,h,'Color',.8*[1 1 1],'DisplayName','Silberts Hertzian');

axis([18 30.001 0 53])

return

function plot_phasediagram(Case) 
plot_hstop_all(Case);
set(gcf,'FileName',['phasediagram' num2str(Case,'_%d')])

global Data Flow
Height=cellfun(@(data)data.FlowHeight,Data{Case}.flow);
Angle=cellfun(@(data)data.ChuteAngle,Data{Case}.flow);
for H=unique(Flow{Case}.H(Flow{Case}.Flowing))'
  ind = Flow{Case}.H==H & Flow{Case}.Flowing;
  plot(Angle(ind),Height(ind),'-.','Color',[1 1 1]*.7);
  y=Height(Angle==28&ind);
  if ~isempty(y)
    text(30,y,['$H=' num2str(H) '$'],'HorizontalAlignment','right')
  end
end

return


% plots the flow rule for first case study
% details can be seen in the first graph, where the hstop data and fit is
% plotted for each case in a separate subplot
% then it produces the hstop fit for each case into one graph
% the third graph shows the comparison Pouliquen flow rule; we assume that
% the curve is linear
function plot_flow_rule_Hstop_fit(CaseList,jenkinscorrection) 
%load data
global Data Title CoeffHstop Flow Compare

%new figure
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['flow_rule_hstop_fit' num2str(CaseList,'_%d')])
if exist('jenkinscorrection','var')
  set(gcf,'FileName',['flow_rule_hstop_fit_jenkins' num2str(CaseList,'_%d')])
end

%create subplots
if ~exist('CaseList','var'), CaseList=1:length(Data); end
% ny=ceil(sqrt(length(CaseList)));
% nx=ceil(length(CaseList)/ny);
ny=length(CaseList);
nx=ceil(length(CaseList)/ny);

for k=1:length(CaseList); Case=CaseList(k);
  subplot(nx,ny,k)
  
  %which hstop we compare with
  cmp=Compare;
  
  %hstop
  data=Data{Case}.hstop;
  FlowHeight=cellfun(@(data)data.FlowHeight,data);
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,data);
  Angle=cellfun(@(data)data.ChuteAngle,data);
  GravityZ=cellfun(@(data)data.Gravity(3),data);
  HeightOverHstop=FlowHeight./hstop(CoeffHstop{Case},Angle); 
  Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
  ind=Angle>atand(CoeffHstop{Case}(1))&Angle<atand(CoeffHstop{Case}(2));
%   scatter(HeightOverHstop(ind),Froude(ind),5,Angle(ind),'filled')
  plot(HeightOverHstop(ind),Froude(ind),'k.')

  hold on
  
  %flow
  FlowHeight=cellfun(@(data)data.FlowHeight,Data{Case}.flow);
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,Data{Case}.flow);
  Angle=cellfun(@(data)data.ChuteAngle,Data{Case}.flow);
  GravityZ=cellfun(@(data)data.Gravity(3),Data{Case}.flow);
  %defined for the rough bottom
  HeightOverHstop=FlowHeight./hstop(CoeffHstop{cmp},Angle); 
  if exist('jenkinscorrection','var')
    HeightOverHstop=HeightOverHstop.*tand(Angle).^2/CoeffHstop{cmp}(1)^2;
  end
  Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
  indAll=Angle>atand(CoeffHstop{cmp}(1))&Angle<atand(CoeffHstop{cmp}(2))&Flow{Case}.Steady&~Flow{Case}.Accelerating;
%   scatter(HeightOverHstop(ind),Froude(ind),'kx')
  %   scatter(HeightOverHstop(ind),Froude(ind),20,Angle(ind),'filled')
  indS=indAll&~Flow{Case}.Oscillating;
  plot(HeightOverHstop(indS),Froude(indS),'ko');
  indL=indAll&Flow{Case}.Oscillating&~Flow{Case}.FullyOscillating;
  plot(HeightOverHstop(indL),Froude(indL),'kx')
  indO=indAll&Flow{Case}.FullyOscillating;
  plot(HeightOverHstop(indO),Froude(indO),'kd')
  
  %fit
  if exist('jenkinscorrection','var')
    betaPO=Flow{Case}.betaPOJ;
    disp(['var=' num2str(Flow{Case}.varPOJ)])
  else
    betaPO=Flow{Case}.betaPO;
    x=sort([1; HeightOverHstop(indL)]);
    if isfield(Flow{Case},'betaPOL'), plot(x,Flow{Case}.betaPOL(1)+Flow{Case}.betaPOL(2)*x,'k:'); end
    disp(['var=' mat2str(Flow{Case}.varPO)])
  end
  x=sort([0; HeightOverHstop(indS)]);
  plot(x,betaPO(1)+betaPO(2)*x,'k','DisplayName',Title(Case));
  
  axis tight
  v = axis; axis([0 v(2) 0 v(4)])
  
  if length(CaseList)>1
    v=axis;
    title(Title{Case},'Interpreter','none')
    text(0.1*(v(2)),0.9*(v(4)),['$\beta=' num2str(betaPO(2),'%.3f') '$'],'Interpreter','none')
  end
  
  if exist('jenkinscorrection','var')
    xlabel('$h/h_{stop}(\tan\theta/\tan\theta_1)^2$','Interpreter','none')
  else
    xlabel('$h/h_{stop}$','Interpreter','none','Interpreter','none')
  end
end
C=get(gca,'Children');
legend(C(length(CaseList):-2:1))
set(legend,'Location','SouthEast','Box','off')

subplot(nx,ny,1);
ylabel('$F$')

return

function plot_flow_rule_Hstop_fitF(CaseList,jenkinscorrection) 
%load data
global Data Title CoeffHstop Flow CoeffHstopF Compare

%new figure
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['flow_rule_hstop_fitF' num2str(CaseList,'_%d')])
if exist('jenkinscorrection','var')
  set(gcf,'FileName',['flow_rule_hstop_fit_jenkins' num2str(CaseList,'_%d')])
end

%create subplots
if ~exist('CaseList','var'), CaseList=1:length(Data); end
% ny=ceil(sqrt(length(CaseList)));
% nx=ceil(length(CaseList)/ny);
ny=length(CaseList);
nx=ceil(length(CaseList)/ny);

for k=1:length(CaseList); Case=CaseList(k);
  subplot(nx,ny,k)
  
  %which hstop we compare with
  
  %hstop
  data=Data{Case}.hstop;
  FlowHeight=cellfun(@(data)data.FlowHeight,data);
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,data);
  Angle=cellfun(@(data)data.ChuteAngle,data);
  GravityZ=cellfun(@(data)data.Gravity(3),data);
  HeightOverHstop=FlowHeight./hstop(CoeffHstop{Case},Angle); 
  Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
  ind=Angle>atand(CoeffHstop{Case}(1))&Angle<atand(CoeffHstop{Case}(2));
%   scatter(HeightOverHstop(ind),Froude(ind),5,Angle(ind),'filled')
  plot(HeightOverHstop(ind),Froude(ind),'k.')

  hold on
  
  %flow
  FlowHeight=cellfun(@(data)data.FlowHeight,Data{Case}.flow);
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,Data{Case}.flow);
  Angle=cellfun(@(data)data.ChuteAngle,Data{Case}.flow);
  GravityZ=cellfun(@(data)data.Gravity(3),Data{Case}.flow);
  %defined for the rough bottom
  HeightOverHstop=FlowHeight./hstop(CoeffHstopF{Case},Angle); 
  if exist('jenkinscorrection','var')
    HeightOverHstop=HeightOverHstop.*tand(Angle).^2/CoeffHstopF{Case}(1)^2;
  end
  Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
  indAll=Angle>atand(CoeffHstopF{Case}(1))&Angle<atand(CoeffHstopF{Case}(2))&Flow{Case}.Steady&~Flow{Case}.Accelerating;
%   scatter(HeightOverHstop(ind),Froude(ind),'kx')
  %   scatter(HeightOverHstop(ind),Froude(ind),20,Angle(ind),'filled')
  indS=indAll&~Flow{Case}.Oscillating;
  plot(HeightOverHstop(indS),Froude(indS),'ko');
  indL=indAll&Flow{Case}.Oscillating&~Flow{Case}.FullyOscillating;
  plot(HeightOverHstop(indL),Froude(indL),'kx')
  indO=indAll&Flow{Case}.FullyOscillating;
  plot(HeightOverHstop(indO),Froude(indO),'kd')
  
  %fit
  if exist('jenkinscorrection','var')
    betaPO=Flow{Case}.betaPOJ;
  else
    betaPO=Flow{Case}.betaPO;
    x=sort([1; HeightOverHstop(indL)]);
    if isfield(Flow{Case},'betaPOL'), plot(x,Flow{Case}.betaPOL(1)+Flow{Case}.betaPOL(2)*x,'k:'); end
  end
  x=sort([0; HeightOverHstop(indS)]);
  plot(x,betaPO(1)+betaPO(2)*x,'k','DisplayName',Title(k));

  axis tight
  v = axis; axis([0 v(2) 0 v(4)])
  
  if length(CaseList)>1
    v=axis;
    title(Title{Case},'Interpreter','none')
    text(0.1*(v(2)),0.9*(v(4)),['$\beta=' num2str(betaPO(2),'%.3f') '$'],'Interpreter','none')
  end
  
  if exist('jenkinscorrection','var')
    xlabel('$h/h_{stop}(\tan\theta/\tan\theta_1)^2$','Interpreter','none')
  else
    xlabel('$h/h_{stop}$','Interpreter','none','Interpreter','none')
  end
end
subplot(nx,ny,1);
ylabel('$F$')

return

function plot_flow_rule_Hstop_fit_color_oneplotF(CaseList,jenkinscorrection) 
%load data
global Data Title CoeffHstop CoeffHstopF Flow Compare
markers='xod*+><xod*+><xod*+><';

%new figure
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['flow_rule_hstop_fit_colorF' num2str(CaseList,'_%d')])
if exist('jenkinscorrection','var')
  set(gcf,'FileName',['flow_rule_hstop_fit_jenkins_color' num2str(CaseList,'_%d')])
end
colors=mylines(max(CaseList));

%create subplots
if ~exist('CaseList','var'), CaseList=1:length(Data); end
% ny=ceil(sqrt(length(CaseList)));
% nx=ceil(length(CaseList)/ny);
for k=1:length(CaseList); Case=CaseList(k);
  %which hstop we compare with
  
  %hstop
%   data=Data{Case}.hstop;
%   FlowHeight=cellfun(@(data)data.FlowHeight,data);
%   FlowSpeed=cellfun(@(data)data.FlowVelocityX,data);
%   Angle=cellfun(@(data)data.ChuteAngle,data);
%   GravityZ=cellfun(@(data)data.Gravity(3),data);
%   HeightOverHstop=FlowHeight./hstop(CoeffHstop{Case},Angle); 
%   Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
%   ind=Angle>atand(CoeffHstop{Case}(1))&Angle<atand(CoeffHstop{Case}(2));
%   plot(HeightOverHstop(ind),Froude(ind),'.','Color',colors(k,:))

  hold on
  
  %flow
  data=Data{Case}.flow;
  FlowHeight=cellfun(@(data)data.FlowHeight,data);
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,data);
  Angle=cellfun(@(data)data.ChuteAngle,data);
  Oscillating=cellfun(@(data)data.Oscillating,data);
  GravityZ=cellfun(@(data)data.Gravity(3),data);
  %defined for the rough bottom
  HeightOverHstop=FlowHeight./hstop(CoeffHstopF{Case},Angle); 
  if exist('jenkinscorrection','var')
    HeightOverHstop=HeightOverHstop.*tand(Angle).^2/CoeffHstopF{Case}(1)^2;
  end
  Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
  indAll=Angle>atand(CoeffHstopF{Case}(1))&Angle<atand(CoeffHstopF{Case}(2))&Flow{Case}.Steady&~Flow{Case}.Accelerating;%&Hydrostaticity<0.2;
  ind=indAll&~Flow{Case}.Oscillating;
  plot(HeightOverHstop(ind),Froude(ind),markers(Case),'Color',colors(k,:),'DisplayName',Title(Case),'MarkerSize',4)
  
  %fit
  if exist('jenkinscorrection','var')
    betaPO=Flow{Case}.betaPOJ;
  else
    betaPO=Flow{Case}.betaPO;
  end
  if Case>2
    x=sort([0; HeightOverHstop(ind)]);
    plot(x,x,'k','Color',colors(k,:),'DisplayName','fit');
  else
    x=sort([0; HeightOverHstop(ind)]);
    plot(x,x,'k','Color',colors(k,:),'DisplayName','fit');
    x=sort([0; HeightOverHstop(ind)]);
    plot(HeightOverHstop(ind),Froude(ind),...
      '.','Color',colors(k,:),'MarkerSize',5,...
      'DisplayName',[Title{Case} ' layered'])
    plot(x,Flow{Case}.betaPOlayered(1)+Flow{Case}.betaPOlayered(2)*x,...
      ':','Color',colors(k,:),...
      'DisplayName',[Title{Case} ' layered']);
  end
  
  axis tight
  v = axis; axis([0 v(2) 0 v(4)])
  
  if exist('jenkinscorrection','var')
    xlabel('$h/h_{stop}(\tan(\theta)/\tan(\theta_1))^2$')
  else
    xlabel('$h/h_{stop}$')
  end
end
child=get(gca,'Children');
legend(child(end:-2:1),'Location','North')
set(legend,'Box','off')
ylabel('$F$')
return

function plot_flow_rule_Hstop_fit_color_oneplot(CaseList,jenkinscorrection) %#ok<INUSD>
%load data
global Data Title CoeffHstop Flow Compare
markers='xod*+><xod*+><xod*+><';

%new figure
figure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['flow_rule_hstop_fit_color' num2str(CaseList,'_%d')])
if exist('jenkinscorrection','var')
  set(gcf,'FileName',['flow_rule_hstop_fit_jenkins_color' num2str(CaseList,'_%d')])
end
colors=mylines(max(CaseList));

%create subplots
if ~exist('CaseList','var'), CaseList=1:length(Data); end
% ny=ceil(sqrt(length(CaseList)));
% nx=ceil(length(CaseList)/ny);
for k=1:length(CaseList); Case=CaseList(k);
  %which hstop we compare with
  cmp=Compare;
  
  %hstop
%   data=Data{Case}.hstop;
%   FlowHeight=cellfun(@(data)data.FlowHeight,data);
%   FlowSpeed=cellfun(@(data)data.FlowVelocityX,data);
%   Angle=cellfun(@(data)data.ChuteAngle,data);
%   GravityZ=cellfun(@(data)data.Gravity(3),data);
%   HeightOverHstop=FlowHeight./hstop(CoeffHstop{Case},Angle); 
%   Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
%   ind=Angle>atand(CoeffHstop{Case}(1))&Angle<atand(CoeffHstop{Case}(2));
%   plot(HeightOverHstop(ind),Froude(ind),'.','Color',colors(k,:))

  hold on
  
  %flow
  data=Data{Case}.flow;
  FlowHeight=cellfun(@(data)data.FlowHeight,data);
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,data);
  Angle=cellfun(@(data)data.ChuteAngle,data);
  Oscillating=Flow{Case}.Oscillating;
  GravityZ=cellfun(@(data)data.Gravity(3),data);
  %defined for the rough bottom
  HeightOverHstop=FlowHeight./hstop(CoeffHstop{cmp},Angle); 
  if exist('jenkinscorrection','var')
    HeightOverHstop=HeightOverHstop.*tand(Angle).^2/CoeffHstop{cmp}(1)^2;
  end
  Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
  indAll=Angle>atand(CoeffHstop{cmp}(1))&Angle<atand(CoeffHstop{cmp}(2))&Flow{Case}.Steady;%&Hydrostaticity<0.2;
  ind=indAll&~Flow{Case}.Oscillating;
  plot(HeightOverHstop(ind),Froude(ind),markers(Case),'Color',colors(k,:),'DisplayName',Title(Case),'MarkerSize',4)
  
  %fit
  if exist('jenkinscorrection','var')
    betaPO=Flow{Case}.betaPOJ;
  else
    betaPO=Flow{Case}.betaPO;
  end
  disp([ 'Case: ' num2str(Case) ', betaPO ' num2str(betaPO') ])
%   if Case>2
    x=sort([0; HeightOverHstop(ind)]);
    plot(x,betaPO(1)+betaPO(2)*x,'k','Color',colors(k,:),'DisplayName',Title(k));
%   else
%     x=sort([0; HeightOverHstop(ind)]);
%     plot(x,betaPO(1)+betaPO(2)*x,'k','Color',colors(k,:),'DisplayName',Title(k));
%     ind=indAll&Flow{Case}.Oscillating&~Flow{Case}.FullyOscillating;
%     x=sort([0; HeightOverHstop(ind)]);
%     plot(HeightOverHstop(ind),Froude(ind),...
%       '.','Color',colors(Case,:),'MarkerSize',5,...
%       'DisplayName',[Title{Case} ' layered'])
%     plot(x,Flow{Case}.betaPOlayered(1)+Flow{Case}.betaPOlayered(2)*x,...
%       ':k','Color',colors(k,:),...
%       'DisplayName',[Title{Case} ' layered']);
%   end
  
  axis tight
  v = axis; axis([0 v(2) 0 v(4)])
  
  if exist('jenkinscorrection','var')
    xlabel('$h/h_{stop}(\tan(\theta)/\tan(\theta_1))^2$')
  else
    xlabel('$h/h_{stop}$')
  end
end
child=get(gca,'Children');
legend(child(end:-2:1),'Location','SouthEast')
set(legend,'Box','off')
ylabel('$F$')
return

function plot_flow_rule_Hstop_fit_color(CaseList,jenkinscorrection) 
%load data
global Data Title CoeffHstop Flow Compare

%new figure
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['flow_rule_hstop_fit_color' num2str(CaseList,'_%d')])
if exist('jenkinscorrection','var')
  set(gcf,'FileName',['flow_rule_hstop_fit_jenkins_color' num2str(CaseList,'_%d')])
end

%create subplots
if ~exist('CaseList','var'), CaseList=1:length(Data); end
ny=length(CaseList);
nx=ceil(length(CaseList)/ny);

for k=1:length(CaseList); Case=CaseList(k);
  subplot(nx,ny,k)
  %which hstop we compare with
  cmp=Compare(Case);
  
  %hstop
  data=Data{Case}.hstop;
  FlowHeight=cellfun(@(data)data.FlowHeight,data);
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,data);
  Angle=cellfun(@(data)data.ChuteAngle,data);
  GravityZ=cellfun(@(data)data.Gravity(3),data);
  HeightOverHstop=FlowHeight./hstop(CoeffHstop{Case},Angle); 
  Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
  ind=Angle>atand(CoeffHstop{Case}(1))&Angle<atand(CoeffHstop{Case}(2));
  scatter(HeightOverHstop(ind),Froude(ind),5,Angle(ind),'filled')
%   plot(HeightOverHstop(ind),Froude(ind),'k.')

  hold on
  
  %flow
  data=Data{Case}.flow;
  FlowHeight=cellfun(@(data)data.FlowHeight,data);
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,data);
  Angle=cellfun(@(data)data.ChuteAngle,data);
  Oscillating=cellfun(@(data)data.Oscillating,data);
  GravityZ=cellfun(@(data)data.Gravity(3),data);
  %defined for the rough bottom
  HeightOverHstop=FlowHeight./hstop(CoeffHstop{cmp},Angle); 
  if exist('jenkinscorrection','var')
    HeightOverHstop=HeightOverHstop.*tand(Angle).^2/CoeffHstop{cmp}(1)^2;
  end
  Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
  indAll=Angle>atand(CoeffHstop{cmp}(1))&Angle<atand(CoeffHstop{cmp}(2))&Flow{Case}.Steady;%&Hydrostaticity<0.2;
%   scatter(HeightOverHstop(ind),Froude(ind),'kx')
  if (Case==2), ind=Oscillating<0.5&indAll;
  else ind=Oscillating<0.25&indAll;
  end
  scatter(HeightOverHstop(indAll&~ind),Froude(indAll&~ind),20,Angle(indAll&~ind))
  scatter(HeightOverHstop(ind),Froude(ind),20,Angle(ind),'filled')
  
  %fit
  if exist('jenkinscorrection','var')
    [betaPO]=flowrule_fit_with_offset(Angle(ind), Froude(ind), HeightOverHstop(ind), CoeffHstop{k},0);
  else
    [betaPO]=flowrule_fit_with_offset(Angle(ind), Froude(ind), HeightOverHstop(ind), CoeffHstop{k},2);
  end
  x=sort([1; HeightOverHstop(ind)]);
  plot(x,betaPO(1)+betaPO(2)*x,'k');
  display(['Pouliquen slope: for case ' num2str(Case) ':' num2str(betaPO(2),'%.3f')])

  %title([Title{Case} ' \beta=' num2str(betaPO(2),'%.3f')])
%   colorbar 
  axis tight
  v = axis; axis([0 v(2) 0 v(4)])
  
  if length(CaseList)>1
    v=axis;
    title(Title{Case})
    text(0.1*(v(2)),0.9*(v(4)),['$\beta=' num2str(betaPO(2),'%.3f') '$'])
  end
  
  if exist('jenkinscorrection','var')
    xlabel('$h/h_{stop}(\tan(\theta)/\tan(\theta_1))^2$')
  else
    xlabel('$h/h_{stop}$')
  end
% ylim([0 7.5])
% xlim([0 20])
end
ylabel('$F$')
return

function plot_flow_rule_Hstop_fit_compare(Case)
%load data
global Data CoeffHstop Flow

%new figure
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['flow_rule_hstop_fit_compare' num2str(Case,'_%d')])

%create subplots
k=Case;
for l=1:2
  if l==2
    subplot(1,2,2);
    jenkinscorrection=true; %#ok<NASGU>
  else
    subplot(1,2,1)
  end
  %hstop
  data=Data{Case}.hstop;
  FlowHeight=cellfun(@(data)data.FlowHeight,data);
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,data);
  Angle=cellfun(@(data)data.ChuteAngle,data);
  GravityZ=cellfun(@(data)data.Gravity(3),data);
  HeightOverHstop=FlowHeight./hstop(CoeffHstop{Case},Angle); 
  Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
  ind=Angle>atand(CoeffHstop{Case}(1))&Angle<atand(CoeffHstop{Case}(2));
%   scatter(HeightOverHstop(ind),Froude(ind),5,Angle(ind),'filled')
  plot(HeightOverHstop(ind),Froude(ind),'kx')

  hold on
  
  %flow
  data=Data{Case}.flow;
  FlowHeight=cellfun(@(data)data.FlowHeight,data);
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,data);
  Angle=cellfun(@(data)data.ChuteAngle,data);
  Oscillating=cellfun(@(data)data.Oscillating,data);
  GravityZ=cellfun(@(data)data.Gravity(3),data);
  %defined for the rough bottom
  HeightOverHstop=FlowHeight./hstop(CoeffHstop{5},Angle); 
  if exist('jenkinscorrection','var')
    HeightOverHstop=HeightOverHstop.*tand(Angle).^2/CoeffHstop{5}(1)^2;
  end
  Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
  indall=Angle>atand(CoeffHstop{5}(1))&Angle<atand(CoeffHstop{5}(2))&Flow{Case}.Steady;%&Hydrostaticity<0.2;
%   scatter(HeightOverHstop(ind),Froude(ind),20,Angle(ind))
  if (Case==2), ind=Oscillating<0.5&indall;
  else ind=Oscillating<0.25&indall;
  end
%   scatter(HeightOverHstop(ind),Froude(ind),20,Angle(ind),'filled')
  plot(HeightOverHstop(indall&~ind),Froude(indall&~ind),'ko')
  plot(HeightOverHstop(ind),Froude(ind),'kx')
  
  %fit
  if exist('jenkinscorrection','var')
    [betaPO]=flowrule_fit_with_offset(Angle(ind), Froude(ind), HeightOverHstop(ind), CoeffHstop{k},0);
  else
    [betaPO]=flowrule_fit_with_offset(Angle(ind), Froude(ind), HeightOverHstop(ind), CoeffHstop{k},1);
  end
  x=sort(HeightOverHstop(ind));
  plot(x,betaPO(1)+betaPO(2)*x,'k');

  %title([Title{Case} ' \beta=' num2str(betaPO(2),'%.3f')])
%   colorbar
  axis tight
  v = axis; axis([0 v(2:4)])
  
  if exist('jenkinscorrection','var')
    xlabel('$h/h_{stop}(\tan(\theta)/\tan(\theta_1))^2$')
  else
    display(['Pouliquen slope: for case ' num2str(Case) ': ' num2str(betaPO(2),'%.3f')])
    xlabel('$h/h_{stop}$')
  end
  ylabel('$F$')
end

return

function plot_flow_rule_H_nonsteady(CaseList)
%load data
global Data Flow

%new figure
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['flow_rule_H_nonsteady' num2str(CaseList,'_%d')])

%create subplots
if ~exist('CaseList','var'), CaseList=1:length(Data); end
ny=ceil(sqrt(length(CaseList)));
nx=ceil(length(CaseList)/ny);
markers='xo+*sdph^v<>';

for k=1:length(CaseList); Case=CaseList(k);
  subplot(nx,ny,k)
  
  data=Data{Case}.flow;
  FlowHeight=cellfun(@(data)data.FlowHeight,data);
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,data);
%   Angle=cellfun(@(data)data.ChuteAngle,data);
%   Hydrostaticity=cellfun(@(data)data.Hydrostaticity,data);
  GravityZ=cellfun(@(data)data.Gravity(3),data);
  Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
  Angles=unique(Flow{Case}.A(Flow{Case}.A>29.5));
  colors=lines(length(Angles));
  for j=1:length(Angles)
    ind=find((Angles(j)==Flow{Case}.A))';
    plot(FlowHeight(ind),Froude(ind),['-' markers(j)],'Color',colors(j,:),...
      'DisplayName',['$\theta=' num2str(Angles(j)) '^\circ$']);
  end
  legend('show','Location','NorthEast')
  set(legend,'Box','off')
  axis tight
  v = axis; axis([0 1.2*v(2) 0 v(4)])
  xlabel('h')
  ylabel('F')
end

return

function plot_flow_rule_H(CaseList)
%load data
global Data Flow Astop Hstop CoeffHstop CoeffHstopF Title

markers='xod*+><xod*+><xod*+><';
colors=mylines(max(CaseList));

%new figure
h=sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['flow_rule_H' num2str(CaseList,'_%d')])

%create subplots
if ~exist('CaseList','var'), CaseList=1:length(Data); end
ny=length(CaseList);
nx=2;

for k=1:length(CaseList); Case=CaseList(k);
  subplot(nx,ny,k); hold on
  
  data=Data{Case}.flow;
  FlowHeight=cellfun(@(data)data.FlowHeight,data);
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,data);
%   Angle=cellfun(@(data)data.ChuteAngle,data);
%   Hydrostaticity=cellfun(@(data)data.Hydrostaticity,data);
  GravityZ=cellfun(@(data)data.Gravity(3),data);
  Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
  Angles=unique(Flow{Case}.A(Flow{Case}.Steady&~Flow{Case}.Oscillating));
  colors=lines(length(Angles));
  hs=[];
  for j=1:length(Angles)
    ind=find((Angles(j)==Flow{Case}.A)&Flow{Case}.Steady&~Flow{Case}.Oscillating)';
    plot(FlowHeight(ind),Froude(ind),'o-','Color',colors(j,:),...
      'DisplayName',['$\theta=' num2str(Angles(j)) '^\circ$']);
    %fit to linear function
    coeff=[ones(size(FlowHeight(ind))) FlowHeight(ind)]\Froude(ind);
%     hs(j,1)=1./coeff(2);
%     gamma(j,1)=1./coeff(1);
    %fit to linear function through origin
    coeff=FlowHeight(ind)\Froude(ind);
    hs(j,1)=1./coeff;
  end
  legend('show','Location','EastOutside')
  axis tight
  v = axis; axis([0 v(2) 0 v(4)])
  xlabel('h')
  ylabel('F')

  subplot(nx,ny,1+length(CaseList)); hold on
  ix=hs>0;
%   ix(1)=false;
  hs=hs(ix);
  Angles=Angles(ix);
  plot(Angles,hs/5,'x','MarkerSize',10,'Color',colors(k,:),'DisplayName',Title{Case})
    
  a=linspace(min(Angles),max(Angles),50);
  h=@(c) c(3)*(tand(c(2))-tand(a))./(tand(a)-tand(c(1)));
%   tantheta=@(x,h) (h*tand(x(1))+x(3)*tand(x(2)))./(h+x(3));
%   c1 = fminsearch(@(x) norm(tand(Angles)-tantheta(x,hs)),[20, 40, 5],optimset('MaxFunEvals',5000));
%   plot(a,h(c1),'r');

  s = fitoptions('Method','NonlinearLeastSquares',...
    'Lower',[0,max(Angles),0],...
    'Upper',[min(Angles),90,Inf],...
    'Startpoint',[0 45 5]);

     f = fittype('(x*tand(a)+c*tand(b))/(x+c)','options',s);
     c2 = fit(hs,tand(Angles),f);
%     plot(a,h(coeffvalues(c2)),'b');
    disp(['Case' num2str(Case) ':'])
%    disp(['Ffit ' num2str(coeffvalues(c2))])
  
  
  f = fittype('c*(tand(b)-x)/(x-tand(a))','options',s);
  c3 = fit(tand(Angles),hs,f);
  plot(a,h(coeffvalues(c3))/5,'Color',colors(k,:),'DisplayName',Title{Case});
   disp(['Ffi3 ' num2str(coeffvalues(c3))])

  c=coeffvalues(c2);
  CoeffHstopF{Case}=[tand(c(1:2)) c(3)];

  s = fitoptions('Method','NonlinearLeastSquares',...
    'Lower',[0],...
    'Upper',[Inf],...
    'Startpoint',[5]);
  c4=atand(CoeffHstop{Case});
  t1=CoeffHstop{Case}(1); t2=CoeffHstop{Case}(2);
  f = fittype('a*(t2-x)/(x-t1)','problem',{'t2','t1'},'options',s);
  coeff = fit(tand(Angles),hs,f,'problem',{t2,t1});
  c4=[atand(CoeffHstop{Case}(1:2)) coeffvalues(coeff)];
  plot(a,h(c4),'g');
  disp(['hfit' num2str(c4)])
 end
legend('show')
return

function plot_depthprofile_(Field,Case)
global Data Title Flow
sfigure(); clf;
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['depthprofile_' Field num2str(Case,'_%d')])

Heights=unique(Flow{Case}.H);
Angles=unique(Flow{Case}.A);
colors=lines(length(Angles));
for j=1:length(Data{Case}.flow) 
%   if Data{Case}.flow{j}.Hydrostaticity<0.1&Flow{Case}.H(j)>.8*Flow{Case}.Height(j)
  if Flow{Case}.H(j)>.5*Flow{Case}.Height(j)
    subplot(1,length(Heights),find(~(Heights-Flow{Case}.H(j))));
    color=colors(Angles==Flow{Case}.A(j),:);
    plot(Data{Case}.flow{j}.z,Data{Case}.flow{j}.(Field),'Color',color,'DisplayName',num2str(Flow{Case}.A(j)));
    hold on
    title([Title{Case} ' H' num2str(Flow{Case}.H(j))]);
    axis tight
  end
end
for j=1:length(Heights)
  subplot(1,length(Heights),j)
  legend('show')
end
return

function plot_scaleddepthprofile(Field,Case) 
global Data Title Flow
sfigure(); clf;
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['depthprofile_' Field num2str(Case,'_%d')])

Heights=unique(Flow{Case}.H);
Angles=unique(Flow{Case}.A);
colors=lines(6);
for j=1:length(Data{Case}.flow) 
  if Data{Case}.flow{j}.Hydrostaticity<0.1&&Flow{Case}.H(j)>.8*Flow{Case}.Height(j)
    subplot(1,length(Heights),find(~(Heights-Flow{Case}.H(j))));
    color=colors(Angles==Flow{Case}.A(j),:);
    plot(Data{Case}.flow{j}.z./Data{Case}.flow{j}.FlowHeight, Data{Case}.flow{j}.(Field),'Color',color)
    hold on
    title([Title{Case} ' H' num2str(Flow{Case}.H(j))]);
    axis tight
  end
end
return

function plot_ene(CaseList,H) 
global Data Title Flow
load_Ene();
for Case=CaseList
  sfigure(); clf;
  set(gcf,'Position',[0  680 560 420])
  set(gcf,'FileName',['ene_' num2str(CaseList,'_%d')])
  Heights=unique(Flow{Case}.H);
  if exist('H','var'), ind=find(Flow{Case}.H==H)';
  else ind=1:length(Flow{Case}.H); end
  Angles=unique(Flow{Case}.A);
  ny=ceil(sqrt(length(ind)));
  nx=ceil(length(ind)/ny);
  [A,ix]=sort(Flow{Case}.A(ind));
  ind=ind(ix);
  
  colors=lines(length(ind))
  for k=1:length(ind); j=ind(k);
%     subplot(ny,nx,k);
    semilogy(Data{Case}.flow{j}.Ene.Time(2:end-1),Data{Case}.flow{j}.Ene.Kin(2:end-1)./mean(Data{Case}.flow{j}.EneShort.Ela(2:end-1)),...
      'Color',colors(k,:),...
      'DisplayName',num2str(Data{Case}.flow{j}.ChuteAngle));
    hold on
    semilogy(Data{Case}.flow{j}.EneShort.Time(2:end-1),Data{Case}.flow{j}.EneShort.Kin(2:end-1)./mean(Data{Case}.flow{j}.EneShort.Ela(2:end-1)),...
      'Color',colors(k,:));
    title([Title{Case} ' H' num2str(Flow{Case}.H(j)) ' A' num2str(Flow{Case}.A(j))])
    axis tight
    v=axis;
%     set(gca,'XLim',[1800 v(2)])
%     set(gca,'YLim',[min(1,v(3)) v(4)])
  end
  children=get(gca,'Children')
  name=get(children(2:2:end),'DisplayName');
  legend(children(2:2:end),name,'Location','NorthWest','Box','off')
end
return

function getNu(Case) 
plot_depthprofile('Nu',Case)
return

function getVelocityX(Case) 
plot_depthprofile('VelocityX',Case)
return

function plot_depthprofile_VelocityX_A(Case) 
global Data Flow
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['depthprofile_VelocityX' num2str(Case,'_%d')])

Heights=unique(Flow{Case}.H(Flow{Case}.Steady));
Angles=unique(Flow{Case}.A(Flow{Case}.Steady));
% Heights=unique(Flow{Case}.H);
% Angles=unique(Flow{Case}.A);
colors=lines(length(Angles));

for j=1:length(Angles)
  subplot(length(Angles),1,j); hold on
  % ind=find((Angles(j)==Flow{Case}.A)&(Flow{Case}.Steady))';
  ind=find((Angles(j)==Flow{Case}.A))';
  for k=ind
    color=colors(Heights==Flow{Case}.H(k),:);
    plot(Data{Case}.flow{k}.z-Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.VelocityX-Data{Case}.flow{k}.SurfaceVelocityX,...
      'Color',color,...
      'DisplayName',['$H='  num2str(Flow{Case}.H(k)) ',\theta=' num2str(Angles(j)) '^\circ$']);
  end
  legend('show','Location','EastOutside')
  xlabel('$z-b$','Interpreter','none')
  ylabel('$v_x-v_x^s$','Interpreter','none')
  axis tight; %v=axis; axis([0 v(2:4)]);
end
return

function plot_depthprofile_VelocityX_HA(CaseList,H,A)
global Data Flow Title
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['depthprofile_VelocityX' num2str(CaseList,'_%d') '_H' num2str(H,'_%d') '_A' num2str(A,'_%d')])

colors=lines(length(CaseList));
for m=1:length(CaseList) 
  Case=CaseList(m);
  ind=find(Flow{Case}.Steady&Flow{Case}.H==H&Flow{Case}.A==A)';
  for k=ind
    color=colors(m,:);
    meanVelocityX = mean(Data{Case}.flow{k}.VelocityX(Data{Case}.flow{k}.z>0&Data{Case}.flow{k}.z<Data{Case}.flow{k}.FlowHeight));
    plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.VelocityX/meanVelocityX,...
      'Color',color,...
      'DisplayName',Title(Case));
  end
end
z=0:.05:1;
u=(5/3)*(1-(1-z).^1.5);
plot(z,u,'--k','Linewidth',2,'DisplayName','Bagnold');

legend('show')
set(legend,'Location','SouthEast')
xlabel('$(z-b)/h$','Interpreter','none')
ylabel('$v/\overline{v}$','Interpreter','none')
axis tight; v=axis; axis([0 1 v(3:4)]);
return

function plot_depthprofile_HA(CaseList,H,A)
global Data Flow Title
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['depthprofile_Nu' num2str(CaseList,'_%d') '_H' num2str(H,'_%d') '_A' num2str(A,'_%d')])

colors=lines(length(CaseList));
for m=1:length(CaseList) 
  Case=CaseList(m);
  ind=find(Flow{Case}.Steady&Flow{Case}.H==H&Flow{Case}.A==A)';
  for k=ind
    color=colors(m,:);
    plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.Nu,...
      'Color',color);
  end
end

legend(Title{CaseList},'Location','SouthEast')
xlabel('$(z-b)/h$','Interpreter','none')
ylabel('$\rho/\rho_p$','Interpreter','none')
axis tight; v=axis; axis([0 v(2:4)]);
return

function plot_depthprofile_StressZZ_HA(CaseList,H,A)
global Data Flow Title
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['depthprofile_StressZZ' num2str(CaseList,'_%d') '_H' num2str(H,'_%d') '_A' num2str(A,'_%d')])

colors=lines(length(CaseList));
for m=1:length(CaseList) 
  Case=CaseList(m);
  ind=find(Flow{Case}.Steady&Flow{Case}.H==H&Flow{Case}.A==A)';
  for k=ind
    color=colors(m,:);
    plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.StressZZ,...
      'Color',color);
  end
end

legend(Title{CaseList},'Location','SouthEast')
xlabel('$(z-b)/h$','Interpreter','none')
ylabel('$\sigma_{zz}$','Interpreter','none')
axis tight; v=axis; axis([0 v(2:4)]);
return

function plot_depthprofile_VelocityX_H(Case,H,linear) 
global Data Flow
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['depthprofile_VelocityX' num2str(Case,'_%d') '_H' num2str(H,'_%d')])

ind=find(~Flow{Case}.Accelerating&mod(Flow{Case}.A,2)==0&Flow{Case}.Flowing&Flow{Case}.H==H)';
% Angles=unique(Flow{Case}.A(ind));
[Angles,ix]=sort(Flow{Case}.A(ind));
ind=ind(ix);
colors=lines(length(Angles));
for k=ind
  color=colors(Angles==Flow{Case}.A(k),:);
  meanVelocityX = mean(Data{Case}.flow{k}.VelocityX(Data{Case}.flow{k}.z>0&Data{Case}.flow{k}.z<Data{Case}.flow{k}.FlowHeight));
  %meanVelocityX = Data{Case}.flow{k}.FlowVelocity(1);
  plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.VelocityX./meanVelocityX,...
    'Color',color,...
    'DisplayName',['$\theta=' num2str(Flow{Case}.A(k)) '^\circ$']);
end
if exist('linear','var')
  z=0:.05:1;
  u=2*z;
  plot(z,u,'k:','Linewidth',2,'DisplayName','Linear');
else
  z=0:.05:1;
  u=(5/3)*(1-(1-z).^1.5);
  plot(z,u,'k--','Linewidth',2,'DisplayName','Bagnold');
end

legend('show','Location','NorthWest')
set(legend,'Box','off')
xlabel('$(z-b)/h$','Interpreter','none')
ylabel('$v/\overline{v}$','Interpreter','none')
axis tight; v=axis; axis([0 1 v(3:4)]);
return

function plot_depthprofile_VelocityX(Case)
global Data Flow
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['depthprofile_VelocityX' num2str(Case,'_%d')])

ind=~Flow{Case}.Accelerating;
% Heights=unique(Flow{Case}.H(ind));
Angles=unique(Flow{Case}.A(ind));
colors=lines(length(Angles));
% markers='*o.ds<>';
for k=find(ind)'
  color=colors(Angles==Flow{Case}.A(k),:);
%   marker=markers(Heights==Flow{Case}.H(k));
  plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.VelocityX./Data{Case}.flow{k}.FlowVelocityX,...
    'Color',color,...
    'DisplayName',['$H=' num2str(Flow{Case}.H(k)) ', \theta=' num2str(Flow{Case}.A(k)) '^\circ$']);
end
z=0:.05:1;
u=(5/3)*(1-(1-z).^1.5);
plot(z,u,'k','Linewidth',2,'DisplayName','Bagnold');

legend('show','Location','SouthEast')
xlabel('$(z-b)/h$','Interpreter','none')
ylabel('$v/\overline{v}$','Interpreter','none')
axis tight; v=axis; axis([0 v(2:4)]);
return


function plot_depthprofile_H(Case,H)
global Data Flow
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['depthprofile_Nu' num2str(Case,'_%d') '_H' num2str(H,'_%d')])

ind=find(Flow{Case}.A<31&Flow{Case}.H==H)';
Angles=unique(Flow{Case}.A(ind));
%Angles=[22;26;30];
colors=lines(length(Angles));
for k=ind
  if sum(Angles==Flow{Case}.A(k))
    color=colors(Angles==Flow{Case}.A(k),:);
  %   plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.VelocityX./Data{Case}.flow{k}.FlowVelocityX,...
    plot((Data{Case}.flow{k}.z-Data{Case}.flow{k}.Base)./Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.Nu,...
      'Color',color,...
      'DisplayName',['$\theta=' num2str(Flow{Case}.A(k)) '^\circ$']);
  end
end
legend('show','Location','SouthEast')
xlabel('$(z-b)/h$','Interpreter','none')
ylabel('$\rho/\rho_p$','Interpreter','none')
axis tight; v=axis; axis([0 v(2:4)]);
return

function plot_depthprofile_VelocityX_H_old(Case)
global Data Flow
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['depthprofile_VelocityX' num2str(Case,'_%d')])

Heights=unique(Flow{Case}.H(Flow{Case}.Steady));
Angles=unique(Flow{Case}.A(Flow{Case}.Steady));
colors=lines(length(Angles));

for j=1:length(Heights)
  subplot(length(Heights),1,j); hold on
  ind=find((Heights(j)==Flow{Case}.H)&(Flow{Case}.Steady))';
  for k=ind
    color=colors(Angles==Flow{Case}.A(k),:);
    plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.VelocityX./Data{Case}.flow{k}.FlowVelocityX,...
      'Color',color,...
      'DisplayName',['$H=' num2str(Heights(j)) ',\theta=' num2str(Flow{Case}.A(k)) '^\circ$']);
  end
  legend('show','Location','EastOutside')
  xlabel('$(z-b)/h$','Interpreter','none')
  ylabel('$v/\overline{v}$','Interpreter','none')
  axis tight; v=axis; axis([0 v(2:4)]);
end
return

function plot_depthprofile_hydrostatic(Case,H)
global Data Flow
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['depthprofile_hydrostatic' num2str(Case,'_%d') '_H' num2str(H,'_%d')])

ind=find(~Flow{Case}.Accelerating&Flow{Case}.Flowing&Flow{Case}.H==H&Flow{Case}.Time>40)';
% Angles=unique(Flow{Case}.A(ind));
[Angles,ix]=sort(Flow{Case}.A(ind));
ind=ind(ix);
colors=lines(length(Angles));
for k=ind
  color=colors(Angles==Flow{Case}.A(k),:);
    Mass=-Data{Case}.flow{k}.Domain(6)/1.2*Data{Case}.flow{k}.ParticleDensity(1)*Data{Case}.flow{k}.ParticleVolume(1)*Data{Case}.flow{k}.Gravity(3);
    plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.StressZZ./Mass,...
    'Color',color,...
    'DisplayName',['$\theta=' num2str(Flow{Case}.A(k)) '^\circ$']);
end

legend('show','Location','NorthWest')
set(legend,'FontSize',8,'Box','off')
xlabel('$(z-b)/h$','Interpreter','none')
ylabel('$\sigma_{zz}/mg$','Interpreter','none')
title('$\sigma_{zz}$ for $H=40$')
axis tight; %v=axis; axis([0 v(2:4)]);
return

function plot_depthprofile_sigmaxx(Case,H)
global Data Flow
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['depthprofile_sigmaxx' num2str(Case,'_%d') '_H' num2str(H,'_%d')])

ind=find(~Flow{Case}.Accelerating&Flow{Case}.Flowing&Flow{Case}.H==H)';
% Angles=unique(Flow{Case}.A(ind));
[Angles,ix]=sort(Flow{Case}.A(ind));
ind=ind(ix);
colors=lines(length(Angles));
for k=ind
  color=colors(Angles==Flow{Case}.A(k),:);
    Mass=-Data{Case}.flow{k}.Domain(6)/1.2*Data{Case}.flow{k}.ParticleDensity(1)*Data{Case}.flow{k}.ParticleVolume(1)*Data{Case}.flow{k}.Gravity(3);
    plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.StressXX./Mass,...
    'Color',color,...
    'DisplayName',['$\theta=' num2str(Flow{Case}.A(k)) '^\circ$']);
end

legend('show','Location','NorthWest')
set(legend,'FontSize',8,'Box','off')
xlabel('$(z-b)/h$','Interpreter','none')
ylabel('$\sigma_{xx}/mg$','Interpreter','none')
axis tight; %v=axis; axis([0 v(2:4)]);
return

function plot_depthprofile_mu(Case,H)
global Data Flow
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['depthprofile_mu' num2str(Case,'_%d') '_H' num2str(H,'_%d')])

ind=find(~Flow{Case}.Accelerating&Flow{Case}.Flowing&Flow{Case}.H==H)';
ind=find(mod(Flow{Case}.A,2)==0&Flow{Case}.A<31&Flow{Case}.A>20&Flow{Case}.H==H)';
% Angles=unique(Flow{Case}.A(ind));
[Angles,ix]=sort(Flow{Case}.A(ind));
ind=ind(ix);
colors=lines(length(Angles));
for k=ind
  color=colors(Angles==Flow{Case}.A(k),:);
    plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.ChuteAngle-atand(-Data{Case}.flow{k}.StressXZ./Data{Case}.flow{k}.StressZZ),...
    'Color',color,...
    'DisplayName',['$\theta=' num2str(Flow{Case}.A(k)) '^\circ$']);
end

legend('show','Location','NorthWest')
set(legend,'FontSize',8,'Box','off')
xlabel('$(z-b)/h$','Interpreter','none')
ylabel('$\theta-tan^{-1}(-\sigma_{xz}/\sigma_{zz})$','Interpreter','tex')
axis tight; %v=axis; axis([0 v(2:4)]);
ylim([-1 1])
xlim([0 1])
return

function plot_depthprofile_I(Case,H)
global Data Flow
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['depthprofile_I' num2str(Case,'_%d') '_H' num2str(H,'_%d')])

ind=find(~Flow{Case}.Accelerating&Flow{Case}.Flowing&Flow{Case}.H==H)';
ind=find(mod(Flow{Case}.A,2)==0&Flow{Case}.A<31&Flow{Case}.A>20&Flow{Case}.H==H)';
% Angles=unique(Flow{Case}.A(ind));
[Angles,ix]=sort(Flow{Case}.A(ind));
ind=ind(ix);
colors=lines(length(Angles));
for k=ind
  color=colors(Angles==Flow{Case}.A(k),:);
  x=Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight;
  y=Data{Case}.flow{k}.ChuteAngle-atand(-Data{Case}.flow{k}.StressXZ./Data{Case}.flow{k}.StressZZ);
  StrainXZ=diff(smooth(Data{Case}.flow{k}.VelocityX))./diff(Data{Case}.flow{k}.z);
  StrainXZ=.5*(StrainXZ([1 1:end])+StrainXZ([1:end end]));
  %StrainZZ=smooth(StrainZZ);
  I=StrainXZ./sqrt(Data{Case}.flow{k}.StressZZ);
  plot(x,I,'Color',color,...
    'DisplayName',['$\theta=' num2str(Flow{Case}.A(k)) '^\circ$']);
end

legend('show','Location','NorthWest')
set(legend,'FontSize',8,'Box','off')
xlabel('$(z-b)/h$','Interpreter','none')
%ylabel('$I=\tau_{xz}/d/\sqrt{\sigma_{zz}/\rho_c}$','Interpreter','tex')
ylabel('$I$','Interpreter','tex')
axis tight; %v=axis; axis([0 v(2:4)]);
ylim([-1 1])
xlim([0 1])
return

function plot_depthprofile_NormalStresses(Case,Height,Angle)
global Data Flow
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['depthprofile_NormalStresses' num2str(Case,'_%d') num2str(Height,'H%d') num2str(Angle,'A%d')])

k=(Height==Flow{Case}.H)&(Angle==Flow{Case}.A);
h=plot(Data{Case}.flow{k}.z,...
  [Data{Case}.flow{k}.StressXX Data{Case}.flow{k}.StressZZ Data{Case}.flow{k}.StressYY -Data{Case}.flow{k}.StressXZ]);
c=get(gca,'Children');
set(c(1),'LineStyle','--');
xlabel('$z-b$','Interpreter','none')
% set(gca,'XTick',unique([get(gca,'XTick') Data{Case}.flow{k}.Base Data{Case}.flow{k}.FlowHeight]))
% Tick=get(gca,'XTick')
% Label=get(gca,'XTickLabel')
% Label(Tick==Data{Case}.flow{k}.Base,:)=' ';
% Label(Tick==Data{Case}.flow{k}.FlowHeight,:)=' ';
% Label(Tick==Data{Case}.flow{k}.Base,1)='b';
% Label(Tick==Data{Case}.flow{k}.FlowHeight,1)='s';
% set(gca,'XTickLabel',Label)
%plot(Data{Case}.flow{k}.Base*[1 1],ylim,':k')
%plot(Data{Case}.flow{k}.FlowHeight*[1 1]-2,ylim,'--k')
%plot(Data{Case}.flow{k}.FlowHeight*[1 1],ylim,'--k')

%legend({'$\sigma_{xx}$','$\sigma_{zz}$','$\sigma_{yy}$','$-\sigma_{xz}$'}...,'base','surface'}...
%  ,'Location','NorthEast')
%set(legend,'Box','off')
X=get(h,'XData');
Y=get(h,'YData');
text(X{1}(end*0.2)*1.2,Y{1}(end*0.2),'$\leftarrow\sigma_{xx}$','HorizontalAlignment','left')
text(X{2}(end*0.2),Y{2}(end*0.2)*0.95,'$\sigma_{zz}\rightarrow$','HorizontalAlignment','right')
text(X{3}(end*0.2),Y{3}(end*0.2)*0.95,'$\sigma_{yy}\rightarrow$','HorizontalAlignment','right')
text(X{4}(end*0.2),Y{4}(end*0.2)*0.95,'$-\sigma_{xz}\rightarrow$','HorizontalAlignment','right')
axis tight; 
xlim([0 Data{Case}.flow{k}.FlowHeight])%v=axis; axis([0 1 0.5 1.5]);
return

function plot_depthprofile_NormalStresses_H(Case,Heights)
global Data Flow
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['depthprofile_NormalStresses' num2str(Case,'_%d')])

% Heights=unique(Flow{Case}.H(Flow{Case}.Steady));
Angles=unique(Flow{Case}.A(Flow{Case}.Steady&(mod(Flow{Case}.A,2)==0)));
colors=lines(length(Angles));

for j=1:length(Heights)
  subplot(length(Heights),1,j); hold on
  ind=find((Heights(j)==Flow{Case}.H)&(Flow{Case}.Steady)&mod(Flow{Case}.A,2)==0)';
  disp('we exclude the otherangles since the stats are not accurate')
  for k=ind
    color=colors(Angles==Flow{Case}.A(k),:);
    plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,...
      Data{Case}.flow{k}.StressXX./Data{Case}.flow{k}.StressZZ,...
      'Color',color,...
      'DisplayName',['$H=' num2str(Heights(j)) ',\theta=' num2str(Flow{Case}.A(k)) '^\circ$']);
  end
  legend('show','Location','EastOutside')
  xlabel('$(z-b)/h$','Interpreter','none')
  ylabel('$v/\overline{v}$','Interpreter','none')
  axis tight; v=axis; axis([0 1 0.5 1.5]);
end
return

function plot_depthprofile_NormalStressesWhalf(Case,Heights)
global DataWhalf
DataWhalf=loadstatistics([...
        '~/DRIVERS/Einder/run/old_runs/stat_flowrule_W0.5/*.stat ']); %#ok<NBRAK>
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['depthprofile_VelocityX_W05' num2str(Case,'_%d')])

% Heights=unique(Flow{Case}.H(Flow{Case}.Steady));
A=cellfun(@(data)data.ChuteAngle, DataWhalf);
Angles=unique(A);
colors=lines(length(Angles));

for j=1:length(Heights)
  subplot(length(Heights),1,j); hold on
  ind=1:length(DataWhalf);
  for k=ind
    color=colors(Angles==A(k),:);
    plot(DataWhalf{k}.z./DataWhalf{k}.FlowHeight,...
      DataWhalf{k}.StressXX./DataWhalf{k}.StressZZ,...
      'Color',color,...
      'DisplayName',['$H=' num2str(Heights(j)) ',\theta=' num2str(A(k)) '^\circ$']);
  end
  legend('show','Location','EastOutside')
  xlabel('$(z-b)/h$','Interpreter','none')
  ylabel('$v/\overline{v}$','Interpreter','none')
  axis tight; v=axis; axis([0 v(2) 0 2]);
end
return

function plot_ha_density(Case,opt)
global Data Flow
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
if exist('opt','var')&&strcmp(opt,'centre')
set(gcf,'FileName',['ha_Nu_centre' num2str(Case,'_%d')])
else
set(gcf,'FileName',['ha_Nu' num2str(Case,'_%d')])
end
markers='xo+*sdph^v<>';
ind=Flow{Case}.Flowing&~Flow{Case}.Oscillating&~Flow{Case}.Accelerating;
ind=Flow{Case}.Flowing&~Flow{Case}.Accelerating;
% ind=~Flow{Case}.Accelerating;
Heights=unique(Flow{Case}.H(ind));
Angles=unique(Flow{Case}.A(ind));
colors=lines(length(Angles));
for j=1:length(Heights)
  indH=(Heights(j)==Flow{Case}.H)&(ind);
  if exist('opt','var')&&strcmp(opt,'centre')
    DensityCentre=cellfun(@(data) mean(data.Nu(data.z>data.FlowHeight*0.33&data.z<data.FlowHeight*0.67)), Data{Case}.flow(indH));
    Density=cellfun(@(data) mean(data.Nu(data.z>data.Base&data.z<data.Surface)), Data{Case}.flow(indH));
    %Density=cellfun(@(data) sum(data.Nu)*diff(data.z(1:2))/data.FlowHeight, Data{Case}.flow(indH));
    [x,ix]=sort(Flow{Case}.A(indH));
    subplot(1,2,2); hold on
    plot(x,DensityCentre(ix),[':' markers(j)], ...
         'Color',colors(j,:),...
         'DisplayName',['H=' num2str(Heights(j))]);
    ylabel('$\rho_c/\rho_p$','Interpreter','none')
    xlabel('$\theta$','Interpreter','none')
    subplot(1,2,1); hold on
    plot(x,Density(ix),[':' markers(j)], ...
         'Color',colors(j,:),...
         'DisplayName',['H=' num2str(Heights(j))]);
  else  
    Density=cellfun(@(data) sum(data.Nu)*diff(data.z(1:2))/data.FlowHeight, Data{Case}.flow(indH));
    [x,ix]=sort(Flow{Case}.A(indH));
    plot(x,Density(ix),[':' markers(j)], ...
         'Color',colors(j,:),...
         'DisplayName',['H=' num2str(Heights(j))]);
  end
  %Density=cellfun(@(data) mean(data.Nu(data.z>0&data.z<data.FlowHeight)), Data{Case}.flow(indH));
  %exclude stopped flows
  %Alpha(Flow{Case}.Froude(indH)<0.01)=nan;
  %I=cellfun(@(data) mean(data.I(data.z>0&data.z<data.FlowHeight)), Data{Case}.flow(indH));
  %I=cellfun(@(data) data.BottomFrictionCoefficient, Data{Case}.flow(indH));
  %[x,ix]=sort(I);

end
legend('show','Location','NorthEast')
set(legend,'Box','off')
xlabel('$\theta$','Interpreter','none')
ylabel('$\bar\rho/\rho_p$','Interpreter','none')
axis tight
return

function plot_ha_density_fit(CaseList,opt)
global Data Flow Title
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
if exist('opt','var')&&strcmp(opt,'centre')
set(gcf,'FileName',['ha_Nu_centre' num2str(CaseList,'_%d')])
else
set(gcf,'FileName',['ha_Nu' num2str(CaseList,'_%d')])
end
markers='xo+*sdph^v<>';
for i=1:length(CaseList); Case=CaseList(i);
ind=Flow{Case}.Flowing&~Flow{Case}.Oscillating&~Flow{Case}.Accelerating;
% ind=~Flow{Case}.Accelerating;
Heights=unique(Flow{Case}.H(ind));
Angles=unique(Flow{Case}.A(ind));
colors=lines(length(Angles));
for j=1:length(Heights)
  indH=(Heights(j)==Flow{Case}.H)&(ind);
  %Density=cellfun(@(data) mean(data.Nu(data.z>data.Base&data.z<data.Surface)), Data{Case}.flow(indH));
  Density=cellfun(@(data) sum(data.Nu)*diff(data.z(1:2))/data.FlowHeight, Data{Case}.flow(indH));
  %Density=Density.*Flow{Case}.Height(indH);
  [x,ix]=sort(Flow{Case}.A(indH));
  if exist('opt','var')&&strcmp(opt,'centre')
    subplot(1,2,2); hold on
    DensityCentre=cellfun(@(data) mean(data.Nu(data.z>data.FlowHeight*0.33&data.z<data.FlowHeight*0.67)), Data{Case}.flow(indH));
    plot(x,DensityCentre(ix),['' markers(j)], ...
         'Color',colors(j,:),...
         'DisplayName',['$H=' num2str(Heights(j)) '$']);
    ylabel('$\rho_c/\rho_p$','Interpreter','none')
    xlabel('$\theta$','Interpreter','none')
    axis tight; %ylim([0.43 0.59])
    subplot(1,2,1); hold on
    plot(x,Density(ix),['' markers(i)], ...
         'Color',colors(j,:),...
         'DisplayName',['$H=' num2str(Heights(j)) '$']);
    axis tight; %ylim([0.43 0.59])
  else  
    %disp('cheat')
    if (j==length(Heights)&Case==5)
      plot(x,Density(ix),['--' markers(i)], ...
         'Color',colors(i,:),...
         'DisplayName',Title{Case});
    else
      plot(x,Density(ix),['' markers(i)], ...
         'Color',colors(i,:),...
         'DisplayName',Title{Case});
    end
  end
  %Density=cellfun(@(data) mean(data.Nu(data.z>0&data.z<data.FlowHeight)), Data{Case}.flow(indH));
  %exclude stopped flows
  %Alpha(Flow{Case}.Froude(indH)<0.01)=nan;
  %I=cellfun(@(data) mean(data.I(data.z>0&data.z<data.FlowHeight)), Data{Case}.flow(indH));
  %I=cellfun(@(data) data.BottomFrictionCoefficient, Data{Case}.flow(indH));
  %[x,ix]=sort(I);

end
end
c=get(gca,'Children');
%legend(c(end:-4:1))
legend(c(1:1:end))
set(legend,'Location','SouthWest')
set(legend,'Box','off')
xlabel('$\theta$','Interpreter','none')
ylabel('$\bar\rho/\rho_p$','Interpreter','none')
return

function plot_ha_density2(CaseList)
global Data Flow Title
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['ha_Nu' num2str(CaseList,'_%d')])
markers='xo+*sdph^v<>';

colors=lines(length(CaseList));
for i=1:length(CaseList), Case=CaseList(i);
  ind=~Flow{Case}.Accelerating&mod(Flow{Case}.Angle,2)==0;
  Heights=unique(Flow{Case}.H(ind));
  Angles=unique(Flow{Case}.A(ind));
    j=1;
    indH=(Heights(j)==Flow{Case}.H)&(ind);
%    Density=cellfun(@(data) sum(data.Nu)*diff(data.z(1:2))/data.FlowHeight, Data{Case}.flow(indH));
    Density=cellfun(@(data) mean(data.Nu(data.z>0&data.z<data.FlowHeight)), Data{Case}.flow(indH));
%    Density=cellfun(@(data) sum(data.Nu.^2)./sum(data.Nu), Data{Case}.flow(indH));
    [x,ix]=sort(Flow{Case}.A(indH));
    plot(x,Density(ix),['-' markers(j)], ...
         'Color',colors(i,:),...
         'DisplayName',Title{Case});
end
legend('show');
set(legend,'Box','off','Location','NorthEast')
for i=1:length(CaseList), Case=CaseList(i);
  ind=~Flow{Case}.Accelerating&mod(Flow{Case}.Angle,2)==0;
  Heights=unique(Flow{Case}.H(ind));
  Angles=unique(Flow{Case}.A(ind));
  for j=2:length(Heights)
    indH=(Heights(j)==Flow{Case}.H)&(ind);
    Density=cellfun(@(data) mean(data.Nu(data.z>0&data.z<data.FlowHeight)), Data{Case}.flow(indH));
    [x,ix]=sort(Flow{Case}.A(indH));
    plot(x,Density(ix),['-' markers(j)], ...
         'Color',colors(i,:),...
         'DisplayName',Title{Case});

  end
end

xlabel('$\theta$','Interpreter','none')
ylabel('$\bar\rho/\rho_p$','Interpreter','none')
axis tight
return


function plot_ha_density_centre_List(CaseList)
global Data Flow Title
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['ha_Nu_centre' num2str(CaseList,'_%d')])
markers='xo+*sdph^v<>';

colors=lines(length(CaseList));
for i=1:length(CaseList), Case=CaseList(i);
  ind=~Flow{Case}.Accelerating&mod(Flow{Case}.Angle,2)==0;
  Heights=unique(Flow{Case}.H(ind));
  Angles=unique(Flow{Case}.A(ind));
    j=1;
    indH=(Heights(j)==Flow{Case}.H)&(ind);
    %Density=cellfun(@(data) sum(data.Nu(data.z>data.FlowHeight*.3&data.z<data.FlowHeight*.8))*diff(data.z(1:2))/(data.FlowHeight/2), Data{Case}.flow(indH));
    Density=cellfun(@(data) mean(data.Nu(data.z>data.FlowHeight*.3&data.z<data.FlowHeight*.8)), Data{Case}.flow(indH));
    [x,ix]=sort(Flow{Case}.A(indH));
    plot(x,Density(ix),['-' markers(j)], ...
         'Color',colors(i,:),...
         'DisplayName',Title{Case});
end
legend('show');
set(legend,'Box','off','Location','NorthEast')
for i=1:length(CaseList), Case=CaseList(i);
  ind=~Flow{Case}.Accelerating&mod(Flow{Case}.Angle,2)==0;
  Heights=unique(Flow{Case}.H(ind));
  Angles=unique(Flow{Case}.A(ind));
  for j=2:length(Heights)
    indH=(Heights(j)==Flow{Case}.H)&(ind);
    Density=cellfun(@(data) mean(data.Nu(data.z>data.FlowHeight*.3&data.z<data.FlowHeight*.8)), Data{Case}.flow(indH));
    [x,ix]=sort(Flow{Case}.A(indH));
    plot(x,Density(ix),['-' markers(j)], ...
         'Color',colors(i,:),...
         'DisplayName',Title{Case});

  end
end

xlabel('$\theta$','Interpreter','none')
ylabel('$\rho_c/\rho_p$','Interpreter','none')
axis tight
return

function plot_ha_alpha(Case)
global Data Flow
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['ha_alpha' num2str(Case,'_%d')])
markers='xo+*sdph^v<>';

ind=Flow{Case}.Flowing&~Flow{Case}.Accelerating&~Flow{Case}.Oscillating&Flow{Case}.Angle<31;
% ind=~Flow{Case}.Accelerating;
Heights=unique(Flow{Case}.H(ind));
Angles=unique(Flow{Case}.A(ind));
colors=lines(length(Angles));
for j=1:length(Heights)
  indH=(Heights(j)==Flow{Case}.H)&(ind);
  %Alpha=cellfun(@(data) norm(data.FlowVelocity2(1))/norm(data.FlowVelocity(1))^2, Data{Case}.flow(indH));
  meanVelocityX2=  cellfun(@(data) sum(data.VelocityX.*data.Nu).^2./sum(data.Nu)*diff(data.z(1:2)), Data{Case}.flow(indH));
  meanSqrVelocityX= cellfun(@(data) sum(data.VelocityX.^2.*data.Nu)*diff(data.z(1:2)), Data{Case}.flow(indH));
  %meanVelocityX=  cellfun(@(data) mean(data.VelocityX(data.z>0&data.z<data.FlowHeight)), Data{Case}.flow(indH));
  %meanVelocityX2= cellfun(@(data) mean(data.VelocityX(data.z>0&data.z<data.FlowHeight).^2), Data{Case}.flow(indH));
  Alpha=meanSqrVelocityX./meanVelocityX2;
  %exclude stopped flows
  Alpha(Flow{Case}.Froude(indH)<0.01)=nan;
  [x,ix]=sort(Flow{Case}.A(indH));
  plot(x,Alpha(ix),[':' markers(j)], ...
       'Color',colors(j,:),...
       'DisplayName',['H=' num2str(Heights(j))]);

end
plot(Angles,4/3*ones(size(Angles)),'k:','Linewidth',2,'DisplayName','Linear profile');
plot(Angles,5/4*ones(size(Angles)),'k','Linewidth',2,'DisplayName','Bagnold profile');
legend('show','Location','NorthEast')
set(legend,'Box','off')
xlabel('$\theta$','Interpreter','none')
ylabel('$\alpha$','Interpreter','none')
axis tight
return

function plot_ha_alpha_List(CaseList)
global Data Flow Title
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['ha_Alpha' num2str(CaseList,'_%d')])
markers='xo+*sdph^v<>';
colors=lines(length(CaseList));

% subplot(1,2,1); hold on
% Case=5;
% ind=Flow{Case}.Flowing&~Flow{Case}.Oscillating&~Flow{Case}.Accelerating;
% Heights=unique(Flow{Case}.H(ind)); %all Heights
% Angles=unique(Flow{Case}.A(ind));
% for j=1:length(Heights)
%   indH=(Heights(j)==Flow{Case}.H)&(ind);
%    meanVelocityX=  cellfun(@(data) sum(data.VelocityX.*data.Nu)/sum(data.Nu), Data{Case}.flow(indH));
%    meanVelocityX2= cellfun(@(data) sum(data.VelocityX.^2.*data.Nu)/sum(data.Nu), Data{Case}.flow(indH));
% %     meanVelocityX=  cellfun(@(data) mean(data.VelocityX(data.z>data.Base&data.z<data.Surface)), Data{Case}.flow(indH));
% %    meanVelocityX2= cellfun(@(data) mean(data.VelocityX(data.z>data.Base&data.z<data.Surface).^2), Data{Case}.flow(indH));
%   Alpha=meanVelocityX2./(meanVelocityX.^2);
%   [x,ix]=sort(Flow{Case}.A(indH));
%   plot(x,Alpha(ix),[':' markers(Heights(j)/10)], ...
%        'Color',colors(Heights(j)/10,:),...
%          'DisplayName',['H=' num2str(Heights(j))]);
% end
% legend('show','Location','NorthEast')
% set(legend,'FontSize',8)
% plot(Angles,4/3*ones(size(Angles)),'k:','Linewidth',2,'DisplayName','Linear profile');
% plot(Angles,5/4*ones(size(Angles)),'k','Linewidth',2,'DisplayName','Bagnold profile');
% xlabel('$\theta$','Interpreter','none')
% ylabel('$\alpha$','Interpreter','none')
% axis tight
% 
% subplot(1,2,2); hold on
for i=1:length(CaseList), Case=CaseList(i);
  ind=Flow{Case}.Flowing&~Flow{Case}.Oscillating&~Flow{Case}.Accelerating&Flow{Case}.Angle>20&Flow{Case}.H>20;
  Angles=unique(Flow{Case}.A(ind));
  meanVelocityX=  cellfun(@(data) sum(data.VelocityX.*data.Nu)/sum(data.Nu), Data{Case}.flow(ind));
  meanVelocityX2= cellfun(@(data) sum(data.VelocityX.^2.*data.Nu)/sum(data.Nu), Data{Case}.flow(ind));
%     meanVelocityX=  cellfun(@(data) mean(data.VelocityX(data.z>data.Base&data.z<data.Surface)), Data{Case}.flow(indH));
 %    meanVelocityX2= cellfun(@(data) mean(data.VelocityX(data.z>data.Base&data.z<data.Surface).^2), Data{Case}.flow(indH));
  Alpha=meanVelocityX2./(meanVelocityX.^2);
  [x,ix]=sort(Flow{Case}.A(ind));
  plot(x,Alpha(ix),['' markers(i)], ...
         'Color',colors(i,:),...
       'DisplayName',Title{Case});
  plot([24 28],mean(Alpha(Flow{Case}.Angle(ind)>23))*[1 1],'--', ...
         'Color',colors(i,:),...
       'DisplayName',Title{Case});
  angles=Alpha(Flow{Case}.Angle(ind)>23);
  disp([Title{Case} ' & ' num2str(mean(angles)) ' & ' num2str(var(angles),'%f')]);
end
c=get(gca,'Children');
legend(c(2:2:end),'Location','SouthWest')
set(legend,'FontSize',8)
plot(Angles,ones(size(Angles)),'k--','Linewidth',2,'DisplayName','Plug flow');
plot(Angles,5/4*ones(size(Angles)),'k','Linewidth',2,'DisplayName','Bagnold profile');
xlabel('$\theta$','Interpreter','none')
ylabel('$\alpha$','Interpreter','none')
axis tight
% colors=lines(length(CaseList));
% colorix=1;
% for i=1:length(CaseList), Case=CaseList(i);
%   ind=Flow{Case}.Flowing&~Flow{Case}.Oscillating&~Flow{Case}.Accelerating;
%   Heights=unique(Flow{Case}.H(ind)); %all Heights
%   if (Case~=5) Heights=30; linetype=':';
%   else linetype='-'; end
%   Angles=unique(Flow{Case}.A(ind));
%   for j=1:length(Heights)
%     indH=(Heights(j)==Flow{Case}.H)&(ind);
%      meanVelocityX=  cellfun(@(data) sum(data.VelocityX.*data.Nu)/sum(data.Nu), Data{Case}.flow(indH));
%      meanVelocityX2= cellfun(@(data) sum(data.VelocityX.^2.*data.Nu)/sum(data.Nu), Data{Case}.flow(indH));
% %     meanVelocityX=  cellfun(@(data) mean(data.VelocityX(data.z>data.Base&data.z<data.Surface)), Data{Case}.flow(indH));
%  %    meanVelocityX2= cellfun(@(data) mean(data.VelocityX(data.z>data.Base&data.z<data.Surface).^2), Data{Case}.flow(indH));
%     Alpha=meanVelocityX2./(meanVelocityX.^2);
%     [x,ix]=sort(Flow{Case}.A(indH));
%     plot(x,Alpha(ix),[linetype markers(i)], ...
%          'Color',colors(Heights(j)/10,:),...
%          'DisplayName',[Title{Case}, ', H=' num2str(Heights(j))]);
%     colorix=colorix+1
%   end
% end
return

function plot_ha_K_List(CaseList)
global Data Flow Title
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['ha_K' num2str(CaseList,'_%d')])
markers='xoxxxxx';

colors=lines(length(CaseList));
for i=1:length(CaseList), Case=CaseList(i);
  ind=(Flow{Case}.Flowing)&Flow{Case}.A<30&~Flow{Case}.Oscillating;
%   ind=~Flow{Case}.Accelerating&~Flow{Case}.Oscillating&~Flow{Case}.Accelerating;
    Heights=unique(Flow{Case}.H(ind));
    Angles=unique(Flow{Case}.A(ind));
    j=1;
    indH=(Heights(j)==Flow{Case}.H)&(ind)&mod(Flow{Case}.A,2)==0;
    indH=ind;
    FlowStressXX=cellfun(@(data) mean(data.StressXX(data.z>data.Base+2&data.z<data.Surface)), Data{Case}.flow(indH));
    FlowStressZZ=cellfun(@(data) mean(data.StressZZ(data.z>data.Base+2&data.z<data.Surface)), Data{Case}.flow(indH));
    K=FlowStressXX./FlowStressZZ;
    [x,ix]=sort(Flow{Case}.A(indH));
    plot(x,K(ix),[':' markers(Case)], ...
         'Color',colors(i,:),...
         'DisplayName',Title{Case});
end
legend('show');
set(legend,'Box','off','Location','Best')
for i=1:length(CaseList), Case=CaseList(i);
  ind=(Flow{Case}.Flowing)&Flow{Case}.A<30&~Flow{Case}.Oscillating;
  Heights=unique(Flow{Case}.H(ind));
  Angles=unique(Flow{Case}.A(ind));
  for j=2:length(Heights)
    indH=(Heights(j)==Flow{Case}.H)&(ind)&mod(Flow{Case}.A,2)==0;
  FlowStressXX=cellfun(@(data) mean(data.StressXX(data.z>0&data.z<data.FlowHeight)), Data{Case}.flow(indH));
  FlowStressZZ=cellfun(@(data) mean(data.StressZZ(data.z>0&data.z<data.FlowHeight)), Data{Case}.flow(indH));
    K=FlowStressXX./FlowStressZZ;
    [x,ix]=sort(Flow{Case}.A(indH));
    plot(x,K(ix),['-' markers(Case)], ...
         'Color',colors(i,:),...
         'DisplayName',Title{Case});

  end
end

xlabel('$\theta$','Interpreter','none')
ylabel('$K$','Interpreter','none')
axis tight
return

function plot_ha_K(CaseList)
global Data Flow Title
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['ha_K' num2str(CaseList,'_%d')])

for i=1:length(CaseList); Case=CaseList(i); subplot(1,length(CaseList),i); hold on
  clear K A
  Heights=unique(Flow{Case}.H(Flow{Case}.Steady));
  Angles=unique(Flow{Case}.A(Flow{Case}.Steady));
  colors=lines(length(Angles));
  for j=1:3
    %ind=(Heights(j)==Flow{Case}.H)&(Flow{Case}.Steady)&(Flow{Case}.Flowing)&(~Flow{Case}.Oscillating);
    ind=Flow{Case}.Flowing&~Flow{Case}.Accelerating;
    if (Case~=2) 
      markers='o';
      if j==1
        ind=ind&Flow{Case}.H>9; 
        Name='$H>10$';
      elseif (j==2)
        break
      else
        break
      end        
    else
      markers='oxd';
      if j==3
        ind=ind&Flow{Case}.FullyOscillating; 
        Name='oscillating';
      elseif j==2
        ind=ind&Flow{Case}.Oscillating&~Flow{Case}.FullyOscillating; 
        Name='layered';
      else
        ind=ind&~Flow{Case}.Oscillating&Flow{Case}.H>19; 
        Name='steady';
      end        
    end

    %disp('we exclude the otherangles since the stats are not accurate')
    %ind=ind&mod(Flow{Case}.A,2)==0;
  %   FlowStressXX=cellfun(@(data) sum(data.StressXX,1)*diff(data.z([1 2]))/diff(data.Domain([5 6]))/data.FlowDensity, Data{Case}.flow(ind));
  %   FlowStressZZ=cellfun(@(data) sum(data.StressZZ,1)*diff(data.z([1 2]))/diff(data.Domain([5 6]))/data.FlowDensity, Data{Case}.flow(ind));
     %FlowStressXX=cellfun(@(data) sum(data.StressXX.*data.Density)./sum(data.Density), Data{Case}.flow(ind));
     %FlowStressZZ=cellfun(@(data) sum(data.StressZZ.*data.Density)./sum(data.Density), Data{Case}.flow(ind));
      FlowStressXX=cellfun(@(data) mean(data.StressXX(data.z>data.Base+2&data.z<data.Surface)), Data{Case}.flow(ind));
      FlowStressZZ=cellfun(@(data) mean(data.StressZZ(data.z>data.Base+2&data.z<data.Surface)), Data{Case}.flow(ind));
    [A{j},ix]=sort(Flow{Case}.A(ind));
    K{j}=FlowStressXX./FlowStressZZ;
    K{j}=K{j}(ix);

    %plot sorted
    %I=cellfun(@(data) mean(data.I(data.z>data.Base&data.z<data.Surface)), Data{Case}.flow(ind));
    %I=cellfun(@(data) data.BottomFrictionCoefficient, Data{Case}.flow(indH));
    %[x,ix]=sort(I);
    plot(A{j},K{j},['' markers(j)], ...
         'Color','k',...
         'DisplayName',Name);
  end
%   [A,ix]=sort(cell2mat(A'));
%   K=cell2mat(K');
%   K=K(ix);
  if (Case~=2)
    [p,S]=polyfit(A{1},K{1},1);
    plot(A{1},p(2)+p(1)*A{1},'k',...
         'DisplayName','fit steady');
  else
    p=polyfit(A{2},K{2},1);
    plot(A{2},p(2)+p(1)*A{2},':',...
         'DisplayName','fit layered');
    [p,S]=polyfit(A{1},K{1},1);
    plot(A{1},p(2)+p(1)*A{1},'k',...
         'DisplayName','fit steady');
  end
  disp([Title{Case} ' & ' num2str(p)]);

  xlabel('$\theta$','Interpreter','none')
  ylabel('$K$','Interpreter','none')
  if (length(CaseList)>1) title(Title{Case}); end
  axis tight
end
subplot(1,length(CaseList),length(CaseList));
% c=get(gca,'Children');
% legend(c(end:-1:3),'Location','NorthOutside')
% subplot(1,length(CaseList),1);
% c=get(gca,'Children');
% legend(c(end:-1:2),'Location','NorthOutside')
%set(legend,'Box','off')
return

function plot_ha_K_hstop(Case)
global Data Flow CoeffHstop
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['ha_K' num2str(Case,'_%d')])

Heights=unique(Flow{Case}.H(Flow{Case}.Steady));
Angles=unique(Flow{Case}.A(Flow{Case}.Steady));
colors=lines(length(Angles));
markers='xo+*sdph^v<>';
for j=1:length(Angles)
  ind=(Angles(j)==Flow{Case}.A)&(Flow{Case}.Steady)&(~Flow{Case}.Oscillating);
  disp('we exclude the otherangles since the stats are not accurate')
  ind=ind&mod(Flow{Case}.A,2)==0&~(Flow{Case}.A==20&Flow{Case}.H==30);
%   FlowStressXX=cellfun(@(data) sum(data.StressXX,1)*diff(data.z([1 2]))/diff(data.Domain([5 6]))/data.FlowDensity, Data{Case}.flow(ind));
%   FlowStressZZ=cellfun(@(data) sum(data.StressZZ,1)*diff(data.z([1 2]))/diff(data.Domain([5 6]))/data.FlowDensity, Data{Case}.flow(ind));
  FlowStressXX=cellfun(@(data) sum(data.StressXX.*data.Density)./sum(data.Density), Data{Case}.flow(ind));
  FlowStressZZ=cellfun(@(data) sum(data.StressZZ.*data.Density)./sum(data.Density), Data{Case}.flow(ind));
  K=FlowStressXX./FlowStressZZ;

  %plot sorted
  HeightOverHstop=Flow{Case}.H(ind)./hstop(CoeffHstop{Case},Flow{Case}.A(ind)); 
  H=Flow{Case}.H(ind);
  [x,ix]=sort(H);
  plot(x,K(ix),['-' markers(j)], ...
       'Color',colors(j,:),...
       'DisplayName',['A=' num2str(Angles(j))]);

end
legend('show','Location','SouthEast')
set(legend,'Box','off')
xlabel('$h/hs$','Interpreter','none')
ylabel('$K$','Interpreter','none')
axis tight
return

function plot_ha_bottom_friction(Case)
global Data Flow
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['ha_bottom_friction' num2str(Case,'_%d')])

Heights=unique(Flow{Case}.H(Flow{Case}.Steady));
Angles=unique(Flow{Case}.A(Flow{Case}.Steady));
Angles=Angles(mod(Angles,2)==0);
colors=lines(length(Angles));
for j=1:length(Heights)
  ind=(Heights(j)==Flow{Case}.H)&(Flow{Case}.Steady);
  disp('we exclude the otherangles since the stats are not accurate')
  ind=ind&mod(Flow{Case}.A,2)==0;%&~(Flow{Case}.A==20&Flow{Case}.H==30);
%   FlowStressXX=cellfun(@(data) sum(data.StressXX,1)*diff(data.z([1 2]))/diff(data.Domain([5 6]))/data.FlowDensity, Data{Case}.flow(ind));
%   FlowStressZZ=cellfun(@(data) sum(data.StressZZ,1)*diff(data.z([1 2]))/diff(data.Domain([5 6]))/data.FlowDensity, Data{Case}.flow(ind));
  BottomFriction=cellfun(@(data)data.BottomFrictionCoefficient, Data{Case}.flow(ind));
  dBottomFriction=Flow{Case}.A(ind)-atand(BottomFriction);

  %plot sorted
  [x,ix]=sort(Flow{Case}.A(ind));
  plot(x,dBottomFriction(ix),':x', ...
       'Color',colors(j,:),...
       'DisplayName',['H=' num2str(Heights(j))]);

end
legend('show','Location','NorthEast')
set(legend,'Box','off')
xlabel('$\theta$','Interpreter','none')
ylabel('$\theta-\tan^{-1}(\mu)$','Interpreter','none')
axis tight
return

function plot_arresting(Case,Height) 
%Ratio of kinetic and time-averaged elastic energy for $\lambda=0.5$,
%$H=$, and varying inclination. While
global Data Flow
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['arresting' num2str(Case,'_%d')])

ind=find(Flow{Case}.H==Height);
[Angles,ix]=sort(Flow{Case}.A(ind));
ind=ind(ix);
colors=lines(length(ind)+1);
set(gca,'YScale','log');
for j=1:length(ind); k=ind(j);
  avgEneEla=mean(Data{Case}.flow{k}.Ene.Ela(ceil(end/2):end-1));
  plot(Data{Case}.flow{k}.Ene.Time(2:end-1),...
    Data{Case}.flow{k}.Ene.Kin(2:end-1)./avgEneEla,'-',...
    'Color',colors(j,:),...
    'Tag',num2str(Flow{Case}.A(k)),...
    'DisplayName',['$\theta=' num2str(Flow{Case}.A(k)) '^\circ$']);
  hold on
end
axis tight
v=axis;
set(gca,'XLim',[0 v(2)])
set(gca,'YLim',[max(1e-0,v(3)) v(4)])
legend('show','Location','EastOutside')
xlabel('$t$','Interpreter','none')
ylabel('$E_{kin}/\langle E_{ela}\rangle$','Interpreter','none')
return

function plot_accelerating(Case,Height) 
global Data Flow
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['accelerating' num2str(Case,'_%d')])

ind=find(Flow{Case}.H==Height);
  [A,ix]=sort(Flow{Case}.A(ind));
  ind=ind(ix);

colors=lines(length(ind));
for j=1:length(ind); k=ind(j);
  %indtime=Data{Case}.flow{k}.EneShort.Time>=1500&Data{Case}.flow{k}.EneShort.Time<=2001;
  indtime=Data{Case}.flow{k}.EneShort.Time>=2001&Data{Case}.flow{k}.EneShort.Kin(end-1)>1e-10;
  indtime([1 end])=false;
  if sum(indtime)
      x=Data{Case}.flow{k}.EneShort.Time(indtime)-Data{Case}.flow{k}.EneShort.Time(end)+2000;
      y=(Data{Case}.flow{k}.EneShort.Kin(indtime)/Data{Case}.flow{k}.EneShort.Kin(find(indtime,1,'last')));
      p=polyfit(x,y,3);
      plot(x,((p(1)*x+p(2)).*x+p(3)).*x+p(4),...
        'Color',colors(j,:),...
        'DisplayName',['$\theta=' num2str(Flow{Case}.A(k)) '^\circ$']);
      hold on
  end
end
axis tight
v=axis;
set(gca,'YLim',[max(1e-1,v(3)) v(4)]);
legend('show','Location','EastOutside')
xlabel('$t$','Interpreter','none')
ylabel('$E_{kin}(t)/E_{kin}(2000)$','Interpreter','none')
return

function plot_accelerating_2(Case) 
global Data Flow
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['accelerating' num2str(Case,'_%d')])

Height=20;
ind=find(Flow{Case}.H==Height);
set(gca,'YScale','log');
set(gca,'XScale','log');
colors=lines(length(ind));
for j=1:length(ind); k=ind(j);
  %indtime=Data{Case}.flow{k}.EneShort.Time>=1500&Data{Case}.flow{k}.EneShort.Time<=2001;
  indtime=Data{Case}.flow{k}.EneShort.Time>=2001&Data{Case}.flow{k}.EneShort.Kin(end-1)>1e-10;
  indtime([1 end])=false;
  plot(-Data{Case}.flow{k}.EneShort.Time(indtime)+Data{Case}.flow{k}.EneShort.Time(end),...
    abs(Data{Case}.flow{k}.EneShort.Kin(indtime)-Data{Case}.flow{k}.EneShort.Kin(find(indtime,1,'last'))),...
    'Color',colors(j,:),...
    'DisplayName',['$\theta=' num2str(Flow{Case}.A(k)) '^\circ$']);
  hold on
end
axis tight
v=axis;
set(gca,'YLim',[max(1e-1,v(3)) v(4)])
% legend('show','Location','EastOutside')
xlabel('$t$','Interpreter','none')
ylabel('$E_{kin}(t)/E_{kin}(2000)$','Interpreter','none')
return

function plot_coeffHstop() 
global Flow CoeffHstop
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName','coeffhstop')

CaseList=2:6;
theta1=cellfun(@(data)atand(data(1)),CoeffHstop(CaseList));
theta2=cellfun(@(data)atand(data(2)),CoeffHstop(CaseList));
% A=cellfun(@(data)data(3),CoeffHstop(CaseList));
lambda=cellfun(@(data)data.L(1),Flow(CaseList));
plot(lambda,[theta1;theta2])
axis tight
% legend('show','Location','EastOutside')
xlabel('$\lambda$','Interpreter','none')
legend({'$\theta_1$','$\theta_2$'})
return

function plot_arresting_H20() 
%Ratio of kinetic and time-averaged elastic energy for $\lambda=0.5$,
%$H=$, and varying inclination. While
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName','arresting')
set(gca,'YScale','log');

filename = strread(ls('~/DRIVERS/FlowRulePaper/run/full_runs/organized_flowrule/ene/H20A*L1M0.5B0.5.ene'),'%s ');
colors=lines(length(filename));
A= cellfun(@(data)str2double(data(strfind(data,'A')+1:strfind(data,'L')-1)),filename);
[A,ix]=sort(A,'descend');
filename=filename(ix);

for i=1:length(filename)
  rawdata = importdata(filename{i},' ',1);
  % if tabs are used as delimiters
  if (size(rawdata.data,2)~=8)
    rawdata = importdata(filename,'\t',1);
  end
  Ene.Time = rawdata.data(:,1);
  Ene.Kin = rawdata.data(:,3);
  Ene.Ela = rawdata.data(:,5);

  avgEneEla=mean(Ene.Ela(ceil(end/2):end-1));
  plot(Ene.Time(2:end-1),...
    Ene.Kin(2:end-1)./avgEneEla,'-',...
    'Color',colors(i,:),...
    'Tag',num2str(A(i)),...
    'DisplayName',['$\theta=' num2str(A(i)) '^\circ$']);
  hold on
end
axis tight
v=axis;
set(gca,'XLim',[0 v(2)])
set(gca,'YLim',[max(1e-0,v(3)) v(4)])
legend('show','Location','EastOutside')
xlabel('$t$','Interpreter','none')
ylabel('$E_{kin}/\langle{E}_{ela}\rangle$','Interpreter','none')
set(legend,'Box','off','FontSize',8)

return

function plot_friction(CaseList) 
%load data
global Data Title CoeffHstop Flow Compare

%new figure
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['friction' num2str(CaseList,'_%d')])

%create subplots
if ~exist('CaseList','var'), CaseList=1:length(Data); end
ny=length(CaseList);
nx=ceil(length(CaseList)/ny);

subplot(nx,ny,1);
ylabel('$F$')
for k=1:length(CaseList); Case=CaseList(k);
  subplot(nx,ny,k); hold on

  %which hstop we compare with
  cmp=Compare(Case);
 
  %flow
  data=Data{Case}.flow;
  FlowHeight=cellfun(@(data)data.FlowHeight,data);
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,data);
  Angle=cellfun(@(data)data.ChuteAngle,data);
  Oscillating=cellfun(@(data)data.Oscillating,data);
  GravityZ=cellfun(@(data)data.Gravity(3),data);
  %defined for the rough bottom
  HeightOverHstop=FlowHeight./hstop(CoeffHstop{cmp},Angle); 
  Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
  indAll=Angle>atand(CoeffHstop{cmp}(1))&Angle<atand(CoeffHstop{cmp}(2))&Flow{Case}.Steady;%&Hydrostaticity<0.2;
  %oscillating
  if (Case==2), ind=Oscillating<0.5&indAll;
  else ind=Oscillating<0.25&indAll;
  end
  %fit
  if exist('jenkinscorrection','var')
    [betaPO]=flowrule_fit_with_offset(Angle(ind), Froude(ind), HeightOverHstop(ind), CoeffHstop{k},0); %#ok<NASGU>
  else
    [betaPO]=flowrule_fit_with_offset(Angle(ind), Froude(ind), HeightOverHstop(ind), CoeffHstop{k},2); %#ok<NASGU>
  end

  x=FlowHeight(ind);
  y=Froude(ind);
  z=tand(Angle(ind));
  TRI=delaunay(x,y);
%   colormap(jet)
  h=trisurf(TRI,x,y,z); %#ok<NASGU>
  shading interp
%   if k==length(CaseList), colorbar; end
%   set(h,'EdgeColor','k')
  
  
%  scatter(FlowHeight(ind),Froude(ind),50,tand(Angle(ind)),'filled');
%   x=sort([1; HeightOverHstop(ind)]);
%   plot(x,betaPO(1)+betaPO(2)*x,'k');
%   display(['Pouliquen slope: for case ' num2str(Case) ':' num2str(betaPO(2),'%.3f')])

  %title([Title{Case} ' \beta=' num2str(betaPO(2),'%.3f')])
%   colorbar 
  axis tight
%   v = axis; axis([0 v(2) 0 v(4)])
  
  if length(CaseList)>1
    title(Title{Case})
  end
  
  xlabel('$h/d$')
end

return

function plot_flow_rule_Mu_fit(CaseList)
%load data
global Data Title CoeffHstop Flow Compare

%new figure
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560*2 420])
set(gcf,'FileName',['friction' num2str(CaseList,'_%d')])

%create subplots
if ~exist('CaseList','var'), CaseList=1:length(Data); end
ny=length(CaseList);
nx=ceil(length(CaseList)/ny);

subplot(nx,ny,1);
ylabel('$\mu$')
for k=1:length(CaseList); Case=CaseList(k);
  subplot(nx,ny,k); hold on

  %which hstop we compare with
  cmp=Compare(Case);
 
  %flow
  data=Data{Case}.flow;
  FlowHeight=cellfun(@(data)data.FlowHeight,data);
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,data);
  Angle=cellfun(@(data)data.ChuteAngle,data);
  Oscillating=cellfun(@(data)data.Oscillating,data);
  GravityZ=cellfun(@(data)data.Gravity(3),data);
  %defined for the rough bottom
  HeightOverHstop=FlowHeight./hstop(CoeffHstop{cmp},Angle); 
  Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
  indAll=Angle>atand(CoeffHstop{cmp}(1))&Angle<atand(CoeffHstop{cmp}(2))&Flow{Case}.Steady;%&Hydrostaticity<0.2;
  %oscillating
  if (Case==2), ind=Oscillating<0.5&indAll;
  else ind=Oscillating<0.25&indAll;
  end
  %fit
  [betaPO]=flowrule_fit_with_offset(Angle(ind), Froude(ind), HeightOverHstop(ind), CoeffHstop{k},1);

%   x=mu_fit(Angle(ind),FlowHeight(ind),Froude(ind));
  t1=CoeffHstop{cmp}(1);
  t21=CoeffHstop{cmp}(2)-t1;
  A=CoeffHstop{cmp}(3);
  gamma=-betaPO(1);
  beta=betaPO(2);
  
  scatter((Froude(ind)+gamma)./FlowHeight(ind),tand(Angle(ind)),50,FlowHeight(ind),'filled')
  
  X=linspace(0,max((Froude(ind)+gamma)./FlowHeight(ind)),100);
  Y=t1+t21./(1+beta/A./X);
  plot(X,Y)

  axis tight
  title(Title(Case))
  xlabel('$(F-\beta)/h$')
end

return

function plot_flow_rule_Mu_fit_oneplot(CaseList)
%load data
global Data Title CoeffHstop Flow Compare

%new figure
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['friction' num2str(CaseList,'_%d')])

%create subplots
if ~exist('CaseList','var'), CaseList=1:length(Data); end
colors=lines(length(CaseList));

ylabel('$\mu$')
for k=1:length(CaseList); Case=CaseList(k);
  %which hstop we compare with
  cmp=Compare(Case);
 
  %flow
  data=Data{Case}.flow;
  FlowHeight=cellfun(@(data)data.FlowHeight,data);
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,data);
  Angle=cellfun(@(data)data.ChuteAngle,data);
  Oscillating=cellfun(@(data)data.Oscillating,data);
  GravityZ=cellfun(@(data)data.Gravity(3),data);
  %defined for the rough bottom
  HeightOverHstop=FlowHeight./hstop(CoeffHstop{cmp},Angle); 
  Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
  indAll=Angle>atand(CoeffHstop{cmp}(1))&Angle<atand(CoeffHstop{cmp}(2))&Flow{Case}.Steady;%&Hydrostaticity<0.2;
  %oscillating
  if (Case==2), ind=Oscillating<0.5&indAll;
  else ind=Oscillating<0.25&indAll;
  end
  %fit
  [betaPO]=flowrule_fit_with_offset(Angle(ind), Froude(ind), HeightOverHstop(ind), CoeffHstop{k},1);

  t1=CoeffHstop{cmp}(1);
  t21=CoeffHstop{cmp}(2)-t1;
  A=CoeffHstop{cmp}(3);
  gamma=-Flow{Case}.betaPO(1);
  beta=Flow{Case}.betaPO(2);
  plot((Froude(ind)+gamma)./FlowHeight(ind),tand(Angle(ind)),'.','Color',colors(k,:))
  X=linspace(0,max((Froude(ind)+gamma)./FlowHeight(ind)),100);
  Y=t1+t21./(1+beta/A./X);
  plot(X,Y,'Color',colors(k,:),'DisplayName',Title(k))

  %show layered cases
%   ind=Flow{Case}.Oscillating&~Flow{Case}.FullyOscillating&indAll;
%   gamma=-Flow{Case}.betaPOlayered(1);
%   beta=Flow{Case}.betaPOlayered(2);
%   plot((Froude(ind)+gamma)./FlowHeight(ind),tand(Angle(ind)),'o','Color',colors(k,:))
%   X=linspace(0,max((Froude(ind)+gamma)./FlowHeight(ind)),100);
%   Y=t1+t21./(1+beta/A./X);
%   plot(X,Y,'Color',colors(k,:),'DisplayName',Title(k))

  
end
xlabel('$(F+\gamma)/h$')
child=get(gca,'Children');
legend(child(end-1:-2:1),Title(CaseList),'Location','SouthEast')
set(legend,'Box','off')
axis tight
v=axis;
axis([0 v(2:4)])

return

function plot_flow_rule_Mu(CaseList)
%load data
global Data Title  

%new figure
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['friction' num2str(CaseList,'_%d')])

ny=ceil(sqrt(length(CaseList)));
nx=ceil(length(CaseList)/ny);
for k=1:length(CaseList), Case=CaseList(k);
  disp(['case' num2str(k)])
  subplot(ny,nx,k); hold on
  
  data=Data{Case}.flow;
  FlowHeight=cellfun(@(data)data.FlowHeight,data);
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,data);
  Angle=cellfun(@(data)data.ChuteAngle,data);
  GravityZ=cellfun(@(data)data.Gravity(3),data);
  Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
%   ind=Angle<atand(CoeffHstop{k}(2));
  ind=Angle<29&Froude>0.2;
  
  scatter(Froude(ind),tand(Angle(ind)),50,FlowHeight(ind),'filled')
  title([Title{Case}])
  colorbar
  axis tight
  xlabel('F')
  ylabel('\mu')
end

return
