%create all plots needed for the flow rule paper
function w()
%clc
%close all
load_data();
%plot_w();
plot_t();
%print_figures();
return

%% loads data for w
function load_data()
global Wdata Tdata Wtitle Ttitle
if true %isempty(Wdata)
  if true %~exist('w.mat','file') %make true if new stat files are added
    %Wdata=loadstatistics('../w/W/H*0*T100W*.stat');
    %Tdata=loadstatistics('../w/T_old/H30*T*W0_25.stat');
    Tdata=loadstatistics(...
      '../w/T_old/H20*.stat' ...
      );
    W=cellfun(@(data)data.w,Wdata);
    T=cellfun(@(data)str2double(data.name(max(strfind(data.name,'T'))+1:max(strfind(data.name,'W'))-1)),Tdata);
    %sort lists 
    [val idx]=sort(W);
    Wdata=Wdata(idx);
    [val idx]=sort(T);
    Tdata=Tdata(idx);
    %save w.mat Wdata Tdata
  else
    load w.mat
  end
  
  Wtitle=cell(0);
  for i=1:length(Wdata)
    Wtitle{i}=['$w=' num2str(Wdata{i}.w) '$'];
  end

  T=cellfun(@(data)str2double(data.name(max(strfind(data.name,'T'))+1:max(strfind(data.name,'W'))-1)),Tdata);
  Ttitle=cell(0);
  for i=1:length(Tdata)
%     Ttitle{i}=['$t=' num2str(diff(Tdata{i}.time),3) '$'];
    Ttitle{i}=['$\Delta t=' num2str(T(i),3) '$'];
  end
else
  disp('using already loaded data')
end
return

%% plots w dependence
function plot_w()
%load Wdata
global Wdata Wtitle

%new figure
figure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName','w_dependence_nu')
colors=lines(length(Wdata)+1);

Wlist=3:length(Wdata)-1;
for i=Wlist
  h=plot(Wdata{i}.z,Wdata{i}.Nu,'Color',colors(i-1,:));
  if (i==4); set(h,'LineWidth',2); end
end
legend(Wtitle(Wlist))
set(legend,'Box','off')
set(gca,'YLim',[0.45 0.7])
xlabel('$z$')
ylabel('$\rho/\rho_p$')
return

%% plots time dependence
function plot_t()
%load Wdata
global Tdata Ttitle

%new figure
figure(); clf; hold on
set(gcf,'Position',[0 0 560 420])
set(gcf,'FileName','t_dependence_nu')
colors=lines(length(Tdata));

Tlist=3:length(Tdata);
for i=Tlist
  plot(Tdata{i}.z,Tdata{i}.Nu,'Color',colors(i,:));
end
legend(Ttitle(Tlist),'Interpreter','none')
set(gca,'YLim',[0.4 0.7])
xlabel('$z$','Interpreter','none')
ylabel('$\rho/\rho_p$','Interpreter','none')

figure();
set(gcf,'Position',[0 0 560 420])
set(gcf,'FileName','t_dependence_momeq')

%plot Remainder
Tlist=[1:length(Tdata)];
RemainderNorm=[];
for i=Tlist
% 	StressZZ = Tdata{i}.StressZZ-Tdata{i}.Density.*Tdata{i}.VelocityZ;
%   dZStressZZ = diff(StressZZ)./diff(Tdata{i}.z);
% 	dZStressZZ = (dZStressZZ([1:end end])+dZStressZZ([1 1:end]))/2;
%   Weight=Tdata{i}.Density*Tdata{i}.Gravity(3);
%   Remainder=dZStressZZ-Weight;
	
   %d/dt (mv_a) = 
   %  - d/dr_b (rho v_a v_b)
   %  + d/dr_b (sigma_ab)
   %  + rho g_a
   StressZ = [...
    Tdata{i}.StressXZ ...
    Tdata{i}.StressYZ ...
    Tdata{i}.StressZZ ...
    ];
   Traction = [...
    Tdata{i}.TractionX ...
    Tdata{i}.TractionY ...
    Tdata{i}.TractionZ ...
    ];
  dZStressZ = diff(StressZ,1)/diff(Tdata{i}.z(1:2));
	dZStressZ = Traction+(dZStressZ([1:end end],:)+dZStressZ([1 1:end],:))/2;
  switch 2
    case 1
      MassFlow = [...
        Tdata{i}.Density.*Tdata{i}.VelocityX ...
        Tdata{i}.Density.*Tdata{i}.VelocityY ...
        Tdata{i}.Density.*Tdata{i}.VelocityZ ...
        ];
      dZMassFlow = diff(MassFlow,1)/diff(Tdata{i}.z(1:2));
      dZMassFlow = (dZMassFlow([1:end end],:)+dZMassFlow([1 1:end],:))/2;
      MaterialDerivative = ...
        (Tdata{i}.VelocityZ*ones(size(Tdata{i}.Gravity))).*dZMassFlow;
    case 2
      MassFlow = [...
        Tdata{i}.Density.*Tdata{i}.VelocityX.*Tdata{i}.VelocityZ ...
        Tdata{i}.Density.*Tdata{i}.VelocityY.*Tdata{i}.VelocityZ ...
        Tdata{i}.Density.*Tdata{i}.VelocityZ.*Tdata{i}.VelocityZ ...
        ];
      dZMassFlow = diff(MassFlow,1)/diff(Tdata{i}.z(1:2));
      dZMassFlow = (dZMassFlow([1:end end],:)+dZMassFlow([1 1:end],:))/2;
      MaterialDerivative = dZMassFlow;
    case 3
      MassFlow = Tdata{i}.Density.*Tdata{i}.VelocityZ;
      dZMassFlow = diff(MassFlow,1)/diff(Tdata{i}.z(1:2));
      dZMassFlow = (dZMassFlow([1:end end],:)+dZMassFlow([1 1:end],:))/2;
      MaterialDerivative = ...
        [Tdata{i}.VelocityX Tdata{i}.VelocityY Tdata{i}.VelocityZ].*(dZMassFlow*ones(size(Tdata{i}.Gravity)));
  end
  Weight=Tdata{i}.Density*Tdata{i}.Gravity;

  Remainder=dZStressZ-Weight;
  RemainderZ=Remainder(abs((Tdata{i}.z/Tdata{i}.FlowHeight)-.5)<.4,1:3);
  RemainderNorm(end+1) = sqrt(sum(sum((RemainderZ.^2)))*diff(Tdata{i}.z([1 2]))); %#ok<AGROW>
  %if (i>=length(Tdata)-3), semilogy(Tdata{i}.z,sum(Remainder(:,3).^2,2).^.5,'-k','Color',colors(i,:),'Displayname',Ttitle{i}); hold on; end
  if (i>=length(Tdata)-3), plot(Tdata{i}.z,Remainder(:,3),'-k','Color',colors(i,:),'Displayname',Ttitle{i}); hold on; end
  %RemainderNorm(end)=norm(StressZ(1,:)+sum(Weight)*diff(Tdata{i}.z(1:2)));
%    if (i==7),
%        plot(Tdata{i}.z,Remainder,'-'); hold on
%    end
%   semilogy(Tdata{i}.z,sqrt(Remainder.^2),'-k','Color',colors(i,:)); hold on
%   set(gca,'Ylim',[1e-5 1])
end
legend('show')
set(legend,'Interpreter','none','Location','Best')
axis tight 
%title('note: small error from cutoff')
%plot remainder norm
figure();
set(gcf,'Position',[0 0 560 260])
set(gcf,'FileName','t_dependence_momeq2')

T=cellfun(@(data)str2double(data.name(strfind(data.name,'.T')+2:strfind(data.name,'W')-1)),Tdata(Tlist));
loglog(T,RemainderNorm,':ko'); hold on
axis tight
%plot triangle
n=3;
slope=1/2;
dx=0;
dy=-.5;
loglog(T(n)*exp(dx-[0 1 1 0]),RemainderNorm(n)*exp(dy+[0 0 slope 0]),'k-'); hold off
text(T(n)*exp(dx-1.3),RemainderNorm(n)*exp(dy+0.5*slope), num2str(slope))
text(T(n)*exp(dx-0.5),RemainderNorm(n)*exp(dy-0.5), '1')

% title('note: small error from cutoff')
xlabel('$T$','Interpreter','none')
ylabel('$r$')

% title('Error in momentum equation','Interpreter','none')
return

