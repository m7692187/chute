function Y = integ(y,x)
%calculates the integral such that Y(end)=0
Y = [0; cumsum((y(2:end)+y(1:end-1))/2.*diff(x))];
Y=Y-Y(end);
return