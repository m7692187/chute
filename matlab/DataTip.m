function output_txt = DataTip(obj,event_obj)
% Display the position of the data cursor
% obj          Currently not used (empty)
% event_obj    Handle to event object
% output_txt   Data cursor text string (string or cell array of strings).

pos = get(event_obj,'Position');
h = get(event_obj,'Target');
X=get(h,'XData');
Y=get(h,'YData');
if length(pos) > 2
  Z=get(h,'ZData');
  i=find(X==pos(1)&Y==pos(2)&Z==pos(3))
else
  i=find(X==pos(1)&Y==pos(2));
end

data=get(h,'UserData');
if iscell(data)
  name=data{i}.name
  output_txt = { name(max(strfind(name,'/'))+1:end), num2str(pos,4) };
else
  name=data.name
  output_txt = { name(max(strfind(name,'/'))+1:end), num2str(pos,4) };
end

% output_txt = {[ get(get(get(h,'Parent'),'xLabel'),'String') ': ',num2str(pos(1),4)],...
%     [get(get(get(h,'Parent'),'yLabel'),'String') ': ',num2str(pos(2),4)]};
% 
% % If there is a Z-coordinate in the position, display it as well
% if length(pos) > 2
%     output_txt{end+1} = [get(get(get(h,'Parent'),'zLabel'),'String')' ': ',num2str(pos(3),4)];
% end
% 
% output_txt{end+1} = name(max(strfind(name,'/'))+1:end);
