% loadstatistics (version 1)
%
% loads data from a .stat or .data file
%
% it accepts a single filename, a cell of filenames, or a string that will
% be inserted into ls
%
% Note for .stat files: should work for any stattype
%
% 
function data=loadstatistics(filenames)

if iscell(filenames) % if argument is a cell of filenames
  data=cell(size(filenames));
  for i=1:length(filenames)
    data{i} = load_file(filenames{i});
  end
  %remove empty data sets
  if any(cellfun(@(d)isempty(d),data))
    warning(['remove ' num2str(sum(cellfun(@(d)isempty(d),data))) ' wrongly loaded data sets'])
    data=data(cellfun(@(d)~isempty(d),data));
  end
else 
  if (isempty(strfind(filenames,'?'))&&isempty(strfind(filenames,'*'))) % if argument is a single filename
  	data = load_file(filenames);
  else % if argument contains * or ?, run it through ls to procuce a cell of filenames
    try
      data = loadstatistics(strread(ls([filenames '']),'%s ')); 
    catch exception
       if (strcmp(exception.identifier, 'MATLAB:ls:OSError'))
          warning('MATLAB:ls:OSError', exception.message);
       else
          throw(exception);
       end
      data=cell(0);
    end
  end
end

return

%load a single .stat or .data file
function data = load_file(filename)
try
    disp(['loading data from ' filename]);

    % distinguishes between stat and data files
    if (strcmp(filename(end-4:end),'.data'))
        data = load_data_file(filename);
    else
        data = load_stat_file(filename);
    end

    if iscell(data)
      for i=1:length(data), 
        data{i} = make_readable(data{i}); 
        data{i} = get_standard_variables(data{i});
        if (data{i}.nz>1), data{i} = get_depth_variables(data{i}); end
      end
    else
      data = make_readable(data);
      data = get_standard_variables(data);
      if (data.nz>1), data = get_depth_variables(data); end
      [data.DensityRate, data.MomentumRate]=get_momentum_equation(data);
    end
catch exception
  %warning or error depending if you want to ignore bad cases
  warning(exception.identifier, ['file:' filename ', ' exception.message]);
  %edit(filename);
  data=cell(0);
end
return

%loads all variables stored in a stat file
function data = load_stat_file(filename)
data.name = filename(1:strfind(filename,'.stat')-1);

% load raw data from file, with the first three lines as header files
% (such that matlab recognizes the array size)
rawdata = importdata(filename,' ',3);

if iscell(rawdata), return; end

if (size(rawdata.data,2)==26)
  % for old .stat files
  disp('WARNING:outdated stat file')
  rawdata.data = [ ...
    rawdata.data(:,1:14) ...
    zeros(size(rawdata.data,1),3) ...
    rawdata.data(:,15:end) ...
    zeros(size(rawdata.data,1),10)];
  data.variable_names = { ...
    'Nu'; 'Density'; ...
    'MomentumX'; 'MomentumY'; 'MomentumZ'; ...
    'MomentumFluxXX'; 'MomentumFluxXY'; 'MomentumFluxXZ'; 'MomentumFluxYY'; 'MomentumFluxYZ'; 'MomentumFluxZZ'; ...
    'EnergyFluxX'; 'EnergyFluxY'; 'EnergyFluxZ'; ...
    'NormalStressXX'; 'NormalStressXY'; 'NormalStressXZ'; 'NormalStressYY'; 'NormalStressYZ'; 'NormalStressZZ'; ...
    'TangentialStressXX'; 'TangentialStressXY'; 'TangentialStressXZ'; 'TangentialStressYY'; 'TangentialStressYZ'; 'TangentialStressZZ'; ...
    'FabricXX'; 'FabricXY'; 'FabricXZ'; 'FabricYY'; 'FabricYZ'; 'FabricZZ'; ...
    'CollisionalHeatFluxX'; 'CollisionalHeatFluxY'; 'CollisionalHeatFluxZ'; ...
    'Dissipation'; ...
    };
else
  % reads variable names from line 1 and text output from line 2
  variable_names = textscan(rawdata.textdata{1},'%s ');
  data.variable_names = variable_names{1};
end

%also allows input from the restart and ene files if they exist
data = read_restart(filename,data);
data = read_ene(filename,data);

%reads header file of stat file
text = textscan(rawdata.textdata{2},'%s ');
data.text = text{1};

% writes the raw data into bits of data for each timestep
index_time = [find(isnan(rawdata.data(:,3))); size(rawdata.data,1)+1];
data.time = cell2mat(textscan(rawdata.textdata{3},'%f '));
data.w = cell2mat(textscan(rawdata.textdata{2}(3:end),'%f '));

data.coordinates = rawdata.data(1:index_time(1)-1,1:3);
data.variables = rawdata.data(1:index_time(1)-1,4:end);
%disp('warning:taking out last point')
if (length(index_time)>1)
  if (length(index_time)==2)
    data.variance = rawdata.data(index_time(1)+1:index_time(2)-1,4:end);
  elseif (length(index_time)==4)
    data.gradx = rawdata.data(index_time(1)+1:index_time(2)-1,4:end);
    data.grady = rawdata.data(index_time(2)+1:index_time(3)-1,4:end);
    data.gradz = rawdata.data(index_time(3)+1:index_time(4)-1,4:end);
  else
    error('too many timesteps')
  end
end

% \todo{why is this needed?}
%data.description = ['Goldhirsch w/d=' num2str(str2double(data.text{2})/1e-3)];

return

%loads from data file the same variables that are present in the
%stat file (for comparison to Stefan's code)
function data = load_data_file(filename)
data.name = filename(1:end-5);

% load raw data from file, with the first three lines as header files (such
% that matlab recognizes the array size)
rawdata = importdata(filename,' ',17);

% reads variable names from line 1 and text output
% from line 2
data.variable_names = { ...
	'Nu'; 'Density'; ...
	'MomentumX'; 'MomentumY'; 'MomentumZ'; ...
	'MomentumFluxXX'; 'MomentumFluxXY'; 'MomentumFluxXZ'; 'MomentumFluxYY'; 'MomentumFluxYZ'; 'MomentumFluxZZ'; ...
	'EnergyFluxX'; 'EnergyFluxY'; 'EnergyFluxZ'; ...
	'NormalStressXX'; 'NormalStressXY'; 'NormalStressXZ'; 'NormalStressYY'; 'NormalStressYZ'; 'NormalStressZZ'; ...
	'TangentialStressXX'; 'TangentialStressXY'; 'TangentialStressXZ'; 'TangentialStressYY'; 'TangentialStressYZ'; 'TangentialStressZZ'; ...
	'FabricXX'; 'FabricXY'; 'FabricXZ'; 'FabricYY'; 'FabricYZ'; 'FabricZZ'; ...
	'CollisionalHeatFluxX'; 'CollisionalHeatFluxY'; 'CollisionalHeatFluxZ'; ...
	'Dissipation'; ...
	};
text = textscan([rawdata.textdata{8} ' ' rawdata.textdata{9}],'%s ');
data.text = [text{1}{5} num2str(str2double(text{1}{9})*100)];
rho_text = textscan(rawdata.textdata{11},'%s ');
rho = str2double(rho_text{1}{end});

% writes the raw data into bits of data for each timestep
%index_time = [find(isnan(rawdata.data(:,2))); size(rawdata.data,1)+1];
data.time = 0;
data.coordinates = rawdata.data(:,4:6);
nu = max(rawdata.data(:,10),1e-200*ones(size(rawdata.data,1),1));
data.variables = [nu ...
	nu*rho ...
	rawdata.data(:,51:53).*nu(:,[1 1 1])*rho ...
	rawdata.data(:,[41:43 45 46 49]) ...
	nan(size(rawdata.data,1),3) ...
	rawdata.data(:,[21:23 25 26 29]) ...
	rawdata.data(:,[31:33 35 36 39]) ...
	rawdata.data(:,[71:73 75 76 79]) ...
	nan(size(rawdata.data,1),4) ...
	];

%data.description = ['Luding ' data.text];

return

% rewrites coordinates and variables into n dimensional shape where n is
% the number of dimensions in stattype (easy for plotting) 
function [data] = make_readable(data)

data.nx=length(data.coordinates(:,1))/sum(data.coordinates(:,1)==data.coordinates(1,1));
data.ny=length(data.coordinates(:,2))/sum(data.coordinates(:,2)==data.coordinates(1,2));
data.nz=length(data.coordinates(:,3))/sum(data.coordinates(:,3)==data.coordinates(1,3));
try
shape=size(squeeze(zeros(data.nz,data.ny,data.nx)));
catch
  i=1;
end
data.x = reshape(data.coordinates(:,1),shape);
data.y = reshape(data.coordinates(:,2),shape);
data.z = reshape(data.coordinates(:,3),shape);
data = rmfield(data,{'coordinates'});

%the array 'variable' is expanded into smaller arrays, each containing one
%variable only (f.e. Density), with the field names defined in
%variable_names. 
%If available, variance, gradx, grady, gradz are also expanded the same way

if isfield(data,'variance')
	for i=1:length(data.variable_names)
  	data.([sscanf(data.variable_names{i},'%s ',16) '_var']) ...
      =reshape(data.variance(:,i),shape);
	end
	data = rmfield(data,{'variance'});
end

if isfield(data,'gradx')
	for i=1:length(data.variable_names)
  	data.([sscanf(data.variable_names{i},'%s ',16) '_dx']) ...
      =reshape(data.gradx(:,i),shape);
  	data.([sscanf(data.variable_names{i},'%s ',16) '_dy']) ...
      =reshape(data.grady(:,i),shape);
  	data.([sscanf(data.variable_names{i},'%s ',16) '_dz']) ...
      =reshape(data.gradz(:,i),shape);
	end
	data = rmfield(data,{'gradx','grady','gradz'});
end

for i=1:length(data.variable_names)
	data.(sscanf(data.variable_names{i},'%s ',16)) ...
    = reshape(data.variables(:,i),shape);
end
data = rmfield(data,{'variables','variable_names'});

return

% extracts a load of extra variables (such as stress) from the basic
% microscopic fields 
function data = get_standard_variables(data)

data.MomentumX(isnan(data.MomentumX)) = 0;
data.MomentumY(isnan(data.MomentumY)) = 0;
data.MomentumZ(isnan(data.MomentumZ)) = 0;

data.VelocityX = data.MomentumX./data.Density;
data.VelocityY = data.MomentumY./data.Density;
data.VelocityZ = data.MomentumZ./data.Density;

data.VelocityX(isnan(data.VelocityX)) = 0;
data.VelocityY(isnan(data.VelocityY)) = 0;
data.VelocityZ(isnan(data.VelocityZ)) = 0;

data.TractionX = data.NormalTractionX + data.TangentialTractionX;
data.TractionY = data.NormalTractionY + data.TangentialTractionY;
data.TractionZ = data.NormalTractionZ + data.TangentialTractionZ;

data.KineticStressXX = data.MomentumFluxXX - data.Density.*data.VelocityX.*data.VelocityX;
data.KineticStressXY = data.MomentumFluxXY - data.Density.*data.VelocityX.*data.VelocityY;
data.KineticStressXZ = data.MomentumFluxXZ - data.Density.*data.VelocityX.*data.VelocityZ;
data.KineticStressYY = data.MomentumFluxYY - data.Density.*data.VelocityY.*data.VelocityY;
data.KineticStressYZ = data.MomentumFluxYZ - data.Density.*data.VelocityY.*data.VelocityZ;
data.KineticStressZZ = data.MomentumFluxZZ - data.Density.*data.VelocityZ.*data.VelocityZ;

%correctstresses for cutoff problem
data.NormalStressXX = data.NormalStressXX*(erf(3/sqrt(2)));
data.NormalStressXY = data.NormalStressXY*(erf(3/sqrt(2)));
data.NormalStressXZ = data.NormalStressXZ*(erf(3/sqrt(2)));
data.NormalStressYX = data.NormalStressYX*(erf(3/sqrt(2)));
data.NormalStressYY = data.NormalStressYY*(erf(3/sqrt(2)));
data.NormalStressYZ = data.NormalStressYZ*(erf(3/sqrt(2)));
data.NormalStressZX = data.NormalStressZX*(erf(3/sqrt(2)));
data.NormalStressZY = data.NormalStressZY*(erf(3/sqrt(2)));
data.NormalStressZZ = data.NormalStressZZ*(erf(3/sqrt(2)));
data.TangentialStressXX = data.TangentialStressXX*(erf(3/sqrt(2)));
data.TangentialStressXY = data.TangentialStressXY*(erf(3/sqrt(2)));
data.TangentialStressXZ = data.TangentialStressXZ*(erf(3/sqrt(2)));
data.TangentialStressYX = data.TangentialStressYX*(erf(3/sqrt(2)));
data.TangentialStressYY = data.TangentialStressYY*(erf(3/sqrt(2)));
data.TangentialStressYZ = data.TangentialStressYZ*(erf(3/sqrt(2)));
data.TangentialStressZX = data.TangentialStressZX*(erf(3/sqrt(2)));
data.TangentialStressZY = data.TangentialStressZY*(erf(3/sqrt(2)));
data.TangentialStressZZ = data.TangentialStressZZ*(erf(3/sqrt(2)));
data.ContactStressXX = data.NormalStressXX + data.TangentialStressXX;
data.ContactStressXY = data.NormalStressXY + data.TangentialStressXY;
data.ContactStressXZ = data.NormalStressXZ + data.TangentialStressXZ;
data.ContactStressYX = data.NormalStressYX + data.TangentialStressYX;
data.ContactStressYY = data.NormalStressYY + data.TangentialStressYY;
data.ContactStressYZ = data.NormalStressYZ + data.TangentialStressYZ;
data.ContactStressZX = data.NormalStressZX + data.TangentialStressZX;
data.ContactStressZY = data.NormalStressZY + data.TangentialStressZY;
data.ContactStressZZ = data.NormalStressZZ + data.TangentialStressZZ;

data.StressXX = data.NormalStressXX + data.TangentialStressXX + data.KineticStressXX;
data.StressXY = data.NormalStressXY + data.TangentialStressXY + data.KineticStressXY; 
data.StressXZ = data.NormalStressXZ + data.TangentialStressXZ + data.KineticStressXZ;
data.StressYX = data.NormalStressYX + data.TangentialStressYX + data.KineticStressXY;
data.StressYY = data.NormalStressYY + data.TangentialStressYY + data.KineticStressYY;
data.StressYZ = data.NormalStressYZ + data.TangentialStressYZ + data.KineticStressYZ;
data.StressZX = data.NormalStressZX + data.TangentialStressZX + data.KineticStressXZ;
data.StressZY = data.NormalStressZY + data.TangentialStressZY + data.KineticStressYZ;
data.StressZZ = data.NormalStressZZ + data.TangentialStressZZ + data.KineticStressZZ;

data.Temperature = (...
	(data.MomentumFluxXX + data.MomentumFluxYY + data.MomentumFluxZZ)./data.Density ...
	- (data.VelocityX.^2 + data.VelocityY.^2 + data.VelocityZ.^2) )/3;

data.Pressure =  (data.StressXX + data.StressYY + data.StressZZ)/3;

data.CoordinationNumber=(data.FabricXX+data.FabricYY+data.FabricZZ)./data.Nu;
%data.TotalCoordinationNumber=sum(data.FabricXX+data.FabricYY+data.FabricZZ)/sum(data.Nu(:));

data.ShearRate = diff(data.VelocityX)./diff(data.z);
data.ShearRate(end+1) = data.ShearRate(end);
data.InertialParameter=data.ShearRate*data.d./sqrt(data.StressZZ./data.Density);

%macroscopic friction coefficient, or deviator-stress ratio sD1:=(S1-S3)/(2*p)  or sD2 (to be discussed)
data.MacroFrictionCoefficient = zeros(size(data.x));
data.Sd = zeros(size(data.x));
data.SdStar = zeros(size(data.x));
for j=1:length(data.x(:))
	Stress = [ ...
		data.StressXX(j) data.StressXY(j) data.StressXZ(j); ...
		data.StressXY(j) data.StressYY(j) data.StressYZ(j); ...
		data.StressXZ(j) data.StressYZ(j) data.StressZZ(j) ];
	if (isequal(Stress,zeros(3))|| sum(sum(isnan(Stress))) )
		data.MacroFrictionCoefficient(j) = NaN;
		data.Sd(j) = nan;
		data.SdStar(j) = nan;
	else
		[v1,d1]=eig(Stress);
		[v,d]=dsort_ev(v1,d1);
    d = diag(d);
    p=sum(d)/3;
		data.MacroFrictionCoefficient(j) = -data.StressXZ(j)/data.StressZZ(j);
		data.Sd(j) = (d(1)-d(3))/(2*p);
		data.SdStar(j) = sqrt(((d(1)-d(3))^2+(d(2)-d(3))^2+(d(1)-d(2))^2) / (6*p^2));
	end
end

if (isfield(data,'Density_dz'))
  data.VelocityX_dz = (data.MomentumX_dz.*data.Density-data.MomentumX.*data.Density_dz)./data.Density.^2;
  data.VelocityY_dz = (data.MomentumY_dz.*data.Density-data.MomentumY.*data.Density_dz)./data.Density.^2;
  data.VelocityZ_dz = (data.MomentumZ_dz.*data.Density-data.MomentumZ.*data.Density_dz)./data.Density.^2;
  data.VelocityX_dz(isnan(data.VelocityX_dz)) = 0;
  data.VelocityY_dz(isnan(data.VelocityY_dz)) = 0;
  data.VelocityZ_dz(isnan(data.VelocityZ_dz)) = 0;

  data.TractionX_dz = data.NormalTractionX_dz + data.TangentialTractionX_dz;
  data.TractionY_dz = data.NormalTractionY_dz + data.TangentialTractionY_dz;
  data.TractionZ_dz = data.NormalTractionZ_dz + data.TangentialTractionZ_dz;

  data.KineticStressXX_dz = data.MomentumFluxXX_dz - data.Density_dz.*data.VelocityX.*data.VelocityX - data.Density.*data.VelocityX_dz.*data.VelocityX - data.Density.*data.VelocityX.*data.VelocityX_dz;
  data.KineticStressXY_dz = data.MomentumFluxXY_dz - data.Density_dz.*data.VelocityX.*data.VelocityY - data.Density.*data.VelocityX_dz.*data.VelocityY - data.Density.*data.VelocityX.*data.VelocityY_dz;
  data.KineticStressXZ_dz = data.MomentumFluxXZ_dz - data.Density_dz.*data.VelocityX.*data.VelocityZ - data.Density.*data.VelocityX_dz.*data.VelocityZ - data.Density.*data.VelocityX.*data.VelocityZ_dz;
  data.KineticStressYY_dz = data.MomentumFluxYY_dz - data.Density_dz.*data.VelocityY.*data.VelocityY - data.Density.*data.VelocityY_dz.*data.VelocityY - data.Density.*data.VelocityY.*data.VelocityY_dz;
  data.KineticStressYZ_dz = data.MomentumFluxYZ_dz - data.Density_dz.*data.VelocityY.*data.VelocityZ - data.Density.*data.VelocityY_dz.*data.VelocityZ - data.Density.*data.VelocityY.*data.VelocityZ_dz;
  data.KineticStressZZ_dz = data.MomentumFluxZZ_dz - data.Density_dz.*data.VelocityZ.*data.VelocityZ - data.Density.*data.VelocityZ_dz.*data.VelocityZ - data.Density.*data.VelocityZ.*data.VelocityZ_dz;

  %correctstresses for cutoff problem
  data.NormalStressXX_dz = data.NormalStressXX_dz*(erf(3/sqrt(2)));
  data.NormalStressXY_dz = data.NormalStressXY_dz*(erf(3/sqrt(2)));
  data.NormalStressXZ_dz = data.NormalStressXZ_dz*(erf(3/sqrt(2)));
  data.NormalStressYX_dz = data.NormalStressYX_dz*(erf(3/sqrt(2)));
  data.NormalStressYY_dz = data.NormalStressYY_dz*(erf(3/sqrt(2)));
  data.NormalStressYZ_dz = data.NormalStressYZ_dz*(erf(3/sqrt(2)));
  data.NormalStressZX_dz = data.NormalStressZX_dz*(erf(3/sqrt(2)));
  data.NormalStressZY_dz = data.NormalStressZY_dz*(erf(3/sqrt(2)));
  data.NormalStressZZ_dz = data.NormalStressZZ_dz*(erf(3/sqrt(2)));
  data.TangentialStressXX_dz = data.TangentialStressXX_dz*(erf(3/sqrt(2)));
  data.TangentialStressXY_dz = data.TangentialStressXY_dz*(erf(3/sqrt(2)));
  data.TangentialStressXZ_dz = data.TangentialStressXZ_dz*(erf(3/sqrt(2)));
  data.TangentialStressYX_dz = data.TangentialStressYX_dz*(erf(3/sqrt(2)));
  data.TangentialStressYY_dz = data.TangentialStressYY_dz*(erf(3/sqrt(2)));
  data.TangentialStressYZ_dz = data.TangentialStressYZ_dz*(erf(3/sqrt(2)));
  data.TangentialStressZX_dz = data.TangentialStressZX_dz*(erf(3/sqrt(2)));
  data.TangentialStressZY_dz = data.TangentialStressZY_dz*(erf(3/sqrt(2)));
  data.TangentialStressZZ_dz = data.TangentialStressZZ_dz*(erf(3/sqrt(2)));

  data.StressXX_dz = data.NormalStressXX_dz + data.TangentialStressXX_dz + data.KineticStressXX_dz;
  data.StressXY_dz = data.NormalStressXY_dz + data.TangentialStressXY_dz + data.KineticStressXY_dz; 
  data.StressXZ_dz = data.NormalStressXZ_dz + data.TangentialStressXZ_dz + data.KineticStressXZ_dz;
  data.StressYX_dz = data.NormalStressYX_dz + data.TangentialStressYX_dz + data.KineticStressXY_dz;
  data.StressYY_dz = data.NormalStressYY_dz + data.TangentialStressYY_dz + data.KineticStressYY_dz;
  data.StressYZ_dz = data.NormalStressYZ_dz + data.TangentialStressYZ_dz + data.KineticStressYZ_dz;
  data.StressZX_dz = data.NormalStressZX_dz + data.TangentialStressZX_dz + data.KineticStressXZ_dz;
  data.StressZY_dz = data.NormalStressZY_dz + data.TangentialStressZY_dz + data.KineticStressYZ_dz;
  data.StressZZ_dz = data.NormalStressZZ_dz + data.TangentialStressZZ_dz + data.KineticStressZZ_dz;
end

return

% extracts extra variables that only exist for stattype Z
function data = get_depth_variables(data)

dz=diff(data.z(1:2,1));
data.FlowNu = sum(data.Nu,1)*dz/diff(data.Domain([5 6]));
data.FlowDensity = sum(data.Density,1)*dz/diff(data.Domain([5 6]));
data.FlowMomentumX = sum(data.MomentumX,1)*dz/diff(data.Domain([5 6]));
data.FlowMomentumY = sum(data.MomentumY,1)*dz/diff(data.Domain([5 6]));
data.FlowMomentumZ = sum(data.MomentumZ,1)*dz/diff(data.Domain([5 6]));
data.FlowVelocityX = data.FlowMomentumX./data.FlowDensity;
data.FlowVelocityY = data.FlowMomentumY./data.FlowDensity;
data.FlowVelocityZ = data.FlowMomentumZ./data.FlowDensity;

data.FlowMomentum(1,:) = sum(data.MomentumX,1)*dz/diff(data.Domain([5 6]));
data.FlowMomentum(2,:) = sum(data.MomentumY,1)*dz/diff(data.Domain([5 6]));
data.FlowMomentum(3,:) = sum(data.MomentumZ,1)*dz/diff(data.Domain([5 6]));
data.FlowVelocity = data.FlowMomentum./data.FlowDensity([1,1,1],:);
FlowMomentumFlux(1,:) = sum(data.MomentumFluxXX,1)*dz/diff(data.Domain([5 6]));
FlowMomentumFlux(2,:) = sum(data.MomentumFluxYY,1)*dz/diff(data.Domain([5 6]));
FlowMomentumFlux(3,:) = sum(data.MomentumFluxZZ,1)*dz/diff(data.Domain([5 6]));
data.FlowVelocity2 = FlowMomentumFlux./data.FlowDensity([1,1,1],:);


data.BottomFrictionCoefficient = -data.StressXZ(1,:)/data.StressZZ(1,:);

% height and average density of the flow
if ~(data.nx==1&&data.ny==1), 
  ContactStressZZ=data.NormalStressZZ+data.TangentialStressZZ;
  data.FlowHeight=max(data.z.*(ContactStressZZ~=0),[],1);
else
  kappa=0.02;
  IntDensity = cumsum(data.Density);
  Base = min(data.z(IntDensity>=kappa*max(IntDensity)));
  Surface = max(data.z(IntDensity<=(1-kappa)*max(IntDensity)));
  data.FlowHeight=(Surface-Base)/(1-2*kappa);
  data.Base=Base-kappa*data.FlowHeight;
  data.Surface=Surface+kappa*data.FlowHeight;
  data.Surface=data.Base+data.FlowHeight;

  FlowHeightIndex = data.z>data.Base & data.z<data.Base+data.FlowHeight;
  if ~isempty(FlowHeightIndex)
    data.Froude=norm([data.FlowVelocityX data.FlowVelocityY data.FlowVelocityZ])/sqrt(data.FlowHeight*(-data.Gravity(3)));
  end
end

% % height and average density of the flow
% FlowHeightIndex = min([length(data.Nu),round(length(data.Nu)/2)-1+find(data.Nu(round(end/2):end)<0.3,1)]);
% if ~isempty(FlowHeightIndex)
% 	AveragingInterval = ceil(FlowHeightIndex/3):ceil(FlowHeightIndex*2/3);
% 	data.FlowDensity = sum(data.Density(AveragingInterval))/length(AveragingInterval);
% 	data.FlowNu = sum(data.Nu(AveragingInterval))/length(AveragingInterval);
% 	data.FlowHeight = data.z(1) + sum(data.Density)/length(data.z)/data.FlowDensity * (data.z(end)-data.z(1));
% 
%   index = data.z>0&data.z<data.FlowHeight;
% 	data.FlowVelocityX = average(data.VelocityX(index));
% 	data.FlowVelocityY = average(data.VelocityY(index));
% 	data.FlowVelocityZ = average(data.VelocityZ(index));
%   data.Froude=norm([data.FlowVelocityX data.FlowVelocityY data.FlowVelocityZ])/sqrt(data.FlowHeight*(-data.Gravity(3)));
% end


return

function [Vs, Ds] = dsort_ev(V, D)
    Ds=D;
    Vs=V;
%     tmpvec=[0;0;0];
    if((Ds(1,1))<(Ds(2,2)))
        tmp=Ds(1,1);
        Ds(1,1)=Ds(2,2);
        Ds(2,2)=tmp;
        tmpvec=Vs(:,1);
        Vs(:,1)=Vs(:,2);
        Vs(:,2)=tmpvec;
    end
    if((Ds(2,2))<(Ds(3,3)))
        tmp=Ds(2,2);
        Ds(2,2)=Ds(3,3);
        Ds(3,3)=tmp;
        tmpvec=Vs(:,2);
        Vs(:,2)=Vs(:,3);
        Vs(:,3)=tmpvec;
    end
    if((Ds(1,1))<(Ds(2,2)))
        tmp=Ds(1,1);
        Ds(1,1)=Ds(2,2);
        Ds(2,2)=tmp;
        tmpvec=Vs(:,1);
        Vs(:,1)=Vs(:,2);
        Vs(:,2)=tmpvec;
    end
    if((Ds(2,2))<(Ds(3,3)))
        tmp=Ds(2,2);
        Ds(2,2)=Ds(3,3);
        Ds(3,3)=tmp;
        tmpvec=Vs(:,2);
        Vs(:,2)=Vs(:,3);
        Vs(:,3)=tmpvec;
    end

return

function [MassRemainder, MomentumRemainder] = get_momentum_equation(data)

NablaDensity = nabla(data.Density,data.x,data.y,data.z);
MassRemainder = -[
    data.VelocityX.*NablaDensity.X + ...
    data.VelocityY.*NablaDensity.Y + ...
    data.VelocityZ.*NablaDensity.Z ];

NablaMomentumX = nabla(data.MomentumX,data.x,data.y,data.z);
NablaMomentumY = nabla(data.MomentumY,data.x,data.y,data.z);
NablaMomentumZ = nabla(data.MomentumZ,data.x,data.y,data.z);

VelocityDotNablaMomentum.X = [
    data.VelocityX.*NablaMomentumX.X + ...
    data.VelocityY.*NablaMomentumX.Y + ...
    data.VelocityZ.*NablaMomentumX.Z ];
VelocityDotNablaMomentum.Y = [
    data.VelocityX.*NablaMomentumY.X + ...
    data.VelocityY.*NablaMomentumY.Y + ...
    data.VelocityZ.*NablaMomentumY.Z ];
VelocityDotNablaMomentum.Z = [
    data.VelocityX.*NablaMomentumZ.X + ...
    data.VelocityY.*NablaMomentumZ.Y + ...
    data.VelocityZ.*NablaMomentumZ.Z ];

%NablaDotStress = nablaMat(data.StressXX,data.StressXY,data.StressXZ,data.StressYX,data.StressYY,data.StressYZ,data.StressZX,data.StressZY,data.StressZZ,data.x,data.y,data.z);
NablaDotStress = nablaMat(data.StressXX,data.StressYX,data.StressZX,data.StressXY,data.StressYY,data.StressZY,data.StressXZ,data.StressYZ,data.StressZZ,data.x,data.y,data.z);

DensityGravity.X = data.Density * data.Gravity(1);
DensityGravity.Y = data.Density * data.Gravity(2);
DensityGravity.Z = data.Density * data.Gravity(3);

MomentumRemainder.X = -VelocityDotNablaMomentum.X +DensityGravity.X -NablaDotStress.X - data.TractionX;
MomentumRemainder.Y = -VelocityDotNablaMomentum.Y +DensityGravity.Y -NablaDotStress.Y - data.TractionY;
MomentumRemainder.Z = -VelocityDotNablaMomentum.Z +DensityGravity.Z -NablaDotStress.Z - data.TractionZ;

%Maximum = max([max(VelocityDotNablaMomentum) max(DensityGravity) max(NablaDotStress)]);
 
return

function data = read_ene(statname,data)

%load ene data
filename = [statname(1:end-5) '.ene'];
if ~exist(filename,'file'), 
  %if unsuccessful, load ene data with last part of filename removed
  dots=strfind(statname,'.');
  if (length(dots)>1), filename = [statname(1:min(dots(end-1:end))) 'ene']; end
  if ~exist(filename,'file')
    disp([filename ' not found']); 
  else
    %if unsuccessful, give up
    %disp([statname(1:end-5) '.ene not found, using ' filename ' instead']); 
  end
end

if exist(filename,'file'),
  % load raw data from file, with the first three lines as header files
  % (such that matlab recognizes the array size)
  rawdata = importdata(filename,' ',1);
  % if tabs are used as delimiters
  if ~isfield(rawdata,'data')|(size(rawdata.data,2)~=8)
    rawdata = importdata(filename,'\t',1);
  end
  if ~isfield(rawdata,'data')|(size(rawdata.data,2)~=8)
    disp([filename ' erroneous'])
  else
    data.Ene.Time = rawdata.data(:,1);
    data.Ene.Gra = rawdata.data(:,2);
    data.Ene.Kin = rawdata.data(:,3);
    data.Ene.Rot = rawdata.data(:,4);
    data.Ene.Ela = rawdata.data(:,5);
    data.Ene.ComX = rawdata.data(:,6);
    data.Ene.ComY = rawdata.data(:,7);
    data.Ene.ComZ = rawdata.data(:,8);
  end
else
  disp([filename ' not found'])
end

return

function data = read_restart(statname,data)

%load restart data
filename = strrep(statname,'.stat','.restart');
fid=fopen(filename);
if (fid==-1), 
  oldfilename=filename;
  %if unsuccessful, load restart data with last part of filename removed
  dots=strfind(statname,'.');
  filename = [statname(1:min(dots(end-1:end))) 'restart'];
  fid=fopen(filename);
  if (fid==-1),
    disp([filename ' not found']); 
  else
    %if unsuccessful, give up
    %disp([oldfilename ' not found, using ' filename ' instead']); 
  end
end
tdata = textscan(fid,'%s');
tdata = tdata{1};
fclose(fid);

%this assumes monodispersed particles
if (length(tdata{1})==1) 
	%old restart files
	data.ParticleDensity = str2double(tdata(29));
	data.d = 2*str2double(tdata(28));
	data.Gravity = str2double(tdata(2:4))';
	data.N = str2double(tdata(end-3));
	data.Domain=str2double(tdata(5:10))';
else
	%new restart version
	i = find(strcmp(tdata,'t'));
	data.time_restart = str2double(tdata(i+1));
	i = find(strcmp(tdata,'rho'));
    %if length(i)>1, disp('warning: two species'); end
	data.ParticleDensity = str2double(tdata(i+1));
	i = find(strcmp(tdata,'mu'));
	data.Mu = str2double(tdata(i+1));
	i = find(strcmp(tdata,'gravity'));
	data.Gravity = str2double(tdata(i+(1:3)))';
	i = find(strcmp(tdata,'Particles'));
	data.N = str2double(tdata{i+1});
% 	data.Radii = str2double(tdata(i+(8:15:15*data.N)))';
% 	data.Speed = sqrt( ... 
%       str2double(tdata(i+(5:15:15*data.N)))'.^2 ...
%     + str2double(tdata(i+(6:15:15*data.N)))'.^2 ...
%     + str2double(tdata(i+(7:15:15*data.N)))'.^2);
% 	data.Depth = str2double(tdata(i+(4:15:15*data.N)))';
	i = find(strcmp(tdata,'xmin'));
	data.Domain = str2double(tdata(i+(1:2:11)))';
	i = find(strcmp(tdata,'FixedParticleRadius'));
	if ~isempty(i), data.FixedParticleRadius = str2double(tdata{i+1}); end
	i = find(strcmp(tdata,'MinInflowParticleRadius'));
	if ~isempty(i), 
		data.InflowParticleRadius = str2double(tdata(i+[1 3]))'; 
		data.d = sum(data.InflowParticleRadius);
	else
		i = find(strcmp(tdata,'Particles'));
		data.d = 2*str2double(tdata{i+8});
	end
end
data.ParticleVolume = pi/6*data.d^3;
data.DomainVolume = prod(data.Domain([2 4 6])-data.Domain([1 3 5]));
data.ChuteAngle = round(atand(-data.Gravity(1)/data.Gravity(3))*400)/400;
return

function NablaVariable = nabla(variable,x,y,z)

% stores the grid dimensions
n(1) = length(x)/sum(x(1)==x);
n(2) = length(y)/sum(y(1)==y);
n(3) = length(z)/sum(z(1)==z);

% stores coordinates in grid
X = zeros(n(end:-1:1));
X(:) = x;
Y = zeros(n(end:-1:1));
Y(:) = y;
Z = zeros(n(end:-1:1));
Z(:) = z;
V = zeros(n(end:-1:1));

% stores differentials (small number if X/Y/Z is constant)
dX = max(1e-60,(X(:,:,[2:end end])-X(:,:,[1 1:end-1])));
dY = max(1e-60,(Y(:,[2:end end],:)-Y(:,[1 1:end-1],:)));
dZ = max(1e-60,(Z([2:end end],:,:)-Z([1 1:end-1],:,:)));

%scalar variable
V(:) = variable;
NablaVariable.X = (V(:,:,[2:end end])-V(:,:,[1 1:end-1])) ./dX;
NablaVariable.Y = (V(:,[2:end end],:)-V(:,[1 1:end-1],:)) ./dY;
NablaVariable.Z = (V([2:end end],:,:)-V([1 1:end-1],:,:)) ./dZ;
return

function NablaVariable = nablaMat(XX,XY,XZ,YX,YY,YZ,ZX,ZY,ZZ,x,y,z)

% stores the grid dimensions
n(1) = length(x)/sum(x(1)==x);
n(2) = length(y)/sum(y(1)==y);
n(3) = length(z)/sum(z(1)==z);

% stores coordinates in grid
X = zeros(n(end:-1:1));
X(:) = x;
Y = zeros(n(end:-1:1));
Y(:) = y;
Z = zeros(n(end:-1:1));
Z(:) = z;
V = zeros(n(end:-1:1));

% stores differentials (small number if X/Y/Z is constant)
dX = max(1e-60,(X(:,:,[2:end end])-X(:,:,[1 1:end-1])));
dY = max(1e-60,(Y(:,[2:end end],:)-Y(:,[1 1:end-1],:)));
dZ = max(1e-60,(Z([2:end end],:,:)-Z([1 1:end-1],:,:)));

%scalar variable
NablaVariable.X = ...
  (XX(:,:,[2:end end])-XX(:,:,[1 1:end-1])) ./dX + ...
  (YX(:,[2:end end],:)-YX(:,[1 1:end-1],:)) ./dY + ...
  (ZX([2:end end],:,:)-ZX([1 1:end-1],:,:)) ./dZ;
NablaVariable.Y = ...
  (XY(:,:,[2:end end])-XY(:,:,[1 1:end-1])) ./dX + ...
  (YY(:,[2:end end],:)-YY(:,[1 1:end-1],:)) ./dY + ...
  (ZY([2:end end],:,:)-ZY([1 1:end-1],:,:)) ./dZ;
NablaVariable.Z = ...
  (XZ(:,:,[2:end end])-XZ(:,:,[1 1:end-1])) ./dX + ...
  (YZ(:,[2:end end],:)-YZ(:,[1 1:end-1],:)) ./dY + ...
  (ZZ([2:end end],:,:)-ZZ([1 1:end-1],:,:)) ./dZ;
return


