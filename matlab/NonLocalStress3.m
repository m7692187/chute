%hstop curve and friction
function NonLocalStress3()
clc
close all
%figure(2); clf
%figure(3); clf
%figure(4); clf

hl=1:.5:10;
tl=18:2:28;

for ti=1:length(tl); inclination=tl(ti);
for hi=1:length(hl); h=hl(hi);

  %resolution in z
  z=linspace(0,h,50);

  %initial shear rate profile
  shearrate=ones(size(z));
  
  %figure(1)
  %clf
  %plot(z,shearrate,'Color',[0 0 0]);
  %ylabel('\dot\gamma','interpreter','none')
  %hold on
  N=60;
  for i=1:N
    [shearrate, Ilocal] = getShearrate(z,shearrate,inclination);
  end
  %plot(z,shearrate,'Color',i/N*[1 0 0]);
  F(hi)=mean(cumsum(shearrate)*diff(z(1:2)))/sqrt(h*cosd(inclination));
  I(ti,hi)=mean(Ilocal);
end
hstop(ti) = max(hl(F<0.00001));
figure(3)
plot(hl,F,'-x','Color',ti/length(tl)*[0 0 1]);
hold on
plot(hstop(ti),0,'o','Color',ti/length(tl)*[0 0 1]);
ylabel('F')
xlabel('h')
figure(5)
plot(I(ti,:),tand(inclination),'x');
hold on
xlabel('I')
ylabel('mu')
end
figure(4)
plot(tl,hstop,'o-');


return

function [g, I] = getShearrate(z,shearrate,inclination)
rhop = 6/pi;
d=1;
gravity = 1;
rho = 0.6 * rhop;
P = rho * gravity * cosd(inclination) * (max(z)+d-z);
tau = tand(inclination) * P;
mu2=0.62;
I0=0.26;
g=zeros(size(z));
%slip
%shearrate(1)=3000;

beta=3.3;%-0*1.9*shearrate./(.26+shearrate);

for i=1:length(z)
  g(i)=sum( ...
    shearrate./(1+shearrate(i)*d/I0/sqrt(P(i)/rhop)) .*( ...
    exp(-(mu2*P(i)-tau(i))*(1+beta.*(z(i)-z).^2/d^2)./P) - ...
    exp(-(mu2*P(i)+tau(i))*(1+beta.*(z(i)-z).^2/d^2)./P) ...
    ) )*diff(z(1:2))/d;
end
I=g*d./sqrt(P/rhop);
return
