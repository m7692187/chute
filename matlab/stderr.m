function err = stderr(a)
err=abs(mean(a))+std(a);
return
