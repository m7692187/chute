data=textread('data');
x=data(:,1)';
y=data(:,2)';
r=data(:,5)'*0.9;
% elongation=2;
% n=300;
% x=rand(1,n)*elongation;
% y=rand(size(x));
% r=rand(size(x))*1e-1;
img=imread('logo.png');
img=sum(img,3);



t=linspace(0,2*pi,30)';
o=ones(size(t));
s=sin(t);
c=cos(t);
colormap(summer)
C=rand(size(x))*200+img(ceil(y*size(img,1))+ ceil(x/2*size(img,2))*size(img,1));
fill(o*x+s*r,o*y+c*r,C)
axis image
axis off
set(gca,'YDir','reverse')

