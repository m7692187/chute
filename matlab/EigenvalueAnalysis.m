function data=EigenvalueAnalysis(data)

D1 = [1 0 0; 0 0 0; 0 0 -1];
D2 = [0 0 0; 0 1 0; 0 0 -1];

for i=1:length(data.z)
    %deviatoric stress tensor
    %     A = [data.StressXX(i)-data.Pressure(i) data.StressXY(i) data.StressXZ(i);
    %         data.StressXY(i) data.StressYY(i)-data.Pressure(i) data.StressYZ(i);
    %         data.StressXZ(i) data.StressYZ(i) data.StressZZ(i)-data.Pressure(i)];
    %thomas (this is symmetrized; no problem in remco's case)
    A=[...
      data.StressXX(i) data.StressXY(i) data.StressXZ(i);...
      data.StressYX(i) data.StressYY(i) data.StressYZ(i);...
      data.StressZX(i) data.StressZY(i) data.StressZZ(i);...
      ] - diag([1 1 1]) * data.Pressure(i);
    A=(A+A')/2;

    %set isnan's to zero
    A(isnan(A))=0;
    
    %get eigenvectors and eigenvalues
    [vec,l] = eig(A);
        
    %eigenvector corresponding to maximum eigenvalue
    if max([l(1,1),l(2,2),l(3,3)])==l(1,1)
        vmax = vec(:,1);
        maxeig = 1;
    elseif max([l(1,1),l(2,2),l(3,3)])==l(2,2)
        vmax = vec(:,2);
        maxeig = 2;
    else
        vmax = vec(:,3);
        maxeig = 3;
    end
    
    %get angles w.r.t. axis
    data.angleyz(i) = atand(vmax(2)/vmax(3)); % divide y and z component of vector
    data.anglexz(i) = atand(vmax(1)/vmax(3)); % divide x and z component of vector
    data.anglexy(i) = atand(vmax(2)/vmax(1)); % divide y and x component of vector
    
    %warning('thomas: where does the factor 2 come from?')
    T = [cosd(2*data.anglexz(i)) 0 sind(2*data.anglexz(i));
        0 0 0;
        sind(2*data.anglexz(i)) 0 -cosd(2*data.anglexz(i))];
    
    R = [cosd(-data.anglexz(i)) 0 -sind(-data.anglexz(i));
        0 1 0;
        sind(-data.anglexz(i)) 0 cosd(-data.anglexz(i))];
    
    
    % transform to principal stress
    %rotate in xz plane
    PStress(:,:,i)=R'*A*R;
    PS(i,1) = PStress(1,1,i);
    PS(i,2) = PStress(2,2,i);
    PS(i,3) = PStress(3,3,i);
    
    PST = [PS(i,1) 0 0; 0 PS(i,2) 0; 0 0 PS(i,3)];
    
    % back transform to deviatoric stress tensor
    ST(:,:,i)=R'*PST*R;
    
    % rotated stress tensor
    %lambda(i,1)=PS3(i);
    %lambda(i,2)=PS2(i);
    %lambda(i,3)=PS1(i);
    
%     lambdaUS(i,:) = sum(l,1)';
%     data.lambda(i,1) = lambdaUS(i,3);
%     data.lambda(i,2) = lambdaUS(i,2);
%     data.lambda(i,3) = lambdaUS(i,1);
    data.lambda(i,3:-1:1) = PS(i,:);
        
    % NOTE:
    % something goes wrong here..... use PS1..3 and ST defined above
    ConstStress(:,:,i) = (data.lambda(i,1)*R'*D1*R + data.lambda(i,2)*R'*D2*R);
    ConstStress2(:,:,i) = (R*A*R');
    
end

ix=data.z>1;
A=([...
  sum(data.Nu(ix).*data.StressXX(ix)) sum(data.Nu(ix).*data.StressXY(ix)) sum(data.Nu(ix).*data.StressXZ(ix));...
  sum(data.Nu(ix).*data.StressYX(ix)) sum(data.Nu(ix).*data.StressYY(ix)) sum(data.Nu(ix).*data.StressYZ(ix));...
  sum(data.Nu(ix).*data.StressZX(ix)) sum(data.Nu(ix).*data.StressZY(ix)) sum(data.Nu(ix).*data.StressZZ(ix));...
  ] - diag([1 1 1]) * sum(data.Nu(ix).*data.Pressure(ix)))./sum(data.Nu(ix));
 [V,D]=eig((A+A')/2);

%set isnan's to zero
A(isnan(A))=0;

%get eigenvectors and eigenvalues
[vec,l] = eig(A);

%eigenvector corresponding to maximum eigenvalue
if max([l(1,1),l(2,2),l(3,3)])==l(1,1)
    vmax = vec(:,1);
    maxeig = 1;
elseif max([l(1,1),l(2,2),l(3,3)])==l(2,2)
    vmax = vec(:,2);
    maxeig = 2;
else
    vmax = vec(:,3);
    maxeig = 3;
end

%get angles w.r.t. axis
data.FlowAngleyz = atand(vmax(2)/vmax(3)); % divide y and z component of vector
data.FlowAnglexz = atand(vmax(1)/vmax(3)); % divide x and z component of vector
data.FlowAnglexy = atand(vmax(2)/vmax(1)); % divide y and x component of vector
    
return