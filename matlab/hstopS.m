function hstopStefan

figure(1); clf; cla; hold on

Case = 9;
[Angle,Height,N,static,Title]=load_hstop(Case);
[Astop Hstop, MidAngle, MidHeight]=fit_Hstop(Angle, Height, N, static);
c=fit_Hstop_Stefan(MidAngle,MidHeight);

plot(Angle(static),Height(static),...
  'ko','MarkerFaceColor','k','DisplayName','arrested'); 
plot(Angle(~static),Height(~static),...
  'ko','DisplayName','steady'); 
plot(Astop,Hstop,...
  'k','DisplayName','fit'); 
plot(Astop,feval(c,Astop),...
  'r','DisplayName','fit Stefan'); 
legend('show')
xlabel('\theta')
ylabel('h')
title(Title)
saveas(1,'hstop.fig')
saveas(1,'hstop.pdf')
return

function [Angle,Height,N,static,Title]=load_hstop(Case)
% loads hstop.mat file; if it does not exist, it will create the file.
% hstop.mat contains the data used to fit hstop
if true%~exist('hstop.mat','file')
    %clear all; clear global
    global Data  %#ok<TLEV>
    loadData_friction
    N=cell(size(Data));
    Height=cell(size(Data));
    Angle=cell(size(Data));
    static=cell(size(Data));
    Title=cell(size(Data));
    for Case = 1:length(Data)
      N{Case} = cellfun(@(data)data.N,Data{Case}.hstop);
      Height{Case} = cellfun(@(data)data.FlowHeight,Data{Case}.hstop);
      Angle{Case}  = cellfun(@(data)data.ChuteAngle,Data{Case}.hstop);
      static{Case} = cellfun(@(data)data.time(end)<499,Data{Case}.hstop);
      Title{Case} = Data{Case}.title;
    end
    save('hstop.mat','Title','Angle','Height','N','static')
else
    load hstop.mat
end
%Only output one case
Angle = Angle{Case};
Height = Height{Case};
N = N{Case};
static = static{Case};
Title = Title{Case};
return

function [Astop, Hstop, midAngle, midHeight, CoeffHstop]=fit_Hstop(Angle, Height, N, static)
% creates fits to the hstop curve

%for each flowing case, look left and down for next static case
leftRange=[];
Amin=min(Angle(static));
downRange=[Amin max(63,max(Height)) max(Height)+5];

%for all arrested flows
for m=find(static)
  %if it is the rightmost arrested case
  if (Angle(m)==max(Angle(Height(m)==Height&(static))))
    %find next flowing case at same height
    right=(N==N(m))&~static;
    Amin=min(Angle(right));
    if ~isempty(Amin), 
      leftRange(end+1,:)=[Height(m) Angle(m) Amin]; %#ok<AGROW>
    end
  end
  %if it is the upmost arrested case
  if (Height(m)==max(Height(Angle(m)==Angle&(static))))
    %find next flowing case at same angle
    up=Angle==Angle(m)&~static;
    Hmin=min(Height(up));
    if ~isempty(Hmin)&&(Hmin-Height(m))<3, 
      downRange(end+1,:)=[Angle(m) Height(m) Hmin];  %#ok<AGROW>
    end
  end
end
if isempty(leftRange)
    Amin=min(Angle(static));
    leftRange = [min(Height) Amin Amin; ...
                         max(Height) Amin Amin;];
end
[Astop,Hstop,CoeffHstop]=hstop_fit2(leftRange,downRange);
midHeight = [leftRange(:,1); mean(downRange(:,2:3),2)];
midAngle = [mean(leftRange(:,2:3),2); downRange(:,1)];
disp(['theta1 theta2 A = ' num2str([atand(CoeffHstop(1:2)) CoeffHstop(3)])])
return

function c=fit_Hstop_Stefan(Angle,Height)
% creates fits to the hstop curve
s = fitoptions('Method','NonlinearLeastSquares',...
'Lower',[0 0 0],...
'Upper',[Inf Inf Inf],...
'Startpoint',[1 1 1]);
f = fittype('h0/(a-a0)-b0','options',s,'independent','a','coefficients',{'a0','b0','h0'});
c = fit(Angle,Height,f);
CoeffHstop = coeffvalues(c)';
disp(['a0 b0+h0/b0 b0  = ' num2str([CoeffHstop(1) CoeffHstop(1)+CoeffHstop(3)/CoeffHstop(2) CoeffHstop(2)]) ' (compare to theta1 theta2 A)'])
disp(['a0 b0 h0        = ' num2str([CoeffHstop'])])
return