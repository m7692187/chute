%% create all plots needed for the flow rule paper
function plots()
close all
clc;
global Title
loadData();
%figure_flowrule_mu
%tools()
plot_IF(7);

%paper()
order_figures();
%print_figures();
return

function tools()
%case 1 behaves very weird, see check_accelerating(1)
%check_oscillating(5) %use to determine which cases are oscillating
%check_accelerating(5)%use to determine which cases are accererating
%codestatus([1 2 4 5]) %to see which cases are assumed osc/acc/ste/
%plot_MuInertial(7);
%plot_flow_rule_Mu_fit(7);
%plot_MuInertial(1);
%plot_flow_rule_Mu_fit(1);
%plot_MuInertial(4);
%plot_flow_rule_Mu_fit(4);
% for i=[1 4:10]
%   plot_MuInertial(i);
% end
% close all
% plot_MuInertial(1);
%plot_flow_rule_Hstop_fit(1);

%plot_MuInertial(4);
%plot_MuInertial(7);
%plot_flow_rule_Mu_fit(1);
%plot_flow_rule_Mu_fit(4);
%plot_flow_rule_Mu_fit(7);
%plot_MuInertial(10);
%plot_flow_rule_Mu_fit(10);
plot_InertialNumber([7]) %shows that mu(I) model works in bulk
figure_shape_InertialNumber(7,30,28) %fig 15
table_flowrule([1:10]) %table 1 fitting
%ylim([0.33,0.55])
plot_flow_rule_Mu_fit([7]);
%ylim([0.33,0.55])
% plot_ene(1,10); ylim([10^3.5 max(ylim)]);
% plot_ene(1,20); ylim([10^3.5 max(ylim)]);
% plot_ene(1,30); ylim([10^3.5 max(ylim)]);
% plot_ene(1,40); ylim([10^3.5 max(ylim)]);
%plot_ene(7,30);
% figure_Field([1 7],20,24,'Nu') 
%figure_Field([1 7],10,22,'Nu') 
%figure_Field([1 7],30,21,'Nu') 
%figure_Field([1 7],30,24,'Nu') 
%figure_Field([1 7],30,25,'Nu') 
%figure_Field([1 7],10,20,'VelocityX') 
%figure_Field([1 7],10,22,'Nu') 
%figure_mu %to check how accurate mu=tantheta is 
%plot_flow_rule_Hstop_fit(9); plot_flow_rule_Hstop_fit_lambda(9); %to compare the hs(lambda) fit to hs(1)

%plot_hstop_all(7); %hstop
%plot_flow_rule_Hstop_fit(7); %flowrule
return

function paper() 
%close all
%plot_arresting_H20() %fig 3
%plot_silbert_comparison(); %fig 4
%w % fig 5/6
%figure_shape_K(); %fig 7
%plot_depthprofile_VelocityX_HA_fixed([1 4 4 5 7 9 10], 30); % fig 8
%plot_hstop_diagram([4 5 7 9 10],'hstoponly');  xlim([18.5 26]) %fig 9
%figure_hstop(); %fig 10
%figure_flowrule_rough(); %fig 11
%supportedflow
%layeredflow
%table_density_fit3(7); %fig 12
fitalpha() %fig 13, table 2
figure_shape_Strain() %fig 14
%figure_shape_InertialNumber([1 4 5 7 9 10],30,25) %fig 15
%table_flowrule([1:10]) %table 1 fitting

%supporting figures:
%plot_beta([1 4:10]); %plot fitting parameters
%plot_beta([1 4:10]); set(gcf,'FileName',['beta_tight']); ylim([-0.15,1]); %plot fitting parameters
%plot_A_theta1_theta2(1:10); %plot fitting parameters
%table_K_fit3([4:10],'useyy'), %check anisotropy in y
%table_K_fit3([4:10]), %check anisotropy in x
%table_K_fit3(1), %check anisotropy in x
%table_K_fit3(1,'useyy'), %check anisotropy in x
%table_K_fit3(1) %page 16, K^{fit} value
return

function plot_beta(CaseList)
global Data Flow Title
figure(); clf; hold on
set(gcf,'Position',[0 680 600 1*420])
set(gcf,'FileName',['beta'])

L=cellfun(@(d)str2num(d(10:end-1)),Title(CaseList));
beta=cellfun(@(d) d.betaPO(2),Flow(CaseList));
gamma=cellfun(@(d) d.betaPO(1),Flow(CaseList));
betamin=cellfun(@(d)  d.confPO(2,1),Flow(CaseList));
betamax=cellfun(@(d)  d.confPO(2,2),Flow(CaseList));
gammamin=cellfun(@(d) d.confPO(1,1),Flow(CaseList));
gammamax=cellfun(@(d) d.confPO(1,2),Flow(CaseList));
%var=cellfun(@(d) d.varPO,Flow(CaseList));
disp(['beta: ' num2str(beta)])
disp(['gamma: ' num2str(gamma)])
hold on
% plot(1:length(L),betamin,':','DisplayName',{'$\beta_{min}$'},'LineWidth',0.5);
% plot(1:length(L),betamax,'-.','DisplayName',{'$\beta_{max}$'},'LineWidth',0.5);
% plot(1:length(L),gammamin,':','Color','g','DisplayName',{'$\gamma_{min}$'},'LineWidth',0.5);
% plot(1:length(L),gammamax,'-.','DisplayName',{'$\gamma_{max}$'},'LineWidth',0.5);
errorbar(L,beta,beta-betamin,betamax-beta,'Color',.8*[1 1 1 ],'DisplayName','$\beta$','LineWidth',0.5)
errorbar(L,gamma,gamma-gammamin,gammamax-gamma,'Color',.8*[1 1 1 ],'DisplayName','$\gamma$','LineWidth',0.5)
plot(L,[beta; gamma],'x-','DisplayName',{'$\beta$';'$\gamma$'},'LineWidth',0.5);

% plot(1:length(L),[ gammamin],':','DisplayName',{'$\beta$';'$\gamma$'},'LineWidth',0.5);
% plot(1:length(L),[ gammamax],'.-','DisplayName',{'$\beta$';'$\gamma$'},'LineWidth',0.5);
c=get(gca,'Children');
set(c(1),'Marker','x'); 
set(c(2),'Marker','o'); 
set(gca,'XTick',L)

LLabel=cell(0);
for Case=CaseList
  LLabel{end+1}=['$' Title{Case}(10:end-1) '$'];
end
set(gca,'XTickLabel',LLabel)

xlabel('$\lambda$')
legend(c(1:2));
set(legend,'Location','North','Box','off','Orientation','horizontal');
ylim([min([beta gamma 0]),max([beta gamma 0])]);
axis tight

% $$\begin{array}{|l| |l|l|l| |l|l| |l|}
% \hline
% \lambda & \delta_{1,\lambda} & \delta_{2,\lambda} & A_\lambda & \beta_\lambda & \gamma_\lambda & \text{err}  \\\hline\hline
% 0 & 12.25 & 12.25 & - & 1.500 & -4.065 & 0.886  \\\hline
% \end{array}$$
disp('$$\begin{array}{|l|l|l|}\hline')
disp('\lambda & \beta_\lambda & \text{err}  \\\hline\hline')
for k=1:length(CaseList); Case = CaseList(k);
  % 0 & 12.25 & 12.25 & - & 1.500 & -4.065 & 0.886  \\\hline
  disp(sprintf('%s & %f & %f \\\\\\hline',Title{Case}(10:end-1),beta(k),gamma(k)))
end
disp('\end{array}$$')

return

function plot_A_theta1_theta2(CaseList)
global Data Stop Title CoeffHstop VarHstop

figure(); clf; hold on
set(gcf,'Position',[0 680 600 1*420])
set(gcf,'FileName',['A_theta1_theta2'])

L=cellfun(@(d)str2num(d(10:end-1)),Title(CaseList));
A=cellfun(@(d) d(3),CoeffHstop(CaseList));

t1=cellfun(@(d) atand(d(1)),CoeffHstop(CaseList));
t2=cellfun(@(d) atand(d(2)),CoeffHstop(CaseList));
Avar=cellfun(@(d) d(3),VarHstop(CaseList));
t1var=cellfun(@(d) atand(d(1)),VarHstop(CaseList));
t2var=cellfun(@(d) atand(d(2)),VarHstop(CaseList));
disp(['A: ' num2str(A)])
disp(['delta1: ' num2str(t1)])
disp(['delta2: ' num2str(t2)])
errorbar(L(3:end),A(3:end),Avar(3:end),'Color',0.6*[1 1 1])
errorbar(L,t1,t1-atand(tand(t1)-tand(t1var)),atand(tand(t1)+tand(t1var))-t1,'Color',0.6*[1 1 1])
errorbar(L,t2,t2-atand(tand(t2)-tand(t2var)),atand(tand(t2)+tand(t2var))-t2,'Color',0.6*[1 1 1])
plot(L(3:end),A(3:end),'bx-','DisplayName','$A$','LineWidth',0.5);
plot(L,t1,'rx-','DisplayName','$\delta_1$','LineWidth',0.5);
plot(L,t2,'gx-','DisplayName','$\delta_2$','LineWidth',0.5);
c=get(gca,'Children');
set(c(1),'Marker','x'); 
set(c(2),'Marker','o'); 
set(c(3),'Marker','d'); 
xlabel('$\lambda$')
legend(c(1:3));
set(legend,'Location','North','Box','off','Orientation','horizontal');
axis tight
ylim([min([t2 t1 A 0]),max([t2 t1 A 40])]);

disp('$$\begin{array}{|l|l|l|l|}\hline')
disp('\lambda & \delta_{1,\lambda} & \delta_{2,\lambda} & A_\lambda & \text{err}  \\\hline\hline')
for k=1:length(CaseList); Case = CaseList(k);
  % 0 & 12.25 & 12.25 & - & 1.500 & -4.065 & 0.886  \\\hline
  disp(sprintf('%s & %f & %f & %f & %f \\\\\\hline',Title{Case}(10:end-1),t1(k),t2(k),A(k)))
end

return


function check_accelerating(CaseList)
for i=CaseList
  for j=1:4
  plot_ene(i,j*10) 
  set(gcf,'Position',[ceil(j/2)*560 rem(j,2)*420  560 420])
  ylim([max(ylim)/5000 max(ylim)])
  axis(axis+1e-10*[-1 1 -1 1])
  end
end
return

function check_oscillating(CaseList)
for i=CaseList
  for j=1:4
  plot_depthprofile(i,j*10,20:30,[],'Nu')
  set(gcf,'Position',[ceil(j/2)*560 rem(j,2)*420  560 420])
  axis(axis+1e-10*[-1 1 -1 1])
  end
end
return

function check_hstop(CaseList)
for i=CaseList
  %plot_flow_rule_Hstop_fit(i);
  plot_simple_hstop_diagram(i);
  axis tight
  axis(axis+1e-10*[-1 1 -1 1])
end
return

function poster() 
close all
%%%% rough case
% plot_hstop_diagram(2:6,'hstoponly');
% plot_flow_rule_Hstop_fit_color_oneplot(2:6);
% % set(gcf,'Position',[2000  680 560*2 420])
% plot_arresting_H20();
% ylabel('E_{kin}/E_{ela}')
% legend('off')
%plot_depthprofile_hydrostatic(5,20);
% plot_friction([2 3 5]);
% disp('press key to print figures'); pause;
%plot_ha_K(5);
plot_depthprofile_NormalStresses(5,30,28);
%plot_ha_alpha(5);
print_poster();
return

function plot_simple_hstop_diagram(CaseList,opt) 
%load data
global Data Astop Hstop Astop_exp Title Stop Flow CoeffHstop
M=5;

%new figure
sfigure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['hstop_diagram'  sprintf('_%d',CaseList)])
if ~exist('CaseList','var'), CaseList=1:length(Data); end

%create subplots
if ~exist('opt','var')|~strcmp(opt,'errorbarsonly')
  for k=1:length(CaseList); Case=CaseList(k);
    plot(Stop{Case}.Angle(Stop{Case}.static),Stop{Case}.Height(Stop{Case}.static),...
      'or','MarkerFaceColor','r','MarkerSize',M); 
    hold on
    plot(Stop{Case}.Angle(~Stop{Case}.static),Stop{Case}.Height(~Stop{Case}.static),...
      'or','MarkerSize',M)
  end
  axis tight; v=axis;
end
for k=1:length(CaseList); Case=CaseList(k);
  %plot Pouliquen fit
  plot(Astop{Case},Hstop{Case},'Color','r','LineWidth',1,'DisplayName',Title{Case});%,linetype(1:length(CaseList),k));
  disp([ 'Case: ' num2str(Case) ', \delta_i: ' num2str(atand(CoeffHstop{Case}(1:2)))  ', A: ' num2str(CoeffHstop{Case}(3)) ])
end

if true %if you want to plot the flow cases
for k=1:length(CaseList); Case=CaseList(k);
  Height=cellfun(@(data)data.FlowHeight,Data{Case}.flow);
  Angle=cellfun(@(data)data.ChuteAngle,Data{Case}.flow);
  for H=unique(Flow{Case}.H)'
    ind = Flow{Case}.H==H & Flow{Case}.Flowing;
    plot(Angle(ind),Height(ind),'ob','MarkerSize',M);
  end
end
end

if true %if you want to plot the intervalls
  for k=1:length(CaseList); Case=CaseList(k);
    %for each flowing case, look left and down for next static case
    downRange=Stop{Case}.downRange(1:end,:);
    downRange(1,3)=1.1*downRange(1,2); %cut top interval
    leftRange=Stop{Case}.leftRange;
    h=errorbar(downRange(:,1),(downRange(:,3)+downRange(:,2))/2,(downRange(:,3)-downRange(:,2))/2)
    set(h,'LineStyle','none')
    set(h,'Color',.8*[1 1 1])
    h=herrorbar((leftRange(:,3)+leftRange(:,2))/2,leftRange(:,1),(leftRange(:,3)-leftRange(:,2))/2);
    set(h(2),'LineStyle','none')
    set(h(1),'Color',.8*[1 1 1])
  end
  axis tight; v=axis;
end

xlabel('$\theta$')
ylabel('$h/d$')
C=get(gca,'Children');
legend(C(end),[Title{CaseList}])
axis tight
set(legend,'Box','off')
ylim([0 55])
xlim([18 29])
return

function figure_enekin() 
plot_arresting_H20();
% plot_arresting(5,20);
for child=get(gca,'Children')'
  Angle=str2double(get(child,'Tag'));
  if Angle>29.5
    set(child,'LineStyle','--');
  elseif Angle<21
    set(child,'LineWidth',2);
  end
end
xlim([0,2000])
% plot_accelerating(5,20);
return

function figure_flowrule_rough() 
%plot_ha_bottom_friction(5);
%plot_flow_rule_Hstop_fit(5,'jenkins');
plot_flow_rule_Hstop_fit(1);
plot_flow_rule_Hstop_fit(4);
% plot_flow_rule_Hstop_fit(5);
% plot_flow_rule_Hstop_fit(7);
 plot_flow_rule_Hstop_fit_color_oneplot([1 4 5 7 9 10]);
return

function figure_flowrule() 
%plot_hstop_diagram(4);
%plot_hstop_diagram(2:6,'hstoponly');
%plot_hstop_diagram(2);
plot_flow_rule_Hstop_fit(1);
%plot_flow_rule_Hstop_fit(2);
plot_flow_rule_Hstop_fit_color_oneplot(1:6);
% %  plot_ha_alpha(3);
% plot_hstop_diagram(2)
return

function figure_flowrule_mu() 
%plot_hstop_diagram(4);
%plot_hstop_diagram(2:6,'hstoponly');
%plot_hstop_diagram(2);
plot_flow_rule_Mu_fit(7);
plot_flow_rule_Mu_fit_oneplot([1 2 4 5 6 8 10 ],struct('nodsata',true));
axis([0 0.25 0.3 0.53])
%plot_flow_rule_Hstop_fit(1);
%plot_flow_rule_Hstop_fit(2);
%plot_flow_rule_Hstop_fit_color_oneplot(1:6);
% %  plot_ha_alpha(3);
% plot_hstop_diagram(2)
return

function figure_depthprofiles() 
%plot_depthprofile_VelocityX_H(5,30);
%ylim([0,2.2])
%plot_depthprofile_VelocityX_HA(1:6,30,24);
plot_depthprofile_VelocityX_HA_fixed();
%plot_depthprofile(2,30,[21 22 23 24 25 26 27 28 ],[],'VelocityX')
%plot_depthprofile(5,30,[21 22 23 24 25 26 27 28 ],[],'VelocityX')
%plot_depthprofile_HA(1:6,30,24);
%plot_depthprofile_StressZZ_HA(1:6,30,24);
return

function figure_hstop() 
plot_hstop_all(4);
c=get(gca,'Children');
delete(c(1:3));

v=axis(); xlim([17.5 max(xlim)])
plot([25.5 25.5 25.5 25.5],[v(4) 15 15 v(3)],'-.','Color',[1 1 1]*0.7)
plot([29 29],[v(4) v(3)],'--','Color',[1 1 1]*0.7)
plot([17.9 17.9],[v(4) v(3)],'--','Color',[1 1 1]*0.7)
plot([20.7 20.7],[v(4) v(3)],'--','Color',[1 1 1]*0.7)
text(17.9,v(3)+2.05,['$\!\shortleftarrow\!\delta_1^{1/2}$'],'HorizontalAlignment','left')
text(20.7,v(4)*.95,['$\leftarrow\delta_2^{1/2}$'],'HorizontalAlignment','left')
text(25.5,v(4)*.95,['$\delta_3^{1/2}\rightarrow$'],'HorizontalAlignment','right')
text(29  ,v(4)*.95,['$\delta_{acc}^{1/2}\rightarrow$'],'HorizontalAlignment','right')

plot_hstop_all(1); xlim([11.5 30])
v=axis();
plot([20.5 20.5 22.5 22.5 23.5 23.5 21 21],[v(3) 15 15 25 25 31 31 v(4)],'-.','Color',[1 1 1]*0.7)
plot([22.5 22.5 24.5 24.5 25.5 25.5 21 21],[v(3) 13 13 25 25 31 31 v(4)],'--','Color',[1 1 1]*0.7)
text(20.5,v(3)+2,['$\delta_3^{0}\rightarrow$'],'HorizontalAlignment','right')
text(22.5  ,v(3)+2,['$\longleftarrow\delta_{acc}^{0}$'],'HorizontalAlignment','left')

return

function figure_mu() 
plot_depthprofile_mu(7,30);
%plot_depthprofile_I_H(7,30);
%plot_depthprofile_I_A(7,26);
plot_depthprofile_mu(7,10);
%plot_depthprofile_I_H(7,10);
%plot_ha_bottom_friction(7);
plot_flow_rule_Mu_fit([7]);
%plot_flow_rule_Mu_fit_oneplot([1 4:10]);
return

function figure_shape_alpha() 
%C=2; H=40; A=20:30; plot_depthprofile(C,H,A,4,'VelocityX');
%C=5; H=20; A=20:30; plot_depthprofile(C,H,A,5,'VelocityX');
%plot_depthprofile_VelocityX_H(5,30);
%plot_depthprofile_VelocityX_H(5,10,'linear');
plot_ha_alpha(7);
%plot_ha_alpha(4);
%plot_ha_alpha(3);
plot_ha_alpha_List([1 4:10]);
%set(legend,'Position',get(legend,'Position')+[.05 .1 0 0])
%table_alpha();

%plot_depthprofile_VelocityX_H(3,30);
%plot_depthprofile_VelocityX_H(3,10,'linear');
%plot_ha_alpha(3);
return

function figure_shape_K() 
%plot_depthprofile_NormalStresses(5,10,30);
plot_depthprofile_NormalStresses(7,30,28);
%plot_ha_K([5 2]);
%plot_ha_K(1);
%plot_ha_K(5);
%plot_ha_K_hstop(5);
%plot_ha_K_List(1:6);
%plot_depthprofile_NormalStresses(6,30,24);
%plot_depthprofile_NormalStresses(2,20,23);
%plot_depthprofile_NormalStresses(2,20,21);
%plot_ha_K(2);
% plot_depthprofile_NormalStressesWhalf(5,20);
return

function figure_shape_density() 
% plot_depthprofile_H(5,30);
% plot_depthprofile_H(1,30);
% %plot_ha_density(5);
% plot_ha_density(5,'centre');
% plot_ha_density(6,'centre');
% plot_ha_density(1,'centre');
plot_ha_density_fit(2:6);
%ylim([0.47,0.62])
%plot_depthprofile_H(2,30);
%plot_ha_density2(2:5);
%plot_ha_density_centre(2:5);
%plot_ha_density_centre(5);
%plot_ha_density2(2);
%plot_ha_density_centre(2);
% plot_depthprofile_NormalStressesWhalf(5,20);
return

function figure_oscillating() 
plot_depthprofile_H(2,30);

data=loadstatistics({'../stat_flowrule_T600short/H30A26L0.5M0.5B0.5.stat','../stat_flowrule/H30A26L0.5M0.5B0.5.stat'});
figure(); clf; hold on
set(gcf,'Position',[2000 680 560 420])
set(gcf,'FileName','oscillating')

plot(data{1}.z,[data{1}.Nu data{2}.Nu])

legend({'t=600','t=2000'},'Location','NorthEast')
xlabel('$(z-b)/h$','Interpreter','none')
ylabel('$\rho/\rho_p$','Interpreter','none')
axis tight; v=axis; axis([0 v(2:4)]);

codestatus_oscillating(1:6);
return

function table_fit()
disp('Fitting for $\bar\rho(\theta,h)$')
disp('$$\begin{array}{|r|r|r|r|r|r|r|r|r|r|r|r|}\hline')
disp('\lambda & 1 & (h-40) & (\theta-30) & (h-40)^2 & (\theta-30)^2 & (h-40)(\theta-30) & \text{var}\\\hline')
table_density_fit(1:6);
disp('\hline\end{array}$$')

disp('Fitting for $\alpha(\theta,h)$')
disp('$$\begin{array}{|r|r|r|r|r|r|r|r|r|r|r|r|}\hline')
disp('\lambda & 1 & (h-40) & (\theta-30) & (h-40)^2 & (\theta-30)^2 & (h-40)(\theta-30) & \text{var}\\\hline')
table_alpha_fit(1:6);
disp('\hline\end{array}$$')

disp('Fitting for $K(\theta,h)$')
disp('$$\begin{array}{|r|r|r|r|r|r|r|r|r|r|r|r|}\hline')
disp('\lambda & 1 & (h-40) & (\theta-30) & (h-40)^2 & (\theta-30)^2 & (h-40)(\theta-30) & \text{var}\\\hline')
table_K_fit(1:6);
disp('\hline\end{array}$$')
return

function table_K_fit(CaseList)
global Data Flow Title
figure(); hold on
colors='kbrg';
%disp('K')
for i=1:length(CaseList); Case=CaseList(i);
  ind=Flow{Case}.Flowing&~Flow{Case}.Oscillating&Flow{Case}.Angle<30;
%   ind=Flow{Case}.Flowing&Flow{Case}.Oscillating&~Flow{Case}.FullyOscillating&Flow{Case}.Angle<30;
  Heights=unique(Flow{Case}.H(ind));
  Angles=unique(Flow{Case}.A(ind));
  FlowStressXX=cellfun(@(data) mean(data.StressXX(data.z>data.Base+2&data.z<data.Surface)), Data{Case}.flow);
  FlowStressZZ=cellfun(@(data) mean(data.StressZZ(data.z>data.Base+2&data.z<data.Surface)), Data{Case}.flow);
  K=FlowStressXX./FlowStressZZ;
  x1=Flow{Case}.Height(ind)-40;
  x2=Flow{Case}.A(ind)-28;
  y=K(ind);
%  X = [ones(size(x1))  x1  x2  x2.^2  x1.^2  x1.*x2  x1.^3  x2.^3  x1.^2.*x2  x2.^2.*x1];
%   X = [ones(size(x1))  x1  x2  x2.^2  x1.^2 x1.*x2];
  X = [ones(size(x1))  x1  x2];
  a = X\y;
  Y = X*a;
  MaxErr = max(abs(Y - y));
  StdErr = stderr(Y-y); 
  disp([Title{Case}(10:end-1) ' & ' num2str(a','%.6f & ') num2str(StdErr,'%.3f\\\\')])
  for j=1:length(Heights)
    indH=(Heights(j)==Flow{Case}.H)&(ind);
    plot(Flow{Case}.A(indH),Y(Heights(j)==Flow{Case}.H(ind)),'-','Color',colors(j))
    plot(Flow{Case}.A(indH),K(indH),'x','Color',colors(j))
  end
end
return

function table_alpha_fit(CaseList)
global Data Flow Title
figure(); hold on
colors='kbrg';
%disp('A')
for i=1:length(CaseList); Case=CaseList(i);
  ind=Flow{Case}.Flowing&~Flow{Case}.Oscillating&Flow{Case}.Angle<30;
  % ind=~Flow{Case}.Accelerating;
  Heights=unique(Flow{Case}.H(ind));
  Angles=unique(Flow{Case}.A(ind));
% %   meanVelocityX=  cellfun(@(data) sum(data.VelocityX.*data.Nu)/sum(data.Nu), Data{Case}.flow);
%   meanVelocityX2= cellfun(@(data) sum(data.VelocityX.^2.*data.Nu)/sum(data.Nu), Data{Case}.flow);
  meanVelocityX=  cellfun(@(data) mean(data.VelocityX(data.z>data.Base&data.z<data.Surface)), Data{Case}.flow);
  meanVelocityX2= cellfun(@(data) mean(data.VelocityX(data.z>data.Base&data.z<data.Surface).^2), Data{Case}.flow);
  Alpha=meanVelocityX2./(meanVelocityX.^2);
  x1=Flow{Case}.Height(ind)-40;
  x2=Flow{Case}.A(ind)-28;
  y=Alpha(ind);
%  X = [ones(size(x1))  x1  x2  x2.^2  x1.^2  x1.*x2  x1.^3  x2.^3  x1.^2.*x2  x2.^2.*x1  x1.^4  x2.^4  x1.^3.*x2  x2.^3.*x1  x2.^2.*x1.^2];
%  X = [ones(size(x1))  x1  x2  x2.^2  x1.^2  x1.*x2  x1.^3  x2.^3  x1.^2.*x2  x2.^2.*x1];
  X = [ones(size(x1))  x1  x2  x2.^2  x1.^2 x1.*x2];
  a = X\y;
  Y = X*a;
  MaxErr = max(abs(Y - y));
  StdErr = stderr(Y-y); 
  disp([Title{Case}(10:end-1) ' & ' num2str(a','%.6f & ') num2str(StdErr,'%.3f\\\\')])
  for j=1:length(Heights)
    indH=(Heights(j)==Flow{Case}.H)&(ind);
    plot(Flow{Case}.A(indH),Y(Heights(j)==Flow{Case}.H(ind)),'-','Color',colors(j))
    plot(Flow{Case}.A(indH),Alpha(indH),'x','Color',colors(j))
  end
end
return

function table_density_fit(CaseList)
global Data Flow Title
figure(); hold on
colors='kbrg';
%disp('\rho')
for i=1:length(CaseList); Case=CaseList(i);
  ind=Flow{Case}.Flowing&~Flow{Case}.Oscillating&Flow{Case}.Angle<30;
  % ind=~Flow{Case}.Accelerating;
  Heights=unique(Flow{Case}.H(ind));
  Angles=unique(Flow{Case}.A(ind));
  Density=cellfun(@(data) sum(data.Nu)*diff(data.z(1:2))/data.FlowHeight, Data{Case}.flow);
%    Density=cellfun(@(data) mean(data.Nu(data.z>data.Base&data.z<data.FlowHeight)), Data{Case}.flow);
  x1=Flow{Case}.Height(ind)-40;
  x2=Flow{Case}.A(ind)-28;
  y=Density(ind);
  %X = [ones(size(x1))  x1  x2  x2.^2  x1.^2  x1.*x2  x1.^3  x2.^3  x1.^2.*x2  x2.^2.*x1];
  X = [ones(size(x1))  x1  x2  x2.^2  x1.^2 x1.*x2];
  a = X\y;
  Y = X*a;
  MaxErr = max(abs(Y - y));
  StdErr = stderr(Y-y); 
  disp([Title{Case}(10:end-1) ' & ' num2str(a','%.6f & ') num2str(StdErr,'%.3f\\\\')])
  for j=1:length(Heights)
    indH=(Heights(j)==Flow{Case}.H)&(ind);
    plot(Flow{Case}.A(indH),Y(Heights(j)==Flow{Case}.H(ind)),'-','Color',colors(j))
    plot(Flow{Case}.A(indH),Density(indH),'x','Color',colors(j))
  end
end
return

function table_fit2()
Case=2:6;

table_density_fit2(Case);

% table_alpha_fit2(Case);
% 
% table_K_fit2(2:6);
% table_Kyy_fit2(Case);

%print_figures(1000);
return

function table_K_fit2(CaseList)
global Data Flow Title CoeffHstop
f=figure(); hold on
set(gcf,'Position',[2000 0 4*560 1.2*420])
set(gcf,'FileName',['table_K_fit2'  sprintf('_%d',CaseList)])
colors='kbrg';
markers='oxd+';
%disp('K')
disp('Fitting for $K(\theta,h)=c_0+c_1(\theta-24^\circ)/1^\circ$')
disp('$$\begin{array}{|r|r|r|r|r|r|r|r|r|r|r|r|}\hline')
disp('c_0 & c_1 & \text{var}\\\hline')

H=[];
A=[];
L=[];
K=[];
for i=1:length(CaseList); Case=CaseList(i);
  ind=Flow{Case}.Flowing&~Flow{Case}.Oscillating&Flow{Case}.Angle<30;
  H = [H; Flow{Case}.Height(ind)];
  A = [A; Flow{Case}.A(ind)];
  L = [L; Flow{Case}.L(ind)];
  FlowStressXX=cellfun(@(data) mean(data.StressXX(data.z>data.Base+2&data.z<data.Surface)), Data{Case}.flow(ind));
  FlowStressZZ=cellfun(@(data) mean(data.StressZZ(data.z>data.Base+2&data.z<data.Surface)), Data{Case}.flow(ind));
  K=[K; FlowStressXX./FlowStressZZ];
end
%fitting
y = K;
X = [ones(size(H)) A-24];
a = X\y;
Y = X*a;
MaxErr = max(abs(Y - y));
StdErr = stderr(Y-y);%sqrt(mean((Y-y).^2));

plot(A,Y,'-k')
plot(A,K,'x')

disp([num2str(a','%.6f & ') num2str(StdErr,'%.3f\\\\')])
disp('\hline\end{array}$$')

return

function table_Kyy_fit2(CaseList)
global Data Flow Title CoeffHstop
f=figure(); hold on
set(gcf,'Position',[2000 0 4*560 1.2*420])
set(gcf,'FileName',['table_K_fit2'  sprintf('_%d',CaseList)])
colors='kbrg';
markers='oxd+';
%disp('K')
disp('Fitting for $K_{yy}(\theta,h)=c_0+c_1(\theta-24^\circ)/1^\circ$')
disp('$$\begin{array}{|r|r|r|r|r|r|r|r|r|r|r|r|}\hline')
disp('c_0 & c_1 & \text{var}\\\hline')

H=[];
A=[];
L=[];
K=[];
for i=1:length(CaseList); Case=CaseList(i);
  ind=Flow{Case}.Flowing&~Flow{Case}.Oscillating&Flow{Case}.Angle<30;
  H = [H; Flow{Case}.Height(ind)];
  A = [A; Flow{Case}.A(ind)];
  L = [L; Flow{Case}.L(ind)];
  FlowStressYY=cellfun(@(data) mean(data.StressYY(data.z>data.Base+2&data.z<data.Surface)), Data{Case}.flow(ind));
  FlowStressZZ=cellfun(@(data) mean(data.StressZZ(data.z>data.Base+2&data.z<data.Surface)), Data{Case}.flow(ind));
  K=[K; FlowStressYY./FlowStressZZ];
end
%fitting
y = K;
X = [ones(size(H)) A-24];
a = X\y;
Y = X*a;
MaxErr = max(abs(Y - y));
StdErr = stderr(Y-y);%sqrt(mean((Y-y).^2));

plot(A,Y,'-k')
plot(A,K,'x')

disp([num2str(a','%.6f & ') num2str(StdErr,'%.3f\\\\')])
disp('\hline\end{array}$$')

return

function table_alpha_fit2(CaseList)
global Data Flow Title CoeffHstop

markers='oxd+';
colors='kbrg';
%disp('A')
disp('Fitting for $\alpha(\theta,h)=c_0+c_1\exp(-c_2(\theta-\theta_s(h)/1^\circ)/h$')
disp('$$\begin{array}{|r|r|r|r|r|r|r|r|r|r|r|r|}\hline')
disp('\lambda & c_0 & c_1 & c_2 & \text{var}\\\hline')
for i=1:length(CaseList); Case=CaseList(i);
  figure(); hold on
  set(gcf,'Position',[2000  340 560 420])
  set(gcf,'FileName',['table_alpha_fit2' num2str(Case)])
  ind=Flow{Case}.Flowing&~Flow{Case}.Oscillating&Flow{Case}.Angle<30&Flow{Case}.Angle>20;
  % ind=~Flow{Case}.Accelerating;
  Heights=unique(Flow{Case}.H(ind));
  Angles=unique(Flow{Case}.A(ind));
% %   meanVelocityX=  cellfun(@(data) sum(data.VelocityX.*data.Nu)/sum(data.Nu), Data{Case}.flow);
%   meanVelocityX2= cellfun(@(data) sum(data.VelocityX.^2.*data.Nu)/sum(data.Nu), Data{Case}.flow);
  meanVelocityX=  cellfun(@(data) mean(data.VelocityX(data.z>data.Base&data.z<data.Surface)), Data{Case}.flow);
  meanVelocityX2= cellfun(@(data) mean(data.VelocityX(data.z>data.Base&data.z<data.Surface).^2), Data{Case}.flow);
  Alpha=meanVelocityX2./(meanVelocityX.^2);
  if (true)
    x1=Flow{Case}.Height(ind);
    x2=Flow{Case}.A(ind)-atand(tanthetastop(CoeffHstop{Case}, Flow{Case}.Height(ind)));
    x3=x2.*x1;
    y=Alpha(ind);
  %  X = [ones(size(x1))  x1  x2  x2.^2  x1.^2  x1.*x2  x1.^3  x2.^3  x1.^2.*x2  x2.^2.*x1  x1.^4  x2.^4  x1.^3.*x2  x2.^3.*x1  x2.^2.*x1.^2];
  %  X = [ones(size(x1))  x1  x2  x2.^2  x1.^2  x1.*x2  x1.^3  x2.^3
  %  x1.^2.*x2  x2.^2.*x1];
    if Case<3
      fitting = @(a) a(1)*ones(size(x2));
    else
      fitting = @(a) a(1)+a(2)*exp(-a(3)*x2)./x1;
    end
    error = @(a) sum((fitting(a)-y).^2);
    options=optimset('MaxFunEvals',1000);
    [a,f] = fminsearch(@(a) error(a),[1 1 0]',options); 
    Y=fitting(a);
  else
    x1=Flow{Case}.Height(ind)-40;
    %x1=Flow{Case}.Height(ind)./hstop(CoeffHstop{5},Flow{Case}.A(ind)); 
    x2=Flow{Case}.A(ind)-28;
    y=Alpha(ind);
  %  X = [ones(size(x1))  x1  x2  x2.^2  x1.^2  x1.*x2  x1.^3  x2.^3  x1.^2.*x2  x2.^2.*x1  x1.^4  x2.^4  x1.^3.*x2  x2.^3.*x1  x2.^2.*x1.^2];
  %  X = [ones(size(x1))  x1  x2  x2.^2  x1.^2  x1.*x2  x1.^3  x2.^3  x1.^2.*x2  x2.^2.*x1];
    X = [ones(size(x1))  x1  x2  x2.^2];
    a = X\y;
    Y = X*a;
  end
  MaxErr = max(abs(Y - y));
  StdErr = stderr(Y-y); 
  if Case<3
    disp([Title{Case}(10:end-1) ' & ' num2str(a(1),'%.6f & & & ') num2str(StdErr,'%.3f\\\\')])
  else
    disp([Title{Case}(10:end-1) ' & ' num2str(a','%.6f & ') num2str(StdErr,'%.3f\\\\')])
  end
  for j=1:length(Heights)
    indH=(Heights(j)==Flow{Case}.H)&(ind);
     plot(Flow{Case}.A(indH)-atand(tanthetastop(CoeffHstop{Case}, Heights(j))),...
       Y(Heights(j)==Flow{Case}.H(ind)),'-','Color',colors(j))
     plot(Flow{Case}.A(indH)-atand(tanthetastop(CoeffHstop{Case}, Heights(j))),...
       Alpha(indH),markers(j),'Color',colors(j))

    %plot((Flow{Case}.A(indH)-atand(tanthetastop(CoeffHstop{Case}, Heights(j))))*Heights(j),Alpha(indH),'x-','Color',colors(j))
%    plot(Flow{Case}.A(indH)-atand(tanthetastop(CoeffHstop{Case}, Heights(j))),Alpha(indH),'x-','Color',colors(j))
  end
  legend('show')
  title(Title{Case});
  xlabel('$\theta-\theta_s(h)$')
  ylabel('$\alpha$')
  %set(gca,'YScale','log')
  %ylim([0 20])

  if (Case==5),
    figure(1000);
    set(gcf,'Position',[2000 340 560 420])
    set(gcf,'FileName',['table_fittingfull_' num2str(Case)])
    cla; hold on
    for j=length(Heights):-1:1
      indH=(Heights(j)==Flow{Case}.H)&(ind);
      plot(Flow{Case}.A(indH)-0*atand(tanthetastop(CoeffHstop{Case}, Heights(j))),...
        Alpha(indH),...
        markers(j),'Color',colors(j),'MarkerSize',3,...
        'DisplayName',['$H=' num2str(Heights(j)) '$']);
    end
    legend('show')
    %set(legend,'Box','off')
    for j=1:length(Heights)
      indH=(Heights(j)==Flow{Case}.H)&(ind);
      plot(Flow{Case}.A(indH)-0*atand(tanthetastop(CoeffHstop{Case}, Heights(j))),...
         Y(Heights(j)==Flow{Case}.H(ind)),'-','Color',colors(j))
    end
%     xlabel('$\theta-\theta_s(h)$')
    xlabel('$\theta$')
    ylabel('$\alpha$')
    axis tight
  end
end
disp('\hline\end{array}$$')
return

function table_density_fit2(CaseList)

global Data Flow Title CoeffHstop
f=figure(); hold on
set(gcf,'Position',[2000 0 4*560 1.2*420])
set(gcf,'FileName',['table_K_fit2'  sprintf('_%d',CaseList)])
colors='kbrg';
markers='oxd+';
%disp('K')
disp('Fitting for $K(\theta,h)=c_0+c_1(\theta-24^\circ)/1^\circ$')
disp('$$\begin{array}{|r|r|r|r|r|r|r|r|r|r|r|r|}\hline')
disp('c_0 & c_1 & \text{var}\\\hline')

H=[];
A=[];
L=[];
Density=[];
for i=1:length(CaseList); Case=CaseList(i);
  ind=Flow{Case}.Flowing&~Flow{Case}.Oscillating&Flow{Case}.Angle<30;
  H = [H; Flow{Case}.Height(ind)];
  A = [A; Flow{Case}.A(ind)];
  L = [L; Flow{Case}.L(ind)];
  Density=[Density; cellfun(@(data) sum(data.Nu)*diff(data.z(1:2))/data.FlowHeight, Data{Case}.flow(ind))];
end
%fitting
y=Density;
fitting = @(a) a(1)-a(2)*exp(a(3)*(A-24))./H.^a(4);
error = @(a) sum((fitting(a)-y).^2);
options=optimset('MaxFunEvals',10000,'MaxIter',10000);
[a,f] = fminsearch(@(a) error(a),[0.6 0.03 0.2 0.2]',options); 
Y=fitting(a);
MaxErr = max(abs(Y - y));
StdErr = stderr(Y-y);%sqrt(mean((Y-y).^2));

plot(A,Y,'-k')
plot(A,Density,'x')

disp([num2str(a','%.6f & ') num2str(StdErr,'%.3f\\\\')])
disp('\hline\end{array}$$')

return

function table_fit3()
Case=1:6;

 table_density_fit3(7);

 table_alpha_fit3(7);

 %table_K_fit3();

%print_figures(1000);
return

function table_KY_fit3(opt)
global Data Flow Title CoeffHstop
CaseList=2:6;
f=figure(); hold on
set(gcf,'Position',[2000 0 560 420])
set(gcf,'FileName',['table_KY_fit'  sprintf('_%d',CaseList)])
hold on
colors='kbrg';
markers='oxd+';
%disp('K')
disp('Fitting for $K(\theta,h)=c_0+c_1(\theta-24^\circ)/1^\circ$')
disp('$$\begin{array}{|r|r|r|r|r|r|r|r|r|r|r|r|}\hline')
disp('\lambda & c_0 & c_1 & \text{var}\\\hline')

A=[];
K=[];
H=[];
L=[];
%  for Case=2:6
for Case=CaseList
  ind=Flow{Case}.Flowing&~Flow{Case}.Oscillating&~Flow{Case}.Accelerating;
  A=[A; Flow{Case}.A(ind)];
  H=[H; Flow{Case}.H(ind)];
  L=[L; Flow{Case}.L(ind)];
  FlowStressYY=cellfun(@(data) mean(data.StressYY(data.z>data.Base+2&data.z<data.Surface)), Data{Case}.flow(ind));
  FlowStressZZ=cellfun(@(data) mean(data.StressZZ(data.z>data.Base+2&data.z<data.Surface)), Data{Case}.flow(ind));
  K=[K; FlowStressYY./FlowStressZZ];
end
uniqueA=unique(A)
for i=1:length(uniqueA) 
  ind=(A==uniqueA(i))&(H==20);
  plot(L(ind),K(ind),'-','Color',colors(rem(i,4)+1))
  hold on
end
hold off
xlabel('$\theta$')
ylabel('$\bar{\sigma}_{yy}/\bar{\sigma}_{zz}$')
return

function table_K_fit3(CaseList,opt)
global Data Flow Title CoeffHstop
f=figure(); hold on
set(gcf,'Position',[0 0 560 420])
set(gcf,'FileName',['table_K_fit2'  sprintf('_%d',CaseList)])
colors='kbrg';
markers='oxd+';
%disp('K')
disp('Fitting for $K(\theta,h)=c_0+c_1(\theta-24^\circ)/1^\circ$')
disp('$$\begin{array}{|r|r|r|r|r|r|r|r|r|r|r|r|}\hline')
disp('\lambda & c_0 & c_1 & \text{var}\\\hline')

x=[];
y=[];
H=[];
%  for Case=2:6
for Case=CaseList
  ind=Flow{Case}.Flowing&~Flow{Case}.Oscillating&~Flow{Case}.Accelerating;
  if exist('opt','var')&strcmp(opt,'layered')
    ind=Flow{Case}.Flowing&Flow{Case}.Oscillating&~Flow{Case}.FullyOscillating&Flow{Case}.Angle<30;
  end
  x=[x; Flow{Case}.A(ind)];
  H=[H; Flow{Case}.H(ind)];
  if exist('opt','var')&strcmp(opt,'useyy')
  FlowStressXX=cellfun(@(data) mean(data.StressYY(data.z>data.Base+2&data.z<data.Surface)), Data{Case}.flow(ind));
  else
  FlowStressXX=cellfun(@(data) mean(data.StressXX(data.z>data.Base+2&data.z<data.Surface)), Data{Case}.flow(ind));
  end
  FlowStressZZ=cellfun(@(data) mean(data.StressZZ(data.z>data.Base+2&data.z<data.Surface)), Data{Case}.flow(ind));
  K=FlowStressXX./FlowStressZZ;
  y=[y; K];
end
X = [ones(size(x)) x];
a = X\y;
Y = X*a;
%print x:a1+a2x -> x=1+(x-b1)/b2

b=[(1-a(1))/a(2) 1/a(2)];
StdErr = stderr(Y-y); 
disp([Title{Case}(10:end-1) ' & ' num2str(b,'%.6f & ') num2str(StdErr,'%.3f\\\\')])

ind=H>0;
[xs,ix]=sort(x);
plot(xs,Y(ix),'k-');
plot(x(ind),y(ind),'kx');
xlabel('$\theta$')
ylabel('$K$')
% for i=1:length(CaseList); Case=CaseList(i);
%   for j=1:length(Heights)
%     indH=(Heights(j)==Flow{Case}.H)&(ind);
%      plot(Flow{Case}.A(indH),...
%        Y(Heights(j)==Flow{Case}.H(ind)),'-','Color',colors(j))
%      plot(Flow{Case}.A(indH),...
%        K(indH),'x:','Color',colors(j))
%   end
%   title(Title{Case});
% end

disp('\hline\end{array}$$')
return

function table_alpha_fit3(CaseList,opt)
global Data Flow Title CoeffHstop
f=figure(); hold on
set(gcf,'Position',[0 0 560 420])
set(gcf,'FileName',['table_alpha_fit2'  sprintf('_%d',CaseList)])

markers='oxd+';
colors='kbrg';
disp('Fitting for $\alpha(\theta,h)=c_0+c_1\exp(-c_2(\theta-\theta_s(h)/1^\circ)/h$')
disp('$$\begin{array}{|r|r|r|r|r|r|r|r|r|r|r|r|}\hline')
disp('\lambda & c_0 & c_1 & c_2 & \text{var}\\\hline')
for i=1:length(CaseList); Case=CaseList(i);
  figure(f); subplot(1,length(CaseList),i); hold on
  ind=Flow{Case}.Flowing&~Flow{Case}.Oscillating&Flow{Case}.Angle<30&Flow{Case}.Angle>20;
  Heights=unique(Flow{Case}.H(ind));
  Angles=unique(Flow{Case}.A(ind));
  meanVelocityX=  cellfun(@(data) mean(data.VelocityX(data.z>data.Base&data.z<data.Surface)), Data{Case}.flow);
  meanVelocityX2= cellfun(@(data) mean(data.VelocityX(data.z>data.Base&data.z<data.Surface).^2), Data{Case}.flow);
  Alpha=meanVelocityX2./(meanVelocityX.^2);
  if (true)
    x1=Flow{Case}.Height(ind);
    x2=Flow{Case}.A(ind)-atand(tanthetastop(CoeffHstop{Case}, Flow{Case}.Height(ind)));
    x3=x2.*x1;
    y=Alpha(ind);
    if Case<5
      fitting = @(a) a(1)*ones(size(x2));
    else
      fitting = @(a) a(1)+a(2)*exp(-a(3)*x2)./x1;
    end
    error = @(a) sum((fitting(a)-y).^2);
    options=optimset('MaxFunEvals',1000);
    a = fminsearch(@(a) error(a),[1 1 0]',options); 
    Y=fitting(a);
  else
    x1=Flow{Case}.Height(ind)-40;
    x2=Flow{Case}.A(ind)-28;
    y=Alpha(ind);
    X = [ones(size(x1))  x1  x2  x2.^2];
    a = X\y;
    Y = X*a;
  end
  MaxErr = max(abs(Y - y));
  StdErr = stderr(Y-y); 
  if Case<5
    disp([Title{Case}(10:end-1) ' & ' num2str(a(1),'%.6f & & & ') num2str(StdErr,'%.3f\\\\')])
  else
    disp([Title{Case}(10:end-1) ' & ' num2str(a','%.6f & ') num2str(StdErr,'%.3f\\\\')])
  end
  for j=1:length(Heights)
    indH=(Heights(j)==Flow{Case}.H)&(ind);
     plot(Flow{Case}.A(indH)-atand(tanthetastop(CoeffHstop{Case}, Heights(j))),...
       Y(Heights(j)==Flow{Case}.H(ind)),'-','Color',colors(j))
     plot(Flow{Case}.A(indH)-atand(tanthetastop(CoeffHstop{Case}, Heights(j))),...
       Alpha(indH),markers(j),'Color',colors(j))
  end
  title(Title{Case});
  xlabel('$\theta-\theta_s(h)$')
  ylabel('$\alpha$')
end
disp('\hline\end{array}$$')
return

function table_density_fit3(CaseList)
global Data Flow Title CoeffHstop
g=figure(); hold on
set(gcf,'Position',[0 0 560 420])
set(gcf,'FileName',['rho_fit'  sprintf('_%d',CaseList)])

f=figure(); hold on
set(gcf,'Position',[0 0 1*560 1*420])
set(gcf,'FileName',['table_K_fit2'  sprintf('_%d',CaseList)])

colors='kbrg';
markers='oxd+*<';
%disp('\rho')
disp('Fitting for $\bar\rho(\theta,h)=c_0-c_1\exp(\theta-c_2))/c_1$')
disp('$$\begin{array}{|r|r|r|r|r|r|r|r|r|r|r|r|}\hline')
disp('\lambda & 1 & c_0 & c_1 & c_2 & c_3 & \text{var}\\\hline')

x=[];
y=[];
z=[];
for Case=[1 4:10]
  ind=Flow{Case}.Flowing&~Flow{Case}.Oscillating&~Flow{Case}.Accelerating&Flow{Case}.Angle>20;
  b=2;
  if (Case<6) 
    ind=ind&Flow{Case}.H>10;
    b=9; 
  end
  CentralDensity=cellfun(@(data) mean(data.Nu(data.z>data.Base+b&data.z<data.Surface-4)), Data{Case}.flow);
  Density=cellfun(@(data) sum(data.Nu)*diff(data.z(1:2))/data.FlowHeight, Data{Case}.flow);
  x=[x; Flow{Case}.A(ind)];
  y=[y; CentralDensity(ind)];
  z=[z; Density(ind)];
end
fittingCD = @(a) a(1)-exp(a(2)*(x-a(3)));
%error = @(a) sum((fittingCD(a)-y).^2);
%options=optimset('MaxFunEvals',100000,'MaxIter',100000);
% a = fminsearch(@(a) error(a),[.6 0.26 40]',options); 
  s = fitoptions('Method','NonlinearLeastSquares',...
    'Lower',[-Inf -Inf -Inf],...
    'Upper',[Inf Inf Inf],...
    'Startpoint',[.6 0.26 40]);
  fi = fittype('a1-exp(a2*(x-a3))','options',s,'independent','x','coefficients',{'a1','a2','a3'});
  c = fit(x,y,fi);
  a = coeffvalues(c)';
  aconf = confint(c)';

Y=fittingCD(a);
StdErr = stderr(Y - y); 
StdErrZ = stderr(Y - z); 
disp(['- & ' num2str([a(1) 1/(a(2)) a(3)],'%.6f & ') num2str(StdErr,'%.3f & ') num2str(StdErrZ,'%.3f\\\\')])



for i=1:length(CaseList); Case=CaseList(i);
  figure(f); subplot(1,length(CaseList),i); hold on

  ind=Flow{Case}.Flowing&~Flow{Case}.Oscillating&~Flow{Case}.Accelerating&Flow{Case}.Angle>20;
  % ind=~Flow{Case}.Accelerating;
  Heights=unique(Flow{Case}.H(ind));
  Angles=unique(Flow{Case}.A(ind));
   Density=cellfun(@(data) sum(data.Nu)*diff(data.z(1:2))/data.FlowHeight, Data{Case}.flow);
%   Density=cellfun(@(data) mean(data.Nu(data.z>data.Base&data.z<data.Surface)), Data{Case}.flow);
  b=2;
%   if (Case<4) 
%     ind=ind&Flow{Case}.H>10;
%     b=9; 
%   end
  CentralDensity=cellfun(@(data) mean(data.Nu(data.z>data.Base+b&data.z<data.Surface-4)), Data{Case}.flow);

  x=Flow{Case}.A(ind);
  fittingCD = @(a) a(1)-exp(a(2)*(x-a(3)));
  CD=fittingCD(a);

  %var = sqrt(mean((beta(1)+beta(2)*x-y).^2));

  
%   %fit deviation from central density
%   x1=Flow{Case}.Height(ind);
%   thetas=atand(tanthetastop(CoeffHstop{Case}, Flow{Case}.Height(ind)));
%   x2=(Flow{Case}.A(ind)-thetas)./(30-thetas); 
%   y=Density(ind);
%   fitting = @(b) CD.*(1-(b(1)+b(2).*x2+b(3).*x2.^2)./x1);
%   error = @(b) sum((fitting(b)-y).^2);
%   options=optimset('MaxFunEvals',10000,'MaxIter',10000);
%   b = fminsearch(@(b) error(b),[.05 .05 .05]',options); 
%   D=fitting(b);
%   CentralDensityFit=y./(1-(b(1)+b(2).*x2+b(3).*x2.^2)./x1);
% 
  D=CD;
  CentralDensityFit=Density(ind);
  
  MaxErr = max(abs(D - Density(ind)));
  StdErr = stderr(D - Density(ind)); 
%   StdErr = sqrt(var(CD - CentralDensityFit)); 
%   disp([Title{Case}(10:end-1) ' & ' num2str(b','%.6f & ') num2str(StdErr,'%.3f\\\\')])
  disp([Title{Case}(10:end-1) ' & ' num2str(StdErr,'%.3f\\\\')])

  [x,ix]=sort(Flow{Case}.A(ind));
  plot(x,CD(ix),'k-');
  
  for j=1:length(Heights)
    indH=(Heights(j)==Flow{Case}.H)&(ind);
    h=Flow{Case}.Height(indH);
    plot(Flow{Case}.A(indH),CentralDensityFit(Heights(j)==Flow{Case}.H(ind)),['' markers(j)],'Color',colors(j));
    % plot(x,CentralDensity(indH),['-' markers(j)],'Color',colors(j));
%     plot(x,CD(Heights(j)==Flow{Case}.H(ind)),'-','Color',colors(j));
%     plot(x,Density(indH),['' markers(j)],'Color',colors(j));
%     plot(x,D(Heights(j)==Flow{Case}.H(ind)),'-','Color',colors(j));
  end
  
  figure(g);
  plot(Flow{Case}.A(ind),CentralDensity(ind),'k*','DisplayName',['$\bar\rho_c/\rho_p$']);
  plot(x,CD(ix),'k-','DisplayName',['$\bar\rho_c^{fit}/\rho_p$']);
  for j=length(Heights):-1:1
    indH=(Heights(j)==Flow{Case}.H)&(ind);
    h=Flow{Case}.Height(indH);
    plot(Flow{Case}.A(indH),CentralDensityFit(Heights(j)==Flow{Case}.H(ind)),...
      ['' markers(j)],'MarkerSize',4,'Color',colors(j),'DisplayName',['$H=' num2str(Heights(j)) '$']);
  end
  xlabel('$\theta$')
%   ylabel('$\bar\rho h/(h-(c_0+c_1\theta))$')
  ylabel('$\bar\rho/\rho_p$')
  axis tight
  legend('show')
  set(legend,'Location','SouthWest','Interpreter','none','Box','off')
end
disp('\hline\end{array}$$')

if length(CaseList)>1
  figure(f)
  C=get(f,'Children');
  YLimF=cell2mat(get(C,'YLim'));
  set(C,'YLim',[min(YLimF(:,1)) max(YLimF(:,2))]);
  XLimF=cell2mat(get(C,'XLim'));
  set(C,'XLim',[min(XLimF(:,1)) max(XLimF(:,2))]);
end
return

function table_flowrule(CaseList,jenkinscorrection)  %#ok<INUSD>
%plot_hstop_diagram(2:6,'hstoponly');
%plot_hstop_diagram(1);
%plot_hstop_diagram(2);
%load data
global Data Title CoeffHstop CoeffHstop_exp Flow

display('\lambda & \delta_1 & \delta_2 & A \\\hline\hline')
for k=1:length(CaseList); Case=CaseList(k);
  if ~isempty(CoeffHstop{k})
  disp([...
    num2str(Title{Case}(10:end-1),'%.3f') ' & '...
    num2str(atand(CoeffHstop{k}(1:2)),'%.3f & ') ' '...
    num2str(CoeffHstop{k}(3),'%.3f') '\\\hline'])
  end
end

display('\lambda & \delta_{min} & \delta_{max} & A & \beta & \gamma & err & Range\\\hline\hline')
for k=1:length(CaseList); Case=CaseList(k);
  if ~isempty(Flow{Case}.betaPO)
%     if (Case<=4)
%       disp([...
%         num2str(Title{Case}(10:end-1),'%.3f') ' & '...
%         num2str(atand(CoeffHstop{k}(1:2)),'%.3f & ') ...
%         num2str(CoeffHstop{k}(3),'%.3f') ' & ' ...
%         num2str(Flow{Case}.betaPOlayered(2),'%.3f') ' & ' ...
%         num2str(Flow{Case}.betaPOlayered(1),'%.3f') ' & '...
%         num2str(Flow{Case}.varPOlayered,'%.3f') ' & ' ...
%         '\\'])
%     end
    disp([...
      num2str(Title{Case}(10:end-1),'%.3f') ' & '...
      num2str(atand(CoeffHstop{k}(1:2)),'%.3f & ') ...
      num2str(CoeffHstop{k}(3),'%.3f') ' & ' ...
      num2str(Flow{Case}.betaPO(2),'%.3f') ' & ' ...
      num2str(Flow{Case}.betaPO(1),'%.3f') ' & '...
      num2str(Flow{Case}.varPO,'%.3f') ' & ' ...
      '\\\hline'])
  else
    disp([...
    num2str(Title{Case}(10:end-1),'%.3f') ' & '...
    num2str(atand(CoeffHstop{k}(1:2)),'%.3f & ') ' '...
    num2str(CoeffHstop{k}(3),'%.3f') '\\\hline'])
  end
end

return

function table_alpha()  %#ok<INUSD>
%load data
global Data Title CoeffHstop CoeffHstop_exp Flow
CaseList=1:6

display('\lambda & \beta & \gamma & \delta_1 & \delta_2 & A \\\hline\hline')
for k=1:length(CaseList); Case=CaseList(k);
  disp([...
    num2str(Title{Case}(10:end-1),'%.3f') ' & '...
    num2str(Flow{Case}.betaPO(2),'%.3f') ' & ' num2str(-Flow{Case}.betaPO(1),'%.3f') ' & '...
    num2str(atand(CoeffHstop{k}(1:2)),'%.3f & ') ' '...
    num2str(CoeffHstop{k}(3),'%.3f') '\\\hline'])
end
return


function codestatus(CaseList)
%load data
global Data Title Flow

%new figure
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['codestatus_flow'  sprintf('_%d',CaseList)])

%create subplots
if ~exist('CaseList','var'), CaseList=1:length(Data); end
ny=ceil(sqrt(length(CaseList))+1);
nx=ceil(length(CaseList)/ny);

for k=1:length(CaseList); Case=CaseList(k);
  subplot(nx,ny,k)
  hold on
  H= cellfun(@(data)str2double(data.name(strfind(data.name,'H')+1:strfind(data.name,'A')-1)),Data{Case}.flow);
  A= cellfun(@(data)str2double(data.name(strfind(data.name,'A')+1:strfind(data.name,'L')-1)),Data{Case}.flow);
  V= cellfun(@(data)data.FlowVelocityX,Data{Case}.flow);
  T= cellfun(@(data)data.time(2),Data{Case}.flow);

  plot(A,H,'.','MarkerSize',1)
  ix=Flow{Case}.Oscillating&~Flow{Case}.FullyOscillating;
  plot(A(ix),H(ix),'kx','MarkerSize',6)
  ix=Flow{Case}.Oscillating&Flow{Case}.FullyOscillating;
  plot(A(ix),H(ix),'kd','MarkerSize',6)
  ix=~Flow{Case}.Steady&~Flow{Case}.Accelerating;
  plot(A(ix),H(ix),'ko','MarkerFaceColor','k','MarkerSize',6)
  ix=Flow{Case}.Accelerating;
  plot(A(ix),H(ix),'k*','MarkerSize',6)
  axis ([19.5,30.5,9-eps,41])
  
  title(Title{Case})
end

return

function codestatus_flow(CaseList)
%load data
global Data Title Flow

%new figure
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['codestatus_flow'  sprintf('_%d',CaseList)])

%create subplots
if ~exist('CaseList','var'), CaseList=1:length(Data); end
ny=ceil(sqrt(length(CaseList)));
nx=ceil(length(CaseList)/ny);

for k=1:length(CaseList); Case=CaseList(k);
  subplot(nx,ny,k)
  H= cellfun(@(data)str2double(data.name(strfind(data.name,'H')+1:strfind(data.name,'A')-1)),Data{Case}.flow);
  A= cellfun(@(data)str2double(data.name(strfind(data.name,'A')+1:strfind(data.name,'L')-1)),Data{Case}.flow);
  V= cellfun(@(data)data.FlowVelocityX,Data{Case}.flow);
  T= cellfun(@(data)data.time(2),Data{Case}.flow);

  scatter(A,H,50,Flow{k}.Accelerating,'filled')
  caxis([0 .2])
  axis ([20,60,10,40.1])
  title(Title{Case})

  %check FlowVelocity
  disp(Title{Case})
  for Angle= unique(A)'
    if ~all(diff(V(A==Angle))>-0.001)
      disp(['Angle:' num2str(Angle) ', height' mat2str(H(A==Angle)) ', vel:' mat2str(V(A==Angle),2) ', time:' mat2str(T(A==Angle),2)]);
    end
  end
  for Height= unique(H)'
    if ~all(diff(V(H==Height))>-0.001)
      disp(['Height:' num2str(Height) ', angle' mat2str(A(H==Height)) ', vel:' mat2str(V(H==Height),2) ', time:' mat2str(T(H==Height),2)]);
    end
  end

end

return

function codestatus_hstop(CaseList)
%load data
global Data Title

%create figure
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['codestatus_hstop'  sprintf('_%d',CaseList)])

%create subplots
if ~exist('CaseList','var'), CaseList=1:length(Data); end
ny=ceil(sqrt(length(CaseList)));
nx=ceil(length(CaseList)/ny);

for k=1:length(CaseList); Case=CaseList(k);
  subplot(nx,ny,k)
  H= cellfun(@(data)str2double(data.name(strfind(data.name,'H')+1:strfind(data.name,'A')-1)),Data{Case}.hstop);
  A= cellfun(@(data)str2double(data.name(strfind(data.name,'A')+1:strfind(data.name,'L')-1)),Data{Case}.hstop);
  
 	static=cellfun(@(data)data.time(end)<499,Data{Case}.hstop);
  plot(A(static),H(static),'ro','MarkerFaceColor','red'); hold on
  plot(A(~static),H(~static),'bo','MarkerFaceColor','blue')
  title(Title{Case})
end
return

function status_ArrestedFlowingAccelerating(CaseList)
%load data
global Data Title Flow

%new figure
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['status_ArrestedFlowingAccelerating'  sprintf('_%d',CaseList)])

%create subplots
if ~exist('CaseList','var'), CaseList=1:length(Data); end
ny=ceil(sqrt(length(CaseList)));
nx=ceil(length(CaseList)/ny);

for k=1:length(CaseList); Case=CaseList(k);
  subplot(nx,ny,k)
  H= cellfun(@(data)str2double(data.name(strfind(data.name,'H')+1:strfind(data.name,'A')-1)),Data{Case}.flow);
  A= cellfun(@(data)str2double(data.name(strfind(data.name,'A')+1:strfind(data.name,'L')-1)),Data{Case}.flow);
  V= cellfun(@(data)data.FlowVelocityX,Data{Case}.flow);
  T= cellfun(@(data)data.time(2),Data{Case}.flow);

%   Oscillating = cellfun(@(data)mean(abs(diff(data.Nu(data.z>0.3*data.FlowHeight&data.z>0.9*data.FlowHeight)))),Data{Case}.flow);
  Time= cellfun(@(data)diff(data.EneShort.Time([1,end]))>40,Data{Case}.flow);
  ind=(~Flow{Case}.Oscillating)&Time&Flow{Case}.Flowing;
  scatter(A(ind),H(ind),50,Flow{Case}.Accelerating(ind),'filled')
%   caxis([0 2])
  axis ([20,60,10,40.1])
  title(Title{Case})

  %check FlowVelocity
  disp(Title{Case})
  for Angle= unique(A)'
    if ~all(diff(V(A==Angle))>-0.001)
      disp(['Angle:' num2str(Angle) ', height' mat2str(H(A==Angle)) ', vel:' mat2str(V(A==Angle),2) ', time:' mat2str(T(A==Angle),2)]);
    end
  end
  for Height= unique(H)'
    if ~all(diff(V(H==Height))>-0.001)
      disp(['Height:' num2str(Height) ', angle' mat2str(A(H==Height)) ', vel:' mat2str(V(H==Height),2) ', time:' mat2str(T(H==Height),2)]);
    end
  end

end

return

% plot in Angle-Height parameter space with color indicating arresting or
% flowing; data is grouped by the three case studies
function plot_hstop_diagram(CaseList,hstoponly) %#ok<INUSD>
%load data
global Data Astop Hstop Astop_exp Title Stop Flow

%new figure
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['hstop_diagram'  sprintf('_%d',CaseList)])
colors=lines(max(CaseList));
if length(CaseList)==1
  colors=[0 0 0];
end
markers='xod*+><';

%create subplots
if ~exist('CaseList','var'), CaseList=1:length(Data); end

if ~exist('hstoponly','var')
  for k=1:length(CaseList); Case=CaseList(k);
    plot(Stop{Case}.Angle(Stop{Case}.static),Stop{Case}.Height(Stop{Case}.static),...
      'o','Color',colors(k,:),'MarkerFaceColor',colors(k,:)); 
    hold on
    plot(Stop{Case}.Angle(~Stop{Case}.static),Stop{Case}.Height(~Stop{Case}.static),...
      'o','Color',colors(k,:))
  end
  axis tight; v=axis;
end

for k=1:length(CaseList); Case=CaseList(k);
  plot(Astop{Case},Hstop{Case},'Color',colors(Case,:),'LineWidth',1);%,linetype(1:length(CaseList),k));
end
xlabel('$\theta$')
ylabel('$h$')
if ~exist('hstoponly','var'), 
  %plot exponential fit as well
  plot(cell2mat(Astop_exp(CaseList)),cell2mat(Hstop(CaseList)),...
    ':','Color',colors(Case,:),'LineWidth',1);%,linetype(1:length(CaseList),k));
  legend({'arrested','flowing','$h_{stop}(\theta)$','$h_{stop}`(\theta)$'})
%   axis([v(1:2) 0 v(4)]); 
  axis tight
else
  legend(Title{CaseList})
  axis tight
end
set(legend,'Box','off')

if ~exist('hstoponly','var'),  
  for k=1:length(CaseList); Case=CaseList(k);
    %for each flowing case, look left and down for next static case
    downRange=Stop{Case}.downRange(2:end,:);
    leftRange=Stop{Case}.leftRange;
    %plot(downRange(:,1),(downRange(:,3)+downRange(:,2))/2,'x','Color',colors(k,:))
    %plot((leftRange(:,3)+leftRange(:,2))/2,leftRange(:,1),'x','Color',colors(k,:));
    errorbar(downRange(:,1),(downRange(:,3)+downRange(:,2))/2,(downRange(:,3)-downRange(:,2))/2,'LineStyle','none','Color',colors(k,:))
    h=herrorbar((leftRange(:,3)+leftRange(:,2))/2,leftRange(:,1),(leftRange(:,3)-leftRange(:,2))/2);
    set(h(2),'LineStyle','none')
    set(h(1),'Color',colors(Case,:))
  end
  axis tight; v=axis;
else
    markers='xod*+><xod*+><';
    colors=lines(max(CaseList));
    if length(CaseList)==1
      colors=[0 0 0];
    end
    for k=1:length(CaseList); Case=CaseList(k);
      %for each flowing case, look left and down for next static case
      downRange=Stop{Case}.downRange(1:end,:);
      downRange(1,3)=downRange(1,2);
      leftRange=Stop{Case}.leftRange;
%       plot(downRange(:,1),(downRange(:,3)+downRange(:,2))/2,'x','Color',colors(k,:))
%       plot((leftRange(:,3)+leftRange(:,2))/2,leftRange(:,1),'x','Color',colors(k,:));

      [x,ix]=sort([downRange(:,1);(leftRange(:,3)+leftRange(:,2))/2]);
      y=[(downRange(:,3)+downRange(:,2))/2;leftRange(:,1)];
      plot(x,y(ix),['' markers(Case)],'Linewidth',1,'Color',colors(Case,:),'MarkerSize',4);

      %errorbar(downRange(:,1),(downRange(:,3)+downRange(:,2))/2,(downRange(:,3)-downRange(:,2))/2,'LineStyle','none','Color',colors(k,:))
      %h=herrorbar((leftRange(:,3)+leftRange(:,2))/2,leftRange(:,1),(leftRange(:,3)-leftRange(:,2))/2);
      %set(h(2),'LineStyle','none')
      %set(h(1),'Color',colors(k,:))
    end
    C=get(gca,'Children');
    legend(C(end/2:-1:1),Title{CaseList})
    xlim([17.5 26])
    ylim([0 55])
  
end

return

% plot in Angle-Height parameter space with color indicating arresting or
% flowing; data is grouped by the three case studies
function plot_hstop_all(CaseList) %#ok<INUSD>
%load data
global Data Astop Hstop Astop_exp Title Stop Flow

%new figure
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['hstop_diagram'  sprintf('_%d',CaseList)])
colors=lines(length(CaseList));
if length(CaseList)==1
  colors=[0 0 0];
end

%create subplots
if ~exist('CaseList','var'), CaseList=1:length(Data); end

%hstoponly=[];
if ~exist('hstoponly','var')
  for k=1:length(CaseList); Case=CaseList(k);
    Height=cellfun(@(data)data.FlowHeight,Data{Case}.hstop);
    %Height=Stop{Case}.H;
    Angle=cellfun(@(data)data.ChuteAngle,Data{Case}.hstop);

    %  	static=cellfun(@(data)data.Ene.Kin(end-1)/data.Ene.Ela(end-1)<1e-5,Data{Case}.hstop);
    static=cellfun(@(data)data.time(end)<499,Data{Case}.hstop);
    plot(Angle(static),Height(static),'o','Color',colors(k,:),'MarkerFaceColor',colors(k,:),...
      'DisplayName','arrested'); 
    hold on
    if (Case==4)
      plot(Angle(~static),Height(~static),'x','Color',colors(k,:))
    elseif Case==1
      plot(Angle(~static&Stop{Case}.Oscillating),Height(~static&Stop{Case}.Oscillating),'x','Color',colors(k,:))
    else
      plot(Angle(~static),Height(~static),'o','Color',colors(k,:))
    end
  end
  axis tight; v=axis;
end

if (isfield(Data{Case},'flow'))
  Height=cellfun(@(data)data.FlowHeight,Data{Case}.flow);
  Angle=cellfun(@(data)data.ChuteAngle,Data{Case}.flow);

  plot(Angle(Flow{Case}.Accelerating&Flow{Case}.A<31),Height(Flow{Case}.Accelerating&Flow{Case}.A<31),'k*','MarkerFaceColor','g',...
    'DisplayName','accelerating'); 
  if Case==1;
    plot(Angle(Flow{Case}.Steady&~Flow{Case}.Oscillating),Height(Flow{Case}.Steady&~Flow{Case}.Oscillating),'ko',...
      'DisplayName','decellerating'); 
  else
    plot(Angle(Flow{Case}.Steady&~Flow{Case}.Oscillating),Height(Flow{Case}.Steady&~Flow{Case}.Oscillating),'ko',...
      'DisplayName','steady'); 
  end
  plot(Angle(Flow{Case}.Oscillating&~Flow{Case}.FullyOscillating),Height(Flow{Case}.Oscillating&~Flow{Case}.FullyOscillating),'kx',...
    'DisplayName','layered'); 
  plot(Angle(Flow{Case}.FullyOscillating),Height(Flow{Case}.FullyOscillating),'kd',...
    'DisplayName','oscillating'); 
end

for k=1:length(CaseList); Case=CaseList(k);
  plot(Astop{Case},Hstop{Case},'Color',colors(k,:),...
      'DisplayName','$h_{stop}(\theta)$'); 
  if true
    plot(Astop_exp{Case},Hstop{Case},':','Color',colors(k,:),...
      'DisplayName','$h_{stop}`(\theta)$'); 
  end
end

xlabel('$\theta$')
ylabel('$h$')
set(legend,'Box','off')

if true
  for k=1:length(CaseList); Case=CaseList(k);
    %for each flowing case, look left and down for next static case
    downRange=Stop{Case}.downRange(2:end,:);
    leftRange=Stop{Case}.leftRange;
    %plot(downRange(:,1),(downRange(:,3)+downRange(:,2))/2,'x','Color',colors(k,:))
    %plot((leftRange(:,3)+leftRange(:,2))/2,leftRange(:,1),'x','Color',colors(k,:));
    errorbar(downRange(:,1),(downRange(:,3)+downRange(:,2))/2,(downRange(:,3)-downRange(:,2))/2,'LineStyle','none','Color',[.8 .8 .8])
    h=herrorbar((leftRange(:,3)+leftRange(:,2))/2,leftRange(:,1),(leftRange(:,3)-leftRange(:,2))/2);
    set(h(2),'LineStyle','none')
    set(h(1),'Color',[.8 .8 .8])
  end
  axis tight; v=axis;
end

axis tight; v=axis; xlim([18 30]); ylim([0 v(4)])

return

function figure_shape_Strain()
%load data
global Data Astop Hstop Astop_exp Title Stop Flow
CaseList=[1 4 5 7];
colors=lines(max(CaseList));
colors=colors([1 3:end],:);

%new figure
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['strain'  sprintf('_%d',CaseList)])

linetype={'-','-','-','-','-',':',};
linewidth=[1 1 1 1 1 1];
for Case=CaseList
  k=find(Flow{Case}.A==26&Flow{Case}.H==30);
  if (Case==1) k=find(Flow{Case}.A==24&Flow{Case}.H==30); end
  meanVelocityX = mean(Data{Case}.flow{k}.VelocityX(Data{Case}.flow{k}.z>Data{Case}.flow{k}.Base&Data{Case}.flow{k}.z<Data{Case}.flow{k}.Surface));
  strain=smooth(deriv(Data{Case}.flow{k}.VelocityX./meanVelocityX,Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight),1);
  meanStrain(Case) = mean(strain(Data{Case}.flow{k}.z>Data{Case}.flow{k}.Base&Data{Case}.flow{k}.z<Data{Case}.flow{k}.Surface));
%   strain=strain./meanStrain(Case); meanStrain(Case)=1;
  h=plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,...
    strain,linetype{find(Case==CaseList)},...
    'Color',colors(find(Case==CaseList),:),...
    'LineWidth',linewidth(find(Case==CaseList)),...
    'DisplayName',Title{Case});
  xlabel('$(z-b)/h$','Interpreter','none')
  ylabel('$h/\bar{u}\partial_zu$','Interpreter','none')
  axis tight; v=axis; axis([0 1 0 3.5]);

end

z=0:.001:1;
% 
% Case=4;
% du=2-2*z;
% du=du/mean(du)*meanStrain(Case);
% plot(z,du,'-k','Linewidth',2,'Color',colors(find(Case==CaseList),:),'DisplayName',['fit $\lambda=1/2,2/3$']);
% 
% Case=5;
% du=1.30-z;
% du=du/mean(du)*meanStrain(Case);
% plot(z,du,'-k','Linewidth',2,'Color',colors(find(Case==CaseList),:),'DisplayName',['fit ' Title{Case}]);

% Case=7;
% k=find(Flow{Case}.A==26&Flow{Case}.H==30);
% h=Data{Case}.flow{k}.FlowHeight;
% b=2.5/h;
% s=(h-5)/h;
% du=5/2*(1-z).^0.5;
% du(z>s)=5/2*(1-s).^0.5;
% du(z<b)=5/2*(1-b).^0.5*(1-2/3*(b-z(z<b))/b);
% du=du/mean(du)*meanStrain(Case);
% plot(z,du,'-k','Linewidth',2,'Color',colors(find(Case==CaseList),:),'DisplayName',['fit ' Title{Case}]);
% Case=1;
% du=1./(2*z);
% du(z<0.5/h)=h;
% u=cumsum(du)*diff(z(1:2));
% du=du/mean(du)*meanStrain(Case);
% plot(z,du,'-k','Linewidth',2,'Color',colors(Case,:),'DisplayName',['fit ' Title{Case}]);

legend('show')
set(legend,'Box','off')
C=get(gca,'Children');
set(gca,'Children',C(end:-1:1))
return

function figure_Field(CaseList,H,A,Field)
%load data
global Data Astop Hstop Astop_exp Title Stop Flow
colors=mylines(length(CaseList));

%new figure
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['T'  sprintf('_%d',CaseList)])
linetype={'-','-','-','-','-','-','-','-','-',};
linewidth=ones(length(colors),1);
for j=1:length(CaseList); Case=CaseList(j)
  k=find(Flow{Case}.A==A&Flow{Case}.H==H);
  h=plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,...
    Data{Case}.flow{k}.(Field),linetype{j},...
    'Color',colors(j,:),...
    'LineWidth',linewidth(j),...
    'DisplayName',[Title{Case}(1:end-1) ', H=' num2str(H)  ', A=' num2str(A) '$']);
  %hold on 
  %h=plot([0 1],Flow{Case}.I(k)*[1 1],':',...
   % 'Color',colors(j,:),...
   % 'LineWidth',linewidth(j),...
   % 'HandleVisibility','off');
  xlabel('$(z-b)/h$','Interpreter','none')
  ylabel(Field,'Interpreter','none')
  axis tight;% v=axis; axis([0 1 0 0.5]);

end
legend('show')
set(legend,'Box','on','Location','North')
return

function figure_shape_InertialNumber(CaseList,H,A)
%load data
global Data Astop Hstop Astop_exp Title Stop Flow
colors=mylines(length(CaseList));

%new figure
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['I'  sprintf('_%d',CaseList)])
linetype={'-','-','-','-','-','-','-','-','-',};
linewidth=ones(length(colors),1);
for j=1:length(CaseList); Case=CaseList(j)
  k=find(Flow{Case}.A==A&Flow{Case}.H==H);
  h=plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,...
    Data{Case}.flow{k}.I,linetype{j},...
    'Color',colors(j,:),...
    'LineWidth',linewidth(j),...
    'DisplayName',Title{Case});
  %hold on 
  %h=plot([0 1],Flow{Case}.I(k)*[1 1],':',...
   % 'Color',colors(j,:),...
   % 'LineWidth',linewidth(j),...
   % 'HandleVisibility','off');
  disp(['I=' num2str(Flow{Case}.I(k))])
  xlabel('$(z-b)/h$','Interpreter','none')
  ylabel('$I$','Interpreter','none')
  axis tight; v=axis; axis([0 1 0 0.5]);

end
legend('show')
set(legend,'Box','on','Location','North')
return

function plot_InertialNumber(CaseList)
%load data
global Flow
colors=mylines(length(CaseList));

%new figure
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['I_Flow'  sprintf('_%d',CaseList)])

for k=1:length(CaseList); Case=CaseList(k);
  ix=Flow{Case}.Steady;
  %scatter((Froude(ind)+gamma)./FlowHeight(ind),tand(Angle(ind)),50,FlowHeight(ind),'filled')
  scatter(Flow{Case}.I(ix),tand(Flow{Case}.A(ix)),50,Flow{Case}.H(ix),'filled')
end
xlabel('$I_{bulk}=median(I)$')
ylabel('$\mu$')

return

function plot_MuInertial(CaseList)
%load data
global Flow CoeffHstop Title
colors=mylines(length(CaseList));

%new figure
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['MuInertial'  sprintf('_%d',CaseList)])
for k=1:length(CaseList); Case=CaseList(k);
  ix=Flow{Case}.Steady&~Flow{Case}.Oscillating&~(Case==4&Flow{Case}.H==40&Flow{Case}.A==26);
  I=5/2*Flow{Case}.Froude./Flow{Case}.Height;
  mu=tand(Flow{Case}.A);
  scatter(I(ix),mu(ix),50,Flow{Case}.H(ix),'filled')
  %scatter(mu,I,50,Flow{Case}.H)
  mu0=CoeffHstop{Case}(1);
  s = fitoptions('Method','NonlinearLeastSquares',...
    'Lower',[0 mu0],...
    'Upper',[Inf Inf],...
    'Startpoint',[1 1]);
  f = fittype('(x-mu0)/(b-x)/a','options',s,'problem','mu0','independent','x','coefficients',{'a','b'});
  c = fit(mu(ix),I(ix),f,'problem',mu0);
  %f = fittype('(x*b+a*mu0)/(a+x)','options',s,'problem','mu0','independent','x','coefficients',{'a','b'});
  %c = fit(I(ix),mu(ix),f,'problem',mu0);
  a = coeffvalues(c)';
  aconf = confint(c)';
  y=linspace(mu0,max(ylim),10);
  plot(feval(c,y),y);
  %plot(c);
  
  err=norm(mu(ix)-(I(ix)*a(2)+mu0/a(1))./(I(ix)+1/a(1)))/sum(ix);
  
  cmp=7;
  t1=CoeffHstop{cmp}(1);
  t21=CoeffHstop{cmp}(2)-t1;
  A=CoeffHstop{cmp}(3);
  gamma=-Flow{Case}.betaPO(1);
  beta=Flow{Case}.betaPO(2);
  x=(Flow{Case}.Froude(ix)+gamma)./Flow{Case}.Height(ix);
  y=t1+t21./(1+beta/A./x);
  errprevious=norm(mu(ix)-y)/sum(ix);
  
  title(num2str([a aconf]));
  disp([Title{Case}(10:end-1) '& ' num2str(a(1),3) '& ' num2str(atand(mu0),3) '& ' num2str(atand(a(2)),3) '& ' num2str(err,2) '& ' num2str(errprevious,2) '\\\hline']);
end
xlabel('$\bar{I}=5/2~F/h$')
ylabel('$\mu$')

% %new figure
% figure(); clf; hold on
% set(gcf,'Position',[2000  680 560 420])
% set(gcf,'FileName',['MuInertial'  sprintf('_%d',CaseList)])
% for k=1:length(CaseList); Case=CaseList(k);
%   ix=Flow{Case}.Steady&~Flow{Case}.Oscillating;
%   scatter(Flow{Case}.I(ix),tand(Flow{Case}.A(ix)),50,Flow{Case}.H(ix),'filled')
% end
% xlabel('$\median(I(z))$')
% ylabel('$\mu$')

% %new figure
% figure(); clf; hold on
% set(gcf,'Position',[2000  680 560 420])
% set(gcf,'FileName',['MuInertial'  sprintf('_%d',CaseList)])
% for k=1:length(CaseList); Case=CaseList(k);
%   for i=1:length(Data{CaseList}.flow)
%     if (Flow{Case}.Steady(i)&~Flow{Case}.Oscillating(i))
%       plot(Data{CaseList}.flow{i}.z,...
%         Data{CaseList}.flow{i}.I)
%       plot(0.5*Data{CaseList}.flow{i}.FlowHeight,Flow{Case}.I(i),'x')
%     end
%   end
% end
% ylim([0 .4])
% xlabel('$\median(I(z))$')
% ylabel('$\mu$')

% %new figure
% figure(); clf; hold on
% set(gcf,'Position',[2000  680 560 420])
% set(gcf,'FileName',['FInertial'  sprintf('_%d',CaseList)])
% for k=1:length(CaseList); Case=CaseList(k);
%   ix=Flow{Case}.Steady&~Flow{Case}.Oscillating;
%   %I=5/2*Flow{Case}.Froude./Flow{Case}.Height;
%   scatter(Flow{Case}.I(ix).*Flow{Case}.Height(ix),Flow{Case}.Froude(ix),50,Flow{Case}.H(ix),'filled')
% end
% xlabel('$median(I)h$')
% ylabel('$F$')

return



function plot_silbert_comparison() 
plot_hstop_all(7);
set(gcf,'FileName','silbert_comparison')
hstop = [5 10 20 30 40];
astop = [24 22 20.5 19.5 19];
hold on; 
plot(astop,hstop,'rx',...
  'DisplayName','Silbert data');

global Data Flow
Case=7;
Height=cellfun(@(data)data.FlowHeight,Data{Case}.flow);
Angle=cellfun(@(data)data.ChuteAngle,Data{Case}.flow);
for H=unique(Flow{Case}.H)'
  ind = Flow{Case}.H==H & Flow{Case}.Flowing;
  plot(Angle(ind),Height(ind),'-.','Color',[1 1 1]*.7);
  %plot(Angle(ind),Height(ind),'ko');
  y=Height(Angle==28&ind);
  text(30,y,['$H=' num2str(H) '$'],'HorizontalAlignment','right')
end

return

function plot_silbert_comparison_cmp_to_old() 
plot_hstop_all(5);
set(gcf,'FileName','silbert_comparison')
hstop = [5 10 20 30 40];
astop = [24 22 20.5 19.5 19];
hold on; 
plot(astop,hstop,'rx',...
  'DisplayName','Silbert data');

t1=tand(19.60);
t2=tand(28.5);
l=8.5;
h=0:50;
a=atand(t1+(t2-t1)*exp(-h/l));
plot(a,h,'r');

plot([20 20.5 21 22.5 23.5 24.5 25 26.5 30],...
     [40 25   12 6    5    4    4  3    3 ],'g');

t1=tand(17.8054);
t2=tand(37.2732);
l=2.2494;
a=t1:.3:30;
h=l*(t2-tand(a))./(tand(a)-t1);
plot(a,h,'--','Linewidth',2);

global Data Flow
Case=5;
Height=cellfun(@(data)data.FlowHeight,Data{Case}.flow);
Angle=cellfun(@(data)data.ChuteAngle,Data{Case}.flow);
for H=unique(Flow{Case}.H)'
  ind = Flow{Case}.H==H & Flow{Case}.Flowing;
  plot(Angle(ind),Height(ind),'-.','Color',[1 1 1]*.7);
  y=Height(Angle==28&ind);
  text(30,y,['$H=' num2str(H) '$'],'HorizontalAlignment','right')
end

return

function plot_flow_rule_Hstop_fit_cmp_to_old(CaseList,jenkinscorrection) %#ok<INUSD>
%load data
global Data Title CoeffHstop Flow Compare

%new figure
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['flow_rule_hstop_fit'  sprintf('_%d',CaseList)])
if exist('jenkinscorrection','var')
  set(gcf,'FileName',['flow_rule_hstop_fit_jenkins'  sprintf('_%d',CaseList)])
end

%create subplots
if ~exist('CaseList','var'), CaseList=1:length(Data); end
% ny=ceil(sqrt(length(CaseList)));
% nx=ceil(length(CaseList)/ny);
ny=length(CaseList);
nx=ceil(length(CaseList)/ny);

for k=1:length(CaseList); Case=CaseList(k);
  subplot(nx,ny,k)
  
  %which hstop we compare with
  cmp=Compare;
  
  %use downward gravity
  GravityZ=cellfun(@(data)data.Gravity(3),Data{Case}.flow);
  %use full gravity
  %GravityZ=cellfun(@(data)-norm(data.Gravity),Data{Case}.flow);

  %plot hstop data
  data=Data{Case}.hstop;
  FlowHeight=cellfun(@(data)data.FlowHeight,data);
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,data);
  Angle=cellfun(@(data)data.ChuteAngle,data);
  HeightOverHstop=FlowHeight./hstop(CoeffHstop{Case},Angle); 
  Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
  ind=Angle>atand(CoeffHstop{Case}(1))&Angle<atand(CoeffHstop{Case}(2));
  %plot(HeightOverHstop(ind),Froude(ind),'k.')

  hold on
  
  %flow
  FlowHeight=cellfun(@(data)data.FlowHeight,Data{Case}.flow);
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,Data{Case}.flow);
  Angle=cellfun(@(data)data.ChuteAngle,Data{Case}.flow);
  %defined for the rough bottom
  HeightOverHstop=FlowHeight./hstop(CoeffHstop{cmp},Angle); 
  if exist('jenkinscorrection','var')
    HeightOverHstop=HeightOverHstop.*tand(Angle).^2/CoeffHstop{cmp}(1)^2;
  end
  Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
  indAll=Angle>atand(CoeffHstop{cmp}(1))&Angle<atand(CoeffHstop{cmp}(2))&Flow{Case}.Steady;%&Hydrostaticity<0.2;
%   scatter(HeightOverHstop(ind),Froude(ind),'kx')
  %   scatter(HeightOverHstop(ind),Froude(ind),20,Angle(ind),'filled')
  indS=indAll&~Flow{Case}.Oscillating&Flow{Case}.H<=30;
  indS=indS&(Flow{Case}.A==23|(Flow{Case}.A==24&Flow{Case}.H==10)|(Flow{Case}.A==30&Flow{Case}.H==20)|(Flow{Case}.A==22&Flow{Case}.H==20)|Flow{Case}.A==25|Flow{Case}.A==27)&Flow{Case}.H<40;
  plot(HeightOverHstop(indS),Froude(indS),'ko');
  indL=indAll&Flow{Case}.Oscillating&~Flow{Case}.FullyOscillating;
  plot(HeightOverHstop(indL),Froude(indL),'kx')
  indO=indAll&Flow{Case}.FullyOscillating;
  plot(HeightOverHstop(indO),Froude(indO),'kd')
  
  %fit
    %fit
  if exist('jenkinscorrection','var')
    betaPO=Flow{Case}.betaPOJ;
  else
    betaPO=Flow{Case}.betaPO;
    x=sort([1; HeightOverHstop(indL)]);
    if isfield(Flow{Case},'betaPOL'), plot(x,Flow{Case}.betaPOL(1)+Flow{Case}.betaPOL(2)*x,'k:'); end
  end
  x=sort([0; HeightOverHstop(indS)]);
  plot(x,betaPO(1)+betaPO(2)*x,'k','DisplayName',Title(k));
  title([num2str(betaPO(1)) '+' num2str(betaPO(2)) '*x'])
  
  axis tight
  v = axis; axis([0 v(2) 0 v(4)])
  
  if length(CaseList)>1
    v=axis;
    title(Title{Case})
    text(0.1*(v(2)),0.9*(v(4)),['$\beta=' num2str(betaPO(2),'%.3f') '$'])
  end
  
  if exist('jenkinscorrection','var')
    xlabel('$h/h_{stop}(\tan\theta/\tan\theta_1)^2$','Interpreter','none','FontSize',14)
  else
    xlabel('$h/h_{stop}(\theta,\lambda=1)$','Interpreter','none','FontSize',14)
  end
end
subplot(nx,ny,1);
ylabel('$F$','FontSize',14)
set(gca,'FontSize',14);

return

% plots the flow rule for first case study
% details can be seen in the first graph, where the hstop data and fit is
% plotted for each case in a separate subplot
% then it produces the hstop fit for each case into one graph
% the third graph shows the comparison Pouliquen flow rule; we assume that
% the curve is linear
function plot_flow_rule_Hstop_fit(CaseList,jenkinscorrection) %#ok<INUSD>
%load data
global Data Title CoeffHstop Flow Compare

%new figure
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['flow_rule_hstop_fit'  sprintf('_%d',CaseList)])
if exist('jenkinscorrection','var')
  set(gcf,'FileName',['flow_rule_hstop_fit_jenkins'  sprintf('_%d',CaseList)])
end

%create subplots
if ~exist('CaseList','var'), CaseList=1:length(Data); end
% ny=ceil(sqrt(length(CaseList)));
% nx=ceil(length(CaseList)/ny);
ny=length(CaseList);
nx=ceil(length(CaseList)/ny);

for k=1:length(CaseList); Case=CaseList(k);
  subplot(nx,ny,k)
  
  %which hstop we compare with
  cmp=Compare;
  
  %use downward gravity
  GravityZ=cellfun(@(data)data.Gravity(3),Data{Case}.flow);
  %use full gravity
  %GravityZ=cellfun(@(data)-norm(data.Gravity),Data{Case}.flow);

  %plot hstop data
  data=Data{Case}.hstop;
  GravityZ=cellfun(@(data)data.Gravity(3),Data{Case}.hstop);
  FlowHeight=cellfun(@(data)data.FlowHeight,data);
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,data);
  Angle=cellfun(@(data)data.ChuteAngle,data);
  HeightOverHstop=FlowHeight./hstop(CoeffHstop{Case},Angle); 
  Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
  ind=Angle>atand(CoeffHstop{Case}(1))&Angle<atand(CoeffHstop{Case}(2));
  %plot(HeightOverHstop(ind),Froude(ind),'k.')

  hold on
  
  %flow
  GravityZ=cellfun(@(data)data.Gravity(3),Data{Case}.flow);
  FlowHeight=cellfun(@(data)data.FlowHeight,Data{Case}.flow);
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,Data{Case}.flow);
  Angle=cellfun(@(data)data.ChuteAngle,Data{Case}.flow);
  %defined for the rough bottom
  HeightOverHstop=FlowHeight./hstop(CoeffHstop{cmp},Angle); 
  if exist('jenkinscorrection','var')
    HeightOverHstop=HeightOverHstop.*tand(Angle).^2/CoeffHstop{cmp}(1)^2;
  end
  Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
  indAll=Angle>atand(CoeffHstop{cmp}(1))&Angle<atand(CoeffHstop{cmp}(2))&Flow{Case}.Steady;%&Hydrostaticity<0.2;
%   scatter(HeightOverHstop(ind),Froude(ind),'kx')
  %   scatter(HeightOverHstop(ind),Froude(ind),20,Angle(ind),'filled')
  indS=indAll&~Flow{Case}.Oscillating;
  plot(HeightOverHstop(indS),Froude(indS),'ko');
  indL=indAll&Flow{Case}.Oscillating&~Flow{Case}.FullyOscillating;
  plot(HeightOverHstop(indL),Froude(indL),'kx')
  indO=indAll&Flow{Case}.FullyOscillating;
  plot(HeightOverHstop(indO),Froude(indO),'kd')
  
  %fit
    %fit
  if exist('jenkinscorrection','var')
    betaPO=Flow{Case}.betaPOJ;
  else
    betaPO=Flow{Case}.betaPO;
    x=sort([1; HeightOverHstop(indL)]);
    if isfield(Flow{Case},'betaPOL'), plot(x,Flow{Case}.betaPOL(1)+Flow{Case}.betaPOL(2)*x,'k:'); end
  end
  x=sort([0; HeightOverHstop(indS)]);
  plot(x,betaPO(1)+betaPO(2)*x,'k','DisplayName',Title(k));
  disp([num2str(betaPO(1)) '+' num2str(betaPO(2)) '*x'])
  
  axis tight
  v = axis; axis([0 v(2) 0 v(4)])
  
  if length(CaseList)>1
    v=axis;
    title(Title{Case})
    text(0.1*(v(2)),0.9*(v(4)),['$\beta=' num2str(betaPO(2),'%.3f') '$'])
  end
  
  if exist('jenkinscorrection','var')
    xlabel('$h/h_{stop}(\tan\theta/\tan\theta_1)^2$','Interpreter','none','FontSize',14)
  else
    xlabel('$h/h_{stop}(\theta,\lambda=1)$','Interpreter','none','FontSize',14)
  end
end
subplot(nx,ny,1);
ylabel('$F$','FontSize',14)
set(gca,'FontSize',14);

return

function plot_IF(CaseList,jenkinscorrection) %#ok<INUSD>
%load data
global Data Title CoeffHstop Flow Compare

%new figure
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['flow_rule_hstop_fit'  sprintf('_%d',CaseList)])

for k=1:length(CaseList); Case=CaseList(k);
  %flow
  FlowHeight=cellfun(@(data)data.FlowHeight,Data{Case}.flow);
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,Data{Case}.flow);
  GravityZ=cellfun(@(data)data.Gravity(3),Data{Case}.flow);
  Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
  Angle=cellfun(@(data)data.ChuteAngle,Data{Case}.flow);
  InertialNumber=cellfun(@(data)median(data.I),Data{Case}.flow);
  cmp=Compare;

  
  %indAll=Angle>atand(CoeffHstop{cmp}(1))&Angle<atand(CoeffHstop{cmp}(2))&Flow{Case}.Steady;%&Hydrostaticity<0.2;
  %indS=indAll&~Flow{Case}.Oscillating;
  plot(InertialNumber,Froude,'ko');
  
    xlabel('$I$','FontSize',14)
    ylabel('$F$','FontSize',14)
    set(gca,'FontSize',14);
end

return


function plot_flow_rule_Hstop_fit_lambda(CaseList,jenkinscorrection) %#ok<INUSD>
%load data
global Data Title CoeffHstop Flow Compare

%new figure
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['flow_rule_hstop_fit_lambda'  sprintf('_%d',CaseList)])
if exist('jenkinscorrection','var')
  set(gcf,'FileName',['flow_rule_hstop_fit_jenkins'  sprintf('_%d',CaseList)])
end

%create subplots
if ~exist('CaseList','var'), CaseList=1:length(Data); end
% ny=ceil(sqrt(length(CaseList)));
% nx=ceil(length(CaseList)/ny);
ny=length(CaseList);
nx=ceil(length(CaseList)/ny);

for k=1:length(CaseList); Case=CaseList(k);
  subplot(nx,ny,k)
  
  %which hstop we compare with
  cmp=Case;
  
  %use downward gravity
  GravityZ=cellfun(@(data)data.Gravity(3),Data{Case}.flow);
  %use full gravity
  %GravityZ=cellfun(@(data)-norm(data.Gravity),Data{Case}.flow);

  %plot hstop data
  data=Data{Case}.hstop;
  GravityZ=cellfun(@(data)data.Gravity(3),Data{Case}.hstop);
  FlowHeight=cellfun(@(data)data.FlowHeight,data);
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,data);
  Angle=cellfun(@(data)data.ChuteAngle,data);
  HeightOverHstop=FlowHeight./hstop(CoeffHstop{Case},Angle); 
  Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
  ind=Angle>atand(CoeffHstop{Case}(1))&Angle<atand(CoeffHstop{Case}(2));
  %plot(HeightOverHstop(ind),Froude(ind),'k.')

  hold on
  
  %flow
  GravityZ=cellfun(@(data)data.Gravity(3),Data{Case}.flow);
  FlowHeight=cellfun(@(data)data.FlowHeight,Data{Case}.flow);
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,Data{Case}.flow);
  Angle=cellfun(@(data)data.ChuteAngle,Data{Case}.flow);
  %defined for the rough bottom
  HeightOverHstop=FlowHeight./hstop(CoeffHstop{cmp},Angle); 
  if exist('jenkinscorrection','var')
    HeightOverHstop=HeightOverHstop.*tand(Angle).^2/CoeffHstop{cmp}(1)^2;
  end
  Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
  indAll=Angle>atand(CoeffHstop{cmp}(1))&Angle<atand(CoeffHstop{cmp}(2))&Flow{Case}.Steady;%&Hydrostaticity<0.2;
%   scatter(HeightOverHstop(ind),Froude(ind),'kx')
  %   scatter(HeightOverHstop(ind),Froude(ind),20,Angle(ind),'filled')
  indS=indAll&~Flow{Case}.Oscillating;
  scatter(HeightOverHstop(indS),Froude(indS),20,Angle(indS));
  %colorbar
  indL=indAll&Flow{Case}.Oscillating&~Flow{Case}.FullyOscillating;
  plot(HeightOverHstop(indL),Froude(indL),'kx')
  indO=indAll&Flow{Case}.FullyOscillating;
  plot(HeightOverHstop(indO),Froude(indO),'kd')
  
  %fit
    %fit
  if exist('jenkinscorrection','var')
    betaPO=Flow{Case}.betaPOJ;
  else
    betaPO =flowrule_fit_with_offset(Angle(indS), Froude(indS), HeightOverHstop(indS), CoeffHstop{cmp},1);
    x=sort([1; HeightOverHstop(indL)]);
    if isfield(Flow{Case},'betaPOL'), plot(x,Flow{Case}.betaPOL(1)+Flow{Case}.betaPOL(2)*x,'k:'); end
  end
  x=sort([0; HeightOverHstop(indS)]);
  plot(x,betaPO(1)+betaPO(2)*x,'k','DisplayName',Title(k));
  disp([num2str(betaPO(1)) '+' num2str(betaPO(2)) '*x'])
  
  axis tight
  v = axis; axis([0 v(2) 0 v(4)])
  
  if length(CaseList)>1
    v=axis;
    title(Title{Case})
    text(0.1*(v(2)),0.9*(v(4)),['$\beta=' num2str(betaPO(2),'%.3f') '$'])
  end
  
  if exist('jenkinscorrection','var')
    xlabel('$h/h_{stop}(\tan\theta/\tan\theta_1)^2$','Interpreter','none','FontSize',14)
  else
    xlabel(['$h/h_{stop}(\theta,' Title{Case}(2:end-1) ')$'],'Interpreter','none','FontSize',14)
  end
end
subplot(nx,ny,1);
ylabel('$F$','FontSize',14)
set(gca,'FontSize',14);

return

function plot_flow_rule_Hstop_fit_color_oneplot(CaseList,jenkinscorrection) %#ok<INUSD>
%load data
global Data Title CoeffHstop Flow Compare
markers='xod*+>xod*+><sp^vh';

%new figure
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['flow_rule_hstop_fit_color'  sprintf('_%d',CaseList)])
if exist('jenkinscorrection','var')
  set(gcf,'FileName',['flow_rule_hstop_fit_jenkins_color'  sprintf('_%d',CaseList)])
end
colors=lines(max(CaseList));

%create subplots
if ~exist('CaseList','var'), CaseList=1:length(Data); end
% ny=ceil(sqrt(length(CaseList)));
% nx=ceil(length(CaseList)/ny);
for k=1:length(CaseList); Case=CaseList(k);
  %which hstop we compare with
  cmp=Compare;
  
  %hstop
%   data=Data{Case}.hstop;
%   FlowHeight=cellfun(@(data)data.FlowHeight,data);
%   FlowSpeed=cellfun(@(data)data.FlowVelocityX,data);
%   Angle=cellfun(@(data)data.ChuteAngle,data);
%   GravityZ=cellfun(@(data)data.Gravity(3),data);
%   HeightOverHstop=FlowHeight./hstop(CoeffHstop{Case},Angle); 
%   Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
%   ind=Angle>atand(CoeffHstop{Case}(1))&Angle<atand(CoeffHstop{Case}(2));
%   plot(HeightOverHstop(ind),Froude(ind),'.','Color',colors(k,:))

  hold on
  
  %flow
  data=Data{Case}.flow;
  FlowHeight=cellfun(@(data)data.FlowHeight,data);
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,data);
  Angle=cellfun(@(data)data.ChuteAngle,data);
  Oscillating=Flow{Case}.Oscillating;
  GravityZ=cellfun(@(data)data.Gravity(3),data);
  %defined for the rough bottom
  HeightOverHstop=FlowHeight./hstop(CoeffHstop{cmp},Angle); 
  if exist('jenkinscorrection','var')
    HeightOverHstop=HeightOverHstop.*tand(Angle).^2/CoeffHstop{cmp}(1)^2;
  end
  Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
  indAll=Angle>atand(CoeffHstop{cmp}(1))&Angle<atand(CoeffHstop{cmp}(2))&Flow{Case}.Steady;%&Hydrostaticity<0.2;
  ind=indAll&~Flow{Case}.Oscillating;
  plot(HeightOverHstop(ind),Froude(ind),markers(Case),'Color',colors(Case,:),'DisplayName',Title(Case),'MarkerSize',4)
  %fit
  if exist('jenkinscorrection','var')
    betaPO=Flow{Case}.betaPOJ;
  else
    betaPO=Flow{Case}.betaPO;
  end
  x=sort([0; HeightOverHstop(ind)]);
  plot(x,betaPO(1)+betaPO(2)*x,'k','Color',colors(Case,:),'DisplayName',Title(Case));
  
  axis tight
  v = axis; axis([0 v(2) 0 v(4)])
  
  if exist('jenkinscorrection','var')
    xlabel('$h/h_{stop}(\tan(\theta)/\tan(\theta_1))^2$','FontSize',14)
  else
    xlabel('$h/h_{stop}(\theta,\lambda=1)$','FontSize',14)
  end
end
child=get(gca,'Children');
legend(child(end:-2:1),'Location','North')
set(legend,'Box','off')
ylabel('$F$','FontSize',14)

set(gca,'FontSize',12);
return

function plot_flow_rule_Hstop_fit_color(CaseList,jenkinscorrection) %#ok<INUSD>
%load data
global Data Title CoeffHstop Flow Compare

%new figure
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['flow_rule_hstop_fit_color'  sprintf('_%d',CaseList)])
if exist('jenkinscorrection','var')
  set(gcf,'FileName',['flow_rule_hstop_fit_jenkins_color'  sprintf('_%d',CaseList)])
end

%create subplots
if ~exist('CaseList','var'), CaseList=1:length(Data); end
ny=length(CaseList);
nx=ceil(length(CaseList)/ny);

for k=1:length(CaseList); Case=CaseList(k);
  subplot(nx,ny,k)
  %which hstop we compare with
  cmp=Compare;
  
  %hstop
  data=Data{Case}.hstop;
  FlowHeight=cellfun(@(data)data.FlowHeight,data);
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,data);
  Angle=cellfun(@(data)data.ChuteAngle,data);
  GravityZ=cellfun(@(data)data.Gravity(3),data);
  HeightOverHstop=FlowHeight./hstop(CoeffHstop{Case},Angle); 
  Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
  ind=Angle>atand(CoeffHstop{Case}(1))&Angle<atand(CoeffHstop{Case}(2));
  scatter(HeightOverHstop(ind),Froude(ind),5,Angle(ind),'filled')
%   plot(HeightOverHstop(ind),Froude(ind),'k.')

  hold on
  
  %flow
  data=Data{Case}.flow;
  FlowHeight=cellfun(@(data)data.FlowHeight,data);
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,data);
  Angle=cellfun(@(data)data.ChuteAngle,data);
  Oscillating=Flow{Case}.Oscillating;
  GravityZ=cellfun(@(data)data.Gravity(3),data);
  %defined for the rough bottom
  HeightOverHstop=FlowHeight./hstop(CoeffHstop{cmp},Angle); 
  if exist('jenkinscorrection','var')
    HeightOverHstop=HeightOverHstop.*tand(Angle).^2/CoeffHstop{cmp}(1)^2;
  end
  Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
  indAll=Angle>atand(CoeffHstop{cmp}(1))&Angle<atand(CoeffHstop{cmp}(2))&Flow{Case}.Steady;%&Hydrostaticity<0.2;
%   scatter(HeightOverHstop(ind),Froude(ind),'kx')
  if (Case==2), ind=Oscillating<0.5&indAll;
  else ind=Oscillating<0.25&indAll;
  end
  scatter(HeightOverHstop(indAll&~ind),Froude(indAll&~ind),20,Angle(indAll&~ind))
  scatter(HeightOverHstop(ind),Froude(ind),20,Angle(ind),'filled')
  
  %fit
  if exist('jenkinscorrection','var')
    [betaPO]=flowrule_fit_with_offset(Angle(ind), Froude(ind), HeightOverHstop(ind), CoeffHstop{k},0);
  else
    [betaPO]=flowrule_fit_with_offset(Angle(ind), Froude(ind), HeightOverHstop(ind), CoeffHstop{k},2);
  end
  x=sort([1; HeightOverHstop(ind)]);
  plot(x,betaPO(1)+betaPO(2)*x,'k');
  display(['Pouliquen slope: for case ' num2str(Case) ':' num2str(betaPO(2),'%.3f')])

  %title([Title{Case} ' \beta=' num2str(betaPO(2),'%.3f')])
%   colorbar 
  axis tight
  v = axis; axis([0 v(2) 0 v(4)])
  
  if length(CaseList)>1
    v=axis;
    title(Title{Case})
    text(0.1*(v(2)),0.9*(v(4)),['$\beta=' num2str(betaPO(2),'%.3f') '$'])
  end
  
  if exist('jenkinscorrection','var')
    xlabel('$h/h_{stop}(\tan(\theta)/\tan(\theta_1))^2$')
  else
    xlabel('$h/h_{stop}$')
  end
% ylim([0 7.5])
% xlim([0 20])
end
ylabel('$F$')
return

function plot_flow_rule_Hstop_fit_compare(Case)
%load data
global Data CoeffHstop Flow

%new figure
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['flow_rule_hstop_fit_compare'  sprintf('_%d',Case)])

%create subplots
k=Case;
for l=1:2
  if l==2
    subplot(1,2,2);
    jenkinscorrection=true; %#ok<NASGU>
  else
    subplot(1,2,1)
  end
  %hstop
  data=Data{Case}.hstop;
  FlowHeight=cellfun(@(data)data.FlowHeight,data);
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,data);
  Angle=cellfun(@(data)data.ChuteAngle,data);
  GravityZ=cellfun(@(data)data.Gravity(3),data);
  HeightOverHstop=FlowHeight./hstop(CoeffHstop{Case},Angle); 
  Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
  ind=Angle>atand(CoeffHstop{Case}(1))&Angle<atand(CoeffHstop{Case}(2));
%   scatter(HeightOverHstop(ind),Froude(ind),5,Angle(ind),'filled')
  plot(HeightOverHstop(ind),Froude(ind),'kx')

  hold on
  
  %flow
  data=Data{Case}.flow;
  FlowHeight=cellfun(@(data)data.FlowHeight,data);
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,data);
  Angle=cellfun(@(data)data.ChuteAngle,data);
  Oscillating=Flow{Case}.Oscillating;
  GravityZ=cellfun(@(data)data.Gravity(3),data);
  %defined for the rough bottom
  HeightOverHstop=FlowHeight./hstop(CoeffHstop{5},Angle); 
  if exist('jenkinscorrection','var')
    HeightOverHstop=HeightOverHstop.*tand(Angle).^2/CoeffHstop{5}(1)^2;
  end
  Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
  indall=Angle>atand(CoeffHstop{5}(1))&Angle<atand(CoeffHstop{5}(2))&Flow{Case}.Steady;%&Hydrostaticity<0.2;
%   scatter(HeightOverHstop(ind),Froude(ind),20,Angle(ind))
  if (Case==2), ind=Oscillating<0.5&indall;
  else ind=Oscillating<0.25&indall;
  end
%   scatter(HeightOverHstop(ind),Froude(ind),20,Angle(ind),'filled')
  plot(HeightOverHstop(indall&~ind),Froude(indall&~ind),'ko')
  plot(HeightOverHstop(ind),Froude(ind),'kx')
  
  %fit
  if exist('jenkinscorrection','var')
    [betaPO]=flowrule_fit_with_offset(Angle(ind), Froude(ind), HeightOverHstop(ind).*tand(Angle(indS)).^2/CoeffHstop{cmp}(1)^2, CoeffHstop{k},1);
  else
    [betaPO]=flowrule_fit_with_offset(Angle(ind), Froude(ind), HeightOverHstop(ind), CoeffHstop{k},1);
  end
  x=sort(HeightOverHstop(ind));
  plot(x,betaPO(1)+betaPO(2)*x,'k');

  %title([Title{Case} ' \beta=' num2str(betaPO(2),'%.3f')])
%   colorbar
  axis tight
  v = axis; axis([0 v(2:4)])
  
  if exist('jenkinscorrection','var')
    xlabel('$h/h_{stop}(\tan(\theta)/\tan(\theta_1))^2$')
  else
    display(['Pouliquen slope: for case ' num2str(Case) ': ' num2str(betaPO(2),'%.3f')])
    xlabel('$h/h_{stop}$')
  end
  ylabel('$F$')
end

return



function plot_flow_rule_H_nonsteady(CaseList)
%load data
global Data Flow

%new figure
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['flow_rule_H_nonsteady'  sprintf('_%d',CaseList)])

%create subplots
if ~exist('CaseList','var'), CaseList=1:length(Data); end
ny=ceil(sqrt(length(CaseList)));
nx=ceil(length(CaseList)/ny);
markers='xo+*sdph^v<>';

for k=1:length(CaseList); Case=CaseList(k);
  subplot(nx,ny,k)
  
  data=Data{Case}.flow;
  FlowHeight=cellfun(@(data)data.FlowHeight,data);
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,data);
%   Angle=cellfun(@(data)data.ChuteAngle,data);
%   Hydrostaticity=cellfun(@(data)data.Hydrostaticity,data);
  GravityZ=cellfun(@(data)data.Gravity(3),data);
  Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
  Angles=unique(Flow{Case}.A(Flow{Case}.A>29.5));
  colors=lines(length(Angles));
  for j=1:length(Angles)
    ind=find((Angles(j)==Flow{Case}.A))';
    plot(FlowHeight(ind),Froude(ind),['-' markers(j)],'Color',colors(j,:),...
      'DisplayName',['$\theta=' num2str(Angles(j)) '^\circ$']);
  end
  legend('show','Location','NorthEast')
  set(legend,'Box','off')
  axis tight
  v = axis; axis([0 1.2*v(2) 0 v(4)])
  xlabel('h')
  ylabel('F')
end

return

function plot_flow_rule_H(CaseList)
%load data
global Data Flow

%new figure
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['flow_rule_H'  sprintf('_%d',CaseList)])

%create subplots
if ~exist('CaseList','var'), CaseList=1:length(Data); end
ny=ceil(sqrt(length(CaseList)));
nx=ceil(length(CaseList)/ny);

for k=1:length(CaseList); Case=CaseList(k);
  subplot(nx,ny,k)
  
  data=Data{Case}.flow;
  FlowHeight=cellfun(@(data)data.FlowHeight,data);
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,data);
%   Angle=cellfun(@(data)data.ChuteAngle,data);
%   Hydrostaticity=cellfun(@(data)data.Hydrostaticity,data);
  GravityZ=cellfun(@(data)data.Gravity(3),data);
  Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
  Angles=unique(Flow{Case}.A);
  colors=lines(length(Angles));
  for j=1:length(Angles)
    ind=find((Angles(j)==Flow{Case}.A)&Flow{Case}.Steady)';
    plot(FlowHeight(ind),Froude(ind),'o-','Color',colors(j,:),...
      'DisplayName',['$\theta=' num2str(Angles(j)) '^\circ$']);
  end
  legend('show','Location','EastOutside')
  axis tight
  v = axis; axis([0 v(2) 0 v(4)])
  xlabel('h')
  ylabel('F')
end

return

function plot_depthprofile_(Field,Case)
global Data Title Flow
figure(); clf;
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['depthprofile_' Field  sprintf('_%d',Case)])

Heights=unique(Flow{Case}.H);
Angles=unique(Flow{Case}.A);
colors=lines(length(Angles));
for j=1:length(Data{Case}.flow) 
%   if Data{Case}.flow{j}.Hydrostaticity<0.1&Flow{Case}.H(j)>.8*Flow{Case}.Height(j)
  if Flow{Case}.H(j)>.5*Flow{Case}.Height(j)
    subplot(1,length(Heights),find(~(Heights-Flow{Case}.H(j))));
    color=colors(Angles==Flow{Case}.A(j),:);
    plot(Data{Case}.flow{j}.z,Data{Case}.flow{j}.(Field),'Color',color,'DisplayName',num2str(Flow{Case}.A(j)));
    hold on
    title([Title{Case} ' H' num2str(Flow{Case}.H(j))]);
    axis tight
  end
end
for j=1:length(Heights)
  subplot(1,length(Heights),j)
  legend('show')
end
return

function plot_scaleddepthprofile(Field,Case) 
global Data Title Flow
figure(); clf;
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['depthprofile_' Field  sprintf('_%d',Case)])

Heights=unique(Flow{Case}.H);
Angles=unique(Flow{Case}.A);
colors=lines(6);
for j=1:length(Data{Case}.flow) 
  if Data{Case}.flow{j}.Hydrostaticity<0.1&&Flow{Case}.H(j)>.8*Flow{Case}.Height(j)
    subplot(1,length(Heights),find(~(Heights-Flow{Case}.H(j))));
    color=colors(Angles==Flow{Case}.A(j),:);
    plot(Data{Case}.flow{j}.z./Data{Case}.flow{j}.FlowHeight, Data{Case}.flow{j}.(Field),'Color',color)
    hold on
    title([Title{Case} ' H' num2str(Flow{Case}.H(j))]);
    axis tight
  end
end
return

function plot_ene(CaseList,H) 
global Data Title Flow
%load_Ene();
for Case=CaseList
  figure(); clf;
  set(gcf,'Position',[2000  680 560 420])
  set(gcf,'FileName',['ene_'  sprintf('_%d',CaseList)])
  Heights=unique(Flow{Case}.H);
  if exist('H','var'), ind=find(Flow{Case}.H==H)';
  else ind=1:length(Flow{Case}.H); end
  Angles=unique(Flow{Case}.A);
  ny=ceil(sqrt(length(ind)));
  nx=ceil(length(ind)/ny);
  [A,ix]=sort(Flow{Case}.A(ind));
  ind=ind(ix);
  
  colors=mylines(length(ind));
  for k=1:length(ind); j=ind(k);
    semilogy(Data{Case}.flow{j}.Ene.Time(2:end-1),Data{Case}.flow{j}.Ene.Kin(2:end-1)./mean(Data{Case}.flow{j}.Ene.Ela(round(end/2):end-1)),...
      'Color',colors(k,:),...
      'DisplayName',num2str(Data{Case}.flow{j}.ChuteAngle));
    hold on
    title([Title{Case} ' H' num2str(Flow{Case}.H(j)) ' A' num2str(Flow{Case}.A(j))])
    axis tight
    v=axis;
  end
  legend('show')
  set(legend,'Location','South','Box','off')
end
return

function getNu(Case) 
plot_depthprofile('Nu',Case)
return

function getVelocityX(Case) 
plot_depthprofile('VelocityX',Case)
return

function plot_depthprofile_VelocityX_A(Case) 
global Data Flow
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['depthprofile_VelocityX'  sprintf('_%d',Case)])

Heights=unique(Flow{Case}.H(Flow{Case}.Steady));
Angles=unique(Flow{Case}.A(Flow{Case}.Steady));
% Heights=unique(Flow{Case}.H);
% Angles=unique(Flow{Case}.A);
colors=lines(length(Angles));

for j=1:length(Angles)
  subplot(length(Angles),1,j); hold on
  % ind=find((Angles(j)==Flow{Case}.A)&(Flow{Case}.Steady))';
  ind=find((Angles(j)==Flow{Case}.A))';
  for k=ind
    color=colors(Heights==Flow{Case}.H(k),:);
    plot(Data{Case}.flow{k}.z-Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.VelocityX-Data{Case}.flow{k}.SurfaceVelocityX,...
      'Color',color,...
      'DisplayName',['$H='  num2str(Flow{Case}.H(k)) ',\theta=' num2str(Angles(j)) '^\circ$']);
  end
  legend('show','Location','EastOutside')
  xlabel('$z-b$','Interpreter','none')
  ylabel('$v_x-v_x^s$','Interpreter','none')
  axis tight; %v=axis; axis([0 v(2:4)]);
end
return

function plot_depthprofile_VelocityX_HA_fixed(CaseList,H)
linewidth=1;
global Data Flow Title
figure(); clf; hold on
%set(gcf,'Position',[2000  680 560 420])
set(gcf,'Position',[2000  680 560 460])
set(gcf,'FileName',['depthprofile_VelocityX'  sprintf('_%d',CaseList)])

colors=mylines(length(CaseList));
for m=1:length(CaseList) 
  Case=CaseList(m);
  if m==3 
    A=26;
    %linewidth=2;
  elseif m==2
    A=22;
  else
    A=24;
  end
  ind=find(Flow{Case}.Steady&Flow{Case}.H==H&Flow{Case}.A==A)';
  for k=ind
    color=colors(m,:);
    meanVelocityX = mean(Data{Case}.flow{k}.VelocityX(Data{Case}.flow{k}.z>0&Data{Case}.flow{k}.z<Data{Case}.flow{k}.FlowHeight));
    ix=Data{Case}.flow{k}.z<Data{Case}.flow{k}.FlowHeight;
    plot(Data{Case}.flow{k}.z(ix)./Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.VelocityX(ix)/meanVelocityX,...
      'Color',color,'LineWidth',linewidth,...
      'DisplayName',[Title{Case}(1:end-1) ',\theta=' num2str(Flow{Case}.A(ind)) '^\circ$']);
  end
end
z=0:.05:1;
u=(5/3)*(1-(1-z).^1.5);
plot(z,u,'--k','Linewidth',2,'DisplayName','Bagnold');

legend('show')
set(legend,'Location','SouthEast','FontSize',8)
xlabel('$(z-b)/h$','Interpreter','none')
ylabel('$u/\overline{u}$','Interpreter','none')
axis tight; v=axis; axis([0 1 v(3:4)]);
return


function plot_depthprofile_VelocityX_HA(CaseList,H,A)
global Data Flow Title
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['depthprofile_VelocityX'  sprintf('_%d',CaseList) '_H' num2str(H,'_%d') '_A' num2str(A,'_%d')])

colors=lines(length(CaseList));
for m=1:length(CaseList) 
  Case=CaseList(m);
  ind=find(Flow{Case}.Steady&Flow{Case}.H==H&Flow{Case}.A==A)';
  for k=ind
    color=colors(m,:);
    meanVelocityX = mean(Data{Case}.flow{k}.VelocityX(Data{Case}.flow{k}.z>0&Data{Case}.flow{k}.z<Data{Case}.flow{k}.FlowHeight));
    plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.VelocityX/meanVelocityX,...
      'Color',color,...
      'DisplayName',Title(Case));
  end
end
z=0:.05:1;
u=(5/3)*(1-(1-z).^1.5);
plot(z,u,'--k','Linewidth',2,'DisplayName','Bagnold');

legend('show')
set(legend,'Location','SouthEast')
xlabel('$(z-b)/h$','Interpreter','none')
ylabel('$v/\overline{v}$','Interpreter','none')
axis tight; v=axis; axis([0 1 v(3:4)]);
return

function plot_depthprofile_HA(CaseList,H,A)
global Data Flow Title
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['depthprofile_Nu'  sprintf('_%d',CaseList) '_H' num2str(H,'_%d') '_A' num2str(A,'_%d')])

colors=lines(length(CaseList));
for m=1:length(CaseList) 
  Case=CaseList(m);
  ind=find(Flow{Case}.Steady&Flow{Case}.H==H&Flow{Case}.A==A)';
  for k=ind
    color=colors(m,:);
    plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.Nu,...
      'Color',color);
  end
end

legend(Title{CaseList},'Location','SouthEast')
xlabel('$(z-b)/h$','Interpreter','none')
ylabel('$\rho/\rho_p$','Interpreter','none')
axis tight; v=axis; axis([0 v(2:4)]);
return

function plot_hOverH(CaseList)
global Data Flow Title
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['hOverH'  sprintf('_%d',CaseList)])

for k=1:length(CaseList), Case=CaseList(k);
  subplot(1,length(CaseList),k); cla; hold on
  Angles=unique(Flow{Case}.A(~Flow{Case}.Accelerating));
  colors=mylines(length(Angles));
  for j=1:length(Angles)
    ind=(Angles(j)==Flow{Case}.A)&~Flow{Case}.Accelerating;
    ix=ind&~Flow{Case}.Steady;
    if ~isempty(ix)
      plot(Flow{Case}.H(ix),Flow{Case}.Height(ix)./Flow{Case}.H(ix),'.',...
          'Color',colors(j,:),...
          'DisplayName',['$\theta=' num2str(Angles(j)) '^\circ$']);
    end
    ix=ind&Flow{Case}.Oscillating&~Flow{Case}.FullyOscillating;
    if ~isempty(ix)
      plot(Flow{Case}.H(ix),Flow{Case}.Height(ix)./Flow{Case}.H(ix),':x',...
          'Color',colors(j,:),...
          'DisplayName',['$\theta=' num2str(Angles(j)) '^\circ$']);
    end
    ix=ind&Flow{Case}.FullyOscillating;
    if ~isempty(ix)
      plot(Flow{Case}.H(ix),Flow{Case}.Height(ix)./Flow{Case}.H(ix),'d',...
          'Color',colors(j,:),...
          'DisplayName',['$\theta=' num2str(Angles(j)) '^\circ$']);
    end
    ix=ind&Flow{Case}.Steady&~Flow{Case}.Oscillating;
    plot(Flow{Case}.H(ix),Flow{Case}.Height(ix)./Flow{Case}.H(ix),'-o',...
        'Color',colors(j,:),...
        'DisplayName',['$\theta=' num2str(Angles(j)) '^\circ$']);
    xlabel('$H$','Interpreter','none')
    ylabel('$h/H$','Interpreter','none')
    axis tight; %v=axis; axis([0 v(2:4)]);
  end
  legend('show','Location','EastOutside')
end
return

function plot_depthprofile_StressZZ_HA(CaseList,H,A)
global Data Flow Title
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['depthprofile_StressZZ'  sprintf('_%d',CaseList) '_H' num2str(H,'_%d') '_A' num2str(A,'_%d')])

colors=lines(length(CaseList));
for m=1:length(CaseList) 
  Case=CaseList(m);
  ind=find(Flow{Case}.Steady&Flow{Case}.H==H&Flow{Case}.A==A)';
  for k=ind
    color=colors(m,:);
    plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.StressZZ,...
      'Color',color);
  end
end

legend(Title{CaseList},'Location','SouthEast')
xlabel('$(z-b)/h$','Interpreter','none')
ylabel('$\sigma_{zz}$','Interpreter','none')
axis tight; v=axis; axis([0 v(2:4)]);
return

function plot_depthprofile_VelocityX_H(Case,H,linear) %#ok<INUSD>
global Data Flow
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['depthprofile_VelocityX'  sprintf('_%d',Case) '_H' num2str(H,'_%d')])

ind=find(~Flow{Case}.Accelerating&mod(Flow{Case}.A,2)==0&Flow{Case}.Flowing&Flow{Case}.A==H)';
% Angles=unique(Flow{Case}.A(ind));
[Angles,ix]=sort(Flow{Case}.H(ind));
ind=ind(ix(1:end));
colors=lines(length(Angles));
for k=ind
  color=colors(Angles==Flow{Case}.H(k),:);
  meanVelocityX = mean(Data{Case}.flow{k}.VelocityX(Data{Case}.flow{k}.z>0&Data{Case}.flow{k}.z<Data{Case}.flow{k}.FlowHeight));
  %meanVelocityX = Data{Case}.flow{k}.FlowVelocity(1);
%   plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.VelocityX./meanVelocityX,...
  plot(Data{Case}.flow{k}.z-0*Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.VelocityX./meanVelocityX,...
    'Color',color,...
    'DisplayName',['$\theta=' num2str(Flow{Case}.A(k)) '^\circ$']);
end
if exist('linear','var')
  z=0:.05:1;
  u=2*z;
  plot(z,u,'k:','Linewidth',2,'DisplayName','Linear');
else
  z=0:.05:1;
  u=(5/3)*(1-(1-z).^1.5);
  %plot(z,u,'k--','Linewidth',2,'DisplayName','Bagnold');
end

legend('show','Location','NorthWest')
set(legend,'Box','off')
xlabel('$(z-b)/h$','Interpreter','none')
ylabel('$v/\overline{v}$','Interpreter','none')
%axis tight; v=axis; axis([0 1 v(3:4)]);
return

function plot_Bagnold(Case) 
global Data Flow CoeffHstop
ind=find(~Flow{Case}.Accelerating&Flow{Case}.Flowing&Flow{Case}.A>20)';
Angles = unique(Flow{Case}.A(ind));
colors=lines(length(Angles));

figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['bagnold'  sprintf('_%d',Case)])
%
for k=ind
  color=colors(Angles==Flow{Case}.A(k),:);
  ix = Data{Case}.flow{k}.z-Data{Case}.flow{k}.Base>0.0*Data{Case}.flow{k}.FlowHeight ...
    & Data{Case}.flow{k}.z-Data{Case}.flow{k}.Base<1.0*Data{Case}.flow{k}.FlowHeight;
%   plot(Data{Case}.flow{k}.z,Data{Case}.flow{k}.VelocityX,...
%     'Color',color);
  plot(Data{Case}.flow{k}.z(ix)-Data{Case}.flow{k}.Surface,Data{Case}.flow{k}.ShearXZ(ix),...
    'Color',color);
end
x=linspace(-40,0,100);
plot(x,sqrt(-x)/6,'k');
%legend('show','Location','NorthWest')
%set(legend,'Box','off')
xlabel('$(z-b)/h$','Interpreter','none')
ylabel('$\partial_z v_x$','Interpreter','none')



%show that the base layer varies with height
ind=find(~Flow{Case}.Accelerating&Flow{Case}.Flowing&Flow{Case}.A>20&Flow{Case}.H==20)';
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['bagnold'  sprintf('_%d',Case)])
%
H=sort(unique(Flow{Case}.H(ind)),'descend');
HstopOverHeight(ind)=hstop(CoeffHstop{Case},Flow{Case}.A(ind))./Flow{Case}.H(ind); 
for k=ind
  color=colors(Angles==Flow{Case}.A(k),:);
%   color=colors(H==Flow{Case}.H(k),:);
  ix = Data{Case}.flow{k}.z-Data{Case}.flow{k}.Base>-0.1*Data{Case}.flow{k}.FlowHeight ...
    & Data{Case}.flow{k}.z-Data{Case}.flow{k}.Base<1.0*Data{Case}.flow{k}.FlowHeight;
  meanVelocityX = mean(Data{Case}.flow{k}.VelocityX(ix));
%   plot((Data{Case}.flow{k}.z(ix)-Data{Case}.flow{k}.Base)/(HstopOverHeight(k)),...
%   Data{Case}.flow{k}.ShearXZ(ix)/meanVelocityX*Data{Case}.flow{k}.FlowHeight,...
%     'Color',color,'DisplayName',['$\theta=' num2str(Flow{Case}.A(k)) '^\circ$']);
  plot((Data{Case}.flow{k}.z(ix)-Data{Case}.flow{k}.Base)/HstopOverHeight(k),...
    Data{Case}.flow{k}.VelocityX(ix),...
    'Color',color,'DisplayName',['$\theta=' num2str(Flow{Case}.A(k)) '^\circ$']);
end
xlim([-0 .7*max(xlim)])
ylim([0 max(ylim)])
legend('show','Location','NorthEast')
set(legend,'Box','off')
xlabel('$z-b$','Interpreter','none')
ylabel('$\partial_z v_x$','Interpreter','none')


%show that it's Bagnold in the bulk
ind=find(~Flow{Case}.Accelerating&Flow{Case}.Flowing&Flow{Case}.A>20&Flow{Case}.H>30)';
H=sort(unique(Flow{Case}.H(ind)),'descend');
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['bagnold'  sprintf('_%d',Case)])
%
for k=ind
  color=colors(Angles==Flow{Case}.A(k),:);
  ix = Data{Case}.flow{k}.z-Data{Case}.flow{k}.Base>0*Data{Case}.flow{k}.FlowHeight ...
    & Data{Case}.flow{k}.z-Data{Case}.flow{k}.Base<0.9*Data{Case}.flow{k}.FlowHeight;
  meanVelocityX = mean(Data{Case}.flow{k}.VelocityX(ix));
  plot(-(Data{Case}.flow{k}.z(ix)-Data{Case}.flow{k}.Surface)/Data{Case}.flow{k}.FlowHeight,...
    Data{Case}.flow{k}.ShearXZ(ix)/meanVelocityX*Data{Case}.flow{k}.FlowHeight,...
    'Color',color,'DisplayName',['$\theta=' num2str(Flow{Case}.A(k)) '^\circ$']);
end
set(gca,'XScale','log')
set(gca,'YScale','log')
axis tight
xlim([0.1 1])
set(legend,'Box','off')
legend('show','Location','NorthWest')
xlabel('$(z-b)/h$','Interpreter','none')
ylabel('$(h/\bar u) \partial_z v_x$','Interpreter','none')

%plot triangle
slope=1/2;
dx=-1.3;
dy=+0.1;
loglog(exp([0 1 1 0]+dx),exp([0 0 slope 0]+dy),'k-');
text(exp(1.1+dx),exp(slope/2+dy), num2str(slope))
text(exp(0.5+dx),exp(-0.1+dy), '1')


return

function plot_depthprofile_VelocityX(Case)
global Data Flow
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['depthprofile_VelocityX'  sprintf('_%d',Case)])

ind=~Flow{Case}.Accelerating;
% Heights=unique(Flow{Case}.H(ind));
Angles=unique(Flow{Case}.A(ind));
colors=lines(length(Angles));
% markers='*o.ds<>';
for k=find(ind)'
  color=colors(Angles==Flow{Case}.A(k),:);
%   marker=markers(Heights==Flow{Case}.H(k));
  plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.VelocityX./Data{Case}.flow{k}.FlowVelocityX,...
    'Color',color,...
    'DisplayName',['$H=' num2str(Flow{Case}.H(k)) ', \theta=' num2str(Flow{Case}.A(k)) '^\circ$']);
end
z=0:.05:1;
u=(5/3)*(1-(1-z).^1.5);
plot(z,u,'k','Linewidth',2,'DisplayName','Bagnold');

legend('show','Location','SouthEast')
xlabel('$(z-b)/h$','Interpreter','none')
ylabel('$v/\overline{v}$','Interpreter','none')
axis tight; v=axis; axis([0 v(2:4)]);
return


function plot_depthprofile_H(Case,H)
global Data Flow
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['depthprofile_Nu'  sprintf('_%d',Case) '_H' num2str(H,'_%d')])

ind=find(Flow{Case}.A<31&Flow{Case}.H==H)';
Angles=unique(Flow{Case}.A(ind));
%Angles=[22;26;30];
colors=lines(length(Angles));
for k=ind
  if sum(Angles==Flow{Case}.A(k))
    color=colors(Angles==Flow{Case}.A(k),:);
  %   plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.VelocityX./Data{Case}.flow{k}.FlowVelocityX,...
    plot((Data{Case}.flow{k}.z-Data{Case}.flow{k}.Base)./Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.Nu,...
      'Color',color,...
      'DisplayName',['$\theta=' num2str(Flow{Case}.A(k)) '^\circ$']);
  end
end
legend('show','Location','SouthEast')
xlabel('$(z-b)/h$','Interpreter','none')
ylabel('$\rho/\rho_p$','Interpreter','none')
axis tight; v=axis; axis([0 v(2:4)]);
return

function plot_depthprofile_VelocityX_H_old(Case)
global Data Flow
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['depthprofile_VelocityX'  sprintf('_%d',Case)])

Heights=unique(Flow{Case}.H(Flow{Case}.Steady));
Angles=unique(Flow{Case}.A(Flow{Case}.Steady));
colors=lines(length(Angles));

for j=1:length(Heights)
  subplot(length(Heights),1,j); hold on
  ind=find((Heights(j)==Flow{Case}.H)&(Flow{Case}.Steady))';
  for k=ind
    color=colors(Angles==Flow{Case}.A(k),:);
    plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.VelocityX./Data{Case}.flow{k}.FlowVelocityX,...
      'Color',color,...
      'DisplayName',['$H=' num2str(Heights(j)) ',\theta=' num2str(Flow{Case}.A(k)) '^\circ$']);
  end
  legend('show','Location','EastOutside')
  xlabel('$(z-b)/h$','Interpreter','none')
  ylabel('$v/\overline{v}$','Interpreter','none')
  axis tight; v=axis; axis([0 v(2:4)]);
end
return

function plot_depthprofile_hydrostatic(Case,H)
global Data Flow
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['depthprofile_hydrostatic'  sprintf('_%d',Case) '_H' num2str(H,'_%d')])

ind=find(~Flow{Case}.Accelerating&Flow{Case}.Flowing&Flow{Case}.H==H&Flow{Case}.Time>40)';
% Angles=unique(Flow{Case}.A(ind));
[Angles,ix]=sort(Flow{Case}.A(ind));
ind=ind(ix);
colors=lines(length(Angles));
for k=ind
  color=colors(Angles==Flow{Case}.A(k),:);
    Mass=-Data{Case}.flow{k}.Domain(6)/1.2*Data{Case}.flow{k}.ParticleDensity(1)*Data{Case}.flow{k}.ParticleVolume(1)*Data{Case}.flow{k}.Gravity(3);
    plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.StressZZ./Mass,...
    'Color',color,...
    'DisplayName',['$\theta=' num2str(Flow{Case}.A(k)) '^\circ$']);
end

legend('show','Location','NorthWest')
set(legend,'FontSize',8,'Box','off')
xlabel('$(z-b)/h$','Interpreter','none')
ylabel('$\sigma_{zz}/mg$','Interpreter','none')
title('$\sigma_{zz}$ for $H=40$')
axis tight; %v=axis; axis([0 v(2:4)]);
return

function plot_depthprofile_sigmaxx(Case,H)
global Data Flow
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['depthprofile_sigmaxx'  sprintf('_%d',Case) '_H' num2str(H,'_%d')])

ind=find(~Flow{Case}.Accelerating&Flow{Case}.Flowing&Flow{Case}.H==H)';
% Angles=unique(Flow{Case}.A(ind));
[Angles,ix]=sort(Flow{Case}.A(ind));
ind=ind(ix);
colors=lines(length(Angles));
for k=ind
  color=colors(Angles==Flow{Case}.A(k),:);
    Mass=-Data{Case}.flow{k}.Domain(6)/1.2*Data{Case}.flow{k}.ParticleDensity(1)*Data{Case}.flow{k}.ParticleVolume(1)*Data{Case}.flow{k}.Gravity(3);
    plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.StressXX./Mass,...
    'Color',color,...
    'DisplayName',['$\theta=' num2str(Flow{Case}.A(k)) '^\circ$']);
end

legend('show','Location','NorthWest')
set(legend,'FontSize',8,'Box','off')
xlabel('$(z-b)/h$','Interpreter','none')
ylabel('$\sigma_{xx}/mg$','Interpreter','none')
axis tight; %v=axis; axis([0 v(2:4)]);
return

function plot_depthprofile_mu(Case,H)
global Data Flow
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['depthprofile_mu'  sprintf('_%d',Case) '_H' num2str(H,'_%d')])

ind=find(~Flow{Case}.Accelerating&Flow{Case}.Flowing&Flow{Case}.H==H)';
ind=find(mod(Flow{Case}.A,2)==0&Flow{Case}.A<31&Flow{Case}.A>20&Flow{Case}.H==H)';
% Angles=unique(Flow{Case}.A(ind));
[Angles,ix]=sort(Flow{Case}.A(ind));
ind=ind(ix);
colors=lines(length(Angles));
for k=ind
  color=colors(Angles==Flow{Case}.A(k),:);
    plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.ChuteAngle-atand(-Data{Case}.flow{k}.StressXZ./Data{Case}.flow{k}.StressZZ),...
    'Color',color,...
    'DisplayName',['$\theta=' num2str(Flow{Case}.A(k)) '^\circ$']);
end

legend('show','Location','NorthWest')
set(legend,'FontSize',8,'Box','off')
xlabel('$(z-b)/h$','Interpreter','none')
ylabel('$\theta-tan^{-1}(-\sigma_{xz}/\sigma_{zz})$','Interpreter','tex')
axis tight; %v=axis; axis([0 v(2:4)]);
ylim([-1 1])
xlim([0 1])
return

function plot_depthprofile_I_H(Case,H)
global Data Flow
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['depthprofile_I'  sprintf('_%d',Case) '_H' num2str(H,'_%d')])

ind=find(~Flow{Case}.Accelerating&Flow{Case}.Flowing&Flow{Case}.H==H)';
ind=find(mod(Flow{Case}.A,2)==0&Flow{Case}.A<31&Flow{Case}.A>20&Flow{Case}.H==H)';
% Angles=unique(Flow{Case}.A(ind));
[Angles,ix]=sort(Flow{Case}.A(ind));
ind=ind(ix);
colors=lines(length(Angles));
for k=ind
  color=colors(Angles==Flow{Case}.A(k),:);
  x=Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight;
  y=Data{Case}.flow{k}.ChuteAngle-atand(-Data{Case}.flow{k}.StressXZ./Data{Case}.flow{k}.StressZZ);
  StrainXZ=diff((Data{Case}.flow{k}.VelocityX))./diff(Data{Case}.flow{k}.z);
  StrainXZ=.5*(StrainXZ([1 1:end])+StrainXZ([1:end end]));
  %StrainZZ=smooth(StrainZZ);
  I=StrainXZ./sqrt(Data{Case}.flow{k}.StressZZ);
  plot(x,I,'Color',color,...
    'DisplayName',['$\theta=' num2str(Flow{Case}.A(k)) '^\circ$']);
end

legend('show','Location','NorthWest')
set(legend,'FontSize',8,'Box','off')
xlabel('$(z-b)/h$','Interpreter','none')
%ylabel('$I=\tau_{xz}/d/\sqrt{\sigma_{zz}/\rho_c}$','Interpreter','tex')
ylabel('$I$','Interpreter','tex')
axis tight; %v=axis; axis([0 v(2:4)]);
ylim([-1 1])
xlim([0 1])
return

function plot_depthprofile_I_A(Case,A)
global Data Flow
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['depthprofile_I'  sprintf('_%d',Case) '_A' num2str(A,'_%d')])

ind=find(~Flow{Case}.Accelerating&Flow{Case}.Flowing&Flow{Case}.A==A)';
%ind=find(mod(Flow{Case}.A,2)==0&Flow{Case}.A<31&Flow{Case}.A>20&Flow{Case}.H==H)';
% Angles=unique(Flow{Case}.A(ind));
[Heights,ix]=sort(Flow{Case}.H(ind));
ind=ind(ix);
colors=lines(length(Heights));
for k=ind
  color=colors(Heights==Flow{Case}.H(k),:);
  x=Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight;
  y=Data{Case}.flow{k}.ChuteAngle-atand(-Data{Case}.flow{k}.StressXZ./Data{Case}.flow{k}.StressZZ);
  StrainXZ=diff((Data{Case}.flow{k}.VelocityX))./diff(Data{Case}.flow{k}.z);
  StrainXZ=.5*(StrainXZ([1 1:end])+StrainXZ([1:end end]));
  %StrainZZ=smooth(StrainZZ);
  I=StrainXZ./sqrt(Data{Case}.flow{k}.StressZZ);
  plot(x,I,'Color',color,...
    'DisplayName',['$N=' num2str(Flow{Case}.H(k)*200) '^\circ$']);
end

legend('show','Location','NorthWest')
set(legend,'FontSize',8,'Box','off')
xlabel('$(z-b)/h$','Interpreter','none')
%ylabel('$I=\tau_{xz}/d/\sqrt{\sigma_{zz}/\rho_c}$','Interpreter','tex')
ylabel('$I$','Interpreter','tex')
axis tight; %v=axis; axis([0 v(2:4)]);
ylim([-1 1])
xlim([0 1])
return

function plot_depthprofile_NormalStresses(Case,Height,Angle)
global Data Flow
figure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['depthprofile_NormalStresses'  sprintf('_%d',Case) num2str(Height,'H%d') num2str(Angle,'A%d')])

k=(Height==Flow{Case}.H)&(Angle==Flow{Case}.A);
h=plot(Data{Case}.flow{k}.z,...
  [Data{Case}.flow{k}.StressXX Data{Case}.flow{k}.StressZZ Data{Case}.flow{k}.StressYY -Data{Case}.flow{k}.StressXZ]);
c=get(gca,'Children');
set(c(1),'LineStyle','--');
xlabel('$z-b$','Interpreter','none')
% set(gca,'XTick',unique([get(gca,'XTick') Data{Case}.flow{k}.Base Data{Case}.flow{k}.FlowHeight]))
% Tick=get(gca,'XTick')
% Label=get(gca,'XTickLabel')
% Label(Tick==Data{Case}.flow{k}.Base,:)=' ';
% Label(Tick==Data{Case}.flow{k}.FlowHeight,:)=' ';
% Label(Tick==Data{Case}.flow{k}.Base,1)='b';
% Label(Tick==Data{Case}.flow{k}.FlowHeight,1)='s';
% set(gca,'XTickLabel',Label)
%plot(Data{Case}.flow{k}.Base*[1 1],ylim,':k')
%plot(Data{Case}.flow{k}.FlowHeight*[1 1]-2,ylim,'--k')
%plot(Data{Case}.flow{k}.FlowHeight*[1 1],ylim,'--k')

%legend({'$\sigma_{xx}$','$\sigma_{zz}$','$\sigma_{yy}$','$-\sigma_{xz}$'}...,'base','surface'}...
%  ,'Location','NorthEast')
%set(legend,'Box','off')
X=get(h,'XData');
Y=get(h,'YData');
text(X{1}(round(end*0.2))*1.2,Y{1}(round(end*0.2)),'$\leftarrow\sigma_{xx}$','HorizontalAlignment','left')
text(X{2}(round(end*0.2)),Y{2}(round(end*0.2))*0.95,'$\sigma_{zz}\rightarrow$','HorizontalAlignment','right')
text(X{3}(round(end*0.2)),Y{3}(round(end*0.2))*0.95,'$\sigma_{yy}\rightarrow$','HorizontalAlignment','right')
text(X{4}(round(end*0.2)),Y{4}(round(end*0.2))*0.95,'$-\sigma_{xz}\rightarrow$','HorizontalAlignment','right')
axis tight; 
xlim([0 Data{Case}.flow{k}.FlowHeight])%v=axis; axis([0 1 0.5 1.5]);
return

function plot_depthprofile_NormalStresses_H(Case,Heights)
global Data Flow
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['depthprofile_NormalStresses'  sprintf('_%d',Case)])

% Heights=unique(Flow{Case}.H(Flow{Case}.Steady));
Angles=unique(Flow{Case}.A(Flow{Case}.Steady&(mod(Flow{Case}.A,2)==0)));
colors=lines(length(Angles));

for j=1:length(Heights)
  subplot(length(Heights),1,j); hold on
  ind=find((Heights(j)==Flow{Case}.H)&(Flow{Case}.Steady)&mod(Flow{Case}.A,2)==0)';
  disp('we exclude the otherangles since the stats are not accurate')
  for k=ind
    color=colors(Angles==Flow{Case}.A(k),:);
    plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,...
      Data{Case}.flow{k}.StressXX./Data{Case}.flow{k}.StressZZ,...
      'Color',color,...
      'DisplayName',['$H=' num2str(Heights(j)) ',\theta=' num2str(Flow{Case}.A(k)) '^\circ$']);
  end
  legend('show','Location','EastOutside')
  xlabel('$(z-b)/h$','Interpreter','none')
  ylabel('$v/\overline{v}$','Interpreter','none')
  axis tight; v=axis; axis([0 1 0.5 1.5]);
end
return

function plot_depthprofile_NormalStressesWhalf(Case,Heights)
global DataWhalf
DataWhalf=loadstatistics([...
        '~/DRIVERS/Einder/run/old_runs/stat_flowrule_W0.5/*.stat ']); %#ok<NBRAK>
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['depthprofile_VelocityX_W05'  sprintf('_%d',Case)])

% Heights=unique(Flow{Case}.H(Flow{Case}.Steady));
A=cellfun(@(data)data.ChuteAngle, DataWhalf);
Angles=unique(A);
colors=lines(length(Angles));

for j=1:length(Heights)
  subplot(length(Heights),1,j); hold on
  ind=1:length(DataWhalf);
  for k=ind
    color=colors(Angles==A(k),:);
    plot(DataWhalf{k}.z./DataWhalf{k}.FlowHeight,...
      DataWhalf{k}.StressXX./DataWhalf{k}.StressZZ,...
      'Color',color,...
      'DisplayName',['$H=' num2str(Heights(j)) ',\theta=' num2str(A(k)) '^\circ$']);
  end
  legend('show','Location','EastOutside')
  xlabel('$(z-b)/h$','Interpreter','none')
  ylabel('$v/\overline{v}$','Interpreter','none')
  axis tight; v=axis; axis([0 v(2) 0 2]);
end
return

function plot_ha_density(Case,opt)
global Data Flow
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
if exist('opt','var')&&strcmp(opt,'centre')
set(gcf,'FileName',['ha_Nu_centre'  sprintf('_%d',Case)])
else
set(gcf,'FileName',['ha_Nu'  sprintf('_%d',Case)])
end
markers='xo+*sdph^v<>';
ind=Flow{Case}.Flowing&~Flow{Case}.Oscillating&Flow{Case}.Angle<30;
ind=Flow{Case}.Flowing&Flow{Case}.Angle<30;
% ind=~Flow{Case}.Accelerating;
Heights=unique(Flow{Case}.H(ind));
Angles=unique(Flow{Case}.A(ind));
colors=lines(length(Angles));
for j=1:length(Heights)
  indH=(Heights(j)==Flow{Case}.H)&(ind);
  if exist('opt','var')&&strcmp(opt,'centre')
    DensityCentre=cellfun(@(data) mean(data.Nu(data.z>data.FlowHeight*0.33&data.z<data.FlowHeight*0.67)), Data{Case}.flow(indH));
    Density=cellfun(@(data) mean(data.Nu(data.z>data.Base&data.z<data.Surface)), Data{Case}.flow(indH));
    %Density=cellfun(@(data) sum(data.Nu)*diff(data.z(1:2))/data.FlowHeight, Data{Case}.flow(indH));
    [x,ix]=sort(Flow{Case}.A(indH));
    subplot(1,2,2); hold on
    plot(x,DensityCentre(ix),[':' markers(j)], ...
         'Color',colors(j,:),...
         'DisplayName',['H=' num2str(Heights(j))]);
    ylabel('$\rho_c/\rho_p$','Interpreter','none')
    xlabel('$\theta$','Interpreter','none')
    subplot(1,2,1); hold on
    plot(x,Density(ix),[':' markers(j)], ...
         'Color',colors(j,:),...
         'DisplayName',['H=' num2str(Heights(j))]);
  else  
    Density=cellfun(@(data) sum(data.Nu)*diff(data.z(1:2))/data.FlowHeight, Data{Case}.flow(indH));
    [x,ix]=sort(Flow{Case}.A(indH));
    plot(x,Density(ix),[':' markers(j)], ...
         'Color',colors(j,:),...
         'DisplayName',['H=' num2str(Heights(j))]);
  end
  %Density=cellfun(@(data) mean(data.Nu(data.z>0&data.z<data.FlowHeight)), Data{Case}.flow(indH));
  %exclude stopped flows
  %Alpha(Flow{Case}.Froude(indH)<0.01)=nan;
  %I=cellfun(@(data) mean(data.I(data.z>0&data.z<data.FlowHeight)), Data{Case}.flow(indH));
  %I=cellfun(@(data) data.BottomFrictionCoefficient, Data{Case}.flow(indH));
  %[x,ix]=sort(I);

end
legend('show','Location','NorthEast')
set(legend,'Box','off')
xlabel('$\theta$','Interpreter','none')
ylabel('$\bar\rho/\rho_p$','Interpreter','none')
axis tight
return

function plot_ha_density_fit(CaseList,opt)
global Data Flow Title
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
if exist('opt','var')&&strcmp(opt,'centre')
set(gcf,'FileName',['ha_Nu_centre'  sprintf('_%d',CaseList)])
else
set(gcf,'FileName',['ha_Nu'  sprintf('_%d',CaseList)])
end
markers='xo+*sdph^v<>';
for i=1:length(CaseList); Case=CaseList(i);
ind=Flow{Case}.Flowing&~Flow{Case}.Oscillating&Flow{Case}.Angle<30;
% ind=~Flow{Case}.Accelerating;
Heights=unique(Flow{Case}.H(ind));
Angles=unique(Flow{Case}.A(ind));
colors=lines(length(Angles));
for j=2:length(Heights)
  indH=(Heights(j)==Flow{Case}.H)&(ind);
  %Density=cellfun(@(data) mean(data.Nu(data.z>data.Base&data.z<data.Surface)), Data{Case}.flow(indH));
  Density=cellfun(@(data) sum(data.Nu)*diff(data.z(1:2))/data.FlowHeight, Data{Case}.flow(indH));
  %Density=Density.*Flow{Case}.Height(indH);
  [x,ix]=sort(Flow{Case}.A(indH));
  if exist('opt','var')&&strcmp(opt,'centre')
    subplot(1,2,2); hold on
    DensityCentre=cellfun(@(data) mean(data.Nu(data.z>data.FlowHeight*0.33&data.z<data.FlowHeight*0.67)), Data{Case}.flow(indH));
    plot(x,DensityCentre(ix),[':' markers(j)], ...
         'Color',colors(j,:),...
         'DisplayName',['$H=' num2str(Heights(j)) '$']);
    ylabel('$\rho_c/\rho_p$','Interpreter','none')
    xlabel('$\theta$','Interpreter','none')
    axis tight; ylim([0.43 0.59])
    subplot(1,2,1); hold on
    plot(x,Density(ix),[':' markers(i)], ...
         'Color',colors(j,:),...
         'DisplayName',['$H=' num2str(Heights(j)) '$']);
    axis tight; ylim([0.43 0.59])
  else  
    %disp('cheat')
    if (j==length(Heights)&Case==5)
      plot(x,Density(ix),['--' markers(i)], ...
         'Color',colors(i,:),...
         'DisplayName',Title{Case});
    else
      plot(x,Density(ix),['' markers(i)], ...
         'Color',colors(i,:),...
         'DisplayName',Title{Case});
    end
  end
  %Density=cellfun(@(data) mean(data.Nu(data.z>0&data.z<data.FlowHeight)), Data{Case}.flow(indH));
  %exclude stopped flows
  %Alpha(Flow{Case}.Froude(indH)<0.01)=nan;
  %I=cellfun(@(data) mean(data.I(data.z>0&data.z<data.FlowHeight)), Data{Case}.flow(indH));
  %I=cellfun(@(data) data.BottomFrictionCoefficient, Data{Case}.flow(indH));
  %[x,ix]=sort(I);

end
end
c=get(gca,'Children');
%legend(c(end:-4:1))
legend(c(2:3:end))
set(legend,'Location','SouthWest')
set(legend,'Box','off')
xlabel('$\theta$','Interpreter','none')
ylabel('$\bar\rho/\rho_p$','Interpreter','none')
return

function plot_ha_density2(CaseList)
global Data Flow Title
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['ha_Nu'  sprintf('_%d',CaseList)])
markers='xo+*sdph^v<>';

colors=lines(length(CaseList));
for i=1:length(CaseList), Case=CaseList(i);
  ind=Flow{Case}.Angle<30&mod(Flow{Case}.Angle,2)==0;
  Heights=unique(Flow{Case}.H(ind));
  Angles=unique(Flow{Case}.A(ind));
    j=1;
    indH=(Heights(j)==Flow{Case}.H)&(ind);
%    Density=cellfun(@(data) sum(data.Nu)*diff(data.z(1:2))/data.FlowHeight, Data{Case}.flow(indH));
    Density=cellfun(@(data) mean(data.Nu(data.z>0&data.z<data.FlowHeight)), Data{Case}.flow(indH));
%    Density=cellfun(@(data) sum(data.Nu.^2)./sum(data.Nu), Data{Case}.flow(indH));
    [x,ix]=sort(Flow{Case}.A(indH));
    plot(x,Density(ix),['-' markers(j)], ...
         'Color',colors(i,:),...
         'DisplayName',Title{Case});
end
legend('show');
set(legend,'Box','off','Location','NorthEast')
for i=1:length(CaseList), Case=CaseList(i);
  ind=Flow{Case}.Angle<30&mod(Flow{Case}.Angle,2)==0;
  Heights=unique(Flow{Case}.H(ind));
  Angles=unique(Flow{Case}.A(ind));
  for j=2:length(Heights)
    indH=(Heights(j)==Flow{Case}.H)&(ind);
    Density=cellfun(@(data) mean(data.Nu(data.z>0&data.z<data.FlowHeight)), Data{Case}.flow(indH));
    [x,ix]=sort(Flow{Case}.A(indH));
    plot(x,Density(ix),['-' markers(j)], ...
         'Color',colors(i,:),...
         'DisplayName',Title{Case});

  end
end

xlabel('$\theta$','Interpreter','none')
ylabel('$\bar\rho/\rho_p$','Interpreter','none')
axis tight
return


function plot_ha_density_centre_List(CaseList)
global Data Flow Title
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['ha_Nu_centre'  sprintf('_%d',CaseList)])
markers='xo+*sdph^v<>';

colors=lines(length(CaseList));
for i=1:length(CaseList), Case=CaseList(i);
  ind=Flow{Case}.Angle<30&mod(Flow{Case}.Angle,2)==0;
  Heights=unique(Flow{Case}.H(ind));
  Angles=unique(Flow{Case}.A(ind));
    j=1;
    indH=(Heights(j)==Flow{Case}.H)&(ind);
    %Density=cellfun(@(data) sum(data.Nu(data.z>data.FlowHeight*.3&data.z<data.FlowHeight*.8))*diff(data.z(1:2))/(data.FlowHeight/2), Data{Case}.flow(indH));
    Density=cellfun(@(data) mean(data.Nu(data.z>data.FlowHeight*.3&data.z<data.FlowHeight*.8)), Data{Case}.flow(indH));
    [x,ix]=sort(Flow{Case}.A(indH));
    plot(x,Density(ix),['-' markers(j)], ...
         'Color',colors(i,:),...
         'DisplayName',Title{Case});
end
legend('show');
set(legend,'Box','off','Location','NorthEast')
for i=1:length(CaseList), Case=CaseList(i);
  ind=Flow{Case}.Angle<30&mod(Flow{Case}.Angle,2)==0;
  Heights=unique(Flow{Case}.H(ind));
  Angles=unique(Flow{Case}.A(ind));
  for j=2:length(Heights)
    indH=(Heights(j)==Flow{Case}.H)&(ind);
    Density=cellfun(@(data) mean(data.Nu(data.z>data.FlowHeight*.3&data.z<data.FlowHeight*.8)), Data{Case}.flow(indH));
    [x,ix]=sort(Flow{Case}.A(indH));
    plot(x,Density(ix),['-' markers(j)], ...
         'Color',colors(i,:),...
         'DisplayName',Title{Case});

  end
end

xlabel('$\theta$','Interpreter','none')
ylabel('$\rho_c/\rho_p$','Interpreter','none')
axis tight
return

function plot_ha_alpha(Case)
global Data Flow
figure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName',['ha_alpha'  sprintf('_%d',Case)])
markers='xo+*sdph^v<>';

ind=Flow{Case}.Flowing&~Flow{Case}.Accelerating&~Flow{Case}.Oscillating&Flow{Case}.Angle<31;
% ind=~Flow{Case}.Accelerating;
Heights=unique(Flow{Case}.H(ind));
Angles=unique(Flow{Case}.A(ind));
colors=lines(length(Angles));
for j=1:length(Heights)
  indH=(Heights(j)==Flow{Case}.H)&(ind);
  %Alpha=cellfun(@(data) norm(data.FlowVelocity2(1))/norm(data.FlowVelocity(1))^2, Data{Case}.flow(indH));
  meanVelocityX2=  cellfun(@(data) sum(data.VelocityX.*data.Nu).^2./sum(data.Nu)*diff(data.z(1:2)), Data{Case}.flow(indH));
  meanSqrVelocityX= cellfun(@(data) sum(data.VelocityX.^2.*data.Nu)*diff(data.z(1:2)), Data{Case}.flow(indH));
  %meanVelocityX=  cellfun(@(data) mean(data.VelocityX(data.z>0&data.z<data.FlowHeight)), Data{Case}.flow(indH));
  %meanVelocityX2= cellfun(@(data) mean(data.VelocityX(data.z>0&data.z<data.FlowHeight).^2), Data{Case}.flow(indH));
  Alpha=meanSqrVelocityX./meanVelocityX2;
  %exclude stopped flows
  Alpha(Flow{Case}.Froude(indH)<0.01)=nan;
  [x,ix]=sort(Flow{Case}.A(indH));
  plot(x,Alpha(ix),[':' markers(j)], ...
       'Color',colors(j,:),...
       'DisplayName',['H=' num2str(Heights(j))]);

end
plot(Angles,4/3*ones(size(Angles)),'k:','Linewidth',2,'DisplayName','Linear profile');
plot(Angles,5/4*ones(size(Angles)),'k','Linewidth',2,'DisplayName','Bagnold profile');
legend('show','Location','NorthEast')
set(legend,'Box','off')
xlabel('$\theta$','Interpreter','none')
ylabel('$\alpha$','Interpreter','none')
axis tight
return

function plot_ha_alpha_List(CaseList)
global Data Flow Title
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['ha_Alpha'  sprintf('_%d',CaseList)])
markers='xo+*sdph^v<>';
colors=lines(length(CaseList));

% subplot(1,2,1); hold on
% Case=5;
% ind=Flow{Case}.Flowing&~Flow{Case}.Oscillating&Flow{Case}.Angle<30;
% Heights=unique(Flow{Case}.H(ind)); %all Heights
% Angles=unique(Flow{Case}.A(ind));
% for j=1:length(Heights)
%   indH=(Heights(j)==Flow{Case}.H)&(ind);
%    meanVelocityX=  cellfun(@(data) sum(data.VelocityX.*data.Nu)/sum(data.Nu), Data{Case}.flow(indH));
%    meanVelocityX2= cellfun(@(data) sum(data.VelocityX.^2.*data.Nu)/sum(data.Nu), Data{Case}.flow(indH));
% %     meanVelocityX=  cellfun(@(data) mean(data.VelocityX(data.z>data.Base&data.z<data.Surface)), Data{Case}.flow(indH));
% %    meanVelocityX2= cellfun(@(data) mean(data.VelocityX(data.z>data.Base&data.z<data.Surface).^2), Data{Case}.flow(indH));
%   Alpha=meanVelocityX2./(meanVelocityX.^2);
%   [x,ix]=sort(Flow{Case}.A(indH));
%   plot(x,Alpha(ix),[':' markers(Heights(j)/10)], ...
%        'Color',colors(Heights(j)/10,:),...
%          'DisplayName',['H=' num2str(Heights(j))]);
% end
% legend('show','Location','NorthEast')
% set(legend,'FontSize',8)
% plot(Angles,4/3*ones(size(Angles)),'k:','Linewidth',2,'DisplayName','Linear profile');
% plot(Angles,5/4*ones(size(Angles)),'k','Linewidth',2,'DisplayName','Bagnold profile');
% xlabel('$\theta$','Interpreter','none')
% ylabel('$\alpha$','Interpreter','none')
% axis tight
% 
% subplot(1,2,2); hold on
for i=1:length(CaseList), Case=CaseList(i);
  ind=Flow{Case}.Flowing&~Flow{Case}.Oscillating&Flow{Case}.Angle<30&Flow{Case}.Angle>20&Flow{Case}.H>20;
  Angles=unique(Flow{Case}.A(ind));
  meanVelocityX=  cellfun(@(data) sum(data.VelocityX.*data.Nu)/sum(data.Nu), Data{Case}.flow(ind));
  meanVelocityX2= cellfun(@(data) sum(data.VelocityX.^2.*data.Nu)/sum(data.Nu), Data{Case}.flow(ind));
%     meanVelocityX=  cellfun(@(data) mean(data.VelocityX(data.z>data.Base&data.z<data.Surface)), Data{Case}.flow(indH));
 %    meanVelocityX2= cellfun(@(data) mean(data.VelocityX(data.z>data.Base&data.z<data.Surface).^2), Data{Case}.flow(indH));
  Alpha=meanVelocityX2./(meanVelocityX.^2);
  [x,ix]=sort(Flow{Case}.A(ind));
  plot(x,Alpha(ix),['' markers(i)], ...
         'Color',colors(i,:),...
       'DisplayName',Title{Case});
  plot([24 28],mean(Alpha(Flow{Case}.Angle(ind)>23))*[1 1],'--', ...
         'Color',colors(i,:),...
       'DisplayName',Title{Case});
  angles=Alpha(Flow{Case}.Angle(ind)>23);
  disp([Title{Case} ' & ' num2str(mean(angles)) ' & ' num2str(var(angles),'%f')]);
end
c=get(gca,'Children');
legend(c(2:2:end),'Location','SouthWest')
set(legend,'FontSize',8)
plot(Angles,ones(size(Angles)),'k--','Linewidth',2,'DisplayName','Plug flow');
plot(Angles,5/4*ones(size(Angles)),'k','Linewidth',2,'DisplayName','Bagnold profile');
xlabel('$\theta$','Interpreter','none')
ylabel('$\alpha$','Interpreter','none')
axis tight
% colors=lines(length(CaseList));
% colorix=1;
% for i=1:length(CaseList), Case=CaseList(i);
%   ind=Flow{Case}.Flowing&~Flow{Case}.Oscillating&Flow{Case}.Angle<30;
%   Heights=unique(Flow{Case}.H(ind)); %all Heights
%   if (Case~=5) Heights=30; linetype=':';
%   else linetype='-'; end
%   Angles=unique(Flow{Case}.A(ind));
%   for j=1:length(Heights)
%     indH=(Heights(j)==Flow{Case}.H)&(ind);
%      meanVelocityX=  cellfun(@(data) sum(data.VelocityX.*data.Nu)/sum(data.Nu), Data{Case}.flow(indH));
%      meanVelocityX2= cellfun(@(data) sum(data.VelocityX.^2.*data.Nu)/sum(data.Nu), Data{Case}.flow(indH));
% %     meanVelocityX=  cellfun(@(data) mean(data.VelocityX(data.z>data.Base&data.z<data.Surface)), Data{Case}.flow(indH));
%  %    meanVelocityX2= cellfun(@(data) mean(data.VelocityX(data.z>data.Base&data.z<data.Surface).^2), Data{Case}.flow(indH));
%     Alpha=meanVelocityX2./(meanVelocityX.^2);
%     [x,ix]=sort(Flow{Case}.A(indH));
%     plot(x,Alpha(ix),[linetype markers(i)], ...
%          'Color',colors(Heights(j)/10,:),...
%          'DisplayName',[Title{Case}, ', H=' num2str(Heights(j))]);
%     colorix=colorix+1
%   end
% end
return

function plot_ha_K_List(CaseList)
global Data Flow Title
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['ha_K'  sprintf('_%d',CaseList)])
markers='xoxxxxx';

colors=lines(length(CaseList));
for i=1:length(CaseList), Case=CaseList(i);
  ind=(Flow{Case}.Flowing)&Flow{Case}.A<30&~Flow{Case}.Oscillating;
%   ind=~Flow{Case}.Accelerating&~Flow{Case}.Oscillating&Flow{Case}.Angle<30;
    Heights=unique(Flow{Case}.H(ind));
    Angles=unique(Flow{Case}.A(ind));
    j=1;
    indH=(Heights(j)==Flow{Case}.H)&(ind)&mod(Flow{Case}.A,2)==0;
    indH=ind;
    FlowStressXX=cellfun(@(data) mean(data.StressXX(data.z>data.Base+2&data.z<data.Surface)), Data{Case}.flow(indH));
    FlowStressZZ=cellfun(@(data) mean(data.StressZZ(data.z>data.Base+2&data.z<data.Surface)), Data{Case}.flow(indH));
    K=FlowStressXX./FlowStressZZ;
    [x,ix]=sort(Flow{Case}.A(indH));
    plot(x,K(ix),[':' markers(Case)], ...
         'Color',colors(i,:),...
         'DisplayName',Title{Case});
end
legend('show');
set(legend,'Box','off','Location','Best')
for i=1:length(CaseList), Case=CaseList(i);
  ind=(Flow{Case}.Flowing)&Flow{Case}.A<30&~Flow{Case}.Oscillating;
  Heights=unique(Flow{Case}.H(ind));
  Angles=unique(Flow{Case}.A(ind));
  for j=2:length(Heights)
    indH=(Heights(j)==Flow{Case}.H)&(ind)&mod(Flow{Case}.A,2)==0;
  FlowStressXX=cellfun(@(data) mean(data.StressXX(data.z>0&data.z<data.FlowHeight)), Data{Case}.flow(indH));
  FlowStressZZ=cellfun(@(data) mean(data.StressZZ(data.z>0&data.z<data.FlowHeight)), Data{Case}.flow(indH));
    K=FlowStressXX./FlowStressZZ;
    [x,ix]=sort(Flow{Case}.A(indH));
    plot(x,K(ix),['-' markers(Case)], ...
         'Color',colors(i,:),...
         'DisplayName',Title{Case});

  end
end

xlabel('$\theta$','Interpreter','none')
ylabel('$K$','Interpreter','none')
axis tight
return

function plot_ha_K(CaseList)
global Data Flow Title
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['ha_K'  sprintf('_%d',CaseList)])

for i=1:length(CaseList); Case=CaseList(i); subplot(1,length(CaseList),i); hold on
  clear K A
  Heights=unique(Flow{Case}.H(Flow{Case}.Steady));
  Angles=unique(Flow{Case}.A(Flow{Case}.Steady));
  colors=lines(length(Angles));
  for j=1:length(Heights)
    ind=Flow{Case}.Flowing&Flow{Case}.Angle<30;
    ind=(Heights(j)==Flow{Case}.H)&(ind);
    FlowStressXX=cellfun(@(data) mean(data.StressXX(data.z>data.Base+2&data.z<data.Surface)), Data{Case}.flow(ind));
    FlowStressZZ=cellfun(@(data) mean(data.StressZZ(data.z>data.Base+2&data.z<data.Surface)), Data{Case}.flow(ind));
    [A{j},ix]=sort(Flow{Case}.A(ind));
    K{j}=FlowStressXX./FlowStressZZ;
    K{j}=K{j}(ix);

    %plot sorted
    plot(A{j},K{j},'o', ...
         'Color',colors(j,:),...
         'DisplayName',['N=' num2str(200*Heights(j))]);
  end
  xlabel('$\theta$','Interpreter','none')
  ylabel('$K$','Interpreter','none')
  if (length(CaseList)>1) title(Title{Case}); end
  axis tight
end
ylim([.9 1.1])
xlim([22-eps 28+eps])
legend('show','Location','SouthEast')
return

function plot_ha_K_hstop(Case)
global Data Flow CoeffHstop
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['ha_K'  sprintf('_%d',Case)])

Heights=unique(Flow{Case}.H(Flow{Case}.Steady));
Angles=unique(Flow{Case}.A(Flow{Case}.Steady));
colors=lines(length(Angles));
markers='xo+*sdph^v<>';
for j=1:length(Angles)
  ind=(Angles(j)==Flow{Case}.A)&(Flow{Case}.Steady)&(~Flow{Case}.Oscillating);
  disp('we exclude the otherangles since the stats are not accurate')
  ind=ind&mod(Flow{Case}.A,2)==0&~(Flow{Case}.A==20&Flow{Case}.H==30);
%   FlowStressXX=cellfun(@(data) sum(data.StressXX,1)*diff(data.z([1 2]))/diff(data.Domain([5 6]))/data.FlowDensity, Data{Case}.flow(ind));
%   FlowStressZZ=cellfun(@(data) sum(data.StressZZ,1)*diff(data.z([1 2]))/diff(data.Domain([5 6]))/data.FlowDensity, Data{Case}.flow(ind));
  FlowStressXX=cellfun(@(data) sum(data.StressXX.*data.Density)./sum(data.Density), Data{Case}.flow(ind));
  FlowStressZZ=cellfun(@(data) sum(data.StressZZ.*data.Density)./sum(data.Density), Data{Case}.flow(ind));
  K=FlowStressXX./FlowStressZZ;

  %plot sorted
  HeightOverHstop=Flow{Case}.H(ind)./hstop(CoeffHstop{Case},Flow{Case}.A(ind)); 
  H=Flow{Case}.H(ind);
  [x,ix]=sort(H);
  plot(x,K(ix),['-' markers(j)], ...
       'Color',colors(j,:),...
       'DisplayName',['A=' num2str(Angles(j))]);

end
legend('show','Location','SouthEast')
set(legend,'Box','off')
xlabel('$h/hs$','Interpreter','none')
ylabel('$K$','Interpreter','none')
axis tight
return

function plot_ha_bottom_friction(Case)
global Data Flow
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['ha_bottom_friction'  sprintf('_%d',Case)])

Heights=unique(Flow{Case}.H(Flow{Case}.Steady));
Angles=unique(Flow{Case}.A(Flow{Case}.Steady));
Angles=Angles(mod(Angles,2)==0);
colors=lines(length(Angles));
for j=1:length(Heights)
  ind=(Heights(j)==Flow{Case}.H)&(Flow{Case}.Steady);
  disp('we exclude the otherangles since the stats are not accurate')
  ind=ind&mod(Flow{Case}.A,2)==0;%&~(Flow{Case}.A==20&Flow{Case}.H==30);
%   FlowStressXX=cellfun(@(data) sum(data.StressXX,1)*diff(data.z([1 2]))/diff(data.Domain([5 6]))/data.FlowDensity, Data{Case}.flow(ind));
%   FlowStressZZ=cellfun(@(data) sum(data.StressZZ,1)*diff(data.z([1 2]))/diff(data.Domain([5 6]))/data.FlowDensity, Data{Case}.flow(ind));
  BottomFriction=cellfun(@(data)data.BottomFrictionCoefficient, Data{Case}.flow(ind));
  dBottomFriction=Flow{Case}.A(ind)-atand(BottomFriction);

  %plot sorted
  [x,ix]=sort(Flow{Case}.A(ind));
  plot(x,dBottomFriction(ix),':x', ...
       'Color',colors(j,:),...
       'DisplayName',['H=' num2str(Heights(j))]);

end
legend('show','Location','NorthEast')
set(legend,'Box','off')
xlabel('$\theta$','Interpreter','none')
ylabel('$\theta-\tan^{-1}(\mu)$','Interpreter','none')
axis tight
return

function plot_arresting(Case,Height) 
%Ratio of kinetic and time-averaged elastic energy for $\lambda=0.5$,
%$H=$, and varying inclination. While
global Data Flow
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['arresting'  sprintf('_%d',Case)])

ind=find(Flow{Case}.H==Height);
[Angles,ix]=sort(Flow{Case}.A(ind));
ind=ind(ix);
colors=lines(length(ind)+1);
set(gca,'YScale','log');
for j=1:length(ind); k=ind(j);
  avgEneEla=mean(Data{Case}.flow{k}.Ene.Ela(ceil(end/2):end-1));
  plot(Data{Case}.flow{k}.Ene.Time(2:end-1),...
    Data{Case}.flow{k}.Ene.Kin(2:end-1)./avgEneEla,'-',...
    'Color',colors(j,:),...
    'Tag',num2str(Flow{Case}.A(k)),...
    'DisplayName',['$\theta=' num2str(Flow{Case}.A(k)) '^\circ$']);
  hold on
end
axis tight
v=axis;
set(gca,'XLim',[0 v(2)])
set(gca,'YLim',[max(1e-0,v(3)) v(4)])
legend('show','Location','EastOutside')
xlabel('$t$','Interpreter','none')
ylabel('$E_{kin}/\langle E_{ela}\rangle$','Interpreter','none')
return

function plot_accelerating(Case,Height) 
global Data Flow
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['accelerating'  sprintf('_%d',Case)])

ind=find(Flow{Case}.H==Height);
  [A,ix]=sort(Flow{Case}.A(ind));
  ind=ind(ix);

colors=lines(length(ind));
for j=1:length(ind); k=ind(j);
  %indtime=Data{Case}.flow{k}.EneShort.Time>=1500&Data{Case}.flow{k}.EneShort.Time<=2001;
  indtime=Data{Case}.flow{k}.EneShort.Time>=2001&Data{Case}.flow{k}.EneShort.Kin(end-1)>1e-10;
  indtime([1 end])=false;
  if sum(indtime)
      x=Data{Case}.flow{k}.EneShort.Time(indtime)-Data{Case}.flow{k}.EneShort.Time(end)+2000;
      y=(Data{Case}.flow{k}.EneShort.Kin(indtime)/Data{Case}.flow{k}.EneShort.Kin(find(indtime,1,'last')));
      p=polyfit(x,y,3);
      plot(x,((p(1)*x+p(2)).*x+p(3)).*x+p(4),...
        'Color',colors(j,:),...
        'DisplayName',['$\theta=' num2str(Flow{Case}.A(k)) '^\circ$']);
      hold on
  end
end
axis tight
v=axis;
set(gca,'YLim',[max(1e-1,v(3)) v(4)]);
legend('show','Location','EastOutside')
xlabel('$t$','Interpreter','none')
ylabel('$E_{kin}(t)/E_{kin}(2000)$','Interpreter','none')
return

function plot_accelerating_2(Case) 
global Data Flow
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['accelerating'  sprintf('_%d',Case)])

Height=20;
ind=find(Flow{Case}.H==Height);
set(gca,'YScale','log');
set(gca,'XScale','log');
colors=lines(length(ind));
for j=1:length(ind); k=ind(j);
  %indtime=Data{Case}.flow{k}.EneShort.Time>=1500&Data{Case}.flow{k}.EneShort.Time<=2001;
  indtime=Data{Case}.flow{k}.EneShort.Time>=2001&Data{Case}.flow{k}.EneShort.Kin(end-1)>1e-10;
  indtime([1 end])=false;
  plot(-Data{Case}.flow{k}.EneShort.Time(indtime)+Data{Case}.flow{k}.EneShort.Time(end),...
    abs(Data{Case}.flow{k}.EneShort.Kin(indtime)-Data{Case}.flow{k}.EneShort.Kin(find(indtime,1,'last'))),...
    'Color',colors(j,:),...
    'DisplayName',['$\theta=' num2str(Flow{Case}.A(k)) '^\circ$']);
  hold on
end
axis tight
v=axis;
set(gca,'YLim',[max(1e-1,v(3)) v(4)])
% legend('show','Location','EastOutside')
xlabel('$t$','Interpreter','none')
ylabel('$E_{kin}(t)/E_{kin}(2000)$','Interpreter','none')
return

function plot_coeffHstop() 
global Flow CoeffHstop
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName','coeffhstop')

CaseList=2:6;
theta1=cellfun(@(data)atand(data(1)),CoeffHstop(CaseList));
theta2=cellfun(@(data)atand(data(2)),CoeffHstop(CaseList));
% A=cellfun(@(data)data(3),CoeffHstop(CaseList));
lambda=cellfun(@(data)data.L(1),Flow(CaseList));
plot(lambda,[theta1;theta2])
axis tight
% legend('show','Location','EastOutside')
xlabel('$\lambda$','Interpreter','none')
legend({'$\theta_1$','$\theta_2$'})
return

function plot_arresting_H20() 
%Ratio of kinetic and time-averaged elastic energy for $\lambda=0.5$,
%$H=$, and varying inclination. While
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName','arresting')
set(gca,'YScale','log');

filename = strread(ls('../ene/H20A*L1M0.5B0.5.ene'),'%s ');
colors=lines(length(filename));
A= cellfun(@(data)str2double(data(strfind(data,'A')+1:strfind(data,'L')-1)),filename);
[A,ix]=sort(A,'descend');
filename=filename(ix);

for i=1:length(filename)
  rawdata = importdata(filename{i},' ',1);
  % if tabs are used as delimiters
  if (size(rawdata.data,2)~=8)
    rawdata = importdata(filename,'\t',1);
  end
  Ene.Time = rawdata.data(:,1);
  Ene.Kin = rawdata.data(:,3);
  Ene.Ela = rawdata.data(:,5);

  avgEneEla=mean(Ene.Ela(ceil(end/2):end-1));
  if (A(i)>=30), linetype='--'; else linetype='-'; end
  plot(Ene.Time(2:end-1),...
    Ene.Kin(2:end-1)./avgEneEla,linetype,...
    'Color',colors(i,:),...
    'Tag',num2str(A(i)),...
    'DisplayName',['$\theta=' num2str(A(i)) '^\circ$']);
  hold on
end
axis tight
v=axis;
set(gca,'XLim',[0 v(2)])
set(gca,'YLim',[max(1e-0,v(3)) v(4)])
legend('show','Location','EastOutside')
xlabel('$t$','Interpreter','none')
ylabel('$E_{kin}/\langle{E}_{ela}\rangle$','Interpreter','none')
set(legend,'Box','off','FontSize',7)

return

function plot_friction(CaseList) 
%load data
global Data Title CoeffHstop Flow Compare

%new figure
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['friction'  sprintf('_%d',CaseList)])

%create subplots
if ~exist('CaseList','var'), CaseList=1:length(Data); end
ny=length(CaseList);
nx=ceil(length(CaseList)/ny);

subplot(nx,ny,1);
ylabel('$F$')
for k=1:length(CaseList); Case=CaseList(k);
  subplot(nx,ny,k); hold on

  %which hstop we compare with
  cmp=Compare;
 
  %flow
  data=Data{Case}.flow;
  FlowHeight=cellfun(@(data)data.FlowHeight,data);
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,data);
  Angle=cellfun(@(data)data.ChuteAngle,data);
  Oscillating=Flow{Case}.Oscillating; 
  GravityZ=cellfun(@(data)data.Gravity(3),data);
  %defined for the rough bottom
  HeightOverHstop=FlowHeight./hstop(CoeffHstop{cmp},Angle); 
  Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
  indAll=Angle>atand(CoeffHstop{cmp}(1))&Angle<atand(CoeffHstop{cmp}(2))&Flow{Case}.Steady;%&Hydrostaticity<0.2;
  %oscillating
  if (Case==2), ind=Oscillating<0.5&indAll;
  else ind=Oscillating<0.25&indAll;
  end
  %fit
  if exist('jenkinscorrection','var')
    [betaPO]=flowrule_fit_with_offset(Angle(ind), Froude(ind), HeightOverHstop(ind), CoeffHstop{k},0); %#ok<NASGU>
  else
    [betaPO]=flowrule_fit_with_offset(Angle(ind), Froude(ind), HeightOverHstop(ind), CoeffHstop{k},2); %#ok<NASGU>
  end

  x=FlowHeight(ind);
  y=Froude(ind);
  z=tand(Angle(ind));
  TRI=delaunay(x,y);
%   colormap(jet)
  h=trisurf(TRI,x,y,z); %#ok<NASGU>
  shading interp
%   if k==length(CaseList), colorbar; end
%   set(h,'EdgeColor','k')
  
  
%  scatter(FlowHeight(ind),Froude(ind),50,tand(Angle(ind)),'filled');
%   x=sort([1; HeightOverHstop(ind)]);
%   plot(x,betaPO(1)+betaPO(2)*x,'k');
%   display(['Pouliquen slope: for case ' num2str(Case) ':' num2str(betaPO(2),'%.3f')])

  %title([Title{Case} ' \beta=' num2str(betaPO(2),'%.3f')])
%   colorbar 
  axis tight
%   v = axis; axis([0 v(2) 0 v(4)])
  
  if length(CaseList)>1
    title(Title{Case})
  end
  
  xlabel('$h$')
end

return

function plot_flow_rule_Mu_fit(CaseList)
%load data
global Data Title CoeffHstop Flow Compare

%new figure
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['friction'  sprintf('_%d',CaseList)])

%create subplots
if ~exist('CaseList','var'), CaseList=1:length(Data); end
ny=length(CaseList);
nx=ceil(length(CaseList)/ny);

subplot(nx,ny,1);
ylabel('$\mu$')
for k=1:length(CaseList); Case=CaseList(k);
  subplot(nx,ny,k); hold on

  %which hstop we compare with
  cmp=Compare;
 
  %flow
  data=Data{Case}.flow;
  FlowHeight=cellfun(@(data)data.FlowHeight,data);
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,data);
  Angle=cellfun(@(data)data.ChuteAngle,data);
  Oscillating=Flow{Case}.Oscillating; 
  GravityZ=cellfun(@(data)data.Gravity(3),data);
  %defined for the rough bottom
  HeightOverHstop=FlowHeight./hstop(CoeffHstop{cmp},Angle); 
  Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
  indAll=Angle>atand(CoeffHstop{cmp}(1))&Angle<atand(CoeffHstop{cmp}(2))&Flow{Case}.Steady;%&Hydrostaticity<0.2;
  %oscillating
  if (Case==2), ind=Oscillating<0.5&indAll;
  else ind=Oscillating<0.25&indAll;
  end
  %fit
  ind=Flow{Case}.Steady&~Flow{Case}.Oscillating&~(Case==4&Flow{Case}.H==40&Flow{Case}.A==26);
  [betaPO]=flowrule_fit_with_offset(Angle(ind), Froude(ind), HeightOverHstop(ind), CoeffHstop{cmp},1);

%   x=mu_fit(Angle(ind),FlowHeight(ind),Froude(ind));
  t1=CoeffHstop{cmp}(1);
  t21=CoeffHstop{cmp}(2)-t1;
  A=CoeffHstop{cmp}(3);
  gamma=-betaPO(1);
  beta=betaPO(2);
  
  x=(Froude(ind)+gamma)./FlowHeight(ind);
  scatter((Froude(ind)+gamma)./FlowHeight(ind),tand(Angle(ind)),50,Flow{Case}.H(ind),'filled')
  
  X=linspace(min(x),max(x),100);
  Y=t1+t21./(1+beta/A./X);
  plot(X,Y);

  %axis tight
  title(Title(Case))
  xlabel('$(F+\gamma)/h$')
end

return

function plot_flow_rule_Mu_fit_oneplot(CaseList,opt)
%load data
global Data Title CoeffHstop Flow Compare

%new figure
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['friction'  sprintf('_%d',CaseList)])

%create subplots
if ~exist('CaseList','var'), CaseList=1:length(Data); end
colors=lines(length(CaseList));

ylabel('$\mu$')
for k=1:length(CaseList); Case=CaseList(k);
  %which hstop we compare with
  cmp=Compare;
 
  %flow
  data=Data{Case}.flow;
  FlowHeight=cellfun(@(data)data.FlowHeight,data);
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,data);
  Angle=cellfun(@(data)data.ChuteAngle,data);
  Oscillating=Flow{Case}.Oscillating; 
  GravityZ=cellfun(@(data)data.Gravity(3),data);
  %defined for the rough bottom
  HeightOverHstop=FlowHeight./hstop(CoeffHstop{cmp},Angle); 
  Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
  indAll=Angle>atand(CoeffHstop{cmp}(1))&Angle<atand(CoeffHstop{cmp}(2))&Flow{Case}.Steady;%&Hydrostaticity<0.2;
  %oscillating
  if (Case==2), ind=Oscillating<0.5&indAll;
  else ind=Oscillating<0.25&indAll;
  end
  %fit
  [betaPO]=flowrule_fit_with_offset(Angle(ind), Froude(ind), HeightOverHstop(ind), CoeffHstop{k},1);

  t1=CoeffHstop{cmp}(1);
  t21=CoeffHstop{cmp}(2)-t1;
  A=CoeffHstop{cmp}(3);
  gamma=-Flow{Case}.betaPO(1);
  beta=Flow{Case}.betaPO(2);
  if isstruct(opt)&&~isfield(opt,'nodata')
    plot((Froude(ind)+gamma)./FlowHeight(ind),tand(Angle(ind)),'.','Color',colors(k,:))
  end
  X=linspace(0,max((Froude(ind)+gamma)./FlowHeight(ind)),100);
  Y=t1+t21./(1+beta/A./X);
  plot(X,Y,'Color',colors(k,:),'DisplayName',Title(k))

  %show layered cases
%   ind=Flow{Case}.Oscillating&~Flow{Case}.FullyOscillating&indAll;
%   gamma=-Flow{Case}.betaPOlayered(1);
%   beta=Flow{Case}.betaPOlayered(2);
%   plot((Froude(ind)+gamma)./FlowHeight(ind),tand(Angle(ind)),'o','Color',colors(k,:))
%   X=linspace(0,max((Froude(ind)+gamma)./FlowHeight(ind)),100);
%   Y=t1+t21./(1+beta/A./X);
%   plot(X,Y,'Color',colors(k,:),'DisplayName',Title(k))

  
end
xlabel('$(F+\gamma)/h$')
child=get(gca,'Children');
if isstruct(opt)&&~isfield(opt,'nodata')
  legend(child(end-1:-2:1),Title(CaseList),'Location','SouthEast')
else
  legend(child(end:-1:1),Title(CaseList),'Location','SouthEast')
end
set(legend,'Box','off')
axis tight
v=axis;
axis([0 v(2:4)])

return

function plot_flow_rule_Mu(CaseList)
%load data
global Data Title  

%new figure
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['friction'  sprintf('_%d',CaseList)])

ny=ceil(sqrt(length(CaseList)));
nx=ceil(length(CaseList)/ny);
for k=1:length(CaseList), Case=CaseList(k);
  disp(['case' num2str(k)])
  subplot(ny,nx,k); hold on
  
  data=Data{Case}.flow;
  FlowHeight=cellfun(@(data)data.FlowHeight,data);
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,data);
  Angle=cellfun(@(data)data.ChuteAngle,data);
  GravityZ=cellfun(@(data)data.Gravity(3),data);
  Froude=FlowSpeed./sqrt(-GravityZ.*FlowHeight);
%   ind=Angle<atand(CoeffHstop{k}(2));
  ind=Angle<29&Froude>0.2;
  
  scatter(Froude(ind),tand(Angle(ind)),50,FlowHeight(ind),'filled')
  title([Title{Case}])
  colorbar
  axis tight
  xlabel('F')
  ylabel('\mu')
end

return

function supportedflow()
plot_depthprofile(1,30,24,[],'Ene');
%title('$H=20,\theta=24^\circ$')
set(gca,'FontSize',20)
set(get(gca,'XLabel'),'FontSize',20)
set(get(gca,'YLabel'),'FontSize',20)
set(get(gca,'Title'),'FontSize',20)
legend('off')
%legend({'H=10','H=20','H=30'},'FontSize',10)
xlim([0,6000])
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName','decellerating_ene')
text(min(xlim)  ,max(ylim)*1.02,['$10^4$'],'HorizontalAlignment','right','FontSize',20)

plot_depthprofile(1,20,24,[],'Nu');
%title('$H=20,\theta=22^\circ$')
set(gca,'FontSize',20)
set(get(gca,'XLabel'),'FontSize',20)
set(get(gca,'YLabel'),'FontSize',20)
set(get(gca,'Title'),'FontSize',20)
legend('off')
xlim([-.033 1.05])
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName','decellerating_nu')
return

function layeredflow()
plot_depthprofile(4,20,25,[],'Ene');
%title('$H=20,\theta=24^\circ$')
set(gca,'FontSize',20)
set(get(gca,'XLabel'),'FontSize',20)
set(get(gca,'YLabel'),'FontSize',20)
set(get(gca,'Title'),'FontSize',20)
legend('off')
%legend({'H=10','H=20','H=30'},'FontSize',10)
xlim([0,2000])
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName','oscillating_ene')
text(min(xlim)  ,max(ylim)*1.02,['$10^4$'],'HorizontalAlignment','right','FontSize',20)

plot_depthprofile(4,20,22,[],'Nu');
%title('$H=20,\theta=22^\circ$')
set(gca,'FontSize',20)
set(get(gca,'XLabel'),'FontSize',20)
set(get(gca,'YLabel'),'FontSize',20)
set(get(gca,'Title'),'FontSize',20)
legend('off')
xlim([-.1 1.1])
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName','layered_nu')
return