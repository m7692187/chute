function plot_PrincipalStressAngles(data,options)
if iscell(data) && exist('options','var') && strcmp(options,'local')
  %plots local PSA of a data array
  newFigure([data{1}.plotname 'LocalPrincipalStressAngles']);
  for i=1:length(data)
    x = data{i}.InertialParameter; xlabel('$\bar{I}$'); 
    FlowAnglexy = cellfun(@(d)d.FlowPrincipalStressAngle(1),data);
    FlowAnglexz = cellfun(@(d)d.FlowPrincipalStressAngle(2),data);
    FlowAngleyz = cellfun(@(d)d.FlowPrincipalStressAngle(3),data);
    plot(x(data{i}.Bulk),[...
      ...%data{i}.PrincipalStressAngle(data{i}.Bulk,1)';...
      data{i}.PrincipalStressAngle(data{i}.Bulk,2)'+45;...
      ...%data{i}.PrincipalStressAngle(data{i}.Bulk,3)';...
      ], ...
      '.','DisplayName',{'$\bar\alpha_{yz}$','$\bar\alpha_{xz}+45^\circ$','$\bar\alpha_{xy}$'},...
      'UserData',data{i});
  end
  zlabel('')
  %legend('show')
  set(legend,'Location','Best','Box','off')
elseif iscell(data)
  %plot global PSA of a data array
  newFigure([data{1}.plotname 'PrincipalStressAngles']);
  x = cellfun(@(d)d.axis,data)'; xlabel(data{1}.label); 
  y = cellfun(@(d)d.FlowInertialParameter,data)'; ylabel('$\bar{I}$'); 
  %y = cellfun(@(d)d.FixedParticleRadius*2,data)'; ylabel('$\lambda$'); 
  FlowAnglexy = cellfun(@(d)d.FlowPrincipalStressAngle(1),data);
  FlowAnglexz = cellfun(@(d)d.FlowPrincipalStressAngle(2),data);
  FlowAngleyz = cellfun(@(d)d.FlowPrincipalStressAngle(3),data);
  plot3(x,y,[FlowAnglexy;FlowAnglexz+45;FlowAngleyz], ...
    'x','DisplayName',{'$\bar\alpha_{yz}$','$\bar\alpha_{xz}+45^\circ$','$\bar\alpha_{xy}$'},...
    'UserData',data);
  zlabel('')
  %legend('show')
  set(legend,'Location','Best','Box','off')
  view(0,0)
  %view(90,0)
else
  %plots local PSA of a single data set
  newFigure([data.plotname 'PrincipalStressAngles']);
  plot(data.z,[data.PrincipalStressAngle(:,1)'; data.PrincipalStressAngle(:,2)'+45; data.PrincipalStressAngle(:,3)'],...
     'DisplayName',{'$\alpha_{yz}$','$\alpha_{xz}+45^\circ$','$\alpha_{xy}$'})
  plot([data.Base data.Surface],(data.FlowPrincipalStressAngle'+[0 45 0]')*[1 1],...
     'HandleVisibility','off');
  xlabel('$z/d$'); 
  xlim([data.Base data.Surface]);
  legend('show')
  set(legend,'Location','South')
end
dcm_obj = datacursormode(gcf);
set(dcm_obj,'UpdateFcn',@DataTip);
return
