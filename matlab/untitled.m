\mu & \beta_\lambda & \text{err}  \\\hline\hline
0.000000 & 0.242916 & -0.065337 \\\hline
0.000977 & 0.242706 & -0.059791 \\\hline
0.007812 & 0.240184 & -0.059757 \\\hline
0.015625 & 0.239064 & -0.038030 \\\hline
0.031250 & 0.235218 & -0.035391 \\\hline
0.062500 & 0.231776 & -0.001996 \\\hline
0.125000 & 0.227394 & 0.032142 \\\hline
0.250000 & 0.226030 & 0.080207 \\\hline
0.500000 & 0.226027 & 0.094220 \\\hline
1.000000 & 0.221988 & 0.080186 \\\hline
2.000000 & 0.224058 & 0.092484 \\\hline
\mu & \delta_{1,\lambda} & \delta_{2,\lambda} & A_\lambda & \text{err}  \\\hline\hline
0.000488 & 17.919563 & 23.137320 & 6.362345 & 
0.000977 & 17.904697 & 22.397619 & 9.331758 & 
0.007812 & 17.652881 & 22.637971 & 11.273533 & 
0.015625 & 17.969990 & 23.563471 & 8.011775 & 
0.031250 & 18.017941 & 24.902595 & 6.672234 & 
0.062500 & 17.630068 & 26.378213 & 7.307654 & 
0.125000 & 16.904961 & 27.986304 & 8.157327 & 
0.250000 & 17.435357 & 31.141567 & 4.913629 & 
0.500000 & 17.385532 & 32.823686 & 4.024448 & 
1.000000 & 17.364551 & 32.939208 & 4.060694 & 
2.000000 & 17.295512 & 32.659549 & 4.205792 & 
\lambda & \delta_{1,\lambda} & \delta_{2,\lambda} & A_\lambda & \text{err}  \\\hline\hline
0 & 11.750000 & 11.750000 & 5.000000 & 
1/6 & 14.750000 & 14.750000 & 5.000000 & 
1/3 & 16.343755 & 20.590853 & 23.000473 & 
1/2 & 17.897663 & 20.696535 & 16.970439 & 
2/3 & 17.767151 & 26.107136 & 5.691576 & 
5/6 & 18.223165 & 28.479188 & 4.410956 & 
1 & 17.561246 & 32.256504 & 3.836394 & 
1.5 & 17.539229 & 32.926345 & 3.684813 & 
2 & 17.447875 & 29.482927 & 5.455456 & 
4 & 17.345512 & 28.605072 & 6.629681 & 
\lambda & \beta_\lambda & \text{err}  \\\hline\hline
0 & 1.621359 & -3.663336 \\\hline
1/2 & 0.240784 & -0.889410 \\\hline
2/3 & 0.209821 & -0.239439 \\\hline
5/6 & 0.193698 & 0.002163 \\\hline
1 & 0.190586 & 0.045340 \\\hline
1.5 & 0.188201 & 0.036338 \\\hline
2 & 0.184734 & 0.033229 \\\hline
4 & 0.180331 & 0.015530 \\\hline


\mu & \delta_{1,\lambda} & \delta_{2,\lambda} & A_\lambda & \text{err}  \\\hline\hline
0.500000 & 17.385532 & 32.823686 & 4.024448 & 
\lambda & \delta_{1,\lambda} & \delta_{2,\lambda} & A_\lambda & \text{err}  \\\hline\hline
1 & 17.561246 & 32.256504 & 3.836394 & 

\mu & \beta_\lambda & \text{err}  \\\hline\hline
0.500000 & 0.226027 & 0.094220 \\\hline
\lambda & \beta_\lambda & \text{err}  \\\hline\hline
1 & 0.190586 & 0.045340 \\\hline
