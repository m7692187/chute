close all
addpath('~/code/MD/matlab/thomas')

%% Std disp
if ~exist('Stop','var')
  Stop=loadstatistics('../Hstop/L1M0.5B0.5/*.stat');
  Flow=loadstatistics('../Flow/L1M0.5B0.5/*.stat');
end

figure(1); clf; hold on
StopH=cellfun(@(d)d.FlowHeight,Stop);
StopA=cellfun(@(d)d.ChuteAngle,Stop);
[Astop,Hstop,CoeffHstop] = fitHstop(Stop);
plot(StopA,StopH,'x')
plot(Astop,Hstop,'-')
axis([18.4 28.1 0 40])

FlowH=cellfun(@(d)d.FlowHeight,Flow);
FlowA=cellfun(@(d)d.ChuteAngle,Flow);
FlowF=cellfun(@(d)d.Froude,Flow);
plot(FlowA,FlowH,'xr')

figure(2); clf
% plot(FlowH,FlowF,'xr')
% hold on
plot(FlowH./hstop(CoeffHstop,FlowA),FlowF ,'xb')
axis([0 25 0 6])

%% Plastic
if ~exist('StopP','var')
  StopP=loadstatistics('../../runPlastic/hstopS4/stats2/*.stat');
  FlowP=loadstatistics('../../runPlastic/L*/*.stat');
end

figure(3); clf; hold on
StopH=cellfun(@(d)d.FlowHeight,StopP);
StopA=cellfun(@(d)d.ChuteAngle,StopP);
[Astop,Hstop,CoeffHstop] = fitHstop(StopP);
plot(StopA,StopH,'x')
plot(Astop,Hstop,'-')
axis([18.4 28.1 0 40])


FlowH=cellfun(@(d)d.FlowHeight,FlowP);
FlowA=cellfun(@(d)d.ChuteAngle,FlowP);
FlowF=cellfun(@(d)d.Froude,FlowP);
plot(FlowA,FlowH,'xr')

figure(4); clf
% plot(FlowH,FlowF ,'xr')
% hold on
plot(FlowH./hstop(CoeffHstop,FlowA),FlowF ,'xb')
axis([0 25 0 6])

%% Hertzian
if ~exist('StopHe','var')
  StopHe=loadstatistics('../Hertz/stats/*.stat');
  FlowHe=loadstatistics('../../runNewCases/S39/stats/H*.stat');
end

figure(5); clf; hold on
StopH=cellfun(@(d)d.FlowHeight,StopHe);
StopA=cellfun(@(d)d.ChuteAngle,StopHe);
[Astop,Hstop,CoeffHstop] = fitHstop(StopHe);
plot(StopA,StopH,'x')
plot(Astop,Hstop,'-')
axis([18.4 28.1 0 40])


FlowH=cellfun(@(d)d.FlowHeight,FlowHe);
FlowA=cellfun(@(d)d.ChuteAngle,FlowHe);
FlowF=cellfun(@(d)d.Froude,FlowHe);
plot(FlowA,FlowH,'xr')

figure(6); clf
%plot(FlowH,FlowF ,'xr')
%hold on
ix=FlowA<26.5;
plot(FlowH(ix)./hstop(CoeffHstop,FlowA(ix)),FlowF(ix) ,'xb')
axis([0 25 0 6])

%% 
order_figures(2)