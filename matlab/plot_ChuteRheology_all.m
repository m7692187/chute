function plot_ChuteRheology_all(data)
%Density, Velocity, Strain, Temperature, normal stresses, contributions of sigmazz, contributions of sigmaxz, momentum conservation, Bagnold rheology,
field={...
  {'Nu','$\rho/\rho_p$'} ...
  {'VelocityX','$u/\sqrt{g d}$'} ...
  {'Temperature','$T$'} ...
  {'dzVelocityX','$\partial_z u/\sqrt{g/d}$'} ...
  {'CoordinationNumber','$C$'} ...
  };
for i=1:length(field)
  newFigure([data.plotname field{i}{1}]);
  plot(data.zhat,data.(field{i}{1}),'k-')
  xlabel('$z/h$'); ylabel(field{i}{2}); xlim([0 1])
end

%plots NormalStresses
newFigure([data.plotname 'NormalStresses']);
plot(data.zhat,[data.StressXX data.StressYY data.StressZZ -data.StressXZ],...
  'DisplayName',{'$\sigma_{xx}$','$\sigma_{yy}$','$\sigma_{zz}$','$-\sigma_{xz}$'});
xlabel('$z/h$'); xlim([0 1]); legend('show')

%plots NormalStresses
newFigure([data.plotname 'PrincipalStresses']);
plot(data.zhat,data.PrincipalStressLambda',...
  'DisplayName',{'$\sigma_1$','$\sigma_2$','$\sigma_3$'});
xlabel('$z/h$'); xlim([0 1]); legend('show')

%plots contributions to downward stress
newFigure([data.plotname 'DownwardStressContributions']);
plot(data.z,[data.TangentialStressZZ./data.StressZZ data.KineticStressZZ./data.StressZZ],...
  'DisplayName',{'$\sigma_{zz}^{c,t}/\sigma_{zz}$', '$\sigma_{zz}^k/\sigma_{zz}$'});
xlabel('$z/h$'); xlim([0 1]); legend('show')

return


function StdMomentumBalance2 (data)
%plots deviations from the momentum balance for downward traction of standard case
newFigure('StdMomentumBalance2');

WeightX = integrate(data.Gravity(1)*data.Density,diff(data.z(1:2)),length(data.z));
WeightZ = integrate(data.Gravity(3)*data.Density,diff(data.z(1:2)),length(data.z));
plot(data.z,[WeightX-data.StressXZ WeightZ-data.StressZZ],...
  'DisplayName',{'$\intrho g \sin\theta-\sigma_{xz}$','$\intrho g \cos\theta-\sigma_{zz}$'});
xlabel('$z/d$'); 
axis tight; xlim([data.Base data.Surface]);
legend('show')
set(legend,'Location','Best')
return

function StdMomentumBalance (data)
%plots deviations from the momentum balance for downward traction of standard case
newFigure('StdMomentumBalance');

plot(data.z,[data.Gravity(1)*data.Density - gradient(data.StressXZ,diff(data.z(1:2))) ...
  data.Gravity(3)*data.Density - gradient(data.StressZZ,diff(data.z(1:2))) ...
  ],'DisplayName',{'$\rho g\sin\theta-\partial_z\sigma_{xz}$','$\rho g\cos\theta-\partial_z\sigma_{zz}$'});
xlabel('$z/d$'); 
axis tight; xlim([data.Base data.Surface]);
legend('show')
set(legend,'Location','Best','Box','off')
return

function StdDownwardStressContributions (data)
%plots contributions to downward stress of standard case
newFigure('StdDownwardStressContributions');

plot(data.z,[data.TangentialStressZZ./data.StressZZ data.KineticStressZZ./data.StressZZ],...
  'DisplayName',{'$\sigma_{zz}^{c,t}/\sigma_{zz}$', '$\sigma_{zz}^k/\sigma_{zz}$'});
%plot in terms of deviatoric stress
%TangentialPressure=(data.TangentialStressXX+data.TangentialStressYY+data.TangentialStressZZ)/3;
%KineticPressure=(data.KineticStressXX+data.KineticStressYY+data.KineticStressZZ)/3;
%plot(data.z,[(data.TangentialStressZZ-TangentialPressure)./(data.StressZZ-data.Pressure) (data.KineticStressZZ-KineticPressure)./(data.StressZZ-data.Pressure)],...
%  'DisplayName',{'$\sigma_{zz}^{c,t,dev}/\sigma_{zz}^{dev}$', '$\sigma_{zz}^{k,dev}/\sigma_{zz}^{dev}$'});
xlabel('$z/d$'); 
axis tight; xlim([data.Base data.Surface]); ylim([0 0.03])
legend('show')
set(legend,'Location','Best')
return

function StdStressAnisotropy (data)
%plots contributions to downward stress of standard case
newFigure('StdStressAnisotropy');

for i=1:length(data.z)
   S=[
     data.StressXX(i) ...
     data.StressXY(i) ...
     data.StressXZ(i); ...
     data.StressYX(i) ...
     data.StressYY(i) ...
     data.StressYZ(i); ...
     data.StressZX(i) ...
     data.StressZY(i) ...
     data.StressZZ(i)];
   J2(i)=sqrt(( (S(1,1)-S(2,2))^2 + (S(2,2)-S(3,3))^2 + ...
     (S(3,3)-S(1,1))^2 + 6*( S(1,2)^2 + S(1,3)^2 +S(2,3)^2 ) )/2);
   k2(i)=J2(i)/(sqrt(6)*sum(diag(S))/3);
end
plot(data.z,k2,'DisplayName',{'$S_D^\star=J_2/(\sqrt{6}P)$'});
plot(data.z,-data.StressXZ./data.StressZZ,'r','DisplayName',{'$\mu$'});
axis tight; xlim([data.Base data.Surface]); ylim([0 0.5])
legend('show')
set(legend,'Location','South')
return


function StdStressSymmetry (data)
%plots contributions to downward stress of standard case
newFigure('StdStressSymmetry');

% for i=1:length(data.z)
%    StressDev=[...
%     data.StressXX(i) data.StressXY(i) data.StressXZ(i);...
%     data.StressYX(i) data.StressYY(i) data.StressYZ(i);...
%     data.StressZX(i) data.StressZY(i) data.StressZZ(i);...
%     ] - diag([1 1 1]) * data.Pressure(i);
%    normStress(i) = norm(StressDev,'inf');
%    asymStress(i) = norm(StressDev-StressDev','inf')/2;
%    Stress=[...
%     data.StressXX(i) data.StressXY(i) data.StressXZ(i);...
%     data.StressYX(i) data.StressYY(i) data.StressYZ(i);...
%     data.StressZX(i) data.StressZY(i) data.StressZZ(i);...
%     ];
%    TangentialStress=[...
%     data.TangentialStressXX(i) data.TangentialStressXY(i) data.TangentialStressXZ(i);...
%     data.TangentialStressYX(i) data.TangentialStressYY(i) data.TangentialStressYZ(i);...
%     data.TangentialStressZX(i) data.TangentialStressZY(i) data.TangentialStressZZ(i);...
%     ];
%     TangStressRatio(i)=norm(TangentialStress)/norm(Stress);
% end
% plot(data.z,asymStress./normStress,...
%   'DisplayName',{'$|\sigma_{asym}|/|\sigma|$'});
plot(data.z,abs(data.StressZX-data.StressXZ)./abs(data.StressZX)/2,'r',...
  'DisplayName',{'$|\sigma_{xz}^{asym}|/|\sigma_{xz}|$'});
%plot(data.z,TangStressRatio,'r',...
%  'DisplayName',{'$|\sigma^{asym}|/|\sigma_{sym}|$'});
xlabel('$z/d$'); 
axis tight; xlim([data.Base data.Surface]); ylim([0 0.01])
legend('show')
set(legend,'Location','Best')
return

function StdStrainVsStress (data)
%plots strain vs stress of standard case
newFigure('StdStrain');

plot(-data.StressXZ,data.dzVelocityX)
ylabel('$\partial_z v_x/\sqrt{g/d}$'); xlabel('$-\sigma_{xz}$')
axis tight; ylim([0 max(ylim)])
return

function StdInertial (data)
%plots Inertial number of standard case
newFigure('StdInertial');

plot(data.z,data.InertialParameter)
plot(data.z(data.Bulk),data.InertialParameter(data.Bulk),'LineWidth',2)
xlabel('$z/d$'); ylabel('$I$')
xlim([data.Base data.Surface]); ylim([0 1])
return

function StdPrincipalStresses (data)
newFigure('StdPrincipalStressesRemco');

plot(data.z,[data.PrincipalStressLambda(:,1) data.PrincipalStressLambda(:,2) data.PrincipalStressLambda(:,3)],...
  'DisplayName',{'$\lambda_1$','$\lambda_2$','$\lambda_3$'})
% plot([data.Base data.Surface],(data.FlowPrincipalStressLambda)*[1 1],...
%   'HandleVisibility','off');
xlabel('$z/d$'); 
xlim([data.Base data.Surface]);
legend('show')
set(legend,'Location','SouthEast')
return

function StdPrincipalStressAngles (data)
newFigure('StdPrincipalAnglesRemco');

plot(data.z,[data.PrincipalStressAngle(:,1)'; data.PrincipalStressAngle(:,2)'+45; data.PrincipalStressAngle(:,3)'],...
   'DisplayName',{'$\alpha_{yz}$','$\alpha_{xz}+45^\circ$','$\alpha_{xy}$'})
plot([data.Base data.Surface],(data.FlowPrincipalStressAngle'+[0 45 0]')*[1 1],...
   'HandleVisibility','off');
xlabel('$z/d$'); 
xlim([data.Base data.Surface]);
legend('show')
set(legend,'Location','South')
return

function StdPrincipalFabric (data)
newFigure('StdPrincipalFabric');

plot(data.z,[data.PrincipalFabricLambda(:,1) data.PrincipalFabricLambda(:,2) data.PrincipalFabricLambda(:,3)],...
  'DisplayName',{'$\lambda_1$','$\lambda_2$','$\lambda_3$'})
% plot([data.Base data.Surface],(data.FlowPrincipalFabricLambda)*[1 1],...
%   'HandleVisibility','off');
xlabel('$z/d$'); 
xlim([data.Base data.Surface]);
legend('show')
set(legend,'Location','SouthEast')
return

function StdPrincipalFabricAngles (data)
newFigure('StdPrincipalFabricAngles');

plot(data.z,[data.PrincipalFabricAngle(:,1)'; data.PrincipalFabricAngle(:,2)'+45; data.PrincipalFabricAngle(:,3)'],...
   'DisplayName',{'$\alpha_{yz}$','$\alpha_{xz}+45^\circ$','$\alpha_{xy}$'})
plot([data.Base data.Surface],(data.FlowPrincipalFabricAngle'+[0 45 0]')*[1 1],...
   'HandleVisibility','off');
xlabel('$z/d$'); 
xlim([data.Base data.Surface]);
legend('show')
set(legend,'Location','South')
return

function StdPrincipalKineticStresses (data)
newFigure('StdPrincipalKineticStresses');

plot(data.z,[data.PrincipalKineticStressLambda(:,1) data.PrincipalKineticStressLambda(:,2) data.PrincipalKineticStressLambda(:,3)],...
  'DisplayName',{'$\lambda_1$','$\lambda_2$','$\lambda_3$'})
% plot([data.Base data.Surface],(data.FlowPrincipalKineticStressLambda)*[1 1],...
%   'HandleVisibility','off');
xlabel('$z/d$'); 
xlim([data.Base data.Surface]);
legend('show')
set(legend,'Location','SouthEast')
return

function StdPrincipalKineticStressAngles (data)
newFigure('StdPrincipalKineticStressAngles');

plot(data.z,[data.PrincipalKineticStressAngle(:,1)'; data.PrincipalKineticStressAngle(:,2)'+45; data.PrincipalKineticStressAngle(:,3)'],...
   'DisplayName',{'$\alpha_{yz}$','$\alpha_{xz}+45^\circ$','$\alpha_{xy}$'})
plot([data.Base data.Surface],(data.FlowPrincipalKineticStressAngle'+[0 45 0]')*[1 1],...
   'HandleVisibility','off');
xlabel('$z/d$'); 
xlim([data.Base data.Surface]);
legend('show')
set(legend,'Location','South')
return

