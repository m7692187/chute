function ChuteRheologyStd
close all
loadStandardData;
% StdPrincipalStresses
% StdPrincipalStressAngles
% StdPrincipalFabric
% StdPrincipalFabricAngles
% StdPrincipalKineticStresses
% StdPrincipalKineticStressAngles

%figure 1: snapshot of the system

%figure 2a: density as a function of w (coarse-grained and smooth)
%figure 2b: density as a function of t
%ChuteRheologyW;

%figure 3: density, velocity, strain, friction, temperature profiles for 
%standard case
StdDensity
StdVelocity
StdStrain
StdFriction
StdTemperature
StdCoordinationNumber

%%figure 4: normal stress profiles, standard case
StdNormalStresses
StdKineticNormalStresses
StdTangentialNormalStresses

%%figure 5: downward normal stress contributions vs gravity, profiles for standard case
%StdMomentumBalance2
StdMomentumBalance

%%figure 6a: downward normal stress contributions, profiles for standard case
%%figure 6b: crossslope and downslope normal stress contributions, profiles for standard case
%%figure 6c: shear stress contributions and gravity, profiles for standard case
StdDownwardStressContributions
StdStressAnisotropy
StdStressSymmetry


%%figure 7a strain vs. shear stress profiles , standard case
%%figure 7b viscosity and fit, standard case
StdStrainVsStress
StdInertial

%%figure 8a: principle stresses, profiles for standard case
%%figure 8b: angles of principle stresses, profiles for standard case
% StdPrincipalStresses
% StdPrincipalAngles
StdPrincipalStressesRemco
StdPrincipalAnglesRemco
%for i=get(0,'Children')'; set(i,'WindowStyle','docked'); end
%print_figures2();
return

function loadStandardData
%loads the standard case
global std

if (true)
  %when you want to quickly load
  if ~isempty(std)
    save('ChuteRheologyStd.mat','std');
  else
    load('ChuteRheologyStd.mat')
    return
  end
end

%std=loadstatistics('../w/H20A24L1M0.5B0.5.T500W0_25.stat');
std=loadstatistics('../../run7/Lucystats/H30A28L1M0.5B0.5.T200W0_5Lucy.stat');
%std=loadstatistics('../w/H30A24L1M0.5B0.5.T200W0_25.stat');
%std=loadstatistics('../w/H30A24L1M0.5B0.5.T100W0_05.stat');
%std=loadstatistics('../w/H30A24L1M0.5B0.5.T100W1.stat');
%std=principalstresses(std);
dzMomentumX = gradient(std.MomentumX,diff(std.z(1:2)));
dzDensity = gradient(std.Density,diff(std.z(1:2)));
std.dzVelocityX = (dzMomentumX.*std.Density - std.MomentumX.*dzDensity)./(std.Density).^2;
std=EigenvalueAnalysis2(std);
%define \hat{z}\in[0,1]
std.zhat=(std.z-std.Base)/std.FlowHeight;
%define bulk inertial parameter as the median value in the flow
std.InertialParameterBulk = median(std.InertialParameter(abs(std.zhat-0.5)<0.5)); 
%define bulk index as the region where the inertial parameter is about the bulk
%inertial parameter
std.Bulk = abs(std.InertialParameter/std.InertialParameterBulk-1)<0.1;
std.ix = std.z<std.Surface&std.z>std.Base; 
return

function F=integrate(f,h,root)
%opens new figure and sets defaults
F=cumsum(f)*h-sum(f(1:root))*h;
%F=(F([1 1:end-1])+F([2:end end]))/2;
return


function StdDensity
%plots density of standard case
newFigure('StdDensity');
global std
plot(std.z,std.Nu)
xlabel('$z/d$'); ylabel('$\rho/\rho_p$')
xlim([std.Base std.Surface]); ylim([0 0.6])
return

function StdVelocity
%plots velocity of standard case
newFigure('StdVelocity');
global std
plot(std.z,std.VelocityX)
xlabel('$z/d$'); ylabel('$u/\sqrt{g d}$')
xlim([std.Base std.Surface]); 
return

function StdStrain
%plots strain of standard case
newFigure('StdStrain');
global std
plot(std.z,std.dzVelocityX)
xlabel('$z/d$'); ylabel('$\partial_z u/\sqrt{g/d}$')
xlim([std.Base std.Surface]);
return

%todo: add traction when traction is computed
function StdFriction
%plots friction of standard case
newFigure('StdFriction');
global std
plot(std.z,-std.StressXZ./std.StressZZ+std.Gravity(1)/std.Gravity(3));
xlabel('$z/d$'); ylabel('$\mu-\tan^{-1}\theta$')
xlim([std.Base std.Surface]);
return

function StdTemperature
%plots temperature of standard case
newFigure('StdTemperature');
global std
plot(std.z,std.Temperature);
xlabel('$z/d$'); ylabel('$T$')
xlim([std.Base std.Surface]);
return

function StdCoordinationNumber
%plots coordination number of standard case
newFigure('StdCoordinationNumber');
global std
plot(std.z,std.CoordinationNumber);
%plot(std.z,[std.CoordinationNumber 3*std.FabricXX./std.Nu 3*std.FabricYY./std.Nu 3*std.FabricZZ./std.Nu]);
xlabel('$z/d$'); ylabel('$C$')
xlim([std.Base std.Surface]);
return

function StdNormalStresses
%plots temperature of standard case
newFigure('StdNormalStresses');
global std
plot(std.z,[std.StressXX std.StressYY std.StressZZ -std.StressXZ],...
  'DisplayName',{'$\sigma_{xx}$','$\sigma_{yy}$','$\sigma_{zz}$','$-\sigma_{xz}$'});
plot(std.Base*[1 1],ylim,':','HandleVisibility','off')
plot(std.Surface*[1 1],ylim,':','HandleVisibility','off')
xlabel('$z/d$'); 
xlim([std.Base std.Surface]+.02*std.FlowHeight*[-2 1]);
legend('show')
return

function StdKineticNormalStresses
%plots kinetic stress components of standard case
newFigure('StdKineticStress');
global std
plot(std.z,[std.KineticStressXX std.KineticStressYY std.KineticStressZZ std.KineticStressXZ],...
  'DisplayName',{'$\sigma_{xx}^k$','$\sigma_{yy}^k$','$\sigma_{zz}^k$','$\sigma_{xz}^k$'});
xlim([std.Base std.Surface]);
legend('show')
return

function StdTangentialNormalStresses
%plots kinetic stress components of standard case
newFigure('StdTangentialStress');
global std
plot(std.z,[std.TangentialStressXX std.TangentialStressYY std.TangentialStressZZ std.TangentialStressXZ std.TangentialStressZX],...
  'DisplayName',{'$\sigma_{xx}^{c,t}$','$\sigma_{yy}^{c,t}$','$\sigma_{zz}^{c,t}$','$\sigma_{xz}^{c,t}$','$\sigma_{zx}^{c,t}$'});
xlim([std.Base std.Surface]);
legend('show')
return

function StdMomentumBalance2
%plots deviations from the momentum balance for downward traction of standard case
newFigure('StdMomentumBalance2');
global std
WeightX = integrate(std.Gravity(1)*std.Density,diff(std.z(1:2)),length(std.z));
WeightZ = integrate(std.Gravity(3)*std.Density,diff(std.z(1:2)),length(std.z));
plot(std.z,[WeightX-std.StressXZ WeightZ-std.StressZZ],...
  'DisplayName',{'$\intrho g \sin\theta-\sigma_{xz}$','$\intrho g \cos\theta-\sigma_{zz}$'});
xlabel('$z/d$'); 
axis tight; xlim([std.Base std.Surface]);
legend('show')
set(legend,'Location','Best')
return

function StdMomentumBalance
%plots deviations from the momentum balance for downward traction of standard case
newFigure('StdMomentumBalance');
global std
plot(std.z,[std.Gravity(1)*std.Density - gradient(std.StressXZ,diff(std.z(1:2))) ...
  std.Gravity(3)*std.Density - gradient(std.StressZZ,diff(std.z(1:2))) ...
  ],'DisplayName',{'$\rho g\sin\theta-\partial_z\sigma_{xz}$','$\rho g\cos\theta-\partial_z\sigma_{zz}$'});
xlabel('$z/d$'); 
axis tight; xlim([std.Base std.Surface]);
legend('show')
set(legend,'Location','Best','Box','off')
return

function StdDownwardStressContributions
%plots contributions to downward stress of standard case
newFigure('StdDownwardStressContributions');
global std
plot(std.z,[std.TangentialStressZZ./std.StressZZ std.KineticStressZZ./std.StressZZ],...
  'DisplayName',{'$\sigma_{zz}^{c,t}/\sigma_{zz}$', '$\sigma_{zz}^k/\sigma_{zz}$'});
%plot in terms of deviatoric stress
%TangentialPressure=(std.TangentialStressXX+std.TangentialStressYY+std.TangentialStressZZ)/3;
%KineticPressure=(std.KineticStressXX+std.KineticStressYY+std.KineticStressZZ)/3;
%plot(std.z,[(std.TangentialStressZZ-TangentialPressure)./(std.StressZZ-std.Pressure) (std.KineticStressZZ-KineticPressure)./(std.StressZZ-std.Pressure)],...
%  'DisplayName',{'$\sigma_{zz}^{c,t,dev}/\sigma_{zz}^{dev}$', '$\sigma_{zz}^{k,dev}/\sigma_{zz}^{dev}$'});
xlabel('$z/d$'); 
axis tight; xlim([std.Base std.Surface]); ylim([0 0.03])
legend('show')
set(legend,'Location','Best')
return

function StdStressAnisotropy
%plots contributions to downward stress of standard case
newFigure('StdStressAnisotropy');
global std
for i=1:length(std.z)
   S=[
     std.StressXX(i) ...
     std.StressXY(i) ...
     std.StressXZ(i); ...
     std.StressYX(i) ...
     std.StressYY(i) ...
     std.StressYZ(i); ...
     std.StressZX(i) ...
     std.StressZY(i) ...
     std.StressZZ(i)];
   J2(i)=sqrt(( (S(1,1)-S(2,2))^2 + (S(2,2)-S(3,3))^2 + ...
     (S(3,3)-S(1,1))^2 + 6*( S(1,2)^2 + S(1,3)^2 +S(2,3)^2 ) )/2);
   k2(i)=J2(i)/(sqrt(6)*sum(diag(S))/3);
end
plot(std.z,k2,'DisplayName',{'$S_D^\star=J_2/(\sqrt{6}P)$'});
plot(std.z,-std.StressXZ./std.StressZZ,'r','DisplayName',{'$\mu$'});
axis tight; xlim([std.Base std.Surface]); ylim([0 0.5])
legend('show')
set(legend,'Location','South')
return


function StdStressSymmetry
%plots contributions to downward stress of standard case
newFigure('StdStressSymmetry');
global std
% for i=1:length(std.z)
%    StressDev=[...
%     std.StressXX(i) std.StressXY(i) std.StressXZ(i);...
%     std.StressYX(i) std.StressYY(i) std.StressYZ(i);...
%     std.StressZX(i) std.StressZY(i) std.StressZZ(i);...
%     ] - diag([1 1 1]) * std.Pressure(i);
%    normStress(i) = norm(StressDev,'inf');
%    asymStress(i) = norm(StressDev-StressDev','inf')/2;
%    Stress=[...
%     std.StressXX(i) std.StressXY(i) std.StressXZ(i);...
%     std.StressYX(i) std.StressYY(i) std.StressYZ(i);...
%     std.StressZX(i) std.StressZY(i) std.StressZZ(i);...
%     ];
%    TangentialStress=[...
%     std.TangentialStressXX(i) std.TangentialStressXY(i) std.TangentialStressXZ(i);...
%     std.TangentialStressYX(i) std.TangentialStressYY(i) std.TangentialStressYZ(i);...
%     std.TangentialStressZX(i) std.TangentialStressZY(i) std.TangentialStressZZ(i);...
%     ];
%     TangStressRatio(i)=norm(TangentialStress)/norm(Stress);
% end
% plot(std.z,asymStress./normStress,...
%   'DisplayName',{'$|\sigma_{asym}|/|\sigma|$'});
plot(std.z,abs(std.StressZX-std.StressXZ)./abs(std.StressZX)/2,'r',...
  'DisplayName',{'$|\sigma_{xz}^{asym}|/|\sigma_{xz}|$'});
%plot(std.z,TangStressRatio,'r',...
%  'DisplayName',{'$|\sigma^{asym}|/|\sigma_{sym}|$'});
xlabel('$z/d$'); 
axis tight; xlim([std.Base std.Surface]); ylim([0 0.01])
legend('show')
set(legend,'Location','Best')
return

function StdStrainVsStress
%plots strain vs stress of standard case
newFigure('StdStrain');
global std
plot(-std.StressXZ,std.dzVelocityX)
ylabel('$\partial_z v_x/\sqrt{g/d}$'); xlabel('$-\sigma_{xz}$')
axis tight; ylim([0 max(ylim)])
return

function StdInertial
%plots Inertial number of standard case
newFigure('StdInertial');
global std
plot(std.z,std.InertialParameter)
plot(std.z(std.Bulk),std.InertialParameter(std.Bulk),'LineWidth',2)
xlabel('$z/d$'); ylabel('$I$')
xlim([std.Base std.Surface]); ylim([0 1])
return

function StdPrincipalStresses
newFigure('StdPrincipalStressesRemco');
global std
plot(std.z,[std.PrincipalStressLambda(:,1) std.PrincipalStressLambda(:,2) std.PrincipalStressLambda(:,3)],...
  'DisplayName',{'$\lambda_1$','$\lambda_2$','$\lambda_3$'})
% plot([std.Base std.Surface],(std.FlowPrincipalStressLambda)*[1 1],...
%   'HandleVisibility','off');
xlabel('$z/d$'); 
xlim([std.Base std.Surface]);
legend('show')
set(legend,'Location','SouthEast')
return

function StdPrincipalStressAngles
newFigure('StdPrincipalAnglesRemco');
global std
plot(std.z,[std.PrincipalStressAngle(:,1)'; std.PrincipalStressAngle(:,2)'+45; std.PrincipalStressAngle(:,3)'],...
   'DisplayName',{'$\alpha_{yz}$','$\alpha_{xz}+45^\circ$','$\alpha_{xy}$'})
plot([std.Base std.Surface],(std.FlowPrincipalStressAngle'+[0 45 0]')*[1 1],...
   'HandleVisibility','off');
xlabel('$z/d$'); 
xlim([std.Base std.Surface]);
legend('show')
set(legend,'Location','South')
return

function StdPrincipalFabric
newFigure('StdPrincipalFabric');
global std
plot(std.z,[std.PrincipalFabricLambda(:,1) std.PrincipalFabricLambda(:,2) std.PrincipalFabricLambda(:,3)],...
  'DisplayName',{'$\lambda_1$','$\lambda_2$','$\lambda_3$'})
% plot([std.Base std.Surface],(std.FlowPrincipalFabricLambda)*[1 1],...
%   'HandleVisibility','off');
xlabel('$z/d$'); 
xlim([std.Base std.Surface]);
legend('show')
set(legend,'Location','SouthEast')
return

function StdPrincipalFabricAngles
newFigure('StdPrincipalFabricAngles');
global std
plot(std.z,[std.PrincipalFabricAngle(:,1)'; std.PrincipalFabricAngle(:,2)'+45; std.PrincipalFabricAngle(:,3)'],...
   'DisplayName',{'$\alpha_{yz}$','$\alpha_{xz}+45^\circ$','$\alpha_{xy}$'})
plot([std.Base std.Surface],(std.FlowPrincipalFabricAngle'+[0 45 0]')*[1 1],...
   'HandleVisibility','off');
xlabel('$z/d$'); 
xlim([std.Base std.Surface]);
legend('show')
set(legend,'Location','South')
return

function StdPrincipalKineticStresses
newFigure('StdPrincipalKineticStresses');
global std
plot(std.z,[std.PrincipalKineticStressLambda(:,1) std.PrincipalKineticStressLambda(:,2) std.PrincipalKineticStressLambda(:,3)],...
  'DisplayName',{'$\lambda_1$','$\lambda_2$','$\lambda_3$'})
% plot([std.Base std.Surface],(std.FlowPrincipalKineticStressLambda)*[1 1],...
%   'HandleVisibility','off');
xlabel('$z/d$'); 
xlim([std.Base std.Surface]);
legend('show')
set(legend,'Location','SouthEast')
return

function StdPrincipalKineticStressAngles
newFigure('StdPrincipalKineticStressAngles');
global std
plot(std.z,[std.PrincipalKineticStressAngle(:,1)'; std.PrincipalKineticStressAngle(:,2)'+45; std.PrincipalKineticStressAngle(:,3)'],...
   'DisplayName',{'$\alpha_{yz}$','$\alpha_{xz}+45^\circ$','$\alpha_{xy}$'})
plot([std.Base std.Surface],(std.FlowPrincipalKineticStressAngle'+[0 45 0]')*[1 1],...
   'HandleVisibility','off');
xlabel('$z/d$'); 
xlim([std.Base std.Surface]);
legend('show')
set(legend,'Location','South')
return

