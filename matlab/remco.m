close all
%clear all
%clc

printfig = 'yes';

data = loadstatistics('~/DRIVERS/FlowRulePaper/run7/wT3/H20A26L1M0.5B0.5.T128W0_05.stat');
data.x = data.z;
size = data.FlowHeight;

line = ['-xk';'-*r';'-db';'-+m'];


h = 2; % Central difference width
N = length(data.x); % Number of bins

%% fitting in bulk region
bulk = find(abs(data.x)<(size/2-3.5));
fluid = find(abs(data.x)<(size/2-0.3));


gamma_t = zeros(N,1);
for i=h+1:N-h
    gamma_t(i) = (data.VelocityZ(i+h)-data.VelocityZ(i-h))/(data.x(i+h)-data.x(i-h));
end


eta = -data.StressXZ./gamma_t;

% IMC method (mesoscopic) for shear stress
bodyforce = min(data.Gravity);
IMCxz = bodyforce*cumsum(data.Density*(data.x(2)-data.x(1)));
IMCxz = IMCxz + max(abs(IMCxz))/2;


% velocity and shear rate
fit = polyfit(data.x(bulk),data.VelocityZ(bulk),2);
fitvel = fit(1)*data.x.^2 + fit(2)*data.x + fit(3);
fitgam = 2*fit(1)*data.x + fit(2);
fiteta = -IMCxz./fitgam;

% velocity and shear rate based on discplacement averaging method
fitDis = polyfit(data.x(bulk),data.DisplacementMomentumZ(bulk)./data.Density(bulk),2);
fitvelDis = fitDis(1)*data.x.^2 + fitDis(2)*data.x + fitDis(3);
fitgamDis = 2*fitDis(1)*data.x + fitDis(2);


PStress = zeros(3,3,N);
PS1 = zeros(1,N);
PS2 = zeros(1,N);
PS3 = zeros(1,N);
ST = zeros(3,3,N);


D1 = [1 0 0; 0 0 0; 0 0 -1];
D2 = [0 0 0; 0 1 0; 0 0 -1];

%% Eigenvalue analysis

for i=1:N
    A = [data.StressXX(i)-data.Pressure(i) data.StressXY(i) data.StressXZ(i);
        data.StressXY(i) data.StressYY(i)-data.Pressure(i) data.StressYZ(i);
        data.StressXZ(i) data.StressYZ(i) data.StressZZ(i)-data.Pressure(i)];
    
    
    dummy = isnan(A);
    A(dummy)=0;
    
    [vec,l] = eig(A);
    
    
    % Eigenvector corresponding to maximum eigenvalue
    if max([l(1,1),l(2,2),l(3,3)])==l(1,1)
        vmax = vec(:,1);
        maxeig = 1;
    elseif max([l(1,1),l(2,2),l(3,3)])==l(1,1)
        vmax = vec(:,2);
        maxeig = 2;
    else
        vmax = vec(:,3);
        maxeig = 3;
    end
    
    angleyz(i) = atand(vmax(2)/vmax(3)); % divide y and z component of vector
    anglexz(i) = atand(vmax(1)/vmax(3)); % divide x and z component of vector
    anglexy(i) = atand(vmax(2)/vmax(1)); % divide y and x component of vector
    
    
    T = [cosd(2*anglexz(i)) 0 sind(2*anglexz(i));
        0 0 0;
        sind(2*anglexz(i)) 0 -cosd(2*anglexz(i))];
    
    R = [cosd(-anglexz(i)) 0 -sind(-anglexz(i));
        0 1 0;
        sind(-anglexz(i)) 0 cosd(-anglexz(i))];
    
    
    % transform to principal stress
    PStress(:,:,i)=R'*A*R;
    PS1(i) = PStress(1,1,i);
    PS2(i) = PStress(2,2,i);
    PS3(i) = PStress(3,3,i);
    
    PST = [PS1(i) 0 0; 0 PS2(i) 0; 0 0 PS3(i)];
    
    % back transform to deviatoric stress tensor
    ST(:,:,i)=R'*PST*R;
    
    % rotated stress tensor
    %lambda(i,1)=PS3(i);
    %lambda(i,2)=PS2(i);
    %lambda(i,3)=PS1(i);
    
    lambdaUS(i,:) = sum(l,1)';
    lambda(i,1) = lambdaUS(i,3);
    lambda(i,2) = lambdaUS(i,2);
    lambda(i,3) = lambdaUS(i,1);
    
    % NOTE:
    % something goes wrong here..... use PS1..3 and ST defined above
    ConstStress(:,:,i) = (lambda(i,1)*R'*D1*R + lambda(i,2)*R'*D2*R);
    ConstStress2(:,:,i) = (R*A*R');
    
end


sigD = abs(sqrt((lambda(:,1)-lambda(:,2)).^2+(lambda(:,2)-lambda(:,3)).^2+(lambda(:,3)-lambda(:,1)).^2))/sqrt(6);
gamDisp = (data.DisplacementMomentumZ_dx.*data.Density-data.DisplacementMomentumZ.*data.Density_dx)./data.Density.^2;
viscD = sigD./abs(gamDisp);


etaDisp = -data.StressXZ./gamDisp;

fit3 = polyfit(data.StressXZ(bulk),gamma_t(bulk),1);
fitline3 = linspace(-0.5,0.5,50);
stressstrainrate = fitline3/fit3(1) + fit3(2);


Sd2 = sqrt((lambda(:,1)-lambda(:,2)).^2+(lambda(:,2)-lambda(:,3)).^2+(lambda(:,3)-lambda(:,1)).^2)./sqrt(6)./data.Pressure;
Sd =(lambda(:,1)-lambda(:,3))/2./data.Pressure;


dsigmaxxdx = zeros(N,1);
dsigmaxzdx = zeros(N,1);
for i=h+1:N-h
    dsigmaxxdx(i) = (data.StressXX(i+h)-data.StressXX(i-h))/(data.x(i+h)-data.x(i-h));
    dsigmaxzdx(i) = (data.StressXZ(i+h)-data.StressXZ(i-h))/(data.x(i+h)-data.x(i-h));
end



%% plotting

figure(1)
hold on
set(gca,'FontSize',20)
plot(data.x,data.StressXX,line(1,:),'Linewidth',1)
plot(data.x,data.StressYY,line(2,:),'Linewidth',1)
plot(data.x,data.StressZZ,line(3,:),'Linewidth',1)
plot(data.x,data.Pressure,'g','Linewidth',2)
plot(-size/2+0.01,1.6025,'x','Linewidth',2,'MarkerSize',18)
plot(size/2-0.01,1.5871,'x','Linewidth',2,'MarkerSize',18)
ylim([-0.5 2.5])
xlim([-size/2 size/2])
hLegend = legend('xx','yy','zz','p','$F_x/A$','Location','South');
set(hLegend,'Interpreter','latex', 'FontSize', 18, 'FontName','AvantGarde');
legend boxoff
hTitle  = title (' ');
hXLabel = xlabel('x');
hYLabel = ylabel('$\sigma_{ii}$','Interpreter','latex');
%cosmet(hTitle,hXLabel,hYLabel)
set(gca,'xtick',[-4 -2 0 2 4])

figure(2)
hold on
set(gca,'FontSize',20)
plot(data.x,data.KineticStressXX,line(1,:),'Linewidth',1)
plot(data.x,data.KineticStressYY,line(2,:),'Linewidth',1)
plot(data.x,data.KineticStressZZ,line(3,:),'Linewidth',1)
ylim([0 1.7])
xlim([-size/2 size/2])
hLegend = legend('xx','yy','zz','Location','South');
set(hLegend,'Interpreter','latex', 'FontSize', 18, 'FontName','AvantGarde');
legend boxoff
hTitle  = title (' ');
hXLabel = xlabel('x');
hYLabel = ylabel('$\sigma^K_{ii}$','Interpreter','latex');
%cosmet(hTitle,hXLabel,hYLabel)
set(gca,'xtick',[-4 -2 0 2 4])

figure(3)
hold on
set(gca,'FontSize',20)
plot(data.x,data.NormalStressXX,line(1,:),'Linewidth',1)
plot(data.x,data.NormalStressYY,line(2,:),'Linewidth',1)
plot(data.x,data.NormalStressZZ,line(3,:),'Linewidth',1)
ylim([-1 1.7])
xlim([-size/2 size/2])
hLegend = legend('xx','yy','zz','Location','South');
set(hLegend,'Interpreter','latex', 'FontSize', 18, 'FontName','AvantGarde');
legend boxoff
hTitle  = title (' ');
hXLabel = xlabel('x');
hYLabel = ylabel('$\sigma^U_{ii}$','Interpreter','latex');
%cosmet(hTitle,hXLabel,hYLabel)
set(gca,'xtick',[-4 -2 0 2 4])

figure(4)
hold on
set(gca,'FontSize',20)
plot(data.x,data.NormalStressXX,line(1,:),'Linewidth',1)
plot(data.x,data.KineticStressXX,line(2,:),'Linewidth',1)
plot(data.x,data.StressXX,line(3,:),'Linewidth',1)
plot(-size/2+0.01,1.6025,'xk','Linewidth',2,'MarkerSize',18)
plot(size/2-0.01,1.5871,'xk','Linewidth',2,'MarkerSize',18)
xlim([-size/2 size/2])
ylim([0 1.8])
hLegend = legend('U','K','T','$F_x/A$','Location','South');
set(hLegend,'Interpreter','latex', 'FontSize', 18, 'FontName','AvantGarde');
legend boxoff
hTitle  = title (' ');
hXLabel = xlabel('x');
hYLabel = ylabel('$\sigma_{xx}$','Interpreter','latex');
%cosmet(hTitle,hXLabel,hYLabel)
set(gca,'xtick',[-4 -2 0 2 4])

figure(5)
hold on
set(gca,'FontSize',20)
plot(data.x,data.NormalStressYY,line(1,:),'Linewidth',1)
plot(data.x,data.KineticStressYY,line(2,:),'Linewidth',1)
plot(data.x,data.StressYY,line(3,:),'Linewidth',1)
xlim([-size/2 size/2])
ylim([-1.5 2.5])
hLegend = legend('U','K','T','Location','South');
set(hLegend,'Interpreter','latex', 'FontSize', 18, 'FontName','AvantGarde');
legend boxoff
hTitle  = title (' ');
hXLabel = xlabel('x');
hYLabel = ylabel('$\sigma_{yy}$','Interpreter','latex');
%cosmet(hTitle,hXLabel,hYLabel)
set(gca,'xtick',[-4 -2 0 2 4])

figure(6)
hold on
set(gca,'FontSize',20)
plot(data.x,data.NormalStressZZ,line(1,:),'Linewidth',1)
plot(data.x,data.KineticStressZZ,line(2,:),'Linewidth',1)
plot(data.x,data.StressZZ,line(3,:),'Linewidth',1)
xlim([-size/2 size/2])
ylim([-1 2.7])
hLegend = legend('U','K','T','Total','Location','South');
set(hLegend,'Interpreter','latex', 'FontSize', 18, 'FontName','AvantGarde');
legend boxoff
hTitle  = title (' ');
hXLabel = xlabel('x');
hYLabel = ylabel('$\sigma_{zz}$','Interpreter','latex');
%cosmet(hTitle,hXLabel,hYLabel)
set(gca,'xtick',[-4 -2 0 2 4])


figure(7)
hold on
set(gca,'FontSize',20)
plot(data.x,data.NormalStressXZ,line(1,:),'Linewidth',1)
plot(data.x,data.KineticStressXZ,line(2,:),'Linewidth',1)
plot(data.x,data.StressXZ,line(3,:),'Linewidth',1)
xlim([-size/2 size/2])
xlabel('x')
ylim([-0.5 0.5])
hLegend = legend('U','K','T','Location','NorthEast');
set(hLegend,'Interpreter','latex', 'FontSize', 18, 'FontName','AvantGarde');
legend boxoff
hTitle  = title (' ');
hXLabel = xlabel('x');
hYLabel = ylabel('$\sigma_{xz}$','Interpreter','latex');
%cosmet(hTitle,hXLabel,hYLabel)
set(gca,'xtick',[-4 -2 0 2 4])

xtemp = 0:0.01:10.4125;
rho0 = 0.409321*6/pi;
alpha0 = 0.232381*2*6/pi;
xwall = 5.61295;
T0 = 0.932565;
x0 = 0.95;
fitdens2 =  rho0 + alpha0*cos(2*pi*(xtemp-xwall)/T0).*exp(-(xtemp-xwall)/x0);

figure(8)
hold on
set(gca,'FontSize',20)
plot(data.x,data.Density,line(1,:),'Linewidth',2)
plot(xtemp-10.4125,fitdens2,'r','LineWidth',1)
plot([data.x(bulk(1)) data.x(bulk(1))],[0 2.1],'k')
plot([data.x(bulk(length(bulk))) data.x(bulk((length(bulk))))],[0 2.1],'k')
xlim([-size/2 size/2])
ylim([0 1.8])
hLegend = legend('data','fit','Location','Best');
set(hLegend,'Interpreter','latex', 'FontSize', 18, 'FontName','AvantGarde');
legend boxoff
hTitle  = title (' ');
hXLabel = xlabel('x');
hYLabel = ylabel('$\rho$','Interpreter','latex');
%cosmet(hTitle,hXLabel,hYLabel)
set(gca,'xtick',[-4 -2 0 2 4])


IntDens = cumsum(data.Density)*(data.x(2)-data.x(1));
fitDens = polyfit(data.x(bulk),IntDens(bulk),1);
IntDens = IntDens - fitDens(1)*data.x - fitDens(2);

SumDens = cumsum(data.Density)*(data.x(2)-data.x(1));
SumDens = SumDens - max(SumDens)/2;


figure(9)
hold on
set(gca,'FontSize',20)
plot(data.x(fluid),data.VelocityZ(fluid),'k','Linewidth',1)
plot(data.x(fluid),data.DisplacementMomentumZ(fluid)./data.Density(fluid),line(2,:),'Linewidth',1)
plot(data.x,fitvel,'--b','Linewidth',1)
plot([data.x(bulk(1)) data.x(bulk(1))],[0.1 -0.5],'k')
plot([data.x(bulk(length(bulk))) data.x(bulk((length(bulk))))],[0.1 -0.5],'k')
ylim([-0.4 0.02])
xlim([-size/2 size/2])
hLegend = legend('$v_z$','$U^{lin}/\Delta t$','$quadr. fit$','Location','North');
set(hLegend,'Interpreter','latex','FontSize', 18, 'FontName','AvantGarde');
legend boxoff
hTitle  = title (' ');
hXLabel = xlabel('x');
hYLabel = ylabel('$v_z$','Interpreter','latex');
%cosmet(hTitle,hXLabel,hYLabel)
set(gca,'xtick',[-4 -2 0 2 4])

figure(10)
hold on
set(gca,'FontSize',20)
plot(data.x,Sd,line(1,:),'LineWidth',2)
plot(data.x,Sd2,'-r','LineWidth',2)
ylim([0 1])
xlim([-size/2 size/2])
hLegend = legend('$S_D$','$S^*_D$','Location','North');
set(hLegend,'Interpreter','latex', 'FontSize', 18, 'FontName','AvantGarde');
legend boxoff
hTitle  = title (' ');
hXLabel = xlabel('x');
hYLabel = ylabel('$S_D$','Interpreter','latex');
%cosmet(hTitle,hXLabel,hYLabel)
set(gca,'xtick',[-4 -2 0 2 4])



% fit of the 'objective viscosity'
fitSigXZ = polyfit(data.x(bulk),data.StressXZ(bulk),1);
fitSigXZProfile = fitSigXZ(1)*data.x + fitSigXZ(2);


fitDisp = polyfit(data.x(bulk),gamDisp(bulk),1);
fitDispProfile = fitDisp(1)*data.x + fitDisp(2);

%fitetaN = -fitSigXZProfile./fitgam;
fitetaN = -fitSigXZ(1)/fit(1)/2*data.x./data.x;

%fitetaDisp = -fitSigXZProfile./fitDispProfile;
fitetaDisp = -fitSigXZ(1)/2/fitDis(1)*data.x./data.x;


figure(12)
hold on
set(gca,'FontSize',20)
plot(data.x(fluid),gamma_t(fluid),'k','LineWidth',1)
plot(data.x(fluid),gamDisp(fluid),line(2,:),'LineWidth',1)
plot(data.x,fitgam,'--b','Linewidth',1)
ylim([-0.22 0.22])
xlim([-size/2 size/2])
hLegend = legend('$d v_z /dx$','$\epsilon^{lin}/\Delta t$','$lin. fit$','Location','North');
set(hLegend, 'Interpreter','latex','FontSize', 18, 'FontName','AvantGarde');
legend boxoff
set(gca,'ytick',[-0.2 -0.1 0 0.1 0.2])
hTitle  = title (' ');
hXLabel = xlabel('x');
hYLabel = ylabel('$\dot{\gamma}$','Interpreter','latex');
%cosmet(hTitle,hXLabel,hYLabel)
set(gca,'xtick',[-4 -2 0 2 4])


figure(13)
hold on
set(gca,'FontSize',20)
plot(data.x,data.StressXZ,line(1,:),'LineWidth',2)
plot(data.x,IMCxz,'r','LineWidth',1)
plot(-size/2+0.01,0.4122,'x','Linewidth',2,'MarkerSize',18)
plot(size/2-0.01,-0.4194,'x','Linewidth',2,'MarkerSize',18)
ylim([-0.62 0.45])
xlim([-size/2 size/2])
hLegend = legend('$\sigma_{xz}$','IMC','$F_z/A$','Location','NorthEast');
set(hLegend,'Interpreter','latex', 'FontSize', 18, 'FontName','AvantGarde');
legend boxoff
hTitle  = title (' ');
hXLabel = xlabel('x');
hYLabel = ylabel('$\sigma_{xz}$','Interpreter','latex');
%cosmet(hTitle,hXLabel,hYLabel)
set(gca,'xtick',[-4 -2 0 2 4])
plot([-size/2 -size/2],[0.25 0.44],'g','LineWidth',2)
plot([-size/2 -3.5],[0.25 0.25],'g','LineWidth',2)
plot([-size/2 -3.5],[0.44 0.44],'g','LineWidth',2)
plot([-3.5 -3.5],[0.25 0.44],'g','LineWidth',2)
% onset
axes('position',[0.3 0.25 0.4 0.38]); % [left bottom width height]
hold on
set(gca,'FontSize',14,'color', 'none')
plot(data.x,IMCxz,'r','LineWidth',1)
plot(data.x,data.StressXZ,line(1,:),'LineWidth',2)
plot(-size/2,0.4122,'x','Linewidth',2,'MarkerSize',18)
ylim([0.25 0.44])
xlim([-size/2 -3.5])


figure(14)
hold on
set(gca,'FontSize',20)
plot(data.x(fluid),data.Temperature(fluid),line(1,:),'LineWidth',2)
plot([-7 7],[1 1],'--r','LineWidth',2)
ylim([0.97 1.03])
xlim([-size/2 size/2])
hLegend = legend('data','target value','Location','Best');
set(hLegend,'Interpreter','latex', 'FontSize', 18, 'FontName','AvantGarde');
legend boxoff
hTitle  = title (' ');
hXLabel = xlabel('x');
hYLabel = ylabel('$T$','Interpreter','latex');
%cosmet(hTitle,hXLabel,hYLabel)
set(gca,'xtick',[-4 -2 0 2 4])

figure(15)
hold on
set(gca,'FontSize',20)
plot(data.x,angleyz,line(1,:),'LineWidth',1)
plot(data.x,anglexz,line(2,:),'LineWidth',2)
plot(data.x,anglexy,line(3,:),'LineWidth',1)
ylim([-90 90])
xlim([-size/2 size/2])
set(gca,'ytick',[-90 -45 0 45 90])
plot([-7 0],[45 45],'k')
plot([0 7],[-45 -45],'k')
hLegend = legend('$\alpha_{yz}$','$\alpha_{xz}$','$\alpha_{xy}$','Location','NorthEast');
set(hLegend,'Interpreter','latex', 'FontSize', 18, 'FontName','AvantGarde');
legend boxoff
hTitle  = title (' ');
hXLabel = xlabel('x');
hYLabel = ylabel('$\alpha$','Interpreter','latex');
%cosmet(hTitle,hXLabel,hYLabel)
set(gca,'xtick',[-4 -2 0 2 4])

figure(16)
hold on
set(gca,'FontSize',20)
plot(data.x,lambda(:,1),line(1,:),'LineWidth',2)
plot(data.x,lambda(:,2),line(2,:),'LineWidth',2)
plot(data.x,lambda(:,3),'--b','LineWidth',1)
xlim([-size/2 size/2])
ylim([-0.8 1.3])
hLegend = legend('$\lambda_1$','$\lambda_2$','$\lambda_3$','Location','North');
set(hLegend,'Interpreter','latex', 'FontSize', 18, 'FontName','AvantGarde');
legend boxoff
hTitle  = title (' ');
hXLabel = xlabel('x');
hYLabel = ylabel('$\lambda_i$','Interpreter','latex');
%cosmet(hTitle,hXLabel,hYLabel)
set(gca,'xtick',[-4 -2 0 2 4])

figure(17)
hold on
set(gca,'FontSize',20)
plot(data.x,lambda(:,2)./lambda(:,1),line(1,:),'LineWidth',1)
xlim([-size/2 size/2])
ylim([-0.8 0.8])
legend boxoff
hTitle  = title (' ');
hXLabel = xlabel('x');
hYLabel = ylabel('$\lambda_2/\lambda_1$','Interpreter','latex');
%cosmet(hTitle,hXLabel,hYLabel)
set(gca,'xtick',[-4 -2 0 2 4])


fitright1 = find(data.x>0);
fitright2 = find(data.x(fitright1)<(size/2-3.5));
rightfit = (fitright1(fitright2));

fitleft1 = find(data.x<-0);
fitleft2 = find(data.x(fitleft1)>-(size/2-3.5));
leftfit = (fitleft1(fitleft2));

fitrightsig = polyfit(data.x(rightfit),sigD(rightfit),1);
fitrightdisp = polyfit(data.x(rightfit),abs(gamDisp(rightfit)),1);
etaD_right = fitrightsig(1)/fitrightdisp(1);

fitleftsig = polyfit(data.x(leftfit),sigD(leftfit),1);
fitleftdisp = polyfit(data.x(leftfit),abs(gamDisp(leftfit)),1);
etaD_left = fitleftsig(1)/fitleftdisp(1);

etaD_fit = (etaD_right + etaD_left)/2;

fitDispStr = polyfit(gamDisp(bulk),data.StressXZ(bulk),1);
fitDispStrline = linspace(-0.5,0.5,50);
stressDisprate = fitDispStrline*fitDispStr(1) + fitDispStr(2);


figure(18)
hold on
set(gca,'FontSize',20)
plot(gamDisp(fluid),data.StressXZ(fluid),line(1,:),'LineWidth',1)
plot(fitDispStrline,stressDisprate,'r','LineWidth',1)
plot([-0.25 0.25],[0 0],'--')
plot([0 0],[-0.5 0.5],'--')
ylim([-0.5 0.5])
xlim([-0.2 0.2])
hTitle  = title (' ');
hXLabel = xlabel('$\dot{\gamma}$','Interpreter','latex');
hYLabel = ylabel('$\sigma_{xz}$','Interpreter','latex');
%cosmet(hTitle,hXLabel,hYLabel)

figure(19)
hold on
set(gca,'FontSize',20)
plot(data.x,etaDisp,line(1,:),'LineWidth',1)
plot(data.x,-fitDispStr(1)*data.x./data.x,'k--','LineWidth',1)
plot(data.x(fluid),viscD(fluid),line(2,:),'LineWidth',1)
plot(data.x,etaD_fit*data.x./data.x,'r-.','LineWidth',1)
ylim([0 5])
xlim([-size/2 size/2])
hLegend = legend('$\eta$','$fit_{\eta}$','$\eta^{D}$','$fit^{\eta^D}$','Location','EastOutside');
set(hLegend,'Interpreter','latex', 'FontSize', 18, 'FontName','AvantGarde');
legend boxoff
hTitle  = title (' ');
hXLabel = xlabel('x');
hYLabel = ylabel('$\eta$','Interpreter','latex');
%cosmet(hTitle,hXLabel,hYLabel)
set(gca,'xtick',[-4 -2 0 2 4])


figure(20)
hold on
set(gca,'FontSize',20)
plot(data.x,data.Density,line(1,:),'Linewidth',2)
plot([data.x(bulk(1)) data.x(bulk(1))],[0 2.1],'k')
plot([data.x(bulk(length(bulk))) data.x(bulk((length(bulk))))],[0 2.1],'k')
xlim([-size/2 size/2])
ylim([0 1.8])
hTitle  = title (' ');
hXLabel = xlabel('x');
hYLabel = ylabel('$\rho$','Interpreter','latex');
%cosmet(hTitle,hXLabel,hYLabel)
set(gca,'xtick',[-4 -2 0 2 4])

% 
% figure
% set(gca,'FontSize',20)
% hold on
% plot(data.x,data.Density,line(1,:),'Linewidth',2)
% plot(data.x,abs(30*data.KineticStressXZ),line(2,:),'Linewidth',2)
% plot(data.x,abs(data.StressXZ),line(3,:),'Linewidth',2)
% xlim([-size/2 size/2])
% title('abs(30*kin shear stress)')
% print -depsc KinShearStress

% these file names correspond with the figure numbers in the paper
if strcmp(printfig,'yes')==1
    figure(20)
        print -depsc Fig4
    figure(9)
        print -depsc Fig5a
    figure(12)
        print -depsc Fig5b
    figure(14)
        print -depsc Fig6
    figure(1)
        print -depsc Fig7
    figure(4)
        print -depsc Fig8
    figure(6)
        print -depsc Fig9
    figure(13)
        print -depsc Fig10
    figure(7)
        print -depsc Fig11   
    figure(18)
        print -depsc Fig12
    figure(19)
        print -depsc Fig13
    figure(16)
        print -depsc Fig27
    figure(10)
        print -depsc Fig28a 
    figure(15)
        print -depsc Fig29a     
        
    movefile('*.eps','./Paper')
    
    close all
end

    