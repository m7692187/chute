function loadData()

%uncomment this to avoid using old data
%delete('Data.mat'); delete('FlowHstopData.mat'); clear global

loadDataFromFiles();
global Compare
Compare=7; if (Compare~=7), disp(['Warning: Compare set to ' num2str(Compare) ', not 7!']); end

load_FlowHeight();
loadFlowStop();
global CoeffHstop
%if ~exist('CoeffHstop','var'), 
  load_Hstop(); 
  get_flowrule();
%end % as this is expensive


return

% creates the Data structure, either from Data.mat (if existent) or from 
% the stat files
function loadDataFromFiles()

%load global variables which store the data loaded here
global Data FlowData HstopData Title

%only load Data if it is not already loaded
if ~isempty(Data), 
  disp('Data is already loaded')
  return; 
end

%add path where loadstatistics is located
addpath('~/code/MD/matlab/thomas')

%if no new stat files are added, just load data from file
if (exist('Data.mat','file'))
  disp('loading Data from Data.mat')
  load Data.mat; 
  return; 
end

%now load data
disp('loading Data from stat files')
if (exist('FlowHstopData.mat','file'))
  load FlowHstopData.mat FlowData HstopData
  %FlowData=loadstatistics('../Flow/L*M0.5B0.5/*.stat');
  %HstopData=loadstatistics('../Hstop/L*M0.5B0.5/*.stat');
  %save FlowHstopData.mat FlowData HstopData
else
  %load FlowHstopData.mat HstopData
  HstopData=loadstatistics('../Hstop/L*M0.5B0.5/*.stat');
  H= cellfun(@(data)str2double(data.name(max(strfind(data.name,'H'))+1:max(strfind(data.name,'A'))-1)),HstopData);
  FlowData=loadstatistics('../Flow/L*M0.5B0.5/*.stat');
  save FlowHstopData.mat FlowData HstopData
end
H= cellfun(@(data)str2double(data.name(max(strfind(data.name,'H'))+1:max(strfind(data.name,'A'))-1)),HstopData);
HstopData=HstopData(H>3.9); %remove low height data

%remove directory from filename
for i=1:length(FlowData)
   FlowData{i}.name= FlowData{i}.name(max([0 strfind( FlowData{i}.name,'/')])+1:end);
   if strfind(FlowData{i}.name,'.T'); FlowData{i}.name=FlowData{i}.name(1:strfind(FlowData{i}.name,'.T')-1); end
end
for i=1:length(HstopData)
  HstopData{i}.name=HstopData{i}.name(max([0,strfind(HstopData{i}.name,'/')])+1:end);
end

CheckNames()
LoadIntoDataStructure()
save Data.mat Data Title
clear FlowData HstopData 

return

% loads the Data from the stat files into the Data structure
function LoadIntoDataStructure()
global Data FlowData HstopData Title

%define Cases
FlowName.H= cellfun(@(data)str2double(data.name(max(strfind(data.name,'H'))+1:max(strfind(data.name,'A'))-1)),FlowData);
FlowName.A= cellfun(@(data)str2double(data.name(max(strfind(data.name,'A'))+1:max(strfind(data.name,'L'))-1)),FlowData);
FlowName.L= cellfun(@(data)str2double(data.name(max(strfind(data.name,'L'))+1:max(strfind(data.name,'M'))-1)),FlowData);
FlowName.M= cellfun(@(data)str2double(data.name(max(strfind(data.name,'M'))+1:max(strfind(data.name,'B'))-1)),FlowData);
FlowName.B= cellfun(@(data)str2double(data.name(max(strfind(data.name,'B'))+1:end)),FlowData);

Case{1} = (FlowName.L==0)&(FlowName.M==.5)&(FlowName.B==.5);
Case{2} = (FlowName.L==0.17)&(FlowName.M==.5)&(FlowName.B==.5);
Case{3} = (FlowName.L==0.33)&(FlowName.M==.5)&(FlowName.B==.5);
Case{4} = (FlowName.L==0.5)&(FlowName.M==.5)&(FlowName.B==.5);
Case{5} = (FlowName.L==0.67)&(FlowName.M==.5)&(FlowName.B==.5);
Case{6} = (FlowName.L==0.83)&(FlowName.M==.5)&(FlowName.B==.5);
Case{7} = (FlowName.L==1)&(FlowName.M==.5)&(FlowName.B==.5);
Case{8} = (FlowName.L==1.5 )&(FlowName.M==.5)&(FlowName.B==.5);
Case{9} = (FlowName.L==2)&(FlowName.M==.5)&(FlowName.B==.5);
Case{10}= (FlowName.L==4   )&(FlowName.M==.5)&(FlowName.B==.5);

%define HstopCases
HstopName.H= cellfun(@(data)str2double(data.name(strfind(data.name,'H')+1:strfind(data.name,'A')-1)),HstopData);
HstopName.A= cellfun(@(data)str2double(data.name(strfind(data.name,'A')+1:strfind(data.name,'L')-1)),HstopData);
HstopName.L= cellfun(@(data)str2double(data.name(strfind(data.name,'L')+1:strfind(data.name,'M')-1)),HstopData);
HstopName.M= cellfun(@(data)str2double(data.name(strfind(data.name,'M')+1:strfind(data.name,'B')-1)),HstopData);
HstopName.B= cellfun(@(data)str2double(data.name(strfind(data.name,'B')+1:end)),HstopData);

HstopCase{1} = (HstopName.L==0)&(HstopName.M==.5)&(HstopName.B==.5);
HstopCase{2} = (HstopName.L==0.17)&(HstopName.M==.5)&(HstopName.B==.5);
HstopCase{3} = (HstopName.L==0.33)&(HstopName.M==.5)&(HstopName.B==.5);
HstopCase{4} = (HstopName.L==0.5)&(HstopName.M==.5)&(HstopName.B==.5);
HstopCase{5} = (HstopName.L==0.67)&(HstopName.M==.5)&(HstopName.B==.5);
HstopCase{6} = (HstopName.L==0.83)&(HstopName.M==.5)&(HstopName.B==.5);
HstopCase{7} = (HstopName.L==1)&(HstopName.M==.5)&(HstopName.B==.5);
HstopCase{8} = (HstopName.L==1.5 )&(HstopName.M==.5)&(HstopName.B==.5);
HstopCase{9} = (HstopName.L==2)&(HstopName.M==.5)&(HstopName.B==.5);
HstopCase{10}= (HstopName.L==4   )&(HstopName.M==.5)&(HstopName.B==.5);


%define Titles
Title = {...
  '$\lambda=0$';...
  '$\lambda=1/6$';...
  '$\lambda=1/3$';...
  '$\lambda=1/2$';...
  '$\lambda=2/3$';...
  '$\lambda=5/6$';...
  '$\lambda=1$';...
  '$\lambda=1.5$';...
  '$\lambda=2$';...
  '$\lambda=4$';...
  };

%write Data
for i=1:length(Title)
  Data{i}.flow=FlowData(Case{i});
  Data{i}.hstop=HstopData(HstopCase{i});
  Data{i}.title=Title{i};
end


return

% checks consistency of the data in the stat files
function CheckNames()
global Data FlowData HstopData Title

%checks if name and data are consistent, and if t>1800
FlowName.H= cellfun(@(data)str2double(data.name(max(strfind(data.name,'H'))+1:max(strfind(data.name,'A'))-1)),FlowData);
FlowName.A= cellfun(@(data)str2double(data.name(max(strfind(data.name,'A'))+1:max(strfind(data.name,'L'))-1)),FlowData);
FlowName.L= cellfun(@(data)str2double(data.name(max(strfind(data.name,'L'))+1:max(strfind(data.name,'M'))-1)),FlowData);
FlowName.M= cellfun(@(data)str2double(data.name(max(strfind(data.name,'M'))+1:max(strfind(data.name,'B'))-1)),FlowData);
FlowName.B= cellfun(@(data)str2double(data.name(max(strfind(data.name,'B'))+1:end)),FlowData);
FlowName.T= cellfun(@(data)data.time(end),FlowData);
%FlowName.T_restart= cellfun(@(data)data.time_restart,FlowData);

Test.H=cellfun(@(data)data.Domain(6)/1.2,FlowData);
Test.L=cellfun(@(data)data.FixedParticleRadius*2,FlowData);
Test.A=cellfun(@(data)data.ChuteAngle,FlowData);
Test.B=cellfun(@(data)data.Mu(end),FlowData);
Test.M=cellfun(@(data)data.Mu(1),FlowData);

WrongH=(abs(FlowName.H-Test.H)>1e-10); 
WrongL=(abs(FlowName.L-Test.L)>1e-2);
WrongA=(abs(FlowName.A-Test.A)>1e-10);
WrongB=(abs(FlowName.B-Test.B)>1e-2);
WrongM=(abs(FlowName.M-Test.M)>1e-2);
WrongT=FlowName.T<1800;
Wrong=WrongH|WrongL|WrongA|WrongB|WrongM|WrongT;

if any(Wrong)
  cellfun(@(data)disp(data.name),FlowData(Wrong))
  error('inconsistent flow files');
else
  disp('flow file names are consistent')
end

% checks if name and data are consistent
HstopName.H= cellfun(@(data)str2double(data.name(strfind(data.name,'H')+1:strfind(data.name,'A')-1)),HstopData);
HstopName.A= cellfun(@(data)str2double(data.name(strfind(data.name,'A')+1:strfind(data.name,'L')-1)),HstopData);
HstopName.L= cellfun(@(data)str2double(data.name(strfind(data.name,'L')+1:strfind(data.name,'M')-1)),HstopData);
HstopName.M= cellfun(@(data)str2double(data.name(strfind(data.name,'M')+1:strfind(data.name,'B')-1)),HstopData);
HstopName.B= cellfun(@(data)str2double(data.name(strfind(data.name,'B')+1:end)),HstopData);
HstopName.T= cellfun(@(data)data.time(2),HstopData);
HstopName.dt_restart= cellfun(@(data)data.time(1)-data.time_restart,HstopData);

Test.H=cellfun(@(data)data.Domain(6)/1.2,HstopData);
Test.L=cellfun(@(data)data.FixedParticleRadius*2,HstopData);
Test.A=cellfun(@(data)data.ChuteAngle,HstopData);
Test.B=cellfun(@(data)data.Mu(end),HstopData);
Test.M=cellfun(@(data)data.Mu(1),HstopData);

WrongH=(abs(HstopName.H-Test.H)>1e-10); 
WrongL=(abs(HstopName.L-Test.L)>1e-2);
WrongA=(abs(HstopName.A-Test.A)>1e-10);
WrongB=(abs(HstopName.B-Test.B)>1e-2);
WrongM=(abs(HstopName.M-Test.M)>1e-2);
WrongT=(abs(HstopName.dt_restart)>1);

Wrong=WrongH|WrongL|WrongA|WrongB|WrongM|WrongT;

if any(Wrong)
  cellfun(@(data)disp(data.name),HstopData(Wrong))
  warning('inconsistent hstop files');
else
  disp('hstop file names are consistent')
end
return

%loads cell arrays Flow and Stop containing arrays of stats for each Case
function loadFlowStop()
global Data Flow Stop
Flow=cell(size(Data));
Stop=cell(size(Data));
for i=1:length(Data)
  if (isfield(Data{i},'flow'))
    Flow{i}.H= cellfun(@(data)str2double(data.name(max(strfind(data.name,'H'))+1:max(strfind(data.name,'A'))-1)),Data{i}.flow);
    Flow{i}.A= cellfun(@(data)str2double(data.name(max(strfind(data.name,'A'))+1:max(strfind(data.name,'L'))-1)),Data{i}.flow);
    Flow{i}.L= cellfun(@(data)str2double(data.name(max(strfind(data.name,'L'))+1:max(strfind(data.name,'M'))-1)),Data{i}.flow);
    Flow{i}.M= cellfun(@(data)str2double(data.name(max(strfind(data.name,'M'))+1:max(strfind(data.name,'B'))-1)),Data{i}.flow);
    Flow{i}.B= cellfun(@(data)str2double(data.name(max(strfind(data.name,'B'))+1:end)),Data{i}.flow);
    Flow{i}.B= cellfun(@(data)str2double(data.name(max(strfind(data.name,'B'))+1:end)),Data{i}.flow);
    %set default values for FullyOscillating (false), Oscillating
    %(false), Accelerating, ...
    Flow{i}.FullyOscillating= cellfun(@(data)false,Data{i}.flow);
    Flow{i}.Oscillating= cellfun(@(data)false,Data{i}.flow);
    Flow{i}.Accelerating= cellfun(@(data)data.ChuteAngle>=29,Data{i}.flow);
    Flow{i}.Steady= (~Flow{i}.Accelerating)&...
      cellfun(@(data)data.FlowVelocityX>0.01,Data{i}.flow);
      Flow{i}.Velocity=cellfun(@(data)data.FlowVelocityX,Data{i}.flow);
    Flow{i}.Friction=cellfun(@(data)data.BottomFrictionCoefficient,Data{i}.flow);
    Flow{i}.Froude=cellfun(@(data)data.Froude,Data{i}.flow);
    Flow{i}.Height=cellfun(@(data)data.FlowHeight,Data{i}.flow);
    Flow{i}.Angle=cellfun(@(data)data.ChuteAngle,Data{i}.flow);
    Flow{i}.Time=cellfun(@(data)diff(data.time),Data{i}.flow);
    Flow{i}.Flowing= cellfun(@(data)data.FlowVelocityX>0.001,Data{i}.flow);
    Flow{i}.I= cellfun(@(data)median(data.I(isfinite(data.I))),Data{i}.flow);
    %Flow{i}.I= cellfun(@(data)sum(data.I(isfinite(data.I)).*data.Nu(isfinite(data.I)))./sum(data.Nu(isfinite(data.I))),Data{i}.flow);
  end
  if (isfield(Data{i},'hstop'))
    Stop{i}.H= cellfun(@(data)str2double(data.name(max(strfind(data.name,'H'))+1:max(strfind(data.name,'A'))-1)),Data{i}.hstop);
    Stop{i}.A= cellfun(@(data)str2double(data.name(max(strfind(data.name,'A'))+1:max(strfind(data.name,'L'))-1)),Data{i}.hstop);
    Stop{i}.L= cellfun(@(data)str2double(data.name(max(strfind(data.name,'L'))+1:max(strfind(data.name,'M'))-1)),Data{i}.hstop);
    Stop{i}.M= cellfun(@(data)str2double(data.name(max(strfind(data.name,'M'))+1:max(strfind(data.name,'B'))-1)),Data{i}.hstop);
    Stop{i}.B= cellfun(@(data)str2double(data.name(max(strfind(data.name,'B'))+1:end)),Data{i}.hstop);
    Stop{i}.Height=cellfun(@(data)data.FlowHeight,Data{i}.hstop);
    Stop{i}.Angle=cellfun(@(data)data.ChuteAngle,Data{i}.hstop);
    Stop{i}.Velocity=cellfun(@(data)data.FlowVelocityX,Data{i}.hstop);
    Stop{i}.Friction=cellfun(@(data)data.BottomFrictionCoefficient,Data{i}.hstop);
    Stop{i}.Froude=cellfun(@(data)data.Froude,Data{i}.hstop);
    Stop{i}.Oscillating=cellfun(@(data)false,Data{i}.hstop);
  end
end
%set special values
Flow{1}.Accelerating(Flow{1}.A>=23&Flow{1}.H==10) = true;
Flow{1}.Accelerating(Flow{1}.A>=25&Flow{1}.H==20) = true;
Flow{1}.Accelerating(Flow{1}.A>=26&Flow{1}.H==30) = true;
Flow{1}.Accelerating(Flow{1}.A>=22&Flow{1}.H==40) = true;
Flow{1}.Steady(Flow{1}.Accelerating) = false;

Flow{4}.Oscillating(Flow{4}.A<=25) = true;
Flow{4}.FullyOscillating(Flow{4}.Oscillating&(Flow{4}.A>=24)) = true;
%Flow{1}.FullyOscillating(Flow{1}.A==23&Flow{1}.H==20) = true;
%Flow{1}.FullyOscillating(Flow{1}.A==24&Flow{1}.H==30) = true;
Flow{1}.Oscillating(Flow{1}.A<=20&Flow{1}.H==10) = true;
Flow{1}.Oscillating(Flow{1}.A<=22&Flow{1}.H==20) = true;
Flow{1}.Oscillating(Flow{1}.A<=23&Flow{1}.H==30) = true;
Flow{1}.Oscillating(Flow{1}.A<=24&Flow{1}.H==40) = true;
Flow{5}.Oscillating(Flow{5}.A<=21&Flow{5}.H==20) = true;
Flow{5}.Oscillating(Flow{5}.A<=24&Flow{5}.H==10) = true;
Flow{5}.Oscillating(Flow{5}.A==21&Flow{5}.H<=30) = true;
Flow{5}.FullyOscillating(Flow{5}.A==23&Flow{5}.H==10) = true;
Flow{5}.FullyOscillating(Flow{5}.A==24&Flow{5}.H==10) = true;

Stop{1}.Oscillating(Stop{1}.A<20.5&Stop{1}.A>11.5)=true;

return

function get_flowrule()
global Data CoeffHstop Flow Compare
for Case=1:length(Data)
  Angle=Flow{Case}.A;
  %which hstop we compare with
  cmp=Compare;
  %see plot_flow_rule_Hstop_fit
  
  %flow
  FlowSpeed=cellfun(@(data)data.FlowVelocityX,Data{Case}.flow);
  GravityZ=cellfun(@(data)data.Gravity(3),Data{Case}.flow);
  %defined for the rough bottom
  if ~isempty(CoeffHstop{cmp})
    HeightOverHstop=Flow{Case}.Height./hstop(CoeffHstop{cmp},Flow{Case}.A); 
    if exist('jenkinscorrection','var')
      HeightOverHstop=HeightOverHstop.*tand(Flow{Case}.A).^2/CoeffHstop{cmp}(1)^2;
    end
    Froude=FlowSpeed./sqrt(-GravityZ.*Flow{Case}.Height);

    indAll=Angle>atand(CoeffHstop{cmp}(1))&Angle<atand(CoeffHstop{cmp}(2))&Flow{Case}.Steady;
    indS=indAll&~Flow{Case}.Oscillating;
    indL=indAll&Flow{Case}.Oscillating&~Flow{Case}.FullyOscillating;
  %   indO=indAll&Flow{Case}.FullyOscillating;
    [Flow{Case}.betaPOJ Flow{Case}.varPOJ Flow{Case}.confPOJ]=flowrule_fit_with_offset(Angle(indS), Froude(indS), HeightOverHstop(indS).*tand(Angle(indS)).^2/CoeffHstop{cmp}(1)^2, CoeffHstop{cmp},1);
    [Flow{Case}.betaPO  Flow{Case}.varPO  Flow{Case}.confPO ]=flowrule_fit_with_offset(Angle(indS), Froude(indS), HeightOverHstop(indS), CoeffHstop{cmp},1);
    [Flow{Case}.betaP  Flow{Case}.varP  Flow{Case}.confP ]=flowrule_fit_with_offset(Angle(indS), Froude(indS), HeightOverHstop(indS), CoeffHstop{cmp},0);
    [Flow{Case}.betaPOlayered  Flow{Case}.varPOlayered  Flow{Case}.confPOlayered]=flowrule_fit_with_offset(Angle(indS), Froude(indL), HeightOverHstop(indL), CoeffHstop{cmp},1);
    if sum(indL), Flow{Case}.betaPOL=flowrule_fit_with_offset(Angle(indL), Froude(indL), HeightOverHstop(indL), CoeffHstop{cmp},1); end
    %display(['Pouliquen slope: for case ' num2str(Case) ':' num2str(Flow{Case}.betaPO(2),'%.3f')])
  end
end
return

% creates fits to the hstop curve
function load_Hstop()
global Data CoeffHstop CoeffHstop_exp Astop Hstop Astop_exp Stop VarHstop
global kappa

% if ~isempty(CoeffHstop)
%   return;
% end

%preallocate arrays
Astop=cell(size(Data));
Hstop=cell(size(Data));
VarHstop=cell(size(Data));
CoeffHstop=cell(size(Data));
Astop_exp=cell(size(Data));
CoeffHstop_exp=cell(size(Data));
for i=1:length(Data)
 	%defines which cases are static which are not
  Stop{i}.static=cellfun(@(data)data.time(end)<499,Data{i}.hstop);
  
  if ~isempty(Stop{i}.A) %if there are Hstop points
      %for each flowing case, look left and down for next static case
      Stop{i}.leftRange=[];
      Amin=min(Stop{i}.A(Stop{i}.static));
      Stop{i}.downRange=[Amin max(max(Stop{i}.Height), 63), 1000];
      %warning()
      %Stop{i}.downRange=[Amin 80 1000]; disp('warning: different hstop fitting');
      %Stop{i}.downRange=[];
      
      %for all arrested flows
      for m=find(Stop{i}.static)
        %if it is the rightmost arrested case
        if (Stop{i}.A(m)==max(Stop{i}.A(Stop{i}.H(m)==Stop{i}.H&(Stop{i}.static))))
          %find next flowing case at same height
          right=Stop{i}.H==Stop{i}.H(m)&~Stop{i}.static;
          Amin=min(Stop{i}.A(right));
          if ~isempty(Amin), 
            Stop{i}.leftRange(end+1,:)=[Stop{i}.Height(m) Stop{i}.A(m) Amin];
          end
        end
        %if it is the upmost arrested case
        if (Stop{i}.H(m)==max(Stop{i}.H(Stop{i}.A(m)==Stop{i}.A&(Stop{i}.static))))
          %find next flowing case at same angle
          up=Stop{i}.A==Stop{i}.A(m)&~Stop{i}.static;
          Hmin=min(Stop{i}.Height(up));
          if ~isempty(Hmin)&(Hmin-Stop{i}.Height(m))<3, 
            Stop{i}.downRange(end+1,:)=[Stop{i}.A(m) Stop{i}.Height(m) Hmin]; 
          end
        end
      end
      if isempty(Stop{i}.leftRange)
          Amin=min(Stop{i}.A(Stop{i}.static));
          Stop{i}.leftRange = [min(Stop{i}.Height) Amin Amin; ...
                               max(Stop{i}.Height) Amin Amin;];
      end
      [Astop{i},Hstop{i},CoeffHstop{i},Astop_exp{i},CoeffHstop_exp{i},VarHstop{i}]=hstop_fit2(Stop{i}.leftRange,Stop{i}.downRange); %#ok<AGROW>
  end
 
end
return

function load_FlowHeight()
global Data kappa
for i=1:length(Data)
  kappa=0.02;
  for j=1:length(Data{i}.hstop);
      %S=Data{i}.hstop{j}.StressZZ+integ(Data{i}.hstop{j}.TractionZ,Data{i}.hstop{j}.z);
      S=integ(Data{i}.hstop{j}.Density,Data{i}.hstop{j}.z)*Data{i}.hstop{j}.Gravity(3);

      Data{i}.hstop{j}.Base=...
        min(Data{i}.hstop{j}.z(S<(1-kappa)*max(S)),[],1);
      Data{i}.hstop{j}.Surface=...
        max(Data{i}.hstop{j}.z(S>kappa*max(S)),[],1);
      Data{i}.hstop{j}.FlowHeight=Data{i}.hstop{j}.Surface-Data{i}.hstop{j}.Base;

      Data{i}.hstop{j}.FlowHeight=Data{i}.hstop{j}.FlowHeight/(1-2*kappa);
      Data{i}.hstop{j}.Base=Data{i}.hstop{j}.Base-kappa*Data{i}.hstop{j}.FlowHeight;
      Data{i}.hstop{j}.Surface=Data{i}.hstop{j}.Surface+kappa*Data{i}.hstop{j}.FlowHeight;
      Data{i}.hstop{j}.z=Data{i}.hstop{j}.z-Data{i}.hstop{j}.Base;
      Data{i}.hstop{j}.Base=0;
      Data{i}.hstop{j}.Surface=Data{i}.hstop{j}.FlowHeight;
  end
  
  if (isfield(Data{i},'flow'))
  for j=1:length(Data{i}.flow);
    if isfield(Data{i}.flow{j},'TractionZ')
      %StressZZ = Data{i}.flow{j}.StressZZ+(cumsum(Data{i}.flow{j}.TractionZ)-sum(Data{i}.flow{j}.TractionZ))*diff(Data{i}.flow{j}.z(1:2));
      StressZZ = (cumsum(Data{i}.flow{j}.Density)-sum(Data{i}.flow{j}.Density))*diff(Data{i}.flow{j}.z(1:2))*Data{i}.flow{j}.Gravity(3);
    else
      %StressZZ = Data{i}.flow{j}.StressZZ;
      StressZZ = (cumsum(Data{i}.flow{j}.Density)-sum(Data{i}.flow{j}.Density))*diff(Data{i}.flow{j}.z(1:2))*Data{i}.flow{j}.Gravity(3);
    end
    Data{i}.flow{j}.Base=...
      min(Data{i}.flow{j}.z(StressZZ<(1-kappa)*StressZZ(1)),[],1);
    Data{i}.flow{j}.Surface=...
      max(Data{i}.flow{j}.z.*(StressZZ>kappa*StressZZ(1)),[],1);
%      Data{i}.flow{j}.Base=...
%        min(Data{i}.flow{j}.z(Data{i}.flow{j}.Nu>0.45*max(Data{i}.flow{j}.Nu)));
%      Data{i}.flow{j}.Surface=...
%        max(Data{i}.flow{j}.z.*(Data{i}.flow{j}.Nu>0.45*max(Data{i}.flow{j}.Nu)));

    Data{i}.flow{j}.FlowHeight=Data{i}.flow{j}.Surface-Data{i}.flow{j}.Base;
    %define FlowHeight by the mean density
%     DensityCentre=mean(Data{i}.flow{j}.Nu(Data{i}.flow{j}.z>Data{i}.flow{j}.FlowHeight*0.33&Data{i}.flow{j}.z<Data{i}.flow{j}.FlowHeight*0.67));
%     Mass=sum(Data{i}.flow{j}.Nu)*diff(Data{i}.flow{j}.z(1:2));
%     Data{i}.flow{j}.FlowHeight=Mass/DensityCentre;
    

    Data{i}.flow{j}.FlowHeight=Data{i}.flow{j}.FlowHeight/(1-2*kappa);
    Data{i}.flow{j}.Base=Data{i}.flow{j}.Base-kappa*Data{i}.flow{j}.FlowHeight;
    Data{i}.flow{j}.Surface=Data{i}.flow{j}.Surface+kappa*Data{i}.flow{j}.FlowHeight;

    Data{i}.flow{j}.z=Data{i}.flow{j}.z-Data{i}.flow{j}.Base;
    Data{i}.flow{j}.Base=0;
    Data{i}.flow{j}.Surface=Data{i}.flow{j}.FlowHeight;
    
    %define FlowHeight by the min max density
%     Data{i}.flow{j}.Base=3*Data{i}.flow{j}.w+min(Data{i}.flow{j}.z(Data{i}.flow{j}.Nu<0.001),[],1);
%     Data{i}.flow{j}.Surface=-3*Data{i}.flow{j}.w+max(Data{i}.flow{j}.z(Data{i}.flow{j}.Nu<0.001),[],1);
%     Data{i}.flow{j}.FlowHeight=Data{i}.flow{j}.Surface-Data{i}.flow{j}.Base;
    
    
    StrainXZ=diff(smooth(Data{i}.flow{j}.VelocityX))./diff(Data{i}.flow{j}.z);
    StrainXZ=.5*(StrainXZ([1 1:end])+StrainXZ([1:end end]));
    %StrainZZ=smooth(StrainZZ);
    %Data{i}.flow{j}.I=StrainXZ./sqrt(Data{i}.flow{j}.StressZZ);
    Data{i}.flow{j}.I=StrainXZ*Data{i}.flow{j}.d./sqrt(Data{i}.flow{j}.StressZZ./Data{i}.flow{j}.Density);
    % data.InertialParameter=data.ShearRate*data.d./sqrt(data.StressZZ/data.ParticleDensity);
    %see ~/papers/JopForterrePouliquen2006.pdf
    
%     clf
%     plot((Data{i}.flow{j}.z-Data{i}.flow{j}.Base)./Data{i}.flow{j}.FlowHeight,Data{i}.flow{j}.StressZZ); xlim([0,1])
%     dStressZZ=max(Data{i}.flow{j}.StressZZ)-min(Data{i}.flow{j}.StressZZ);
%     ind=Data{i}.flow{j}.StressZZ<max(Data{i}.flow{j}.StressZZ)-0.02*dStressZZ&...
%         Data{i}.flow{j}.StressZZ>min(Data{i}.flow{j}.StressZZ)+0.02*dStressZZ;
%     p=polyfit(Data{i}.flow{j}.z(ind),Data{i}.flow{j}.StressZZ(ind),1)
%     errorLS=@(coeff) sum((ContactStressZZ(ind)-(coeff(1)*Data{i}.flow{j}.z(ind)+coeff(2))).^2);
%     coeff=fminsearch(errorLS,[0 0]);
%     Mass=-Data{i}.flow{j}.Domain(6)/1.2*Data{i}.flow{j}.ParticleDensity(1)*Data{i}.flow{j}.ParticleVolume(1)*Data{i}.flow{j}.Gravity(3);
%     Data{i}.flow{j}.Base=(Mass-coeff(2))/coeff(1);
%     Data{i}.flow{j}.FlowHeight2=-Mass/coeff(1);
%     hold on
%     plot((Data{i}.flow{j}.z-Data{i}.flow{j}.Base)./Data{i}.flow{j}.FlowHeight2,Data{i}.flow{j}.StressZZ,'r'); xlim([0,1])
  end
  end
  
  if (false) 
    disp('using Silbert''s height definition')
    global Stop
    for j=1:length(Data{i}.hstop);
        Data{i}.hstop{j}.Base=0;
        Data{i}.hstop{j}.Surface=Stop{i}.H(j)*pi/6/0.6;
        Data{i}.hstop{j}.FlowHeight=Stop{i}.H(j)*pi/6/0.6;
    end
    global Flow
    if (isfield(Data{i},'flow'))
    for j=1:length(Data{i}.flow);
        Data{i}.flow{j}.Base=0;
        Data{i}.flow{j}.Surface=Flow{i}.H(j)*pi/6/0.6;
        Data{i}.flow{j}.FlowHeight=Flow{i}.H(j)*pi/6/0.6;
    end
    end
  end
end
return

