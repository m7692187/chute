function print_figures(figlist)
%print all figures by default
if ~exist('figlist','var'), figlist=get(0,'Children'); end
system('mkdir -p Figures');

for i=figlist'
  %set(get(get(i,'CurrentAxes'),'Title'),'String','')
  %set default file name
  %set(get(get(i,'CurrentAxes'),'Title'),'String','')
  if isempty(get(i,'FileName')), set(i,'FileName',['figure' num2str(i)]); end
  P=get(i,'Position');
  PrintLaTeX(i,['Figures/' get(i,'FileName')],P(3)*8/560);
  Axes=get(i,'CurrentAxes');
  title(Axes(1),['saved as Figures/' get(i,'FileName')],'Interpreter','none')
  disp(['~/.Matlabepstopdf/myMatlabepstopdf ' pwd '/Figures/' get(i,'FileName') '.matlab.tex'])
end
return
