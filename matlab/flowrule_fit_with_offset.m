%used to fit flow rule
function [beta, var,betaconf]=flowrule_fit(Angle, Froude, HeightOverHstop, tantheta1,offsettype)

x=HeightOverHstop(:);
y=Froude(:);

if isempty(x),
  beta=[];
  betaconf=[];
  var=[];
  return
end

% if offsettype==0
%   %use this if you want no offset, Froude=beta*HeightOverHstop
%   X = [x];
%   beta(1)=0;
%   beta(2,1) = X\y;
% elseif offsettype==1
%   % use this if you want a general offset, Froude=beta(1)+beta(2)*HeightOverHstop
%   X = [ones(size(x))  x];
%   beta = X\y;
% % elseif offsettype==2
% %   % use this if you want an offset such that, Froude(HeightOverHstop=1)=0
% %   X = [x-1];
% %   beta(2,1) = X\y;
% %   beta(1)=-beta(2);
% end
% X = [ones(size(x))  x];
% Y = X*beta;
% var = sqrt(mean((Y-y).^2));

if offsettype==0
  s = fitoptions('Method','NonlinearLeastSquares',...
    'Lower',[0],...
    'Upper',[Inf],...
    'Startpoint',[1]);
  f = fittype('beta*x','options',s,'independent','x','coefficients','beta');
  c = fit(x,y,f);
  beta = coeffvalues(c)';
  betaconf = confint(c)';
  var = sqrt(mean((beta*x-y).^2));
elseif offsettype==1
  s = fitoptions('Method','NonlinearLeastSquares',...
    'Lower',[-Inf -Inf],...
    'Upper',[Inf Inf],...
    'Startpoint',[1 1]);
  f = fittype('beta*x+gamma','options',s,'independent','x','coefficients',{'gamma','beta'});
  c = fit(x,y,f);
  beta = coeffvalues(c)';
  betaconf = confint(c)';
  var = sqrt(mean((beta(1)+beta(2)*x-y).^2));

  %uncomment if you want to fix gamma
%   beta1=beta;
%   var1=var;
%   s = fitoptions('Method','NonlinearLeastSquares',...
%     'Lower',[0],...
%     'Upper',[Inf],...
%     'Startpoint',[1]);
%   f = fittype('beta*x','options',s,'independent','x','coefficients','beta');
%   c = fit(x,y,f);
%   beta = [0 coeffvalues(c)]';
%   betaconf = [0 0;confint(c)'];
%   var = sqrt(mean((beta(1)+beta(2)*x-y).^2));
%   %disp(['Error gamma=var: ' num2str(var1) ', gamma=0: ' num2str(var)])
%   disp(['beta gamma=var: ' num2str(beta1(2)) ', gamma=0: ' num2str(beta(2))])
  %uncomment if you want to fix beta
%   beta1=beta;
%   var1=var;
%   s = fitoptions('Method','NonlinearLeastSquares',...
%     'Lower',[0],...
%     'Upper',[Inf],...
%     'Startpoint',[1]);
%   f = fittype('0.23*x+gamma','options',s,'independent','x','coefficients','gamma');
%   c = fit(x,y,f);
%   beta = [coeffvalues(c) 0.23]';
%   betaconf = [confint(c)';0 0];
%   var = sqrt(mean((beta(1)+beta(2)*x-y).^2));
%   %disp(['Error gamma=var: ' num2str(var1) ', gamma=0: ' num2str(var)])
%   disp(['gamma beta=var: ' num2str(beta1(1)) ', beta=0.23: ' num2str(beta(1))])
 
else
  error('wrong offsettype')
end

% use this to display the error in the hstop curve (can be also checked
% manually; how far are points form the line?
% disp(['Error in P fit with offset=' num2str(fval), ', beta=' num2str(betaPouliquenWithOffset)])
return
