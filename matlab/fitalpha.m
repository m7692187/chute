%fitted to h/hs
function fitalpha2()
%close all
loadData
global surfaceheight
surfaceheight=5;
%fit(1,'')
fit(7,'')
%fit(5,'')
% fit(2,'layered');
%fit(6,'')
%fit(7,'')
%fit(8,'')
%fit(9,'')
%fit(10,'')
% a_0=1.01
% a_1/2=1.142
% a_2/3=1.201

%print_figures();
return

function fit(Case,opt)
[z, U, Ubar, h, H, a, Alpha] = extractdata(Case,opt);
[jerk, slip, base]=fitvelocity(Case, z, U, h, H, a, Alpha, opt);
[d]=plotjerkbasefitting(Case, z, U, h, H, a, Alpha, jerk, slip, base);
plotjerkbaseplotting(Case, z, U, Ubar, h, H, a, Alpha, jerk, slip, base);
return

%extract velocity data
function [z, U, Ubar, h, H, a, Alpha]=extractdata(Case,opt)

global Data Flow
% all steady unordered flows
ind=find((Flow{Case}.Flowing)&(~Flow{Case}.Accelerating)&(~Flow{Case}.Oscillating));
if exist('opt','var')&strcmp(opt,'layered')
  ind=find((Flow{Case}.Flowing)&(~Flow{Case}.Accelerating)&(Flow{Case}.Oscillating)&(~Flow{Case}.FullyOscillating));
end

h=Flow{Case}.Height(ind);
H=Flow{Case}.H(ind);
a=Flow{Case}.A(ind);
for k=1:length(ind)
    data=Data{Case}.flow{ind(k)};
    s=0;
    ix=data.z>data.Base-s&data.z<data.Surface;
    meanVelocityX = mean(data.VelocityX(ix));
    z{k}=(data.z(ix)-data.Base)/data.FlowHeight;
    U{k}=smooth(data.VelocityX(ix)./meanVelocityX,500/h(k));
    Ubar(k)=meanVelocityX;
end
meanVelocityX=  cellfun(@(data) mean(data.VelocityX(data.z>data.Base&data.z<data.Surface)), Data{Case}.flow(ind));
meanVelocityX2= cellfun(@(data) mean(data.VelocityX(data.z>data.Base&data.z<data.Surface).^2), Data{Case}.flow(ind));
Alpha=meanVelocityX2./(meanVelocityX.^2);

return

function [jerk, slip, base]=fitvelocity(Case, z, U, h, H, a, Alpha,opt)
%fit velocity profiles

figure(100+Case); clf;
set(gcf,'Position',[0 680 560 420])
set(gcf,'FileName',['velocity_fit' num2str(Case,'_%d')])

alpha=zeros(size(a));
jerk=zeros(size(a));
base=zeros(size(a));
slip=zeros(size(a));

Angles=unique(a);
Heights=unique(H);
for k=1:length(a)
    subplot(length(Heights),length(Angles),...
      length(Angles)*(find(Heights==H(k))-1)+find(Angles==a(k))); 
    hold on
    plot(z{k},U{k});
    axis tight; v=axis; axis([0 1 v(3:4)]);

    error=@(vari) norm(veljerk(vari,h(k),z{k},Case)-U{k});
    vari=fminsearch(error,[4]');
    [u b s ub us]=veljerk(vari,h(k),z{k},Case,opt);

    alpha(k)=mean(u(z{k}>0).^2)/mean(u(z{k}>0))^2;
    jerk(k)=0;
    slip(k)=0;
    base(k)=abs(vari(1));
    title(['H=' num2str(H(k)) ',A=' num2str(a(k)) ',b=' num2str(base(k)) ],'Interpreter','none')
    plot(b,ub,'rx');
    plot(s,us,'rx');
    %bar([.5 .7 .9],1.5*[vari(1)*4 vari(2)])
    plot(z{k},u,'r');
    axis([-s/h(k) 1 0 2]);
end
disp(mean(base));
%plot alpha based on velocity fit
figure(110+Case); clf; hold on
set(gcf,'Position',[0 680 560 420])
set(gcf,'FileName',['alpha_ufit' num2str(Case,'_%d')])

plot(a,alpha,'x');
plot(a,Alpha,'or');
xlabel(['alpha, velocity fit, ' num2str(sqrt(var(Alpha-alpha)),2)])
global Title
%disp([Title{Case}(2:end-1) ', alpha=' mat2str([min(alpha) max(alpha)]) ', err=' num2str(sqrt(var(Alpha-alpha)),2)])
return

function [u b s ub us]=veljerk(vari,h,z,Case,opt)
if Case>5
  %disp('Bagnold')
  [u b s ub us]=veljerkBagnold(vari,h,z);
elseif Case==1
  %disp('Log')
  [u b s ub us]=veljerkLog(vari,h,z);
elseif Case==5
  [u b s ub us]=veljerkSquared(vari,h,z);
elseif Case==4
  [u b s ub us]=veljerkSquaredSlip(vari,h,z);
end  
return

function [u b s ub us]=veljerkBagnold(vari,h,z)

%assume no slip
slip=0;

%velocity assuming Bagnold profile
u=(1-(1-z).^1.5);

%set base point to min(4d,.9h)
b=min((h-.1)/h,abs(vari(1))/h);
%base point velocity
ub=(1-(1-b).^1.5);
%base point shear (differentiable)
ab=(3/2)*(1-b).^0.5;
%base shear rate (at z=0)
jerk=(1-1/3*ab)/(2*b);
%correct profile at the base
u(z<b)=ub+ab*(z(z<b)-b).*(1+jerk*(z(z<b)-b));

%surface point 
global surfaceheight %height of surface layer
s=max(b,(h-surfaceheight)/h);
%surface point velocity
us=(1-(1-s).^1.5);
%surface point shear
as=(3/2)*(1-s).^0.5;
%correct profile at the surface to linear
u(z>s)=us+as*(z(z>s)-s);

%correct for slip and mean value
meanu = mean(u-u(1));
minu = u(1);
ub = (ub-minu)./meanu;
us = (us-minu)./meanu;
u = (u-minu)./meanu;
u = slip+(1-slip)*u;
return

function [u b s ub us]=veljerkSquared(vari,h,z)
% jerk=abs(vari(1))*h;
slip=0;
ub=0;
b=0;

%u=(5/3-(1-z).^2);
u=((2-z).*z);

s=max(b,(h-0)/h);
us=(5/3-(1-s).^2);
as=(2)*(1-s).^1;
u(z>s)=us+as*(z(z>s)-s);

meanu = mean(u-u(1));
minu = u(1);
us = (us-minu)./meanu;
u = (u-minu)./meanu;
u = slip+(1-slip)*u;
return

function [u b s ub us]=veljerkSquaredSlip(vari,h,z)
% jerk=abs(vari(1))*h;
slip=0.16;
ub=0;
b=0;

%u=(5/3-(1-z).^2);
u=((2-z).*z);

s=max(b,(h-0)/h);
us=((2-s).*s);
as=(2)*(1-s).^1;
u(z>s)=us+as*(z(z>s)-s);

meanu = mean(u-u(1));
minu = u(1);
us = (us-minu)./meanu;
u = (u-minu)./meanu;
u = slip+(1-slip)*u;
return

function [u b s ub us]=veljerkLog(vari,h,z)
% jerk=abs(vari(1))*h;
slip=1;
ub=0;
b=0;

u=log(z);
u(z<0.5/h)=log(0.5/h);

s=1;
us=0;

meanu = mean(u-u(1));
minu = u(1);
u = (u-minu)./meanu;

u = slip+(1-slip)*u;
return

function [d]=plotjerkbasefitting(Case, z, U, h, H, a, Alpha, jerk, slip, base)
markers='oxd+';
colors='kbrg';

global CoeffHstop Compare
thetas=atand(tanthetastop(CoeffHstop{Case}, h));
aacc=29;
% ahat=((tand(aacc)-tand(thetas))./(tand(a)-tand(thetas))-1); 
% ahats=((tand(a)-tand(thetas))./(tand(aacc)-tand(thetas))); 
ahat=((aacc)-(a))./((a)-(thetas)); 
ahats=(((a)-(thetas))./((aacc)-(thetas))); 
hhs=h./hstop(CoeffHstop{Compare},a);

%plot base based on velocity fit, plus fit it
f=figure(120+Case); hold on
set(gcf,'Position',[0 680 560 420])
set(gcf,'FileName',['b_fit' num2str(Case,'_%d')])
colors='kbrg';
Heights=unique(H);
for k=1:length(Heights),
  ix = (H==Heights(k))&(a>20);
%   subplot(1,2,1);hold on
%   plot(ahat(ix),base(ix),[colors(k) markers(k)],'DisplayName',['$H=' num2str(Heights(k)) '$']);
% xlabel(['$\hat\theta=(\tan(\theta)-\tan(\delta_s^{' lambda '}(h)))/(\tan(\delta_{acc}^{' lambda '})-\tan(\delta_s^{' lambda '}(h))$'])
%   subplot(1,2,2);hold on
  plot(1./hhs(ix),base(ix),[colors(k) markers(k)],'DisplayName',['$H=' num2str(Heights(k)) '$']);
  xlabel(['$h_{stop}/h$'])
end
% set(gca,'YScale','log')
% set(gca,'XScale','log')
axis tight
%set(gca,'YScale','log')
%v=axis; ylim([.1 v(4)])
%ylim([0 5])
% xlim([0 1])

% fitting to exponential
% basefit=@(d) d(1)+d(2)*exp(-d(3)*(a(a>20)-thetas(a>20)));
if (false)
  basefit=@(d) d*ahat(a>21);
  error=@(d) norm(basefit(d)-base(a>21));
  d=fminsearch(error,[10]',optimset('MaxFunEvals',10000));
  basefit=@(d) d*ahat;
  basevar = basefit(d);
  StdErrD = stderr(basevar-base); 
  [A,ix]=sort(ahats);
else
  basefit=@(d) d./hhs(a>21);
  error=@(d) norm(basefit(d)-base(a>21),2);
  d=fminsearch(error,[10]',optimset('MaxFunEvals',10000));
  basefit=@(d) d./hhs;
  basevar = basefit(d);
  StdErrD = stderr(basevar-base); 
  [A,ix]=sort(1./hhs);
end
plot(A,basevar(ix),'k','DisplayName',['$fit$']);
% xlabel(['base, ' num2str(d',3) ', ' num2str(StdErrD,2)])
global Title
lambda=Title{Case}(10:end-1);
ylabel(['$b^{' lambda '}$'])
C=get(gca,'Children');
legend(C);
set(legend,'Location','SouthEast')
v=axis;
axis([0 v(2) 0 v(4)]);

%plot alpha based on jerk-base fit
f=figure(130+Case); hold on
set(gcf,'Position',[0 680 560 420])
set(gcf,'FileName',['alpha_bfit' num2str(Case,'_%d')])
for k=1:length(a)
    u=veljerk([basevar(k)],h(k),z{k},Case);
    alpha(k,1)=mean(u(z{k}>0).^2)/mean(u(z{k}>0))^2;
end
for k=length(Heights):-1:1,
  ix = (H==Heights(k))&(a>20);
  plot(a(ix),alpha(ix),[colors(k) ':']);
  plot(a(ix),Alpha(ix),[colors(k) markers(k)]);
  plot([a(ix) a(ix)]',[Alpha(ix) alpha(ix)]',[colors(k) '-']);
end
StdErrA = stderr(Alpha(a>20)-alpha(a>20)); 
% ylim([1.25 1.80])
% xlabel(['alpha, ' num2str(StdErrA,2)])
xlabel(['$\theta$'])
ylabel('$\alpha$')
axis tight

disp(['d,err: ' num2str(Case) ('   ') num2str(d',4) '   ' num2str(StdErrA)])

if (true)
  figure(140+Case); clf;
  set(gcf,'Position',[0 680 560 420])
  set(gcf,'FileName',['velocity_bfit' num2str(Case,'_%d')])

  Angles=unique(a);
  Heights=unique(H);
  for k=1:length(a)
      subplot(length(Heights),length(Angles),...
        length(Angles)*(find(Heights==H(k))-1)+find(Angles==a(k))); 
      hold on
      plot(z{k},U{k});
      title(['H=' num2str(H(k)) ',A=' num2str(a(k)) ',bfit=' num2str(basevar(k)) ],'Interpreter','none')
      axis tight; v=axis; axis([0 1 v(3:4)]);

      [u b s ub us]=veljerk(basevar(k),h(k),z{k},Case);
      plot(b,ub,'rx');
      plot(s,us,'rx');
      plot(z{k},u,'r');
      axis([-s/h(k) 1 0 2]);
  end
end
return

function [d]=plotjerkbaseplotting(Case, z, U, Ubar, h, H, a, Alpha, jerk, slip, base)
markers='oxd+';
colors='kbrg';

global CoeffHstop Compare
thetas=atand(tanthetastop(CoeffHstop{Case}, h));
aacc=29;
% ahat=((tand(aacc)-tand(thetas))./(tand(a)-tand(thetas))-1); 
% ahats=((tand(a)-tand(thetas))./(tand(aacc)-tand(thetas))); 
ahat=((aacc)-(a))./((a)-(thetas)); 
ahats=(((a)-(thetas))./((aacc)-(thetas))); 
hhs=h./hstop(CoeffHstop{Compare},a);

%plot base based on velocity fit, plus fit it
f=figure(220+Case); hold on
set(gcf,'Position',[0 680 560 420])
set(gcf,'FileName',['b_fit' num2str(Case,'_%d')])
colors='kbrg';
Heights=unique(H);
for k=1:length(Heights),
  ix = (H==Heights(k))&(a>20);
%   subplot(1,2,1);hold on
%   plot(ahat(ix),base(ix),[colors(k) markers(k)],'DisplayName',['$H=' num2str(Heights(k)) '$']);
% xlabel(['$\hat\theta=(\tan(\theta)-\tan(\delta_s^{' lambda '}(h)))/(\tan(\delta_{acc}^{' lambda '})-\tan(\delta_s^{' lambda '}(h))$'])
%   subplot(1,2,2);hold on
  plot(1./hhs(ix),base(ix),[colors(k) markers(k)],'DisplayName',['$H=' num2str(Heights(k)) '$']);
  xlabel(['$h_{stop}/h$'])
end
% set(gca,'YScale','log')
% set(gca,'XScale','log')
axis tight
%set(gca,'YScale','log')
%v=axis; ylim([.1 v(4)])
%ylim([0 5])
% xlim([0 1])

% fitting to exponential
% basefit=@(d) d(1)+d(2)*exp(-d(3)*(a(a>20)-thetas(a>20)));
if (false)
  basefit=@(d) d*ahat(a>21);
  error=@(d) norm(basefit(d)-base(a>21));
  d=fminsearch(error,[10]',optimset('MaxFunEvals',10000));
  basefit=@(d) d*ahat;
  basevar = basefit(d);
  StdErrD = stderr(basevar-base); 
  [A,ix]=sort(ahats);
else
  basefit=@(d) d./hhs(a>21);
  error=@(d) norm(basefit(d)-base(a>21),2);
  d=fminsearch(error,[10]',optimset('MaxFunEvals',10000));
  basefit=@(d) d./hhs;
  basevar = basefit(d);
  StdErrD = stderr(basevar-base); 
  [A,ix]=sort(1./hhs);
end
plot([0; A],[0; basevar(ix)],'k','DisplayName',['$fit$']);
% xlabel(['base, ' num2str(d',3) ', ' num2str(StdErrD,2)])
global Title
lambda=Title{Case}(10:end-1);
ylabel(['$b^{' lambda '}$'])
C=get(gca,'Children');
legend(C);
set(legend,'Location','SouthEast')
v=axis;
axis([0 v(2) 0 v(4)]);

%plot alpha based on jerk-base fit
f=figure(230+Case); hold on
set(gcf,'Position',[0 680 560 420])
set(gcf,'FileName',['alpha_bfit' num2str(Case,'_%d')])
for k=1:length(a)
    u=veljerk([basevar(k)],h(k),z{k},Case);
    alpha(k,1)=mean(u(z{k}>0).^2)/mean(u(z{k}>0))^2;
end
for k=length(Heights):-1:1,
  ix = (H==Heights(k))&(a>20);
  plot(a(ix),alpha(ix),[colors(k) ':']);
  plot(a(ix),Alpha(ix),[colors(k) markers(k)]);
  plot([a(ix) a(ix)]',[Alpha(ix) alpha(ix)]',[colors(k) '-']);
end
StdErrA = stderr(Alpha(a>20)-alpha(a>20)); 
% ylim([1.25 1.80])
% xlabel(['alpha, ' num2str(StdErrA,2)])
xlabel(['$\theta$'],'Interpreter','none')
ylabel('$\alpha$','Interpreter','none')
axis tight

disp(['d,err: ' num2str(Case) ('   ') num2str(d',4) '   ' num2str(StdErrA)])

figure(240+Case); clf;
set(gcf,'Position',[0 680 560 420])
set(gcf,'FileName',['velocityfit' num2str(Case,'_%d')])
hold on
%
colors=lines(7);
i=0;
Angles=unique(a);
Heights=unique(H);
for k=1:length(a)
    if H(k)==20&&any([22 23 24 26 28]==a(k))
      i=i+1;
      plot(z{k},U{k},'Color',colors(i,:),'DisplayName',['$h=' num2str(h(k),3) ', \theta=' num2str(a(k)) '^\circ$']);
      %title(['H=' num2str(H(k)) ',A=' num2str(a(k)) ',bfit=' num2str(basevar(k)) ],'Interpreter','none')
      axis tight; v=axis; axis([0 1 v(3:4)]);

      [u b s ub us]=veljerk(basevar(k),h(k),z{k},Case);
      plot(b,ub,'o','Color',colors(i,:),'MarkerFaceColor',colors(i,:),'HandleVisibility','off');
      plot(s,us,'s','Color',colors(i,:),'MarkerFaceColor',colors(i,:),'HandleVisibility','off');
      %if (a(k)==22) plot(z{k},u,'r'); end
    end
end
xlabel('$z/h$','Interpreter','none')
ylabel('$u/\bar{u}$','Interpreter','none')
axis([0 1 0 2]);
legend('show')
set(legend,'Box','off','Location','SouthEast')

figure(250+Case); clf;
set(gcf,'Position',[0 680 .5*560 .5*420])
set(gcf,'FileName',['velocityfit_inset1' num2str(Case,'_%d')])
hold on
%
colors=lines(7);
i=0;
Angles=unique(a);
Heights=unique(H);
hs=hstop(CoeffHstop{Compare},a);
for k=1:length(a)
    if H(k)==20&&any([22 23 24 26 28]==a(k))
      i=i+1;
      [u b s ub us]=veljerk(basevar(k),h(k),z{k},Case);
      plot(z{k}*h(k)/hs(k),U{k},'Color',colors(i,:),'DisplayName',['$h=' num2str(h(k),3) ', \theta=' num2str(a(k)) '^\circ$']);
      plot(b*h(k)/hs(k),ub,'o','Color',colors(i,:),'MarkerFaceColor',colors(i,:));
    end
end
xlabel('$z/h_{stop}$','Interpreter','none')
ylabel('$u/\bar{u}$','Interpreter','none')
axis([0 .7 0 .7]);
set(gca,'XTick',xlim)
set(gca,'YTick',ylim)

%show that it's Bagnold in the bulk
figure(260+Case); clf;
set(gcf,'Position',[0 680 .5*560 .5*420])
set(gcf,'FileName',['velocityfit_inset2' num2str(Case,'_%d')])
hold on
%
colors=lines(7);
i=0;
Angles=unique(a);
Heights=unique(H);
hs=hstop(CoeffHstop{Compare},a);
for k=1:length(a)
    if H(k)==30&&any([22 23 24 26 28]==a(k))
      i=i+1;
      [u b s ub us]=veljerk(basevar(k),h(k),z{k},Case);
%       plot(z{k}*h(k)/hs(k),U{k},'Color',colors(i,:),'DisplayName',['$h=' num2str(h(k),3) ', \theta=' num2str(a(k)) '^\circ$']);
%       plot(b*h(k)/hs(k),ub,'o','Color',colors(i,:),'MarkerFaceColor',colors(i,:));
      ix = z{k}>b & z{k}<s;
      Shear=diff(U{k})./diff(z{k});
      Shear=smooth(Shear([1 1:end])+Shear([1:end end]),40)/2;
      plot(1-z{k}(ix),Shear(ix),'Color',colors(i,:),'DisplayName',['$h=' num2str(h(k),3) ', \theta=' num2str(a(k)) '^\circ$']);
      plot(1-z{k},Shear,':','Color',colors(i,:),'DisplayName',['$h=' num2str(h(k),3) ', \theta=' num2str(a(k)) '^\circ$']);
      plot(1-b,Shear(min(find(ix))),'o','Color',colors(i,:),'MarkerFaceColor',colors(i,:),'HandleVisibility','off');
      plot(1-s,Shear(max(find(ix))),'s','Color',colors(i,:),'MarkerFaceColor',colors(i,:),'HandleVisibility','off');
    end
end
xlabel('$(h-z)/h$','Interpreter','none')
ylabel('$\partial_z u (h/\bar{u})$','Interpreter','none')
axis tight
xlim([0.1 1])
% ylim([1 max(ylim)])
ylim([.8 10^0.5])
set(gca,'XTick',xlim)
set(gca,'YTick',ylim)
%set(gca,'YTickLabel',{'$10^0$'; '$10^{1/2}$'})
set(gca,'XScale','log')
set(gca,'YScale','log')

%plot triangle
slope=1/2;
dx=-1.3;
dy=+0.1;
loglog(exp([0 1 1 0]+dx),exp([0 0 slope 0]+dy),'k-');
text(exp(1.1+dx),exp(slope/2+dy), num2str(slope))
text(exp(0.5+dx),exp(-0.1+dy), '1')


return