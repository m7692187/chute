function plot_ChuteRheology
close all
load_ChuteRheology

global std mu lambda mub regime muH
%PrincipalStressAngles(std);
%PrincipalStressAngles(mu,struct('local',true));
% PrincipalStressAngles(lambda);
% PrincipalStressAngles(mub);
% PrincipalStressAngles(regime);
%H=cellfun(@(d)d.FlowHeight,muH);
%A=cellfun(@(d)d.ChuteAngle,muH);
%PrincipalStressAngles(muH(H>15&A<30));
%PrincipalStressAngles(muH(H>15&A<30),'local');
% MuPrincipalStressAngles
%LambdaPrincipalStressAngles
% RegimePrincipalStressAngles

fitNu(std)

% global std
%for i=1:length(regime)
%plot_ChuteRheology_all(regime{i})
%end
%RegimeAll

%print_figures();

return

function PrincipalStressAngles(data,options)
if iscell(data) && exist('options','var') && isfield(options,'local')
  %plots local PSA of a data array
  newFigure([data{1}.plotname 'LocalPrincipalStressAngles']);
  for i=1:length(data)
    x = data{i}.InertialParameter; xlabel('$\bar{I}$','Interpreter','none'); 
    FlowAnglexy = cellfun(@(d)d.FlowPrincipalStressAngle(1),data);
    FlowAnglexz = cellfun(@(d)d.FlowPrincipalStressAngle(2),data);
    FlowAngleyz = cellfun(@(d)d.FlowPrincipalStressAngle(3),data);
    y=[...
      ...%data{i}.PrincipalStressAngle(:,1)';...
      data{i}.PrincipalStressAngle(:,2)'+45;...
      ...%data{i}.PrincipalStressAngle(:,3)';...
      ];
    z=data{i}.Pressure';
    plot3(x(data{i}.Bulk),y(data{i}.Bulk),z(data{i}.Bulk));
    scatter3(x(data{i}.Bulk),y(data{i}.Bulk),z(data{i}.Bulk),40*ones(size(y(data{i}.Bulk))),z(data{i}.Bulk), ...
      'DisplayName',{'$\bar\alpha_{yz}$','$\bar\alpha_{xz}+45^\circ$','$\bar\alpha_{xy}$'},...
      'Marker','.', ...
      'UserData',data{i});
  end
  zlabel('')
  %legend('show')
  set(legend,'Location','Best','Box','off')
elseif iscell(data)
  %plot global PSA of a data array
  newFigure([data{1}.plotname 'PrincipalStressAngles']);
  x = cellfun(@(d)d.axis,data)'; xlabel(data{1}.label); 
  y = cellfun(@(d)d.FlowInertialParameter,data)'; ylabel('$\bar{I}$'); 
  %y = cellfun(@(d)d.FixedParticleRadius*2,data)'; ylabel('$\lambda$'); 
  FlowAnglexy = cellfun(@(d)d.FlowPrincipalStressAngle(1),data);
  FlowAnglexz = cellfun(@(d)d.FlowPrincipalStressAngle(2),data);
  FlowAngleyz = cellfun(@(d)d.FlowPrincipalStressAngle(3),data);
  plot3(x,y,[FlowAnglexy;FlowAnglexz+45;FlowAngleyz], ...
    'x','DisplayName',{'$\bar\alpha_{yz}$','$\bar\alpha_{xz}+45^\circ$','$\bar\alpha_{xy}$'},...
    'UserData',data);
  zlabel('')
  %legend('show')
  set(legend,'Location','Best','Box','off')
  view(0,0)
  %view(90,0)
else
  %plots local PSA of a single data set
  newFigure([data.plotname 'PrincipalStressAngles']);
  plot(data.z,[data.PrincipalStressAngle(:,1)'; data.PrincipalStressAngle(:,2)'+45; data.PrincipalStressAngle(:,3)'],...
     'DisplayName',{'$\alpha_{yz}$','$\alpha_{xz}+45^\circ$','$\alpha_{xy}$'})
  plot([data.Base data.Surface],(data.FlowPrincipalStressAngle'+[0 45 0]')*[1 1],...
     'HandleVisibility','off');
  xlabel('$z/d$'); 
  xlim([data.Base data.Surface]);
  legend('show')
  set(legend,'Location','South')
end
dcm_obj = datacursormode(gcf);
set(dcm_obj,'UpdateFcn',@DataTip);
return

function RegimeAll
global regime regimeTitle
colors=lines(length(regime));

for field={'Nu','VelocityX','StressZZ','StressXX','Pressure',{'PrincipalStressAngle',2}}
  if (iscell(field{1})) 
    num=field{1}{2}; field{1}=field{1}{1};
  else
    num=1;
  end
  newFigure(['Regime' field{1}]);
  for i=1:length(regime)
    plot(regime{i}.zhat,regime{i}.(field{1})(:,num), ...
      '-','DisplayName',regimeTitle{i},'Color',colors(i,:))
  end
  xlabel('$z$'); ylabel(field{1}); legend('show'); set(legend,'Location','Best'); xlim([0 1])
end

  newFigure(['RegimeMomentumRate']);
  for i=4
    plot(regime{i}.zhat,regime{i}.MomentumRate.Z, ...
      '-','DisplayName',regimeTitle{i},'Color',colors(i,:))
  end
  xlabel('$z$'); ylabel(field{1}); legend('show'); set(legend,'Location','Best'); xlim([0 1])

return

function fitNu(data)
colors=lines(length(2));

newFigure(['NuFit']);
plot(data.z(data.Bulk),data.Nu(data.Bulk), ...
    'k-','DisplayName',data.plotname)

s = fitoptions('Method','NonlinearLeastSquares',...
'Lower',[0 0    -Inf 0   0],...
'Upper',[1 2*pi  Inf Inf Inf],...
'Startpoint',[.5 0 0 1 1]);
%f = fittype('nu0+alpha*cos(2*pi*(x-xw)/L)*exp(-(x-xw)/x0)','options',s,'independent','x',...
%    'coefficients',{'nu0','xw','alpha','x0','L'});
%c = fit(data.z(data.Bulk),data.Nu(data.Bulk),f);
%save('fitNu.mat','c');
%plot(c)
%CoeffHstop = coeffvalues(c)';


%plot(std.z,NuFit, ...
%    'r-','DisplayName',regimeTitle{i})
xlabel('$z$','Interpreter','none'); 
ylabel('$\nu','Interpreter','none'); 
legend('show'); 
set(legend,'Location','Best'); 
%xlim([0 1])
return


