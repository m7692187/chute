%fitted to h/hs
function fitalpha2()
close all
 fit(2)
% print_figures();
return

function fit(Case)
[z, dU, U, h, H, a, Alpha] = extractdata(Case);
[jerk, slip, base]=fitstrain(Case, z, dU, U, h, H, a, Alpha);
% [d]=plotjerkbasefitting(Case, z, U, h, H, a, Alpha, jerk, slip, base);
return

%extract velocity data
function [z, dU, U, h, H, a, Alpha]=extractdata(Case)

global Data Flow
% all steady unordered flows
ind=find((Flow{Case}.Flowing)&(~Flow{Case}.Accelerating)&(~Flow{Case}.Oscillating));

h=Flow{Case}.Height(ind);
H=Flow{Case}.H(ind);
a=Flow{Case}.A(ind);
for k=1:length(ind)
    data=Data{Case}.flow{ind(k)};
    s=0;
    ix=data.z>data.Base-s&data.z<data.Surface;
    z{k}=(data.z(ix)-data.Base)/data.FlowHeight;
    meanVelocityX = mean(data.VelocityX(ix));
%     U{k}=smooth(data.VelocityX(ix)./meanVelocityX,500/h(k));
    U{k}=data.VelocityX(ix)./meanVelocityX;
    dU{k}=deriv(U{k},z{k});
end
meanVelocityX=  cellfun(@(data) mean(data.VelocityX(data.z>data.Base&data.z<data.Surface)), Data{Case}.flow(ind));
meanVelocityX2= cellfun(@(data) mean(data.VelocityX(data.z>data.Base&data.z<data.Surface).^2), Data{Case}.flow(ind));
Alpha=meanVelocityX2./(meanVelocityX.^2);

return

function [jerk, slip, base]=fitstrain(Case, z, dU, U, h, H, a, Alpha)
%fit velocity profiles

figure(100+Case); clf;
set(gcf,'Position',[0 680 560 420])
set(gcf,'FileName',['velocity_fit' num2str(Case,'_%d')])

alpha=zeros(size(a));
jerk=zeros(size(a));
base=zeros(size(a));
slip=zeros(size(a));

Angles=unique(a);
Heights=unique(H);

b1=0./h;
b2=9./h;
slip=mean(cellfun(@(U)min(U),U));
if (false) %same expo for all
  X=[];
  y=[];
  for k=1:length(a)
      ix=z{k}>b1(k)&z{k}<b2(k);
      X=[X;[ones(size(z{k}(ix))) z{k}(ix)]];
      y=[y; log(dU{k}(ix))];
  end
  for k=1:length(a)
    expo{k} = X\y;
  end
else
  for k=1:length(a)
      ix=z{k}>b1(k)&z{k}<b2(k);
      expo{k} = [ones(size(z{k}(ix))) z{k}(ix)]\log(dU{k}(ix));
  end
end
if (false)
  X=[];
  y=[];
  for k=1:length(a)
      ix2=z{k}>b2(k);
      s2=exp(expo{k}(1)+expo{k}(2)*b2(k));
      X=[X; dU{k}(ix2)-s2];
      y=[y; z{k}(ix2)-b2(k)];
  end
  for k=1:length(a)
    slope(k) = X\y;
  end
else
  for k=1:length(a)
      ix2=z{k}>b2(k);
      s2=exp(expo{k}(1)+expo{k}(2)*b2(k));
      slope(k)= (dU{k}(ix2)-s2)\(z{k}(ix2)-b2(k));
  end
end

for k=1:length(a)
    subplot(length(Heights),length(Angles),...
      length(Angles)*(find(Heights==H(k))-1)+find(Angles==a(k))); 
    hold on
    du=[];
    u=[];
%     ix=z{k}>b1(k)&z{k}<b2(k);
%     expo = [ones(size(z{k}(ix))) z{k}(ix)]\log(dU{k}(ix));
    du=exp(expo{k}(1)+expo{k}(2)*z{k});
    ix2=z{k}>b2(k);
%      s2=exp(expo{k}(1)+expo{k}(2)*b2(k));
%      slope(k) = (z{k}(ix2)-b2(k))\(dU{k}(ix2)-s2);
    du(ix2,1)=s2+slope(k)*(z{k}(ix2)-b2(k));
    u=cumsum(du)*diff(z{k}(1:2));
%     slip=mean(U{k}-u);
    u=u+slip;
%     figure(1); plot(z{k},[dU{k} du]);
%     figure(2); plot(z{k},[U{k} u]);
    plot(z{k},[U{k} u]);
    plot(b2(k),s2+slip,'rx');
    axis tight; v=axis; axis([0 1 0 2]);

%     figure
%     
%     
%     ix
%     error=@(vari) bulkstrain(vari,h(k),z{k},dU{k},Case);
%     vari=fminsearch(error,[1 .1]');
%     [du b s dub dus]=veljerk(vari,h(k),z{k},Case);
% 
% %     alpha(k)=mean(u(z{k}>0).^2)/mean(u(z{k}>0))^2;
%     jerk(k)=0;
%     slip(k)=0;
%     base(k)=abs(vari(1));
    title(['H=' num2str(H(k)) ',A=' num2str(a(k)) ',b=' num2str([expo{k}' slope(k) slip],'%.2f ') ],'Interpreter','none')
end

%plot alpha based on velocity fit
figure(110+Case); clf; hold on
set(gcf,'Position',[0 680 560 420])
set(gcf,'FileName',['alpha_ufit' num2str(Case,'_%d')])

plot(a,alpha,'x');
plot(a,Alpha,'or');
xlabel(['alpha, velocity fit, ' num2str(sqrt(var(Alpha-alpha)))])
return


function [d]=plotjerkbasefitting(Case, z, U, h, H, a, Alpha, jerk, slip, base)
markers='oxd+';
colors='kbrg';

global CoeffHstop
thetas=atand(tanthetastop(CoeffHstop{Case}, h));
aacc=29;
% ahat=((tand(aacc)-tand(thetas))./(tand(a)-tand(thetas))-1); 
% ahats=((tand(a)-tand(thetas))./(tand(aacc)-tand(thetas))); 
ahat=((aacc)-(a))./((a)-(thetas)); 
ahats=(((a)-(thetas))./((aacc)-(thetas))); 
hhs=h./hstop(CoeffHstop{5},a);

%plot base based on velocity fit, plus fit it
f=figure(120+Case); hold on
set(gcf,'Position',[0 680 560 420])
set(gcf,'FileName',['b_fit' num2str(Case,'_%d')])
colors='kbrg';
Heights=unique(H);
for k=1:length(Heights),
  ix = (H==Heights(k))&(a>20);
%   subplot(1,2,1);hold on
%   plot(ahat(ix),base(ix),[colors(k) markers(k)],'DisplayName',['$H=' num2str(Heights(k)) '$']);
% xlabel(['$\hat\theta=(\tan(\theta)-\tan(\delta_s^{' lambda '}(h)))/(\tan(\delta_{acc}^{' lambda '})-\tan(\delta_s^{' lambda '}(h))$'])
%   subplot(1,2,2);hold on
  plot(1./hhs(ix),base(ix),[colors(k) markers(k)],'DisplayName',['$H=' num2str(Heights(k)) '$']);
  xlabel(['$h_{stop}/h$'])
end
% set(gca,'YScale','log')
% set(gca,'XScale','log')
axis tight
%set(gca,'YScale','log')
%v=axis; ylim([.1 v(4)])
%ylim([0 5])
% xlim([0 1])

% fitting to exponential
% basefit=@(d) d(1)+d(2)*exp(-d(3)*(a(a>20)-thetas(a>20)));
if (false)
  basefit=@(d) d*ahat(a>21);
  error=@(d) norm(basefit(d)-base(a>21));
  d=fminsearch(error,[10]',optimset('MaxFunEvals',10000));
  basefit=@(d) d*ahat;
  basevar = basefit(d);
  StdErrD = stderr(basevar-base); 
  [A,ix]=sort(ahats);
else
  basefit=@(d) d./hhs(a>21);
  error=@(d) norm(basefit(d)-base(a>21),2);
  d=fminsearch(error,[10]',optimset('MaxFunEvals',10000));
  basefit=@(d) d./hhs;
  basevar = basefit(d);
  StdErrD = stderr(basevar-base); 
  [A,ix]=sort(1./hhs);
end
plot(A,basevar(ix),'k','DisplayName',['$fit$']);
% xlabel(['base, ' num2str(d',3) ', ' num2str(StdErrD,2)])
global Title
lambda=Title{Case}(10:end-1);
ylabel(['$b^{' lambda '}$'])
C=get(gca,'Children');
legend(C);
set(legend,'Location','SouthEast')
v=axis;
axis([0 v(2) 0 v(4)]);

%plot alpha based on jerk-base fit
f=figure(130+Case); hold on
set(gcf,'Position',[0 680 560 420])
set(gcf,'FileName',['alpha_bfit' num2str(Case,'_%d')])
for k=1:length(a)
    u=veljerk([basevar(k)],h(k),z{k},Case);
    alpha(k,1)=mean(u(z{k}>0).^2)/mean(u(z{k}>0))^2;
end
for k=length(Heights):-1:1,
  ix = (H==Heights(k))&(a>20);
  plot(a(ix),alpha(ix),[colors(k) ':']);
  plot(a(ix),Alpha(ix),[colors(k) markers(k)]);
  plot([a(ix) a(ix)]',[Alpha(ix) alpha(ix)]',[colors(k) '-']);
end
StdErrA = stderr(Alpha(a>20)-alpha(a>20)); 
% ylim([1.25 1.80])
% xlabel(['alpha, ' num2str(StdErrA,2)])
xlabel(['$\theta$'])
ylabel('$\alpha$')
axis tight

disp([num2str(Case) ('   ') num2str(d',4) '   ' num2str(StdErrA)])

if (true)
  figure(140+Case); clf;
  set(gcf,'Position',[0 680 560 420])
  set(gcf,'FileName',['velocity_bfit' num2str(Case,'_%d')])

  Angles=unique(a);
  Heights=unique(H);
  for k=1:length(a)
      subplot(length(Heights),length(Angles),...
        length(Angles)*(find(Heights==H(k))-1)+find(Angles==a(k))); 
      hold on
      plot(z{k},U{k});
      title(['H=' num2str(H(k)) ',A=' num2str(a(k)) ',bfit=' num2str(basevar(k)) ],'Interpreter','none')
      axis tight; v=axis; axis([0 1 v(3:4)]);

      [u b s ub us]=veljerk(basevar(k),h(k),z{k},Case);
      plot(b,ub,'rx');
      plot(s,us,'rx');
      plot(z{k},u,'r');
      axis([-s/h(k) 1 0 2]);
  end
end
return