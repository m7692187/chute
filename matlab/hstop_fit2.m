%used to fit h_stop
function [angle_stop,height_stop, x, angle_stop_exp, x_exp, x_std]=hstop_fit2(leftRange,downRange)
%   amin=min(leftRange(:,2));
%   amax=max(leftRange(:,3));
  amin=0;
  amax=45;
  [x, fval,exitflag,output] = fminsearch(@(x)distance(x,leftRange,downRange),[tand(amin), tand(amax), 5],...
    optimset('MaxFunEvals',5000,'MaxIter',5000));

%uncomment if you want to fix A
% x1=x; fval1=fval; [x, fval,exitflag,output] = fminsearch(@(x)distance([x,5],leftRange,downRange),[tand(0), tand(45)],...
% optimset('MaxFunEvals',5000)); x=[x,5];
% %disp(['Error A=var: ' num2str(fval1) ', A=5: ' num2str(fval)])
% disp(['theta1/2 A=var: ' num2str(atand(x1(1:2))) ', A=5: ' num2str(atand(x(1:2)))])
%uncomment if you want to fix A
% x1=x; fval1=fval; [x, fval,exitflag,output] = fminsearch(@(x)distance([x(1) tand(33) x(2)],leftRange,downRange),[tand(0), 5],...
% optimset('MaxFunEvals',5000)); x=[x(1) tand(33) x(2)];
% disp(['Error A=var: ' num2str(fval1) ', A=5: ' num2str(fval)])
% disp(['theta1/2 t2=var: ' num2str(atand(x1([1 3]))) ', t2=5: ' num2str(atand(x([1 3])))])

  hleftRange=[leftRange(:,1) zeros(size(leftRange,1),2)];
  hdownRange=[zeros(size(downRange,1),1) downRange(:,2:3)];
%   [xmin] = fminsearch(@(x)distance(x,leftRange-0.05*hleftRange,downRange-0.05*hdownRange),[tand(0), tand(45), 5],...
%     optimset('MaxFunEvals',5000));
%   [xmax] = fminsearch(@(x)distance(x,leftRange+0.05*hleftRange,downRange+0.05*hdownRange),[tand(0), tand(45), 5],...
%     optimset('MaxFunEvals',5000));
  for i=1:1
    exitflag=0;
    while (exitflag~=1)
      %prefactor 0.4 in mub paper
      leftPert=0.2*(rand(size(hleftRange))-0.5).*hleftRange;
      downPert=0.2*(rand(size(hdownRange,1),1)*[1 1 1]-0.5).*hdownRange;
      [xpert, dummy,exitflag,dummy] = fminsearch(@(x)distance(x, ...
      leftRange+leftPert, ...
      downRange+downPert), ...
      [tand(0), tand(45), 5], ...
      optimset('MaxFunEvals',5000,'MaxIter',5000));
    end
    xvar(i,:)=xpert-x;
  end
  %mean(xvar)
  x_std=std(xvar);
  %disp(['rel. Error' num2str(x_std./x)]);
  
  %if (size(xvar,1)<50) warning('errorbar in A t1 t2 might not be accurate'); end
  %x
  %height_stop=linspace(-.1+min([leftRange(:,1); downRange(:,2)]),downRange(1,2),60)';
  [x_exp, fval_exp] = fminsearch(@(x)distance_exp(x,leftRange,downRange),[tand(0), tand(45), 5],...
    optimset('MaxFunEvals',5000));
  %disp([fval,fval_exp])

  if (size(leftRange,1)==1 && size(downRange,1)==1)
    a=mean(leftRange(1,2:3));
    x=[tand(a), tand(a), 5];
    x_exp = x;
    x_std = [0 0 0];
  end

  height_stop=linspace(0,55,60)';
  angle_stop=atand(PJ(x, height_stop));
  angle_stop_exp=atand(PJ_exp(x_exp, height_stop));
return

function d = distance(x, leftRange,downRange)
  height=leftRange(:,1);
  amin=leftRange(:,2);
  amax=leftRange(:,3);
  astop=atand(PJ(x,height));
  d=norm(max(amin-astop,0)+max(astop-amax,0));
  
  angle=downRange(:,1);
  hmin=downRange(:,2);
  hmax=downRange(:,3);
  hs=hstop(x,angle);
  d=d+norm(max(hmin-hs,0)+max(hs-hmax,0));
  
%     disp(mat2str([atand(x(1:2)) x(3) d]))
return

function tantheta = PJ(x, height)
  tantheta=(height*x(1)+x(3)*x(2))./(height+x(3));
return

function height = hstop(x, angle)
  height=x(3)*(x(2)-tand(angle))./(tand(angle)-x(1));
return

function d = distance_exp(x, leftRange,downRange)
  height=leftRange(:,1);
  amin=leftRange(:,2);
  amax=leftRange(:,3);
  astop=atand(PJ_exp(x,height));
  d=norm(max(amin-astop,0)+max(astop-amax,0));
  
  angle=downRange(:,1);
  hmin=downRange(:,2);
  hmax=downRange(:,3);
  hstop=hstop_exp(x,angle);
  d=d+norm(max(hmin-hstop,0)+max(hstop-hmax,0));
return

function tantheta = PJ_exp(x, height)
  tantheta=x(1)+(x(2)-x(1))*exp(-height/x(3));
return

function height = hstop_exp(x, angle)
  height=x(3)*log((x(2)-x(1))./(tand(angle)-x(1)));
return