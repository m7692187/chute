function ChuteRheology
close all
clc;
loadData
%
%ChuteRheologyStd
VariationMu
%VariationMuH

%print_figures2();
return

function VariationMu
loadMuData
MuPrincipalStressAngles
MuPrincipalFabricAngles
MuPrincipalKineticStressAngles
MuPrincipalStresses
MuPrincipalFabric
MuPrincipalKineticStresses
return

MuStressAsymmetry

%%figure 9 depth-av stress contributions, function of mu
MuStressContributions
MuKineticStressContributions
MuInertial

%%figure 9 other stuff as function of mu

return

function VariationMuH
loadMuHData
MuHPrincipalAnglesRemco
return

%%figure 9 depth-av stress contributions, function of mu
%MuHInertial
%MuHInertialProfile
%MuHFroude
MuHLocalDensity
MuHLocalFriction

%MuHStressSymmetry

%%figure 9 other stuff as function of mu

return

function loadMuData
%loads the standard case
global mu muTitle muColor muZLim muSteady Data Flow
% Case=9;
% ix = (Flow{7}.H==20)&~Flow{7}.Accelerating;
% mu=Data{Case}.flow(ix);
mu=loadstatistics('../../run7/Lucystats/H30A*L1M0.5B0.5.T200W0_5Lucy.stat');
muZLim=[mu{1}.Base mu{1}.Surface];
for i=1:length(mu)
 mu{i}=EigenvalueAnalysis2(mu{i});
 muTitle{i}=['$tan^{-1}\mu=' num2str(mu{i}.ChuteAngle)  '^\deg$'];
 muZLim=[min(muZLim(1),mu{i}.Base) max(muZLim(2),mu{i}.Surface)];
 mu{i}.zhat=(mu{i}.z-mu{i}.Base)/mu{i}.FlowHeight;
 mu{i}.ShearRate = diff(mu{i}.VelocityX)./diff(mu{i}.z);
 mu{i}.ShearRate(end+1) = mu{i}.ShearRate(end);
 mu{i}.InertialParameter=mu{i}.ShearRate*mu{i}.d./sqrt(mu{i}.StressZZ./mu{i}.ParticleDensity);
 mu{i}.InertialParameterBulk = median(mu{i}.InertialParameter(mu{i}.Density>0)); 
 mu{i}.Bulk = abs(mu{i}.InertialParameter/mu{i}.InertialParameterBulk-1)<0.1&abs(mu{i}.zhat-0.5)<0.5;
end
muColor=jet(length(mu));
muSteady=true(size(mu));
return

function loadMuHData
%loads the standard case
global muH muHTitle muHColor muHStyle muHSteady Data Flow
Case=7;
ix = ~Flow{7}.Accelerating;
muH=Data{Case}.flow(ix);
for i=1:length(muH)
 muH{i}=EigenvalueAnalysis(muH{i});
 muHTitle{i}=['$tan^{-1}\mu=' num2str(muH{i}.ChuteAngle)  '^\deg$'];
 muH{i}.zhat=(muH{i}.z-muH{i}.Base)/muH{i}.FlowHeight;
 FlowRange = [find(muH{i}.zhat>0,1,'first') find(muH{i}.zhat<1,1,'last')];
 muH{i}.FlowShearRate = diff(muH{i}.VelocityX(FlowRange))./diff(muH{i}.z(FlowRange));
 muH{i}.FlowStressZZ = sum(muH{i}.StressZZ.*muH{i}.Density)/sum(muH{i}.Density);
 muH{i}.FlowInertialParameter=muH{i}.FlowShearRate*muH{i}.d./sqrt(muH{i}.FlowStressZZ/muH{i}.ParticleDensity);
 muH{i}.InertialParameterBulk = median(muH{i}.InertialParameter(abs(muH{i}.zhat-0.5)<0.5)); 
 muH{i}.Bulk = abs(muH{i}.InertialParameter/muH{i}.InertialParameterBulk-1)<0.1&abs(muH{i}.zhat-0.5)<0.5;
 %muH{i}.Flow = abs(muH{i}.zhat-0.5)<0.5;
end
color=jet(max(Flow{Case}.A(ix)-19));
muHColor=color(Flow{Case}.A(ix)-19,:);
style={'-.';'--';'-';':';};
muHStyle=style(Flow{Case}.H(ix)/10);
muHSteady=Flow{7}.Steady(ix);
return

function MuDensity
%plots density as a function of Mu
newFigure('MuDensity');
global mu muColor muZLim
for i=1:length(mu)
  plot(mu{i}.zhat,mu{i}.Nu,'Color',muColor(i,:));
end
xlabel('$z/d$'); ylabel('$\rho/\rho_p$')
xlim([0 1]); ylim([0 0.65]);
return

function MuInertial
%plots Inertial as a function of Mu
newFigure('MuInertial');
global mu muColor muZLim
for i=1:length(mu)
  plot(mu{i}.zhat,mu{i}.I,'Color',muColor(i,:));
  plot(mu{i}.zhat(mu{i}.Bulk),mu{i}.I(mu{i}.Bulk),'Color',muColor(i,:),'LineWidth',2);
end
xlabel('$z/h$'); ylabel('$I$')
xlim([0,1]); ylim([0 0.3]);
return

function MuTemperature
%plots Temperature as a function of z
newFigure('MuTemperature');
global mu muColor muZLim muSteady
for i=1:length(mu)
  if (mu{i}.ChuteAngle>20)
    plot(mu{i}.zhat,sqrt(mu{i}.Temperature)./mu{i}.ShearRate,'Color',muColor(i,:));
    %plot(mu{i}.zhat,(mu{i}.Temperature),'Color',muColor(i,:));
    %plot(mu{i}.zhat(mu{i}.Bulk),mu{i}.I(mu{i}.Bulk),'Color',muColor(i,:),'LineWidth',2);
  end
end
xlabel('$z/h$'); ylabel('$T/(\partial_zu_x)$')
xlim([0,1]); ylim([0 2])
return

function MuHInertialProfile
%plots density as a function of Mu
newFigure('MuHInertialProfile');
global muH muHColor 
for i=1:length(muH)
  plot(muH{i}.zhat,muH{i}.InertialParameter,'Color',muHColor(i,:));
  plot(muH{i}.zhat(muH{i}.Bulk),muH{i}.InertialParameter(muH{i}.Bulk),'Color',muHColor(i,:),'LineWidth',2);
  Inertial(i)=...
    sum(muH{i}.InertialParameter(muH{i}.Bulk).*muH{i}.Density(muH{i}.Bulk)) ...
    /sum(muH{i}.Density(muH{i}.Bulk)); %#ok<AGROW>
  plot(0.5,Inertial(i),'o','Color',muHColor(i,:));
  Inertial2(i)=...
    median(muH{i}.InertialParameter(muH{i}.Density>0)); %#ok<AGROW>
  plot(0.4,Inertial2(i),'x','Color',muHColor(i,:));
end
xlabel('$\hat{z}$'); ylabel('$I$')
xlim([0 1]);
return

function MuStressContributions
%plots stress contributions as a function of Mu
newFigure('MuStressContributions');
global mu  
for i=1:length(mu)
  Stress(i,:)=-[...
    sum(mu{i}.StressXX.*mu{i}.Density) ...
    sum(mu{i}.StressYY.*mu{i}.Density) ...
    sum(mu{i}.StressZZ.*mu{i}.Density) ...
    ]/sum(mu{i}.Density)/mu{i}.Gravity(3); %#ok<AGROW>
  ContactStress(i,:)=-[...
    sum(mu{i}.ContactStressXX.*mu{i}.Density) ...
    sum(mu{i}.ContactStressYY.*mu{i}.Density) ...
    sum(mu{i}.ContactStressZZ.*mu{i}.Density) ...
    ]/sum(mu{i}.Density)/mu{i}.Gravity(3); %#ok<AGROW>
end
theta=cellfun(@(d)d.ChuteAngle,mu);
h1=plot(theta,Stress,'x--',...
  'DisplayName',{'$\bar\sigma_{xx}/\cos\theta$','$\bar\sigma_{yy}/\cos\theta$','$\bar\sigma_{zz}/\cos\theta$'});
h2=plot(theta,ContactStress,'x-',...
  'DisplayName',{'$\bar\sigma_{xx}^c/\cos\theta$','$\bar\sigma_{yy}^c/\cos\theta$','$\bar\sigma_{zz}^c/\cos\theta$'});
set([h1;h2],{'Marker'},{'x';'o';'d';'x';'o';'d'})
xlabel('$\theta$');
legend('show')
axis tight
ylim([min(ylim)-diff(ylim) max(ylim)])
set(legend,'Location','Best')
return

function MuKineticStressContributions
%plots stress contributions as a function of Mu
newFigure('MuStressContributions');
global mu muSteady
for i=1:length(mu)
  Stress(i,:)=[...
    sum(mu{i}.KineticStressXX.*mu{i}.Density) ...
    sum(mu{i}.KineticStressYY.*mu{i}.Density) ...
    sum(mu{i}.KineticStressZZ.*mu{i}.Density) ...
    ]/sum(mu{i}.Density); %#ok<AGROW>
end
theta=cellfun(@(d)d.ChuteAngle,mu);
h=plot(theta(muSteady),Stress(muSteady,:),'-',...
  'DisplayName',{'$\bar\sigma_{xx}^k$','$\bar\sigma_{yy}^k$','$\bar\sigma_{zz}^k$'});
set([h],{'Marker'},{'x';'o';'d'})
xlabel('$\theta$');
legend('show')
set(gca,'YScale','Log')
set(legend,'Location','NorthWest')
axis tight
return

function MuInertialBulk
newFigure('MuInertialBulk');
global mu 
for i=1:length(mu)
  Inertial(i,:)=...
    sum(mu{i}.InertialParameter(mu{i}.Density>0).*mu{i}.Density(mu{i}.Density>0)) ...
    /sum(mu{i}.Density); %#ok<AGROW>
end
theta=cellfun(@(d)d.ChuteAngle,mu);
plot(theta,Inertial,'-x',...
  'DisplayName',{'$I$'});
xlabel('$\theta$');
legend('show')
set(legend,'Location','Best')
axis tight
return

function MuHInertial
%plots inertial number as a function of Mu
newFigure('MuHInertial');
global muH 
for i=1:length(muH)
  Inertial(i,:)=...
    sum(muH{i}.InertialParameter(muH{i}.Density>0).*muH{i}.Density(muH{i}.Density>0)) ...
    /sum(muH{i}.Density); %#ok<AGROW>
  plot(muH{i}.ChuteAngle,muH{i}.InertialParameter(muH{i}.Bulk)','r-',...
   'HandleVisibility','off');
end
theta=cellfun(@(d)d.ChuteAngle,muH);
plot(theta,Inertial,'x',...
  'DisplayName',{'$\bar{I}$'});
plot(theta,cellfun(@(d)d.FlowInertialParameter,muH),'.',...
  'DisplayName',{'$I_{flow}$'});
%  'DisplayName',{'$\bar{\partial_zu_x}d/\sqrt{\bar{\sigma_{zz}}\rho_p}$'});
global CoeffHstop Flow
beta = Flow{7}.betaPO(2);
plot(unique(theta),2.5*beta*muH{i}.d/sqrt(0.58)./hstop(CoeffHstop{7},unique(theta)),'-',...
  'DisplayName',{'$I_{theory}$'});
%plot(unique(theta),hstop(CoeffHstop{7},unique(theta)),'-',...
%  'DisplayName',{'$I_{theory}$'});
xlabel('$\theta$');
legend('show')
set(legend,'Location','Best')
axis tight
return

function MuHFroude
%plots Froude number as a function of h/hs
newFigure('MuHFroude');
global muH 
theta=cellfun(@(d)d.ChuteAngle,muH);
Froude=cellfun(@(d)d.Froude,muH);
Height=cellfun(@(d)d.FlowHeight,muH);
global CoeffHstop Flow
Hs=hstop(CoeffHstop{7},theta);
plot(Height./Hs,Froude,'o','Displayname','Simulations');
plot([0; sort(Height./Hs)],[0; Flow{7}.betaP*sort(Height./Hs)],'-','Displayname','Fit');
xlabel('$h/h_{stop}$');
ylabel('$F$');
legend('show')
set(legend,'Location','Best')
axis tight
return

function MuHStressSymmetry
%plots contributions to downward stress of standard case
newFigure('MuHStressSymmetry');
global muH
for j=1:length(muH)
for i=1:length(muH{j}.z)
   Stress=[...
    muH{j}.StressXX(i) muH{j}.StressXY(i) muH{j}.StressXZ(i);...
    muH{j}.StressYX(i) muH{j}.StressYY(i) muH{j}.StressYZ(i);...
    muH{j}.StressZX(i) muH{j}.StressZY(i) muH{j}.StressZZ(i);...
    ];
   TangentialStress=[...
    muH{j}.TangentialStressXX(i) muH{j}.TangentialStressXY(i) muH{j}.TangentialStressXZ(i);...
    muH{j}.TangentialStressYX(i) muH{j}.TangentialStressYY(i) muH{j}.TangentialStressYZ(i);...
    muH{j}.TangentialStressZX(i) muH{j}.TangentialStressZY(i) muH{j}.TangentialStressZZ(i);...
    ];
    TangStressRatio(i)=norm(TangentialStress)/norm(Stress);
end
plot(muH{j}.zhat,TangStressRatio,'r',...
  'DisplayName',{'$|\sigma_{xz}^{asym}|/|\sigma_{xz}|$'});
end
xlabel('$z/d$'); 
axis tight; xlim([0 1]); ylim([0 0.1])
legend('show')
set(legend,'Location','Best')
return

function MuStressAsymmetry
%plots contributions to downward stress of standard case
newFigure('MuStressAsymmetry');
global mu
for i=1:length(mu)
%    S=[...
%     sum(mu{i}.StressXX.*mu{i}.Density) ...
%     sum(mu{i}.StressXY.*mu{i}.Density) ...
%     sum(mu{i}.StressXZ.*mu{i}.Density); ...
%     sum(mu{i}.StressYX.*mu{i}.Density) ...
%     sum(mu{i}.StressYY.*mu{i}.Density) ...
%     sum(mu{i}.StressYZ.*mu{i}.Density); ...
%     sum(mu{i}.StressZX.*mu{i}.Density) ...
%     sum(mu{i}.StressZY.*mu{i}.Density) ...
%     sum(mu{i}.StressZZ.*mu{i}.Density) ...
%    ]/sum(mu{i}.Density);
   j=100;
    S=[
     mu{i}.StressXX(j) ...
     mu{i}.StressXY(j) ...
     mu{i}.StressXZ(j); ...
     mu{i}.StressYX(j) ...
     mu{i}.StressYY(j) ...
     mu{i}.StressYZ(j); ...
     mu{i}.StressZX(j) ...
     mu{i}.StressZY(j) ...
     mu{i}.StressZZ(j)];
   %SD=S-eye(3)*sum(diag(S))/3;
   J2(i)=sqrt(( (S(1,1)-S(2,2))^2 + (S(2,2)-S(3,3))^2 + ...
     (S(3,3)-S(1,1))^2 + 6*( S(1,2)^2 + S(1,3)^2 +S(2,3)^2 ) )/2);
   k2(i)=J2(i)/(sum(diag(S))/3);
end
plot(cellfun(@(d)d.ChuteAngle,mu),k2,'rx',...
    'DisplayName',{'$k2$'});
legend('show')
set(legend,'Location','Best')
return

function MuHLocalDensity
%plots density as a function of I
newFigure('MuLocalDensity');
global muH muHColor muHZLim
for i=1:length(muH)
  %surface
  ix=~muH{i}.Bulk&muH{i}.z>mean(muH{i}.z);
  plot(muH{i}.I(ix),muH{i}.Nu(ix),'-','Color',1-0.5*muHColor(i,:));
end
for i=1:length(muH)
  ix=muH{i}.Bulk;
  plot(muH{i}.I(ix),muH{i}.Nu(ix),'.','Color',muHColor(i,:));
end
for i=1:length(muH)
  FlowNu=sum(muH{i}.Nu,1)*diff(muH{i}.z(1:2))/muH{i}.FlowHeight;
  plot(muH{i}.FlowInertialParameter,FlowNu,'o');
end
xlabel('$I$'); ylabel('$\rho/\rho_p$')
xlim([0 0.8]); %ylim([0.4 0.6]);
return

function MuHLocalFriction
%plots friction as a function of I
newFigure('MuLocalFriction');
global muH muHColor muHZLim
% for i=1:length(muH)
%   ix=~muH{i}.Bulk&muH{i}.z>mean(muH{i}.z);
%   plot(muH{i}.I(ix),-muH{i}.StressXZ(ix)./muH{i}.StressZZ(ix),'-','Color',1-0.5*muHColor(i,:));
% end
for i=1:length(muH)
  ix=muH{i}.Bulk;
  plot(muH{i}.I(ix),-muH{i}.StressXZ(ix)./muH{i}.StressZZ(ix),'.','Color',muHColor(i,:));
  plot(muH{i}.FlowInertialParameter,tand(muH{i}.ChuteAngle),'o');
end
xlabel('$I$'); ylabel('$\mu$')
axis tight; xlim([0 0.4]); %ylim([0.4 0.6]);
return

function MuPrincipalStressAngles
newFigure('MuPrincipalStressAngles');
global mu
ChuteAngle = cellfun(@(d)d.ChuteAngle,mu)';
FlowAnglexy = cellfun(@(d)d.FlowPrincipalStressAngle(1),mu)';
FlowAnglexz = cellfun(@(d)d.FlowPrincipalStressAngle(2),mu)';
FlowAngleyz = cellfun(@(d)d.FlowPrincipalStressAngle(3),mu)';
plot((ChuteAngle),[FlowAnglexy;FlowAnglexz+45;FlowAngleyz], ...
  'x-','DisplayName',{'$\bar\alpha_{yz}$','$\bar\alpha_{xz}+45^\circ$','$\bar\alpha_{xy}$'})
xlabel('$\theta$'); ylabel('')
legend('show')
set(legend,'Location','Best')
return

function MuPrincipalStresses
newFigure('MuPrincipalStresses');
global mu
ChuteAngle = cellfun(@(d)d.ChuteAngle,mu)';
FlowLambda1 = cellfun(@(d)d.FlowPrincipalStressLambda(1)/mean(d.FlowPrincipalStressLambda),mu)';
FlowLambda2 = cellfun(@(d)d.FlowPrincipalStressLambda(2)/mean(d.FlowPrincipalStressLambda),mu)';
FlowLambda3 = cellfun(@(d)d.FlowPrincipalStressLambda(3)/mean(d.FlowPrincipalStressLambda),mu)';
plot((ChuteAngle),[FlowLambda1;FlowLambda2;FlowLambda3], ...
  'x-','DisplayName',{'$\bar\hat\lambda_1$','$\bar\lambda_2$','$\bar\lambda_3$'})
xlabel('$\theta$'); ylabel('')
legend('show')
set(legend,'Location','Best')
return

function MuPrincipalFabricAngles
newFigure('MuPrincipalFabricAngles');
global mu
ChuteAngle = cellfun(@(d)d.ChuteAngle,mu)';
FlowAnglexy = cellfun(@(d)d.FlowPrincipalFabricAngle(1),mu)';
FlowAnglexz = cellfun(@(d)d.FlowPrincipalFabricAngle(2),mu)';
FlowAngleyz = cellfun(@(d)d.FlowPrincipalFabricAngle(3),mu)';
plot((ChuteAngle),[FlowAnglexy;FlowAnglexz+45;FlowAngleyz], ...
  'x-','DisplayName',{'$\bar\alpha_{yz}$','$\bar\alpha_{xz}+45^\circ$','$\bar\alpha_{xy}$'})
xlabel('$\theta$'); ylabel('')
legend('show')
set(legend,'Location','Best')
return

function MuPrincipalKineticStressAngles
newFigure('MuPrincipalKineticStressAngles');
global mu
ChuteAngle = cellfun(@(d)d.ChuteAngle,mu)';
FlowAnglexy = cellfun(@(d)d.FlowPrincipalKineticStressAngle(1),mu)';
FlowAnglexz = cellfun(@(d)d.FlowPrincipalKineticStressAngle(2),mu)';
FlowAngleyz = cellfun(@(d)d.FlowPrincipalKineticStressAngle(3),mu)';
plot((ChuteAngle),[FlowAnglexy;FlowAnglexz+45;FlowAngleyz], ...
  'x-','DisplayName',{'$\bar\alpha_{yz}$','$\bar\alpha_{xz}+45^\circ$','$\bar\alpha_{xy}$'})
xlabel('$\theta$'); ylabel('')
legend('show')
set(legend,'Location','Best')
return

function MuPrincipalKineticStresses
newFigure('MuPrincipalKineticStresses');
global mu
ChuteAngle = cellfun(@(d)d.ChuteAngle,mu)';
FlowLambda1 = cellfun(@(d)d.FlowPrincipalKineticStressLambda(1)/mean(d.FlowPrincipalKineticStressLambda),mu)';
FlowLambda2 = cellfun(@(d)d.FlowPrincipalKineticStressLambda(2)/mean(d.FlowPrincipalKineticStressLambda),mu)';
FlowLambda3 = cellfun(@(d)d.FlowPrincipalKineticStressLambda(3)/mean(d.FlowPrincipalKineticStressLambda),mu)';
plot((ChuteAngle),[FlowLambda1;FlowLambda2;FlowLambda3], ...
  'x-','DisplayName',{'$\bar\hat\lambda_1$','$\bar\lambda_2$','$\bar\lambda_3$'})
xlabel('$\theta$'); ylabel('')
legend('show')
set(legend,'Location','Best')
return

function MuPrincipalFabric
newFigure('MuPrincipalFabric');
global mu
ChuteAngle = cellfun(@(d)d.ChuteAngle,mu)';
FlowLambda1 = cellfun(@(d)d.FlowPrincipalFabricLambda(1)/mean(d.FlowPrincipalFabricLambda),mu)';
FlowLambda2 = cellfun(@(d)d.FlowPrincipalFabricLambda(2)/mean(d.FlowPrincipalFabricLambda),mu)';
FlowLambda3 = cellfun(@(d)d.FlowPrincipalFabricLambda(3)/mean(d.FlowPrincipalFabricLambda),mu)';
plot((ChuteAngle),[FlowLambda1;FlowLambda2;FlowLambda3], ...
  'x-','DisplayName',{'$\bar\hat\lambda_1$','$\bar\lambda_2$','$\bar\lambda_3$'})
xlabel('$\theta$'); ylabel('')
legend('show')
set(legend,'Location','Best')
return

function MuHPrincipalAngles
newFigure('MuHPrincipalAngles');
global muH
Height = cellfun(@(d)d.FlowHeight,muH)';
ChuteAngle = cellfun(@(d)d.ChuteAngle,muH(Height>15))';
FlowAnglexy = cellfun(@(d)d.FlowAnglexy,muH(Height>15))';
FlowAnglexz = cellfun(@(d)d.FlowAnglexz,muH(Height>15))';
FlowAngleyz = cellfun(@(d)d.FlowAngleyz,muH(Height>15))';
plot((ChuteAngle),[FlowAnglexy;FlowAnglexz+45;FlowAngleyz],'x', ...
  'DisplayName',{'$\bar\alpha_{yz}$','$\bar\alpha_{xz}+45^\circ$','$\bar\alpha_{xy}$'})
xlabel('$\theta$'); ylabel('')
legend('show')
%xlim([0,1]); 
ylim([-2 2])
return