function NonLocalStress2()
clc
mu=linspace(0,1,400)
plot(getInertial(mu),mu);
return

function I = getInertial(mu)
rhop = 6/pi;
d=1;
gravity = 1;
rho = 0.6 * rhop;
P = rho * gravity * d *100;
mu2=0.62;

I0=0.26;
beta=3.3;

for i=1:100
  g=I0*sqrt(P/rhop)/d*(-1+exp(-mu2+mu)*sqrt(pi)./sqrt(beta.*(mu2-mu)) ...
    -exp(-mu2-mu)*sqrt(pi)./sqrt(beta.*(mu2+mu)));
  I=g*d/sqrt(P/rhop);
  beta=3.3+9*I./(.26+I);
end
return
