function NonLocalStressKernel()
% this function plots velocity profiles for granular chute flows based on
% Pouliquen-Forterre's non-local rheology
clc
close all

hl=[1 2 5 7.5 10];
for hi=1:length(hl); h=hl(hi);
tl=20;
for ti=1:length(tl); theta=tl(ti);
  [z,shearrate] = getShearrate(h,theta);
  figure(1); hold on;
  plot(z-h,shearrate,'Color',[0 0 0]);
  xlabel('z-h'); ylabel('\dot{\gamma}','Interpreter','none');
  figure(2); hold on;
  plotKernel(z-h,shearrate,theta);
  xlabel('z-h'); ylabel('\int k(z,z'') dz''');
end
end
order_figures();
return

function [g] = plotKernel(z,shearrate,theta)
h=max(z);
global mu2 I0 beta

rhop = 6/pi;
d=1;
gravity = 1;
rho = 0.6 * rhop;
P = rho * gravity * cosd(theta) * (h+d-z);
tau = tand(theta) * P;

for i=1:length(z)
  kernel =  1./(1+shearrate(i)*d/I0/sqrt(P(i)/rhop)) .*( ...
    exp(-(mu2*P(i)-tau(i))*(1+beta.*(z(i)-z).^2/d^2)./P) - ...
    exp(-(mu2*P(i)+tau(i))*(1+beta.*(z(i)-z).^2/d^2)./P) ...
    );
  kernelS =  ( ...
    exp(-(mu2*P(i)-tau(i))*(1+beta.*(z(i)-z).^2/d^2)./P) - ...
    exp(-(mu2*P(i)+tau(i))*(1+beta.*(z(i)-z).^2/d^2)./P) ...
    );
  if i==ceil(length(z)/2)
     plot(z-h,kernel,'Color',.5*[1 1 1],'DisplayName',['k(' num2str(z(i)) ',z)']);
     hold on
  end
  kernelInt(i) = sum(kernel)*diff(z(1:2));
  kernelSInt(i) = sum(kernelS)*diff(z(1:2));
end
%legend('show')
%legend('Location','SouthWest')
plot(z-h,kernelInt,'DisplayName','\int k(z,z'') dz''');
plot(z-h,kernelSInt,'r--','DisplayName','\int k_s(z,z'') dz''');
return
