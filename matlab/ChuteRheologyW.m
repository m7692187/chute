%create all plots needed for the flow rule paper
function ChuteRheologyW
%clc
close all
load_data();
plot_w();
plot_t();
%print_figures();
return

%% loads data for w
function load_data()
global CGdata Wdata Tdata CGtitle Wtitle Ttitle
Wdata=[];
if true %isempty(Wdata)
  if true %~exist('w.mat','file') %make true if new stat files are added
    CGdata=loadstatistics('../w/CG/H3*.stat');
    %Wdata=loadstatistics('../w/W/H3*.stat');
    Wdata=loadstatistics('../../run7/LucyW/H30A26*.stat');
    Tdata=loadstatistics('../w/T/H3*.stat');
    W=cellfun(@(data)data.w,Wdata);
    T=cellfun(@(data)str2double(data.name(strfind(data.name,'.T')+2:strfind(data.name,'W')-1)),Tdata);
    %sort lists 
    [val idx]=sort(W);
    Wdata=Wdata(idx);
    [val idx]=sort(T);
    Tdata=Tdata(idx);
    save w.mat Wdata Tdata
  else
    load w.mat
  end
  
  CGtitle=cell(size(CGdata));
  for i=1:length(CGdata)
    CGtitle{i}=CGdata{i}.name(strfind(CGdata{i}.name,'W')+5:end);
  end

  Wtitle=cell(size(Wdata));
  for i=1:length(Wdata)
    Wtitle{i}=['$w=' num2str(Wdata{i}.w) '$'];
  end

  T=cellfun(@(data)str2double(data.name(strfind(data.name,'.T')+2:strfind(data.name,'W')-1)),Tdata);
  Ttitle=cell(size(Tdata));
  for i=1:length(Tdata)
%     Ttitle{i}=['$t=' num2str(diff(Tdata{i}.time),3) '$'];
    Ttitle{i}=['$\Delta t=' num2str(T(i),3) '$'];
  end
else
  disp('using already loaded data')
end
return

%% plots w dependence
function plot_w()
%load Wdata
global Wdata Wtitle

%new figure
figure(); clf; hold on
set(gcf,'Position',[0  680 560 420])
set(gcf,'FileName','w_dependence_nu')
colors=lines(length(Wdata)+1);

%Wlist=[2 5 6 8];
Wlist=[1:length(Wdata)];
for i=Wlist
  h=plot(Wdata{i}.z,Wdata{i}.Nu,'Color',colors(i,:));
  if (i==5); set(h,'LineWidth',2); end
  if (i==6); set(h,'LineWidth',2); end
end
legend(Wtitle(Wlist))
set(legend,'Box','off')
set(gca,'YLim',[0.45 0.7])
xlabel('$z$')
ylabel('$\rho/\rho_p$')
return

%% plots time dependence
function plot_t()
%load Wdata
global Tdata Ttitle

%new figure
figure(); clf; hold on
set(gcf,'Position',[0 0 560 420])
set(gcf,'FileName','t_dependence_nu')
colors=lines(length(Tdata));

Tlist=3:length(Tdata);
for i=Tlist
  plot(Tdata{i}.z,Tdata{i}.Nu,'Color',colors(i,:));
end
legend(Ttitle(Tlist),'Interpreter','none')
set(gca,'YLim',[0.4 0.7])
xlabel('$z$','Interpreter','none')
ylabel('$\rho/\rho_p$','Interpreter','none')

figure();
set(gcf,'Position',[0 0 560 420])
set(gcf,'FileName','t_dependence_momeq')

%plot Remainder
Tlist=1:length(Tdata);
RemainderNorm=[];
for i=Tlist
    StressZ = [...
      Tdata{i}.StressXZ ...
      Tdata{i}.StressYZ ...
      Tdata{i}.StressZZ ...
      ];
    dZStressZ = diff(StressZ,1)/diff(Tdata{i}.z(1:2));
    dZStressZ = [Tdata{i}.TractionX Tdata{i}.TractionY Tdata{i}.TractionZ] ...
      +(dZStressZ([1:end end],:)+dZStressZ([1 1:end],:))/2;

    MassFlow = [...
      Tdata{i}.Density.*Tdata{i}.VelocityX ...
      Tdata{i}.Density.*Tdata{i}.VelocityY ...
      Tdata{i}.Density.*Tdata{i}.VelocityZ ...
      ];
    dZMassFlow = diff(MassFlow,1)/diff(Tdata{i}.z(1:2));
    dZMassFlow = (dZMassFlow([1:end end],:)+dZMassFlow([1 1:end],:))/2;

    MaterialDerivative = (Tdata{i}.VelocityZ*ones(size(Tdata{i}.Gravity))).*dZMassFlow;
    Weight=Tdata{i}.Density*Tdata{i}.Gravity;
    Remainder=dZStressZ-Weight-MaterialDerivative;
    RemainderNorm(end+1) = sqrt(sum(sum((Remainder(Tdata{i}.z>2,:).^2)))*diff(Tdata{i}.z([1 2]))); %#ok<AGROW>
    plot(Tdata{i}.z,Remainder,'-k','Color',colors(i,:)); hold on
  %   semilogy(Tdata{i}.z,sqrt(Remainder.^2),'-k','Color',colors(i,:)); hold on
  %   set(gca,'Ylim',[1e-5 1])

end
legend(Ttitle(Tlist),'Interpreter','none','Location','Best')
axis tight 
%title('note: small error from cutoff')
%plot remainder norm
figure();
set(gcf,'Position',[0 0 560 260])
set(gcf,'FileName','t_dependence_momeq2')

T=cellfun(@(data)str2double(data.name(max(strfind(data.name,'T'))+1:strfind(data.name,'W')-1)),Tdata(Tlist));
loglog(T,RemainderNorm,':ko'); hold on
axis tight
%plot triangle
n=3;
slope=1;
dx=0;
dy=-.3;
loglog(T(n)*exp(dx-[0 1 1 0]),RemainderNorm(n)*exp(dy+[0 0 slope 0]),'k-'); hold off
text(T(n)*exp(dx-1.3),RemainderNorm(n)*exp(dy+0.5*slope), num2str(slope))
text(T(n)*exp(dx-0.5),RemainderNorm(n)*exp(dy-0.5), '1')

% title('note: small error from cutoff')
xlabel('$T$','Interpreter','none')
ylabel('$r$')

% title('Error in momentum equation','Interpreter','none')
return

