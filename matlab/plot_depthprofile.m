function plot_depthprofile(CaseList,HList,AList,fignum,Var)
global Data Flow Title
if isempty(fignum)
  figure();
else
  figure(fignum); 
end
clf; hold on;
set(gcf,'Position',[0  680 560 420])

set(gcf,'FileName',['depthprofile_' Var num2str(CaseList,'_%d') '_H' num2str(HList,'_%d')])

for Case=CaseList
if isempty(HList), HList = unique(Flow{Case}.H)'; end
if isempty(AList), AList = unique(Flow{Case}.A)'; end
for H=HList
for A=AList
k=find(Flow{Case}.A==A&Flow{Case}.H==H);
if ~isempty(k)
  c=get(gca,'Children');
  colors=mylines(length(c)+1);
  color=colors(end,:);
  if strcmp(Var,'VelocityX')
    h=plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.VelocityX);
    xlabel('$z/h$','Interpreter','none')
    ylabel('$u$','Interpreter','none')
    axis tight; v=axis; axis([0 1 v(3:4)]);
  elseif strcmp(Var,'Alpha')
    data=Data{Case}.flow{k};
    meanVelocityX=  mean(data.VelocityX(data.z>data.Base&data.z<data.Surface));
    meanVelocityX2= mean(data.VelocityX(data.z>data.Base&data.z<data.Surface).^2);
    Alpha=meanVelocityX2./(meanVelocityX.^2);
    h=plot(Flow{Case}.A(k),Alpha,'o');
    xlabel('$z/h$','Interpreter','none')
    ylabel('$v_x$','Interpreter','none')
    axis tight; %v=axis; axis([0 1 v(3:4)]);
  elseif strcmp(Var,'Momentum')
    h=plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.MomentumZ,Data{Case}.flow{k}.VelocityX./meanVelocityX);
    xlabel('$z/h$','Interpreter','none')
    ylabel('$v_x$','Interpreter','none')
    axis tight; v=axis; axis([0 1 v(3:4)]);
  elseif strcmp(Var,'VelocityProfile')
    meanVelocityX = mean(Data{Case}.flow{k}.VelocityX(Data{Case}.flow{k}.z>0&Data{Case}.flow{k}.z<Data{Case}.flow{k}.FlowHeight));
    h=plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.VelocityX./meanVelocityX);
    xlabel('$z/h$','Interpreter','none')
    ylabel('$v_x$','Interpreter','none')
    axis tight; v=axis; axis([0 1 v(3:4)]);
  elseif strcmp(Var,'StrainProfile')
    meanVelocityX = max(Data{Case}.flow{k}.VelocityX(Data{Case}.flow{k}.z>0&Data{Case}.flow{k}.z<Data{Case}.flow{k}.FlowHeight));
    h=plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,...
       deriv(Data{Case}.flow{k}.VelocityX./meanVelocityX,Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight));
%      1-Data{Case}.flow{k}.StressXZ./min(Data{Case}.flow{k}.StressXZ));
    xlabel('$1-\hat\sigma_{xz}$','Interpreter','none')
    ylabel('$\hat{\partial_zv_x}$','Interpreter','none')
    axis tight; v=axis; axis([0 1 0 2.5]);
  elseif strcmp(Var,'logVelocityProfile')
    meanVelocityX = mean(Data{Case}.flow{k}.VelocityX(Data{Case}.flow{k}.z>0&Data{Case}.flow{k}.z<Data{Case}.flow{k}.FlowHeight));
    h=plot(1-Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,max(Data{Case}.flow{k}.VelocityX./meanVelocityX)-Data{Case}.flow{k}.VelocityX./meanVelocityX);
    xlabel('$1-z/h$','Interpreter','none')
    ylabel('$v_x$','Interpreter','none')
    axis tight; v=axis; axis([0 1 v(3:4)]);
  elseif strcmp(Var,'StressZZ')
    h=plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.StressZZ,...
      '-','Color',color);
    xlabel('$z/h$','Interpreter','none')
    ylabel('$\sigma_{zz}$','Interpreter','none')
    axis tight; v=axis; axis([-0.1 1 v(3:4)]);
  elseif strcmp(Var,'DynamicStress')
    h=plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.StressXZ,...
      '-','Color',color);
    hold on
    h=plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.KineticStressXZ,...
      ':','Color','r');
    xlabel('$z/h$','Interpreter','none')
    ylabel('$\sigma_{zz}$','Interpreter','none')
    axis tight; v=axis; axis([-0.1 1 v(3:4)]);
  elseif strcmp(Var,'Mu')
    ind = Data{Case}.flow{k}.z<Data{Case}.flow{k}.FlowHeight;
    h=plot(Data{Case}.flow{k}.z(ind)./Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.StressXZ(ind)./Data{Case}.flow{k}.StressZZ(ind)+tand(Data{Case}.flow{k}.ChuteAngle),...
      '-','Color',color);
    xlabel('$z/h$','Interpreter','none')
    ylabel('$\mu-\tan\theta$','Interpreter','none')
    axis tight; %xlim([-0.1 1]);
  elseif strcmp(Var,'Stress')
    plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,...
...%      (Data{Case}.flow{k}.Nu*[1 1 1 1]).*...
      [Data{Case}.flow{k}.StressXX Data{Case}.flow{k}.StressYY Data{Case}.flow{k}.StressZZ -Data{Case}.flow{k}.StressXZ ],...
      '-','DisplayName',{'\sigma_{xx}','\sigma_{yy}','\sigma_{zz}','-\sigma_{xz}',});
    h=plot([0],[0])
    xlabel('$z/h$','Interpreter','none')
    ylabel('$\sigma_{zz}$','Interpreter','none')
    axis tight; v=axis; axis([-0.1 1 v(3:4)]);
  elseif strcmp(Var,'Ene')
    MeanEneEla=mean(Data{Case}.flow{k}.Ene.Ela(round(end/2):end-1));
    %MeanEneEla=1;%mean(Data{Case}.flow{k}.EneShort.Kin(end-1));
    %h=plot(Data{Case}.flow{k}.EneShort.Time(2:end-1),Data{Case}.flow{k}.EneShort.Kin(2:end-1)./MeanEneEla);
    h=plot(Data{Case}.flow{k}.Ene.Time(2:end-1),Data{Case}.flow{k}.Ene.Kin(2:end-1)./MeanEneEla);
    %axis tight; v=axis; axis([v(1:3) min(v(4),2*v(3))]);
    xlabel('$t$','Interpreter','none')
    ylabel('$E_{kin}/\langle E_{ela}\rangle$','Interpreter','none')
    axis tight;
  else
    h=plot(Data{Case}.flow{k}.z./Data{Case}.flow{k}.FlowHeight,Data{Case}.flow{k}.Nu);
    xlabel('$z/h$','Interpreter','none')
    ylabel('$\rho/\rho_p$','Interpreter','none')
    axis tight; v=axis; axis([0 1 v(3:4)]);
  end
  set(h,'Color',color,...
      'DisplayName',[Title{Case} ', $\theta=' num2str(Flow{Case}.A(k)) '^\circ,H=' num2str(Flow{Case}.H(k)) '$']);
else
  disp(['A= ' num2str(A) ',H= ' num2str(H) ' not found']); 
end
end
end
end

if strcmp(Var,'VelocityProfile')
  z=0:.05:1;
  u=(5/3)*(1-(1-z).^1.5);
  plot(z,u,'--k','Linewidth',2,'DisplayName','Bagnold');
elseif strcmp(Var,'StrainProfile')
  z=0:.001:1;
  u=(1-(1-z).^1.5);
  plot(z,deriv(u,z),'--k','Linewidth',2,'DisplayName','Bagnold');
elseif strcmp(Var,'logVelocityProfile')
  z=0:.05:1;
  u=(5/3)*z.^1.5+(rand(size(z))-.7)*.5;
    plot(z,max(u)-u,'-g','Linewidth',1,'DisplayName','Bagnold');

  z=0:.05:1;
  u=(5/3)*z.^1.5;
  plot(z,u,'--k','Linewidth',2,'DisplayName','Bagnold');
  set(gca,'YScale','log')
  set(gca,'XScale','log')
  v=axis()
  xlim([1e-2 v(2)])
  ylim([1e-2 v(4)])
end
legend(get(gca,'Children'),'Location','Best')

set(legend,'Box','off')
return
