function load_ChuteRheology()
if (true)
  %when you want to quickly load
  global std mu lambda mub regime muH
  if ~isempty(std)
    %save('load_ChuteRheology.mat','std','mu','lambda','mub','regime','muH');
  else
    load('load_ChuteRheology.mat')
  end
  return
end
loadStandardData
loadMuData
loadMuSmoothData
loadMuFlatData
loadMuHData
loadMubData
loadRegimeData
loadLambdaData
return

function loadStandardData
%loads the standard case
global std
if ~isempty(std), disp('std already loaded'); return; end
std=loadstatistics('../Datasets/0.25.stat');
%std=loadstatistics('../Flow/L1M0.5B0.5/H30A28L1M0.5B0.5.stat');
%std=loadstatistics('../w/H20A24L1M0.5B0.5.T500W0_25.stat');
%std=loadstatistics('../../run7/Lucystats/H30A28L1M0.5B0.5.T200W0_5Lucy.stat');
%std=loadstatistics('../w/H30A24L1M0.5B0.5.T200W0_25.stat');
%std=loadstatistics('../w/H30A24L1M0.5B0.5.T100W0_05.stat');
%std=loadstatistics('../w/H30A24L1M0.5B0.5.T100W1.stat');
std=addons(std);
std.plotname = 'Std';
return

function loadMuHData
global muH
if ~isempty(muH), disp('muH already loaded'); return; end
%load files
muH = [loadstatistics('../Flow/L1M0.5B0.5/H*.stat') ];%...
%   loadstatistics('../Flow/L2*M0.5B0.5/H*.stat') ...
%   loadstatistics('../Flow/L4*M0.5B0.5/H*.stat') ...
%   loadstatistics('../Flow/L0.83*M0.5B0.5/H*.stat') ...
%   loadstatistics('../Flow/L0.67*M0.5B0.5/H*.stat') ...
%  ];
%add extras
muH = addons(muH);
for i=1:length(muH)
  muH{i}.title= ['$tan^{-1}\muH=' num2str(muH{i}.ChuteAngle)  '^\deg$'];
  %muH{i}.axis = muH{i}.ChuteAngle;
  muH{i}.axis = muH{i}.InertialParameterBulk;
  %add group properties to first element
  muH{i}.label = '$I$';
  muH{i}.plotname = 'muH';
end
%sort muH
[~,ix]=sort(cellfun(@(d)d.axis,muH)); muH = muH(ix);
return

function loadMuData
global mu
if ~isempty(mu), disp('mu already loaded'); return; end
%load files
mu = loadstatistics('../Flow/L1M0.5B0.5/H30A*.stat');
%add extras
mu = addons(mu);
for i=1:length(mu)
  mu{i}.title= ['$tan^{-1}\mu=' num2str(mu{i}.ChuteAngle)  '^\deg$'];
  mu{i}.axis = mu{i}.InertialParameterBulk;
end
%sort mu
[~,ix]=sort(cellfun(@(d)d.axis,mu)); mu = mu(ix);
%add group properties to first element
mu{1}.label = '$I$';
mu{1}.plotname = 'mu';
return

function loadMuSmoothData
global muSmooth
if ~isempty(muSmooth), disp('muSmooth already loaded'); return; end
%load files
muSmooth = loadstatistics('../Flow/L0.5M0.5B0.5/H30A*.stat');
%add extras
muSmooth = addons(muSmooth);
for i=1:length(muSmooth)
  muSmooth{i}.title= ['$tan^{-1}\muSmooth=' num2str(muSmooth{i}.ChuteAngle)  '^\deg$'];
  muSmooth{i}.axis = muSmooth{i}.ChuteAngle;
end
%sort muSmooth
[~,ix]=sort(cellfun(@(d)d.axis,muSmooth)); muSmooth = muSmooth(ix);
%add group properties to first element
muSmooth{1}.label = '$\theta$';
muSmooth{1}.plotname = 'muSmooth';
return

function loadMuFlatData
global muFlat
if ~isempty(muFlat), disp('muFlat already loaded'); return; end
%load files
muFlat = loadstatistics('../Flow/L0M0.5B0.5/H30A*.stat');
%add extras
muFlat = addons(muFlat);
for i=1:length(muFlat)
  muFlat{i}.title= ['$tan^{-1}\muFlat=' num2str(muFlat{i}.ChuteAngle)  '^\deg$'];
  muFlat{i}.axis = muFlat{i}.ChuteAngle;
end
%sort muFlat
[~,ix]=sort(cellfun(@(d)d.axis,muFlat)); muFlat = muFlat(ix);
%add group properties to first element
muFlat{1}.label = '$\theta$';
muFlat{1}.plotname = 'muFlat';
return


function loadMubData
global mub
if ~isempty(mub), disp('mub already loaded'); return; end
%load files
mub=loadstatistics('../Flow/L1M0.5B*/H30A26L*.stat');
%add extras
mub = addons(mub);
for i=1:length(mub)
  %0=2^-5, infty=2^15
  mub{i}.axis = max(-15,min(5,log2(mub{i}.Mu(end))));
  mub{i}.title= ['$\log_2\mu_b=' num2str(log2(mub{i}.Mu(end)))];
end
%sort mub
[~,ix]=sort(cellfun(@(d)d.axis,mub)); mub = mub(ix);
%add group properties to first element
mub{1}.label = '$\log_2\mu_b$';
mub{1}.plotname = 'Mub';
return

function loadLambdaData
global lambda
if ~isempty(lambda), disp('lambda already loaded'); return; end
%load files
lambda=loadstatistics('../Flow/L*M0.5B0.5/H30A28L*.stat');
%add extras
lambda = addons(lambda);
for i=1:length(lambda)
  lambda{i}.title= ['$\lambda=' num2str(lambda{i}.FixedParticleRadius*2)  '$'];
  lambda{i}.axis = lambda{i}.FixedParticleRadius*2;
  %add group properties to first element
  lambda{i}.label = '$\lambda$';
  lambda{i}.plotname = 'Lambda';
end
%sort lambda
[~,ix]=sort(cellfun(@(d)d.axis,lambda)); lambda = lambda(ix);
return

function loadRegimeData
global regime
if ~isempty(regime), disp('regime already loaded'); return; end
%load files
regime=loadstatistics({ ...
  '../Flow/L1M0.5B0.5/H30A26L1M0.5B0.5.stat'...
  '../Flow/L0.5M0.5B0.5/H30A22L0.5M0.5B0.5.stat'...
  '../Flow/L0M0.5B0.5/H30A24L0M0.5B0.5.stat'...
  '../Flow/L1M0.5B0.5/H20A20L1M0.5B0.5.stat'...
  });
%add extras
regime = addons(regime);
regimeTitle={'steady','layered','decellerating','static'};
for i=1:length(regime)
  regime{i}.title= regimeTitle{i};
  regime{i}.axis = i;
end
%sort regime
[~,ix]=sort(cellfun(@(d)d.axis,regime)); regime = regime(ix);
%add group properties to first element
regime{1}.label = 'Regime';
regime{1}.plotname = 'Regime';
return

function data=addons(data)
if ~iscell(data), data={data}; end
dataZlim=[min(cellfun(@(d)d.Base,data)) max(cellfun(@(d)d.Surface,data))];
dataColor=jet(length(data));
for i=1:length(data)
 data{i}=EigenvalueAnalysis2(data{i});
 if ~isfield(data{i},'title'), data{i}.title=data{i}.name; end
 data{i}.zhat=(data{i}.z-data{i}.Base)/data{i}.FlowHeight;
 data{i}.ShearRate = diff(data{i}.VelocityX)./diff(data{i}.z);
 data{i}.ShearRate(end+1) = data{i}.ShearRate(end);
 data{i}.InertialParameter=data{i}.ShearRate*data{i}.d./sqrt(data{i}.StressZZ./data{i}.ParticleDensity(1));
 data{i}.InertialParameterBulk = median(data{i}.InertialParameter(data{i}.Density>0)); 
 data{i}.FlowInertialParameter = 5/2*max(data{i}.VelocityX(data{i}.zhat<=1))*data{i}.d/data{i}.FlowHeight/sqrt(-data{i}.Gravity(3)*data{i}.FlowHeight); 
 data{i}.Bulk = abs(data{i}.InertialParameter/data{i}.InertialParameterBulk-1)<0.1&abs(data{i}.zhat-0.5)<0.5;
 data{i}.Color = dataColor(i,:);
 data{i}.zlim = dataZlim;
 dzMomentumX = gradient(data{i}.MomentumX,diff(data{i}.z(1:2)));
 dzDensity = gradient(data{i}.Density,diff(data{i}.z(1:2)));
 data{i}.dzVelocityX = (dzMomentumX.*data{i}.Density - data{i}.MomentumX.*dzDensity)./(data{i}.Density).^2;

end
if length(data)==1; data=data{1}; end
return
