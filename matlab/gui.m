function gui
% SIMPLE_GUI2 Select a data set from the pop-up menu, then
% click one of the plot-type push buttons. Clicking the button
% plots the selected data in the axes.

	 % addpath('~/code/MD/matlab')

   %  Create and then hide the GUI as it is being constructed.
   f = figure('Visible','off','Position',[360,500,600,300]);

   %Create a data set.
	 filenames=strread(ls('../Flow/L1M0.5B0.5/*.stat'),'%s ');
	 filenames=cellfun(@(name) name(1:end-5),filenames,'UniformOutput',0)
   current_dataset=filenames{1};
   current_variablex='z';
   current_variabley='Nu';
	 data=[];
	 loaddata();
	 Variables=fieldnames(data);
	 ColumnVariables={};
	 for i=1:length(Variables)
		 var=getfield(data,Variables{i});
 		 if (strcmp(class(var),'double')&size(var,2)==size(data.x,2))
			 ColumnVariables{end+1}=Variables{i};
		 end
	 end
	 Valuex = find(strcmp('z',ColumnVariables));
	 Valuey = find(strcmp('Nu',ColumnVariables));
	 
   %  Construct the components.
   hvariablex = uicontrol('Style','popupmenu',...
          'String',ColumnVariables,...
					'Value',Valuex,...
          'Position',[300,250,250,25],...
          'Callback',{@variablex_Callback});
   hvariabley = uicontrol('Style','popupmenu',...
          'String',ColumnVariables,...
					'Value',Valuey,...
          'Position',[300,200,250,25],...
          'Callback',{@variabley_Callback});
   hdataset = uicontrol('Style','popupmenu',...
          'String',filenames,...
          'Position',[300,150,250,25],...
          'Callback',{@dataset_Callback});
   ha = axes('Units','Pixels','Position',[50,60,200,200]); 
   align([hdataset,hvariablex,hvariabley],'Center','None');
   
   % Create the data to plot.
   
   % Initialize the GUI.
   % Change units to normalized so components resize 
   % automatically.
   set([f,ha,hdataset,hvariablex,hvariabley],...
   'Units','normalized');
   %Create a plot in the axes.
	 plotdata();
	 % Assign the GUI a name to appear in the window title.
   set(f,'Name','Plot stat files')
   % Move the GUI to the center of the screen.
   movegui(f,'center')
   % Make the GUI visible.
   set(f,'Visible','on');
 
   %  Callbacks for simple_gui. These callbacks automatically
   %  have access to component handles and initialized data 
   %  because they are nested at a lower level.
 
   %  Pop-up menu callback. Read the pop-up menu Value property
   %  to determine which item is currently displayed and make it
   %  the current data.
      function dataset_Callback(source,eventdata) 
         % Determine the selected data set.
         str = get(source,'String');
         val = get(source,'Value');
				 if ~strcmp(current_dataset,str{val})
		       current_dataset = str{val};
	  			 loaddata();
				 end
				 plotdata(); 
      end
      function variablex_Callback(source,eventdata) 
         % Determine the selected data set.
         str = get(source,'String');
         val = get(source,'Value');
				 if ~strcmp(current_variablex,str{val})
	         current_variablex = str{val};
					 plotdata();
				 end
      end
      function variabley_Callback(source,eventdata) 
         % Determine the selected data set.
         str = get(source,'String');
         val = get(source,'Value');
				 if ~strcmp(current_variabley,str{val})
	         current_variabley = str{val};
					 plotdata();
				 end
      end
      function loaddata() 
         % Determine the selected data set.
				 data=loadstatistics([current_dataset '.stat']);
 				 data=EigenvalueAnalysis2(data);
			end
      function plotdata() 
         % Determine the selected data set.
				 plot(getfield(data,current_variablex),getfield(data,current_variabley));
				 axis tight;
			end
  
   % Push button callbacks. Each callback plots current_data in
   % the specified plot type.

end 