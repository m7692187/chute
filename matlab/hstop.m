function height = hstop(x, angle,exponential) %#ok<INUSD>
if exist('exponential','var')
  %tantheta=x(1)+(x(2)-x(1))*exp(-height/x(3));
  height=x(3)*log((x(2)-x(1))./(tand(angle)-x(1)));
else
  height=x(3)*(x(2)-tand(angle))./(tand(angle)-x(1));
end
return
