function NonLocalTest()
clc
close all
global mu2 I0 beta slip
mu2=0.62;
I0=0.26;
beta=3.3;
slip=.66;

%for beta>4.15 mu2=0.62, H=30, theta=20, flow ceases
%beta=0 is a bagnold profile, beta=4.15 is convex
%for beta=3.3, mu2>=0.66, H=30, theta=20, flow ceases
%I0 changes max(shearrate), but theshape shearrate/max(eps,shearrate(end))
%is constant
newfigure('nonlocal',struct('size',800)); clf

for a = 24
hl=[20];
for hi=1:length(hl); h=hl(hi);
  disp(['h=' num2str(h)]);
  [z,shearrate] = getShearrate(h,a);
  v=cumsum(shearrate)*diff(z(1:2));
  %plot(z/h,v/v(end),'Color',hi/length(hl)*[0 0 1]);
  plot(z/h,shearrate/max(shearrate),'Color',hi/length(hl)*[0 0 1]);
  max(shearrate)
  hold on
end
end
xlabel('z/h'); ylabel('d_zu/d_zu(h)'); 

return
% load_ChuteRheology
% global std mu muSmooth muSmooth lambda mub regime muH
% hold on
% for i=find(cellfun(@(d)d.FlowHeight<15&&d.FixedParticleRadius==0.5,muH))
%   data=muH{i};
%   %axis tight
%   dzu=[0; diff(data.VelocityX(data.zhat<1))];
%   plot(data.zhat(data.zhat<1), dzu/max(dzu),'r')
% end

newfigure('nonlocal',struct('size',800)); clf
for a = 19:2:31
for h = 10:10:40;
  disp(['h=' num2str(h) ', a=' num2str(h)]);
  [z,shearrate] = getShearrate(h,a);
  v=cumsum(shearrate)*diff(z(1:2));
  alpha=sum(v.^2)./(sum(v)^2*diff(z(1:2))/max(z));
  plot(a,alpha,'x');
  hold on
end
end
xlabel('z/h'); ylabel('d_zu/d_zu(h)'); 



return