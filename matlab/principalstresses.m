function data = principalstresses(data)
for i=1:length(data.z)
  Stress=[...
    data.StressXX(i) data.StressXY(i) data.StressXZ(i);...
    data.StressYX(i) data.StressYY(i) data.StressYZ(i);...
    data.StressZX(i) data.StressZY(i) data.StressZZ(i);...
    ] - diag([1 1 1]) * data.Pressure(i);
   [V,D]=eig((Stress+Stress')/2);
   [data.a1(i),data.a2(i),data.a3(i)] = decompose_rotation(V);
   data.d1(i) = D(1,1);
   data.d2(i) = D(2,2);
   data.d3(i) = D(3,3);
%   [V2,D2]=eig((StressDev-StressDev')/2)
end

  Stress=([...
    sum(data.Nu.*data.StressXX) sum(data.Nu.*data.StressXY) sum(data.Nu.*data.StressXZ);...
    sum(data.Nu.*data.StressYX) sum(data.Nu.*data.StressYY) sum(data.Nu.*data.StressYZ);...
    sum(data.Nu.*data.StressZX) sum(data.Nu.*data.StressZY) sum(data.Nu.*data.StressZZ);...
    ] - diag([1 1 1]) * sum(data.Nu.*data.Pressure))./sum(data.Nu);
   [V,D]=eig((Stress+Stress')/2);
   [data.FlowA1(i),data.FlowA2(i),data.FlowA3(i)] = decompose_rotation(V);
   data.FlowD1(i) = D(1,1);
   data.FlowD2(i) = D(2,2);
   data.FlowD3(i) = D(3,3);
%   [V2,D2]=eig((StressDev-StressDev')/2)


return

function [x,y,z] = decompose_rotation(R)
  %see http://nghiaho.com/?page_id=846
	x = atan2(R(3,2), R(3,3))*180/pi;
	y = atan2(-R(3,1), sqrt(R(3,2)*R(3,2) + R(3,3)*R(3,3)))*180/pi;
	z = atan2(R(2,1), R(1,1))*180/pi;
  
  x = mod(x + 90, 180) - 90; % bring x into the range [-90,90]
  y = -abs(y); % bring x into the range [-,90] %why
  z = mod(z + 90, 180) - 90; % bring x into the range [-90,90]
return