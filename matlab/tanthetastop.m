function  tantheta = tanthetastop(x, height,exponential) %#ok<INUSD>
if exist('exponential','var')
  tantheta=x(1)+(x(2)-x(1))*exp(-height/x(3));
else
  tantheta=(height*x(1)+x(3)*x(2))./(height+x(3));
end
return
