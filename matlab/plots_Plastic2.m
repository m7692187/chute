function plots_Plastic2()

close all
addpath('~/code/MD/matlab/thomas')

%plotspecialcases()

%% Std disp
global StopN FlowN
if isempty(StopN)
  StopN=loadstatistics('../Hstop/L1M0.5B0.5/*.stat');
  FlowN=loadstatistics('../Flow/L1M0.5B0.5/*.stat');
end

FlowH=cellfun(@(d)d.FlowHeight,FlowN);
FlowA=cellfun(@(d)d.ChuteAngle,FlowN);
FlowF=cellfun(@(d)d.Froude,FlowN);
FlowS=(~(FlowH<15&FlowA<22.5));
plot_hstop('normal',StopN,FlowA,FlowH,FlowS,FlowF);


%% Plastic
global StopP FlowP
if isempty(StopP)
  StopP=loadstatistics('../Hstop/Plastic/*.stat');
  FlowP=loadstatistics('../Flow/Plastic/*.stat');
end

FlowH=cellfun(@(d)d.FlowHeight,FlowP);
FlowA=cellfun(@(d)d.ChuteAngle,FlowP);
FlowF=cellfun(@(d)d.Froude,FlowP);
FlowS=(~(FlowA<21&FlowH<25))&(~(FlowH<15&FlowA<22.5));
plot_hstop('plastic',StopP,FlowA,FlowH,FlowS,FlowF);


%% Hertzian
global StopHe FlowHe
if isempty(StopHe)
  StopHe=loadstatistics('../Hstop/Hertz/*.stat');
  FlowHe=loadstatistics('../Flow/S39/stats/H*.stat');
end

FlowH=cellfun(@(d)d.FlowHeight,FlowHe);
FlowA=cellfun(@(d)d.ChuteAngle,FlowHe);
FlowF=cellfun(@(d)d.Froude,FlowHe);
FlowS=FlowA>20&FlowA<27&(~(FlowH<15&FlowA<22.5));
plot_hstop('hertzian',StopHe,FlowA,FlowH,FlowS,FlowF);
%xlim([19 30])
%% 
%print_figures();
%order_figures(2)

return

function plotspecialcases()

% standard exponential
global StopStdE FlowStdE
if isempty(StopStdE)
  StopStdE=loadstatistics('../../runNewCases/hstopS4/stats/H*.stat');
  FlowStdE=loadstatistics('../Flow/L1M0.5B0.5/H*.stat');
end

FlowH=cellfun(@(d)d.FlowHeight,FlowStdE);
FlowA=cellfun(@(d)d.ChuteAngle,FlowStdE);
FlowF=cellfun(@(d)d.Froude,FlowStdE);
FlowS=(~(FlowH<15&FlowA<22.5));
plot_hstop('StdExponential',StopStdE,FlowA,FlowH,FlowS,FlowF);

% Foerster
global StopFo FlowFo
if isempty(StopFo)
  StopFo=loadstatistics('../../runNewCases/hstopS40/stats/H*.stat');
  FlowFo=loadstatistics('../Flow/S40/H*.stat');
end

FlowH=cellfun(@(d)d.FlowHeight,FlowFo);
FlowA=cellfun(@(d)d.ChuteAngle,FlowFo);
FlowF=cellfun(@(d)d.Froude,FlowFo);
FlowS=(~(FlowH<15&FlowA<22.5));
plot_hstop('Foerster',StopFo,FlowA,FlowH,FlowS,FlowF);

% Lorenz steel
global StopLo FlowLo
if isempty(StopLo)
  StopLo=loadstatistics('../../runNewCases/hstopS41/stats/H*.stat');
  FlowLo=loadstatistics('../Flow/S41/H*.stat');
end

FlowH=cellfun(@(d)d.FlowHeight,FlowLo);
FlowA=cellfun(@(d)d.ChuteAngle,FlowLo);
FlowF=cellfun(@(d)d.Froude,FlowLo);
FlowS=(~(FlowH<15&FlowA<22.5));
plot_hstop('Lorenz',StopLo,FlowA,FlowH,FlowS,FlowF);

% Lorenz glass
global StopLoG FlowLoG
if isempty(StopLoG)
  StopLoG=loadstatistics('../../runNewCases/hstopS42/stats/H*.stat');
  FlowLoG=loadstatistics('../Flow/S42/H*.stat');
end

FlowH=cellfun(@(d)d.FlowHeight,FlowLoG);
FlowA=cellfun(@(d)d.ChuteAngle,FlowLoG);
FlowF=cellfun(@(d)d.Froude,FlowLoG);
FlowS=(~(FlowH<15&FlowA<22.5));
plot_hstop('LorenzGlass',StopLoG,FlowA,FlowH,FlowS,FlowF);

% Foerster with rolling friction
global StopFoR FlowFoR
if isempty(StopFoR)
  StopFoR=loadstatistics('../../runNewCases/hstopS44/stats/H*.stat');
  FlowFoR=loadstatistics('../Flow/S44/H*.stat');
end

FlowH=cellfun(@(d)d.FlowHeight,FlowFoR);
FlowA=cellfun(@(d)d.ChuteAngle,FlowFoR);
FlowF=cellfun(@(d)d.Froude,FlowFoR);
FlowS=(~(FlowH<15&FlowA<22.5));
plot_hstop('FoersterR',StopFoR,FlowA,FlowH,FlowS,FlowF);

% Lorenz steel with rolling friction
global StopLoR FlowLoR
if isempty(StopLoR)
  StopLoR=loadstatistics('../../runNewCases/hstopS45/stats/H*.stat');
  FlowLoR=loadstatistics('../Flow/S45/H*.stat');
end

FlowH=cellfun(@(d)d.FlowHeight,FlowLoR);
FlowA=cellfun(@(d)d.ChuteAngle,FlowLoR);
FlowF=cellfun(@(d)d.Froude,FlowLoR);
FlowS=(~(FlowH<15&FlowA<22.5));
plot_hstop('LorenzR',StopLoR,FlowA,FlowH,FlowS,FlowF);

% Lorenz glass with rolling friction
global StopLoGR FlowLoGR
if isempty(StopLoGR)
  StopLoGR=loadstatistics('../../runNewCases/hstopS46/stats/H*.stat');
  FlowLoGR=loadstatistics('../Flow/S46/H*.stat');
end

FlowH=cellfun(@(d)d.FlowHeight,FlowLoGR);
FlowA=cellfun(@(d)d.ChuteAngle,FlowLoGR);
FlowF=cellfun(@(d)d.Froude,FlowLoGR);
FlowS=(~(FlowH<15&FlowA<22.5));
plot_hstop('LorenzGlassR',StopLoGR,FlowA,FlowH,FlowS,FlowF);

% Silbert with rolling friction
global StopSiR FlowSiR
if isempty(StopSiR)
  StopSiR=loadstatistics('../../runNewCases/hstopS43/stats/H*.stat');
  FlowSiR=loadstatistics('../Flow/S43/H*.stat');
end

FlowH=cellfun(@(d)d.FlowHeight,FlowSiR);
FlowA=cellfun(@(d)d.ChuteAngle,FlowSiR);
FlowF=cellfun(@(d)d.Froude,FlowSiR);
FlowS=(~(FlowH<15&FlowA<22.5));
plot_hstop('SilbertR',StopSiR,FlowA,FlowH,FlowS,FlowF);

% no tangential dissipation
global StopDispt0 FlowDispt0
if isempty(StopDispt0)
  StopDispt0=loadstatistics('../../runNewCases/hstopS37/stats/H*.stat');
  FlowDispt0=loadstatistics('../Flow/S37/H*.stat');
end

FlowH=cellfun(@(d)d.FlowHeight,FlowDispt0);
FlowA=cellfun(@(d)d.ChuteAngle,FlowDispt0);
FlowF=cellfun(@(d)d.Froude,FlowDispt0);
FlowS=(~(FlowH<15&FlowA<22.5));
plot_hstop('Dispt0',StopDispt0,FlowA,FlowH,FlowS,FlowF);

% eps=0.98
global Stopeps98 Floweps98
if isempty(Stopeps98)
  Stopeps98=loadstatistics('../../runNewCases/hstopS38/stats/H*.stat');
  Floweps98=loadstatistics('../Flow/S38/H*.stat');
end

FlowH=cellfun(@(d)d.FlowHeight,Floweps98);
FlowA=cellfun(@(d)d.ChuteAngle,Floweps98);
FlowF=cellfun(@(d)d.Froude,Floweps98);
FlowS=(~(FlowH<15&FlowA<22.5));
plot_hstop('Eps0.98',Stopeps98,FlowA,FlowH,FlowS,FlowF);

return

function plot_hstop(name,Stop,FlowA,FlowH,FlowS,FlowF)
StopH=cellfun(@(d)d.FlowHeight,Stop);
StopA=cellfun(@(d)d.ChuteAngle,Stop);
StopS=cellfun(@(data)data.time(end)<499,Stop);
[Astop,Hstop,CoeffHstop] = fitHstop(Stop,name);

%new figure
figure(); clf; hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName',['hstop_'  name])

plot(StopA(StopS),StopH(StopS),'ko','MarkerFaceColor','k',...
  'DisplayName','arrested'); 
hold on
plot(StopA(~StopS),StopH(~StopS),'ko')

plot(FlowA(FlowS),FlowH(FlowS),'ko',...
  'DisplayName','steady'); 
plot(Astop,Hstop,'k',...
  'DisplayName','$h_{stop}(\theta)$'); 

xlabel('$\theta$')
ylabel('$h$')
disp([name ' theta ' num2str(atand(CoeffHstop(1:2))) ' A ' num2str(CoeffHstop(3)) ])

%plot only hstop, not flow curve
%if true; return; end

for H=10:10:40
  ind = FlowS&abs(FlowH-H)<5;
  [x,ix]=sort(FlowA(ind));
  y=FlowH(ind);
  plot(x,y(ix),'-.','Color',[1 1 1]*.7);
  y=FlowH(FlowA==28&ind);
  if isempty(y)
    y=FlowH(FlowA==26&ind);
    text(28,y,['$H=' num2str(H) '$'],'HorizontalAlignment','right')
  else
    text(30,y,['$H=' num2str(H) '$'],'HorizontalAlignment','right')
  end
end
axis([18.4 30 0 40])

n=gcf;
markers='xod';
colors='krg';
figure(4); %clf; 
hold on
set(gcf,'Position',[2000  680 560 420])
set(gcf,'FileName','flowrule_all')
HOverHstop=FlowH(FlowS)./hstop(CoeffHstop,FlowA(FlowS));
plot(HOverHstop,FlowF(FlowS),[colors(n) markers(n)],'HandleVisibility','off')
betaPO=flowrule_fit_with_offset(...
  FlowA(FlowS), FlowF(FlowS), HOverHstop, CoeffHstop, 1);
beta=betaPO(2);
gamma=betaPO(1);
plot(sort(HOverHstop),beta*sort(HOverHstop)+gamma,[colors(n)],'Displayname',name)
disp([name ' F= ' num2str(beta) ' *h/hs- ' num2str(-gamma) ])
xlabel('$h/h_{stop}$')
ylabel('$F$')
if (n==3), legend('show'); set(legend,'Location','NorthWest','box','off'); axis tight; end

%axis([0 25 0 6])

return

