function [Astop,Hstop,CoeffHstop] = fitHstop(Stop,name)

%warning why does that work better than
static=cellfun(@(d)d.Froude<0.005,Stop);
if strcmp(name,'plastic');
  static=cellfun(@(d)d.Froude>0.005,Stop);
end
Height=cellfun(@(d)d.FlowHeight,Stop);
H=cellfun(@(d)(d.N-289)/200,Stop);
A=cellfun(@(d)d.ChuteAngle,Stop);

%for each flowing case, look left and down for next static case
leftRange=[];
Amin=min(A(static));
downRange=[Amin max(max(Height), 63), 1000];

%for all arrested flows
for m=squeeze(find(static))
  %if it is the rightmost arrested case
  if (A(m)==max(A(H(m)==H&(static))))
    %find next flowing case at same height
    right=H==H(m)&~static;
    Amin=min(A(right));
    if ~isempty(Amin), 
      leftRange(end+1,:)=[Height(m) A(m) Amin];
    end
  end
  %if it is the upmost arrested case
  if (H(m)==max(H(A(m)==A&(static))))
    %find next flowing case at same angle
    up=A==A(m)&~static;
    Hmin=min(Height(up));
    if ~isempty(Hmin)&&(Hmin-Height(m))<3, 
      downRange(end+1,:)=[A(m) Height(m) Hmin]; 
    end
  end
end
if isempty(leftRange)
    Amin=min(A(static));
    leftRange = [min(Height) Amin Amin; ...
                         max(Height) Amin Amin;];
end
[Astop,Hstop,CoeffHstop]=hstop_fit2(leftRange,downRange); 
 
return
