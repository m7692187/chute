close all
figure(1)
data=textread('4.4.25.Glass/Report4')
scatter(data(:,2),data(:,1)*pi/6/0.60,[],data(:,6))

xlabel('\theta')
ylabel('h_{stop}')
title('Glass')

figure(2)
hold off
data=textread('4.4.25.Hertz/Report4b')
scatter(data(:,2),data(:,1)*pi/6/0.60,[],data(:,6),'DisplayName','Simulations Hertzian')

hold on
h=1:.1:50;
t1=tand(19.55);
t2=tand(27.75);
l=8.5;
a=atand(t1+(t2-t1)*exp(-h/l));
plot(a,h,'DisplayName','Silberts Hertzian');

hold on
a=18:.1:32;
t=tand(a);
t1=tand(17.708);
t2=tand(32.780);
l=3.355;
h=l*(t2-t)./(t-t1);
plot(a,h,'r','DisplayName','Fit Linear (FlowRulePaper)');
ylim([0 42])
xlim([18 26])

global Stop
Stop{5}.L
scatter(Stop{5}.A,Stop{5}.H*pi/6/0.60,[],~Stop{5}.static,'filled','DisplayName','Simulations Linear (FlowRulePaper)')

xlabel('\theta')
ylabel('h_{stop}')
legend('show')
