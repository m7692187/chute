function out = mylines(num)
  if num>15
    data=hsv(100);
    out = [lines(7);1-lines(num-7);data(1:(num-7),:)];
  elseif num==15
    data=hsv(100);
    out = [lines(7);.2+.8*(1-lines(num-7));[0 0 0]];
  elseif num>7
    data=hsv(10);
    out = [lines(7);.2+.8*(1-lines(num-7))];
  elseif num==1
    out = [0 0 0];
  else
    out = lines(num);
  end
return