#this is the plastic flow simulations
#set -x 
exe=flowRulePlastic_studyHeightAngle
exe2=statistics_while_running2
#make $exe -C .. || exit
#make $exe2 -C .. || exit
#cp ../$exe.exe .
#cp ../$exe2.exe .
root=$(pwd)

t=100
w=0.25

for S in 4
do
	case $S in
  4) Case=L1M0.5B0.5 ;;
  6) Case=L1M0B0 ;;
  7) Case=L1M1B1 ;;
  8) Case=L1M1e+20B1e+20 ;;
  14) Case=L1M0.25B0.25 ;;
  15) Case=L1M0.125B0.125 ;;
  *) Case=S$S 
	esac
	mkdir -p $Case
	cd $Case

	for H in 10 20 30 40 
	do
		if [ $H == 40 ]; then
			AList="20 22 24 26 28"
		else 
			AList="22 23 24 25 26 27 28 30 20"
		fi
		#AList="21"

		for A in $AList
		do

			testfile=~/DRIVERS/FlowRulePaper/runfinished4/Flow/$Case/H${H}A${A}${Case}.restart.0020
			if [ -f $testfile ] && [ ! $(stat -c%s "$testfile") -lt 100000 ]; then
				#echo H${H}A${A}${Case} already exists
				continue
			fi
			
			#echo H${H}A${A}${Case} does not exist
			echo '#/bin/bash
set -x
cd '$(pwd)' 
'../$exe.exe $S $H $A -tmax 2000'
cp 'H${H}A${A}${Case}'.restart.0020 'H${H}A${A}${Case}'.restart
sleep 2s
'../$exe2.exe H${H}A${A}${Case} $t -w $w'
' > exe.H${H}A${A}${Case}
			chmod +x ./exe.H${H}A${A}${Case}
			~/clusterscriptexecute ./exe.H${H}A${A}${Case}

		done
	done
	cd ..
done
