#this is the simulation from the hstop data,
#./doDown.sh S
# do S 4 37-46

#set -x 
S=4
A=25
dir=$(pwd)/hstopS${S}
exe=hstopPlastic_StudyHeightHmaxAngle
rm -rf $dir
mkdir $dir
cp ./$exe.exe $dir 
cd $dir 

#write arg file
echo $S 4 60 $A> $dir/arg
#H46A18.5L1M0.5B0.5.stat

#write exe file
echo '
#/bin/bash
set -x
cd '$dir' 
for i in 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 
do
	sleep 2s
	a1=$(awk "{ print \$1}" arg)
	a2=$(awk "{ print \$2}" arg)
	a3=$(awk "{ print \$3}" arg)
	a4=$(awk "{ print \$4}" arg)
	echo $argum	
	'$(pwd)/$exe.exe' $a1 $a2 $a3 $a4
done
' > exe
chmod +x ./exe

#run exe file
sleep 3s
more arg
~/clusterscriptexecute ./exe


